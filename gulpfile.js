var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir.config.sourcemaps = false;

elixir(function(mix) {
    //mix.sass('app.scss');
    
    mix.styles([
        'materializecss/bower_components/materialize/dist/css/materialize.min.css',
        'materializecss/bower_components/fontawesome/css/font-awesome.min.css',
        'materializecss/css/whop.css',
        ], 'public/css/materializecss/all.css', 'resources/assets')

    .scripts([
        'materializecss/bower_components/jquery/dist/jquery.min.js',
        'materializecss/bower_components/angular/angular.min.js',
        'materializecss/bower_components/materialize/dist/js/materialize.min.js',
        'materializecss/bower_components/ace-builds/src-min/ace.js',
        'materializecss/bower_components/Chart.js/Chart.js',
        'materializecss/bower_components/linren-jquery-multiselect/jquery-multiselect.js',
        'materializecss/js/jquery.sortable.min.js',
        'materializecss/js/whop.js',
        'materializecss/js/controllers.js',
        ], 'public/js/materializecss/all.js', 'resources/assets')


    .scripts([
        'materializecss/js/controllers-admin.js',
        ], 'public/js/materializecss/all-admin.js', 'resources/assets')

    .copy('resources/assets/materializecss/bower_components/fontawesome/fonts', 'public/build/css/fonts')
    .copy('resources/assets/materializecss/bower_components/materialize/font', 'public/build/css/font')
    .copy('resources/assets/materializecss/bower_components/ace-builds/src-min', 'public/build/js/ace')

    .version([
        'css/materializecss/all.css',
        'js/materializecss/all.js',
        'js/materializecss/all-admin.js'
    ]);
});