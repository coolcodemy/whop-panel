<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_packages', function (Blueprint $table) {

            $table->increments('id');
            
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->integer('package_id')->unsigned();
            $table->foreign('package_id')
                ->references('id')
                ->on('packages')
                ->onUpdate('cascade')
                ->onDelete('cascade');

            $table->boolean('apcuSupport');
            $table->boolean('ssh');
            $table->integer('diskQuota');
            $table->integer('emailQuota');
            $table->integer('ftpQuota');
            $table->integer('websiteQuota');
            $table->integer('numberOfDatabase');
            $table->integer('numberOfDatabaseUser');
            $table->integer('maximumQueriesPerHour');
            $table->integer('maximumConnectionsPerHour');
            $table->integer('maximumUpdatesPerHour');
            $table->integer('maximumUserConnections');
            $table->integer('numberOfEmailAccount');
            $table->integer('numberOfFtpAccount');
            $table->integer('numberOfDomain');
            $table->integer('numberOfWhoplet');

            $table->date('activeDate');
            $table->date('endDate');

            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('user_packages');
    }
}
