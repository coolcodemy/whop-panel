<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecordIdToMailForwarding extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mail_forwardings', function(Blueprint $table)
        {
            $table->integer('record_id')->unsigned()->after('user_id');

            $table->foreign('record_id')
                ->references('id')
                ->on('records')
                ->onUpdate('cascade')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mail_forwardings', function(Blueprint $table)
        {
            $table->dropColumn('record_id');
        });
    }
}
