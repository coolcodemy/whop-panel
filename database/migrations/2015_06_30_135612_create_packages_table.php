<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique();
            $table->string('validFor');
            $table->boolean('apcuSupport');
            $table->boolean('ssh');
            $table->integer('diskQuota');
            $table->integer('emailQuota');
            $table->integer('ftpQuota');
            $table->integer('websiteQuota');
            $table->integer('numberOfDatabase');
            $table->integer('numberOfDatabaseUser');
            $table->integer('maximumQueriesPerHour');
            $table->integer('maximumConnectionsPerHour');
            $table->integer('maximumUpdatesPerHour');
            $table->integer('maximumUserConnections');
            $table->integer('numberOfEmailAccount');
            $table->integer('numberOfFtpAccount');
            $table->integer('numberOfDomain');
            $table->integer('numberOfWhoplet');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('packages');
    }
}
