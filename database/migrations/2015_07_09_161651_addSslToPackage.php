<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSslToPackage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packages', function(Blueprint $table)
        {
            $table->boolean('enableSsl')->after('apcuSupport');
        });

        Schema::table('user_packages', function(Blueprint $table)
        {
            $table->boolean('enableSsl')->after('apcuSupport');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packages', function(Blueprint $table)
        {
            $table->dropColumn('enableSsl');
        });

        Schema::table('user_packages', function(Blueprint $table)
        {
            $table->dropColumn('enableSsl');
        });
    }
}
