<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMailUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mail_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('record_id')->unsigned();
            $table->foreign('record_id')
                ->references('id')
                ->on('records')
                ->onDelete('cascade')
                ->onUpdate('cascade');
                
            $table->string('email');
            $table->string('password');
            $table->bigInteger('quota');
            $table->tinyInteger('disable')->default(0);
            $table->tinyInteger('masterDisable')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mail_users');
    }
}
