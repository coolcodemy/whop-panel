<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFtpUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ftp_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

            $table->integer('record_id')->unsigned();
            $table->foreign('record_id')
                ->references('id')
                ->on('records')
                ->onDelete('cascade')
                ->onUpdate('cascade');
                
            $table->string('username', 255)->unique();
            $table->string('password', 32);
            $table->integer('uid');
            $table->integer('gid');
            $table->string('homedir', 255);
            $table->smallInteger('ULBW');
            $table->smallInteger('DLBW');
            $table->smallInteger('quotaSize');
            $table->integer('quotaFile');
            $table->tinyInteger('disable');
            $table->tinyInteger('masterDisable')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ftp_users');
    }
}
