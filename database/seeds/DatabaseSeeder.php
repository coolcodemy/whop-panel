<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call(PackageTableSeeder::class);
        $this->call(UserTableSeeder::class);
        $this->call(WhopletTableSeeder::class);
        $this->call(SettingTableSeeder::class);

        Model::reguard();
    }
}
