<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use WHoP\Setting;

class SettingTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->delete();

        Setting::create([
            'settingName' => 'MaxUserTemplate',
            'value' => '100',
            ]);

        Setting::create([
            'settingName' => 'OwnerPerPage',
            'value' => '30',
            ]);

        Setting::create([
            'settingName' => 'BruteforceTimes',
            'value' => '10',
            ]);

        Setting::create([
            'settingName' => 'BruteforceMinutes',
            'value' => '10',
            ]);

        Setting::create([
            'settingName' => 'OwnerDomainItemPerPage',
            'value' => '15',
            ]);

        Setting::create([
            'settingName' => 'OwnerMailForwardingItemPerPage',
            'value' => '15',
            ]);

        Setting::create([
            'settingName' => 'OwnerMailItemPerPage',
            'value' => '15',
            ]);

        Setting::create([
            'settingName' => 'OwnerFtpItemPerPage',
            'value' => '15',
            ]);
    }
}
