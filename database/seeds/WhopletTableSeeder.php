<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use WHoP\Whoplet;

class WhopletTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();


        // Delete any content in the roles table
        DB::table('whoplets')->delete();


        Whoplet::create(
            array(
                'user_id' => 1,
                'name' => 'skeleton_80',
                'content' => "# Do not change the default skeleton file!!!!
# Just add your line without deleting the skeleton line
# Importantly do not delete this line -> try_files \$uri /index.php =404
# Unless you know what you are doing

server {
    listen                  80;
    listen                  [::]:80;
    server_name             {DOMAINNAME};
    server_tokens           off;
    error_log               /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_error.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_access.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/traffic.log traffic;

    # Header option for security purpose
    add_header X-Frame-Options \"SAMEORIGIN\";
    add_header X-XSS-Protection \"1; mode=block\";
    add_header X-Content-Type-Options \"nosniff\";


    root /var/www/{USERNAME}/web/public{ROOT};
    index index.php index.html index.htm;

    #BEGIN

    location ~ \.php\$ {
        try_files \$uri /index.php =404;

        add_header X-Cache \$upstream_cache_status;
        #fastcgi_cache WHOPWEBAPP;
        #fastcgi_cache_valid 302 200 {CACHETIME};
        #fastcgi_cache_valid 404 1m;
        #fastcgi_no_cache \$http_pragma \$http_authorization \$cookie_nocache \$arg_nocache;

        include /etc/nginx/fastcgi_params;
        fastcgi_buffers 256 16k;
        fastcgi_max_temp_file_size 0;
        fastcgi_pass  unix:/home/whop/hosting/{USERNAME}/php.socket;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    }
}"
                )
        );







        Whoplet::create(
                    array(
                        'user_id' => 1,
                        'name' => 'skeleton_443',
                        'content' => "# Do not change the default skeleton file!!!!
# Just add your line without deleting the skeleton line
# Importantly do not delete this line -> try_files \$uri /index.php =404
# Unless you know what you are doing

server {
    listen                  443 ssl spdy;
    listen                  [::]:443 ssl spdy;
    server_name             {DOMAINNAME};
    server_tokens           off;
    error_log               /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_error.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_access.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/traffic.log traffic;

    # Header option for security purpose
    add_header X-Frame-Options \"SAMEORIGIN\";
    add_header X-XSS-Protection \"1; mode=block\";
    add_header X-Content-Type-Options \"nosniff\";
    add_header Strict-Transport-Security \"max-age=31536000\";


    ssl                  on;
    ssl_certificate      /home/whop/hosting/{USERNAME}/SSL/{SSL}.crt;
    ssl_certificate_key  /home/whop/hosting/{USERNAME}/SSL/{SSL}.key;
    ssl_session_cache shared:SSL:10m;

    ssl_session_timeout  5m;

    ssl_protocols  TLSv1.2 TLSv1.1 TLSv1;
    ssl_ciphers    \"ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-RC4-SHA:ECDHE-RSA-RC4-SHA:ECDH-ECDSA-RC4-SHA:ECDH-RSA-RC4-SHA:ECDHE-RSA-AES256-SHA:RC4-SHA\";
    ssl_prefer_server_ciphers   on;

    root /var/www/{USERNAME}/web/public{ROOT};
    index index.php index.html index.htm;

    #BEGIN

    location ~ \.php\$ {
        try_files \$uri /index.php =404;

        add_header X-Cache \$upstream_cache_status;
        #fastcgi_cache WHOPWEBAPP;
        #fastcgi_cache_valid 302 200 {CACHETIME};
        #fastcgi_cache_valid 404 1m;
        #fastcgi_no_cache \$http_pragma \$http_authorization \$cookie_nocache \$arg_nocache;

        include /etc/nginx/fastcgi_params;
        fastcgi_buffers 256 16k;
        fastcgi_max_temp_file_size 0;
        fastcgi_pass  unix:/home/whop/hosting/{USERNAME}/php.socket;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    }
}"
                        )
                );



        Whoplet::create(
            array(
                'user_id' => 1,
                'name' => 'WHoPlet_universal',
                'status' => 1,
                'content' => "# Do not change the default skeleton file!!!!
# Just add your line without deleting the skeleton line
# Importantly do not delete this line -> try_files \$uri /index.php =404
# Unless you know what you are doing

server {
    listen                  80;
    listen                  [::]:80;
    server_name             {DOMAINNAME};
    server_tokens           off;
    error_log               /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_error.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_access.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/traffic.log traffic;

    # Header option for security purpose
    add_header X-Frame-Options \"SAMEORIGIN\";
    add_header X-XSS-Protection \"1; mode=block\";
    add_header X-Content-Type-Options \"nosniff\";


    root /var/www/{USERNAME}/web/public{ROOT};
    index index.php index.html index.htm;

    #BEGIN

    location ~ \.php\$ {
        try_files \$uri /index.php =404;

        add_header X-Cache \$upstream_cache_status;
        #fastcgi_cache WHOPWEBAPP;
        #fastcgi_cache_valid 302 200 {CACHETIME};
        #fastcgi_cache_valid 404 1m;
        #fastcgi_no_cache \$http_pragma \$http_authorization \$cookie_nocache \$arg_nocache;

        include /etc/nginx/fastcgi_params;
        fastcgi_buffers 256 16k;
        fastcgi_max_temp_file_size 0;
        fastcgi_pass  unix:/home/whop/hosting/{USERNAME}/php.socket;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    }
}"
                )
        );


        Whoplet::create(
                    array(
                        'user_id' => 1,
                        'name' => 'WHoPlet_universal_https',
                        'status' => 1,
                        'content' => "# Do not change the default skeleton file!!!!
# Just add your line without deleting the skeleton line
# Importantly do not delete this line -> try_files \$uri /index.php =404
# Unless you know what you are doing

server {
    listen                  443 ssl spdy;
    listen                  [::]:443 ssl spdy;
    server_name             {DOMAINNAME};
    server_tokens           off;
    error_log               /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_error.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_access.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/traffic.log traffic;

    # Header option for security purpose
    add_header X-Frame-Options \"SAMEORIGIN\";
    add_header X-XSS-Protection \"1; mode=block\";
    add_header X-Content-Type-Options \"nosniff\";
    add_header Strict-Transport-Security \"max-age=31536000\";

    ssl                  on;
    ssl_certificate      /home/whop/hosting/{USERNAME}/SSL/{SSL}.crt;
    ssl_certificate_key  /home/whop/hosting/{USERNAME}/SSL/{SSL}.key;
    ssl_session_cache shared:SSL:10m;

    ssl_session_timeout  5m;

    ssl_protocols  TLSv1.2 TLSv1.1 TLSv1;
    ssl_ciphers    \"ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-RC4-SHA:ECDHE-RSA-RC4-SHA:ECDH-ECDSA-RC4-SHA:ECDH-RSA-RC4-SHA:ECDHE-RSA-AES256-SHA:RC4-SHA\";
    ssl_prefer_server_ciphers   on;

    root /var/www/{USERNAME}/web/public{ROOT};
    index index.php index.html index.htm;

    #BEGIN

    location ~ \.php\$ {
        try_files \$uri /index.php =404;

        add_header X-Cache \$upstream_cache_status;
        #fastcgi_cache WHOPWEBAPP;
        #fastcgi_cache_valid 302 200 {CACHETIME};
        #fastcgi_cache_valid 404 1m;
        #fastcgi_no_cache \$http_pragma \$http_authorization \$cookie_nocache \$arg_nocache;

        include /etc/nginx/fastcgi_params;
        fastcgi_buffers 256 16k;
        fastcgi_max_temp_file_size 0;
        fastcgi_pass  unix:/home/whop/hosting/{USERNAME}/php.socket;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    }
}"
                        )
                );



        Whoplet::create(
            array(
                'user_id' => 1,
                'name' => 'WHoPlet_without_php',
                'status' => 1,
                'content' => "# Do not change the default skeleton file!!!!
# Just add your line without deleting the skeleton line
# Importantly do not delete this line -> try_files \$uri /index.php =404
# Unless you know what you are doing

server {
    listen                  80;
    listen                  [::]:80;
    server_name             {DOMAINNAME};
    server_tokens           off;
    error_log               /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_error.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_access.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/traffic.log traffic;

    # Header option for security purpose
    add_header X-Frame-Options \"SAMEORIGIN\";
    add_header X-XSS-Protection \"1; mode=block\";
    add_header X-Content-Type-Options \"nosniff\";


    root /var/www/{USERNAME}/web/public{ROOT};
    index index.php index.html index.htm;

    #BEGIN

}"
                )
        );



        Whoplet::create(
                    array(
                        'user_id' => 1,
                        'name' => 'WHoPlet_without_php_https',
                        'status' => 1,
                        'content' => "# Do not change the default skeleton file!!!!
# Just add your line without deleting the skeleton line
# Importantly do not delete this line -> try_files \$uri /index.php =404
# Unless you know what you are doing

server {
    listen                  443 ssl spdy;
    listen                  [::]:443 ssl spdy;
    server_name             {DOMAINNAME};
    server_tokens           off;
    error_log               /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_error.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_access.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/traffic.log traffic;

    # Header option for security purpose
    add_header X-Frame-Options \"SAMEORIGIN\";
    add_header X-XSS-Protection \"1; mode=block\";
    add_header X-Content-Type-Options \"nosniff\";
    add_header Strict-Transport-Security \"max-age=31536000\";

    ssl                  on;
    ssl_certificate      /home/whop/hosting/{USERNAME}/SSL/{SSL}.crt;
    ssl_certificate_key  /home/whop/hosting/{USERNAME}/SSL/{SSL}.key;
    ssl_session_cache shared:SSL:10m;

    ssl_session_timeout  5m;

    ssl_protocols  TLSv1.2 TLSv1.1 TLSv1;
    ssl_ciphers    \"ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-RC4-SHA:ECDHE-RSA-RC4-SHA:ECDH-ECDSA-RC4-SHA:ECDH-RSA-RC4-SHA:ECDHE-RSA-AES256-SHA:RC4-SHA\";
    ssl_prefer_server_ciphers   on;

    root /var/www/{USERNAME}/web/public{ROOT};
    index index.php index.html index.htm;

    #BEGIN
}"
                        )
                );



        Whoplet::create(
            array(
                'user_id' => 1,
                'name' => 'WHoPlet_laravel',
                'status' => 1,
                'content' => "# Do not change the default skeleton file!!!!
# Just add your line without deleting the skeleton line
# Importantly do not delete this line -> try_files \$uri /index.php =404
# Unless you know what you are doing

server {
    listen                  80;
    listen                  [::]:80;
    server_name             {DOMAINNAME};
    server_tokens           off;
    error_log               /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_error.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_access.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/traffic.log traffic;

    # Header option for security purpose
    add_header X-Frame-Options \"SAMEORIGIN\";
    add_header X-XSS-Protection \"1; mode=block\";
    add_header X-Content-Type-Options \"nosniff\";


    root /var/www/{USERNAME}/web/public{ROOT};
    index index.php index.html index.htm;

    #BEGIN

    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }

    location ~ \.php\$ {
        try_files \$uri /index.php =404;

        add_header X-Cache \$upstream_cache_status;
        #fastcgi_cache WHOPWEBAPP;
        #fastcgi_cache_valid 302 200 {CACHETIME};
        #fastcgi_cache_valid 404 1m;
        #fastcgi_no_cache \$http_pragma \$http_authorization \$cookie_nocache \$arg_nocache;

        include /etc/nginx/fastcgi_params;
        fastcgi_buffers 256 16k;
        fastcgi_max_temp_file_size 0;
        fastcgi_pass  unix:/home/whop/hosting/{USERNAME}/php.socket;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    }
}"
                )
        );


        Whoplet::create(
                    array(
                        'user_id' => 1,
                        'name' => 'WHoPlet_laravel_https',
                        'status' => 1,
                        'content' => "# Do not change the default skeleton file!!!!
# Just add your line without deleting the skeleton line
# Importantly do not delete this line -> try_files \$uri /index.php =404
# Unless you know what you are doing

server {
    listen                  443 ssl spdy;
    listen                  [::]:443 ssl spdy;
    server_name             {DOMAINNAME};
    server_tokens           off;
    error_log               /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_error.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_access.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/traffic.log traffic;

    # Header option for security purpose
    add_header X-Frame-Options \"SAMEORIGIN\";
    add_header X-XSS-Protection \"1; mode=block\";
    add_header X-Content-Type-Options \"nosniff\";
    add_header Strict-Transport-Security \"max-age=31536000\";

    ssl                  on;
    ssl_certificate      /home/whop/hosting/{USERNAME}/SSL/{SSL}.crt;
    ssl_certificate_key  /home/whop/hosting/{USERNAME}/SSL/{SSL}.key;
    ssl_session_cache shared:SSL:10m;

    ssl_session_timeout  5m;

    ssl_protocols  TLSv1.2 TLSv1.1 TLSv1;
    ssl_ciphers    \"ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-RC4-SHA:ECDHE-RSA-RC4-SHA:ECDH-ECDSA-RC4-SHA:ECDH-RSA-RC4-SHA:ECDHE-RSA-AES256-SHA:RC4-SHA\";
    ssl_prefer_server_ciphers   on;

    root /var/www/{USERNAME}/web/public{ROOT};
    index index.php index.html index.htm;


    location / {
        try_files \$uri \$uri/ /index.php?\$query_string;
    }

    #BEGIN

    location ~ \.php\$ {
        try_files \$uri /index.php =404;

        add_header X-Cache \$upstream_cache_status;
        #fastcgi_cache WHOPWEBAPP;
        #fastcgi_cache_valid 302 200 {CACHETIME};
        #fastcgi_cache_valid 404 1m;
        #fastcgi_no_cache \$http_pragma \$http_authorization \$cookie_nocache \$arg_nocache;

        include /etc/nginx/fastcgi_params;
        fastcgi_buffers 256 16k;
        fastcgi_max_temp_file_size 0;
        fastcgi_pass  unix:/home/whop/hosting/{USERNAME}/php.socket;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    }
}"
                        )
                );



        Whoplet::create(
            array(
                'user_id' => 1,
                'name' => 'WHoPlet_wordpress',
                'status' => 1,
                'content' => "# Do not change the default skeleton file!!!!
# Just add your line without deleting the skeleton line
# Importantly do not delete this line -> try_files \$uri /index.php =404
# Unless you know what you are doing

server {
    listen                  80;
    listen                  [::]:80;
    server_name             {DOMAINNAME};
    server_tokens           off;
    error_log               /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_error.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_access.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/traffic.log traffic;

    # Header option for security purpose
    add_header X-Frame-Options \"SAMEORIGIN\";
    add_header X-XSS-Protection \"1; mode=block\";
    add_header X-Content-Type-Options \"nosniff\";


    root /var/www/{USERNAME}/web/public{ROOT};
    index index.php index.html index.htm;

    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    location / {
        try_files \$uri \$uri/ /index.php?\$args;
    }

    #BEGIN

    location ~ \.php\$ {
        try_files \$uri /index.php =404;

        add_header X-Cache \$upstream_cache_status;
        #fastcgi_cache WHOPWEBAPP;
        #fastcgi_cache_valid 302 200 {CACHETIME};
        #fastcgi_cache_valid 404 1m;
        #fastcgi_no_cache \$http_pragma \$http_authorization \$cookie_nocache \$arg_nocache;

        include /etc/nginx/fastcgi_params;
        fastcgi_buffers 256 16k;
        fastcgi_max_temp_file_size 0;
        fastcgi_pass  unix:/home/whop/hosting/{USERNAME}/php.socket;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    }
}"
                )
        );


        Whoplet::create(
                    array(
                        'user_id' => 1,
                        'name' => 'WHoPlet_wordpress_https',
                        'status' => 1,
                        'content' => "# Do not change the default skeleton file!!!!
# Just add your line without deleting the skeleton line
# Importantly do not delete this line -> try_files \$uri /index.php =404
# Unless you know what you are doing

server {
    listen                  443 ssl spdy;
    listen                  [::]:443 ssl spdy;
    server_name             {DOMAINNAME};
    server_tokens           off;
    error_log               /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_error.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_access.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/traffic.log traffic;

    # Header option for security purpose
    add_header X-Frame-Options \"SAMEORIGIN\";
    add_header X-XSS-Protection \"1; mode=block\";
    add_header X-Content-Type-Options \"nosniff\";
    add_header Strict-Transport-Security \"max-age=31536000\";

    ssl                  on;
    ssl_certificate      /home/whop/hosting/{USERNAME}/SSL/{SSL}.crt;
    ssl_certificate_key  /home/whop/hosting/{USERNAME}/SSL/{SSL}.key;
    ssl_session_cache shared:SSL:10m;

    ssl_session_timeout  5m;

    ssl_protocols  TLSv1.2 TLSv1.1 TLSv1;
    ssl_ciphers    \"ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-RC4-SHA:ECDHE-RSA-RC4-SHA:ECDH-ECDSA-RC4-SHA:ECDH-RSA-RC4-SHA:ECDHE-RSA-AES256-SHA:RC4-SHA\";
    ssl_prefer_server_ciphers   on;

    root /var/www/{USERNAME}/web/public{ROOT};
    index index.php index.html index.htm;


    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    location / {
        try_files \$uri \$uri/ /index.php?\$args;
    }

    #BEGIN

    location ~ \.php\$ {
        try_files \$uri /index.php =404;

        add_header X-Cache \$upstream_cache_status;
        #fastcgi_cache WHOPWEBAPP;
        #fastcgi_cache_valid 302 200 {CACHETIME};
        #fastcgi_cache_valid 404 1m;
        #fastcgi_no_cache \$http_pragma \$http_authorization \$cookie_nocache \$arg_nocache;

        include /etc/nginx/fastcgi_params;
        fastcgi_buffers 256 16k;
        fastcgi_max_temp_file_size 0;
        fastcgi_pass  unix:/home/whop/hosting/{USERNAME}/php.socket;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    }
}"
                        )
                );



        Whoplet::create(
            array(
                'user_id' => 1,
                'name' => 'WHoPlet_joomla',
                'status' => 1,
                'content' => "# Do not change the default skeleton file!!!!
# Just add your line without deleting the skeleton line
# Importantly do not delete this line -> try_files \$uri /index.php =404
# Unless you know what you are doing

server {
    listen                  80;
    listen                  [::]:80;
    server_name             {DOMAINNAME};
    server_tokens           off;
    error_log               /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_error.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_access.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/traffic.log traffic;

    # Header option for security purpose
    add_header X-Frame-Options \"SAMEORIGIN\";
    add_header X-XSS-Protection \"1; mode=block\";
    add_header X-Content-Type-Options \"nosniff\";


    root /var/www/{USERNAME}/web/public{ROOT};
    index index.php index.html index.htm;

    location / {
        try_files \$uri \$uri/ /index.php?\$args;
    }

    #BEGIN

    # deny running scripts inside writable directories
    location ~* /(images|cache|media|logs|tmp)/.*\.(php|pl|py|jsp|asp|sh|cgi)\$ {
            return 403;
            error_page 403 /403_error.html;
    }

    # caching of files as suggested by https://docs.joomla.org/Nginx
    location ~* \.(ico|pdf|flv)\$ {
            expires 1y;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|swf|xml|txt)\$ {
            expires 14d;
    }

    location ~ \.php\$ {
        try_files \$uri /index.php =404;

        add_header X-Cache \$upstream_cache_status;
        #fastcgi_cache WHOPWEBAPP;
        #fastcgi_cache_valid 302 200 {CACHETIME};
        #fastcgi_cache_valid 404 1m;
        #fastcgi_no_cache \$http_pragma \$http_authorization \$cookie_nocache \$arg_nocache;

        include /etc/nginx/fastcgi_params;
        fastcgi_buffers 256 16k;
        fastcgi_max_temp_file_size 0;
        fastcgi_pass  unix:/home/whop/hosting/{USERNAME}/php.socket;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    }
}"
                )
        );


        Whoplet::create(
                    array(
                        'user_id' => 1,
                        'name' => 'WHoPlet_joomla_https',
                        'status' => 1,
                        'content' => "# Do not change the default skeleton file!!!!
# Just add your line without deleting the skeleton line
# Importantly do not delete this line -> try_files \$uri /index.php =404
# Unless you know what you are doing

server {
    listen                  443 ssl spdy;
    listen                  [::]:443 ssl spdy;
    server_name             {DOMAINNAME};
    server_tokens           off;
    error_log               /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_error.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_access.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/traffic.log traffic;

    # Header option for security purpose
    add_header X-Frame-Options \"SAMEORIGIN\";
    add_header X-XSS-Protection \"1; mode=block\";
    add_header X-Content-Type-Options \"nosniff\";
    add_header Strict-Transport-Security \"max-age=31536000\";

    ssl                  on;
    ssl_certificate      /home/whop/hosting/{USERNAME}/SSL/{SSL}.crt;
    ssl_certificate_key  /home/whop/hosting/{USERNAME}/SSL/{SSL}.key;
    ssl_session_cache shared:SSL:10m;

    ssl_session_timeout  5m;

    ssl_protocols  TLSv1.2 TLSv1.1 TLSv1;
    ssl_ciphers    \"ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-RC4-SHA:ECDHE-RSA-RC4-SHA:ECDH-ECDSA-RC4-SHA:ECDH-RSA-RC4-SHA:ECDHE-RSA-AES256-SHA:RC4-SHA\";
    ssl_prefer_server_ciphers   on;

    root /var/www/{USERNAME}/web/public{ROOT};
    index index.php index.html index.htm;

    location / {
        try_files \$uri \$uri/ /index.php?\$args;
    }

    #BEGIN

    # deny running scripts inside writable directories
    location ~* /(images|cache|media|logs|tmp)/.*\.(php|pl|py|jsp|asp|sh|cgi)\$ {
            return 403;
            error_page 403 /403_error.html;
    }

    # caching of files as suggested by https://docs.joomla.org/Nginx
    location ~* \.(ico|pdf|flv)\$ {
            expires 1y;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|swf|xml|txt)\$ {
            expires 14d;
    }

    location ~ \.php\$ {
        try_files \$uri /index.php =404;

        add_header X-Cache \$upstream_cache_status;
        #fastcgi_cache WHOPWEBAPP;
        #fastcgi_cache_valid 302 200 {CACHETIME};
        #fastcgi_cache_valid 404 1m;
        #fastcgi_no_cache \$http_pragma \$http_authorization \$cookie_nocache \$arg_nocache;

        include /etc/nginx/fastcgi_params;
        fastcgi_buffers 256 16k;
        fastcgi_max_temp_file_size 0;
        fastcgi_pass  unix:/home/whop/hosting/{USERNAME}/php.socket;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    }
}"
                        )
                );



        Whoplet::create(
            array(
                'user_id' => 1,
                'name' => 'WHoPlet_phpbb',
                'status' => 1,
                'content' => "# Do not change the default skeleton file!!!!
# Just add your line without deleting the skeleton line
# Importantly do not delete this line -> try_files \$uri /index.php =404
# Unless you know what you are doing

server {
    listen                  80;
    listen                  [::]:80;
    server_name             {DOMAINNAME};
    server_tokens           off;
    error_log               /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_error.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_access.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/traffic.log traffic;

    # Header option for security purpose
    add_header X-Frame-Options \"SAMEORIGIN\";
    add_header X-XSS-Protection \"1; mode=block\";
    add_header X-Content-Type-Options \"nosniff\";


    root /var/www/{USERNAME}/web/public{ROOT};
    index index.php index.html index.htm;

    #BEGIN

    location ~ /(config\.php|common\.php|cache|files|images/avatars/upload|includes|store) {
        deny all;
        return 403;
    }

    location ~* \.(gif|jpe?g|png|css)\$ {
        expires   30d;
    }

    location ~ \.php\$ {
        try_files \$uri /index.php =404;

        add_header X-Cache \$upstream_cache_status;
        #fastcgi_cache WHOPWEBAPP;
        #fastcgi_cache_valid 302 200 {CACHETIME};
        #fastcgi_cache_valid 404 1m;
        #fastcgi_no_cache \$http_pragma \$http_authorization \$cookie_nocache \$arg_nocache;

        include /etc/nginx/fastcgi_params;
        fastcgi_buffers 256 16k;
        fastcgi_max_temp_file_size 0;
        fastcgi_pass  unix:/home/whop/hosting/{USERNAME}/php.socket;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    }
}"
                )
        );


        Whoplet::create(
                    array(
                        'user_id' => 1,
                        'name' => 'WHoPlet_phpbb_https',
                        'status' => 1,
                        'content' => "# Do not change the default skeleton file!!!!
# Just add your line without deleting the skeleton line
# Importantly do not delete this line -> try_files \$uri /index.php =404
# Unless you know what you are doing

server {
    listen                  443 ssl spdy;
    listen                  [::]:443 ssl spdy;
    server_name             {DOMAINNAME};
    server_tokens           off;
    error_log               /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_error.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_access.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/traffic.log traffic;

    # Header option for security purpose
    add_header X-Frame-Options \"SAMEORIGIN\";
    add_header X-XSS-Protection \"1; mode=block\";
    add_header X-Content-Type-Options \"nosniff\";
    add_header Strict-Transport-Security \"max-age=31536000\";

    ssl                  on;
    ssl_certificate      /home/whop/hosting/{USERNAME}/SSL/{SSL}.crt;
    ssl_certificate_key  /home/whop/hosting/{USERNAME}/SSL/{SSL}.key;
    ssl_session_cache shared:SSL:10m;

    ssl_session_timeout  5m;

    ssl_protocols  TLSv1.2 TLSv1.1 TLSv1;
    ssl_ciphers    \"ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-RC4-SHA:ECDHE-RSA-RC4-SHA:ECDH-ECDSA-RC4-SHA:ECDH-RSA-RC4-SHA:ECDHE-RSA-AES256-SHA:RC4-SHA\";
    ssl_prefer_server_ciphers   on;

    root /var/www/{USERNAME}/web/public{ROOT};
    index index.php index.html index.htm;

    #BEGIN

    location ~ /(config\.php|common\.php|cache|files|images/avatars/upload|includes|store) {
        deny all;
        return 403;
    }

    location ~* \.(gif|jpe?g|png|css)\$ {
        expires   30d;
    }

    location ~ \.php\$ {
        try_files \$uri /index.php =404;

        add_header X-Cache \$upstream_cache_status;
        #fastcgi_cache WHOPWEBAPP;
        #fastcgi_cache_valid 302 200 {CACHETIME};
        #fastcgi_cache_valid 404 1m;
        #fastcgi_no_cache \$http_pragma \$http_authorization \$cookie_nocache \$arg_nocache;

        include /etc/nginx/fastcgi_params;
        fastcgi_buffers 256 16k;
        fastcgi_max_temp_file_size 0;
        fastcgi_pass  unix:/home/whop/hosting/{USERNAME}/php.socket;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    }

}"
                        )
                );




        Whoplet::create(
                    array(
                        'user_id' => 1,
                        'name' => 'WHoPlet_Redirect_Http_To_Https',
                        'status' => 1,
                        'content' => "# Do not change the default skeleton file!!!!
# Just add your line without deleting the skeleton line
# Importantly do not delete this line -> try_files \$uri /index.php =404
# Unless you know what you are doing

server {
    listen                  80;
    listen                  [::]:80;
    server_name             {DOMAINNAME};
    server_tokens           off;
    error_log               /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_error.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_access.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/traffic.log traffic;

    # Header option for security purpose
    add_header X-Frame-Options \"SAMEORIGIN\";
    add_header X-XSS-Protection \"1; mode=block\";
    add_header X-Content-Type-Options \"nosniff\";

    root /var/www/{USERNAME}/web/public{ROOT};
    index index.php index.html index.htm;

    #BEGIN

    return 301 https://\$server_name\$request_uri;

}"
                        )
                );










        Whoplet::create(
                    array(
                        'user_id' => 1,
                        'name' => 'WHoPlet_opencart',
                        'status' => 1,
                        'content' => "# Do not change the default skeleton file!!!!
# Just add your line without deleting the skeleton line
# Importantly do not delete this line -> try_files \$uri /index.php =404
# Unless you know what you are doing

server {
    listen                  80;
    listen                  [::]:80;
    server_name             {DOMAINNAME};
    server_tokens           off;
    error_log               /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_error.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_access.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/traffic.log traffic;

    # Header option for security purpose
    add_header X-Frame-Options \"SAMEORIGIN\";
    add_header X-XSS-Protection \"1; mode=block\";
    add_header X-Content-Type-Options \"nosniff\";

    root /var/www/{USERNAME}/web/public{ROOT};
    index index.php index.html index.htm;

    #BEGIN

    location /image/data {
        autoindex on;
    }
    location /admin {
        index index.php;
    }
    location / {
        try_files \$uri @opencart;
    }
    location @opencart {
        rewrite ^/(.+)$ /index.php?_route_=$1 last;
    }
    
    # Make sure files with the following extensions do not get loaded by nginx because nginx would display the source code, and these files can contain PASSWORDS!
    location ~* \.(engine|inc|info|install|make|module|profile|test|po|sh|.*sql|theme|tpl(\.php)?|xtmpl)$|^(\..*|Entries.*|Repository|Root|Tag|Template)$|\.php_ {
        deny all;
    }
    # Deny all attempts to access hidden files such as .htaccess, .htpasswd, .DS_Store (Mac).
    location ~ /\. {
        deny all;
        access_log off;
        log_not_found off;
    }
    location ~*  \.(jpg|jpeg|png|gif|css|js|ico)$ {
        expires max;
        log_not_found off;
    }

    location ~ \.php\$ {
        try_files \$uri /index.php =404;

        add_header X-Cache \$upstream_cache_status;
        #fastcgi_cache WHOPWEBAPP;
        #fastcgi_cache_valid 302 200 {CACHETIME};
        #fastcgi_cache_valid 404 1m;
        #fastcgi_no_cache \$http_pragma \$http_authorization \$cookie_nocache \$arg_nocache;

        include /etc/nginx/fastcgi_params;
        fastcgi_buffers 256 16k;
        fastcgi_max_temp_file_size 0;
        fastcgi_pass  unix:/home/whop/hosting/{USERNAME}/php.socket;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    }

}"
                        )
                );








        Whoplet::create(
                    array(
                        'user_id' => 1,
                        'name' => 'WHoPlet_opencart_https',
                        'status' => 1,
                        'content' => "# Do not change the default skeleton file!!!!
# Just add your line without deleting the skeleton line
# Importantly do not delete this line -> try_files \$uri /index.php =404
# Unless you know what you are doing

server {
    listen                  443 ssl spdy;
    listen                  [::]:ssl spdy;
    server_name             {DOMAINNAME};
    server_tokens           off;
    error_log               /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_error.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/{DOMAINNAME}_access.log;
    access_log              /home/whopjail/{USERNAME}/var/log/nginx/traffic.log traffic;

    # Header option for security purpose
    add_header X-Frame-Options \"SAMEORIGIN\";
    add_header X-XSS-Protection \"1; mode=block\";
    add_header X-Content-Type-Options \"nosniff\";
    add_header Strict-Transport-Security \"max-age=31536000\";

    ssl                  on;
    ssl_certificate      /home/whop/hosting/{USERNAME}/SSL/{SSL}.crt;
    ssl_certificate_key  /home/whop/hosting/{USERNAME}/SSL/{SSL}.key;
    ssl_session_cache shared:SSL:10m;

    ssl_session_timeout  5m;

    ssl_protocols  TLSv1.2 TLSv1.1 TLSv1;
    ssl_ciphers    \"ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256:ECDHE-ECDSA-RC4-SHA:ECDHE-RSA-RC4-SHA:ECDH-ECDSA-RC4-SHA:ECDH-RSA-RC4-SHA:ECDHE-RSA-AES256-SHA:RC4-SHA\";
    ssl_prefer_server_ciphers   on;

    root /var/www/{USERNAME}/web/public{ROOT};
    index index.php index.html index.htm;

    #BEGIN

    location /image/data {
        autoindex on;
    }
    location /admin {
        index index.php;
    }
    location / {
        try_files \$uri @opencart;
    }
    location @opencart {
        rewrite ^/(.+)$ /index.php?_route_=$1 last;
    }
    
    # Make sure files with the following extensions do not get loaded by nginx because nginx would display the source code, and these files can contain PASSWORDS!
    location ~* \.(engine|inc|info|install|make|module|profile|test|po|sh|.*sql|theme|tpl(\.php)?|xtmpl)$|^(\..*|Entries.*|Repository|Root|Tag|Template)$|\.php_ {
        deny all;
    }
    # Deny all attempts to access hidden files such as .htaccess, .htpasswd, .DS_Store (Mac).
    location ~ /\. {
        deny all;
        access_log off;
        log_not_found off;
    }
    location ~*  \.(jpg|jpeg|png|gif|css|js|ico)$ {
        expires max;
        log_not_found off;
    }

    location ~ \.php\$ {
        try_files \$uri /index.php =404;

        add_header X-Cache \$upstream_cache_status;
        #fastcgi_cache WHOPWEBAPP;
        #fastcgi_cache_valid 302 200 {CACHETIME};
        #fastcgi_cache_valid 404 1m;
        #fastcgi_no_cache \$http_pragma \$http_authorization \$cookie_nocache \$arg_nocache;

        include /etc/nginx/fastcgi_params;
        fastcgi_buffers 256 16k;
        fastcgi_max_temp_file_size 0;
        fastcgi_pass  unix:/home/whop/hosting/{USERNAME}/php.socket;
        fastcgi_index index.php;
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
    }

}"
                        )
                );






        Model::reguard();
    }
}
