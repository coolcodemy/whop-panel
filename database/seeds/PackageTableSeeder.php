<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use WHoP\Package;

class PackageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Delete any content in the roles table
        DB::table('packages')->delete();

        Package::create(
            array(
                'name' => 'Unlimited',
                'validFor' => '999 YEAR',
                'apcuSupport' => 1,
                'enableSsl' => 1,
                'ssh' => 1,
                'diskQuota' => 0,
                'emailQUota' => 0,
                'ftpQuota' => 0,
                'websiteQuota' => 0,
                'numberOfDatabase' => 0,
                'numberOfDatabaseUser' => 0,
                'numberOfEmailAccount' => 0,
                'numberOfFtpAccount' => 0,
                'numberOfDomain' => 0,
                'numberOfWhoplet' => 0,
                'maximumQueriesPerHour' => 0,
                'maximumConnectionsPerHour' => 0,
                'maximumUpdatesPerHour' => 0,
                'maximumUserConnections' => 0,
                )
        );
    }
}
