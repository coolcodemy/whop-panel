<?php

return [

	/**
	 * Defining themes for WHoP
	 */
	'themes' => env('WHOP_TEMPLATE', 'materializecss'),

	'version' => env('WHOP_VERSION'),

    'codename' => env('WHOP_CODENAME'),

    'nodekey' => env('NODE_KEY', 'whopkey'),
];