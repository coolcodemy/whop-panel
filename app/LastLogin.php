<?php

namespace WHoP;

use Illuminate\Database\Eloquent\Model;

class LastLogin extends Model
{
    public $timestamps = false;

    protected $dates = ['time'];
    
    public function user()
    {
    	return $this->belongsTo('WHoP\User');
    }
}
