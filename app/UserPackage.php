<?php

namespace WHoP;

use Illuminate\Database\Eloquent\Model;

class UserPackage extends Model
{
    public function package()
    {
    	return $this->belongsTo('WHoP\Package');
    }
}
