<?php

namespace WHoP;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = [
        'name',
        'apcuSupport',
        'enableSsl',
        'ssh',
        'diskQuota',
        'emailQuota',
        'ftpQuota',
        'websiteQuota',
        'numberOfDatabase',
        'numberOfDatabaseUser',
        'maximumQueriesPerHour',
        'maximumConnectionsPerHour',
        'maximumUpdatesPerHour',
        'maximumUserConnections',
        'numberOfEmailAccount',
        'numberOfFtpAccount',
        'numberOfDomain',
        'numberOfWhoplet'
    ];


    public function userpackage()
    {
        return $this->hasMany('WHoP\UserPackage');
    }
}
