<?php

namespace WHoP;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword, EntrustUserTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'username', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];



    public function roles()
    {
        return $this->belongsToMany('WHoP\Role','role_user');
    }


    public function userpackage()
    {
        return $this->hasOne('WHoP\UserPackage');
    }


    public function logintime()
    {
        return $this->hasMany('WHoP\LastLogin');
    }

    public function scopeLastLogin($q)
    {
        return $q->where('user_id', '=', 1);
    }

    public function emails()
    {
        return $this->hasMany('WHoP\MailUser');
    }
}
