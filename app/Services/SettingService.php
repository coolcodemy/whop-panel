<?php


namespace WHoP\Services;

use Artisan, Cache;

use WHoP\Setting;

use WHoP\Events\PanelEmailWasUpdated;

class SettingService
{
    public function getString($settingName)
    {
    	return Cache::rememberForever($settingName, function() use ($settingName)
    	{
    		return Setting::where('settingName', '=', $settingName)->first()->value;
    	});
    }





    public function getInt($settingName)
    {
    	return Cache::rememberForever($settingName, function() use ($settingName)
    	{
    		return (int) Setting::where('settingName', '=', $settingName)->first()->value;
    	});
    }





    public function update($request)
    {
        foreach ($request->all() as $key => $value) {
            
            $setting = Setting::where('settingName', '=', $key)->first();

            if ( $setting != null ) {

                $setting->value = $value;
                $setting->save();

                Cache::forget($key);

            }
        }
    }



    public function updateEmail($request)
    {

        $path = base_path('.env');

        if (file_exists($path)) {

            if ($request->mailHost) {

                env('MAIL_HOST') == null ? $mh = 'null' : $mh = env('MAIL_HOST');

                file_put_contents($path, str_replace('MAIL_HOST=' . $mh, 'MAIL_HOST=' . $request->mailHost, file_get_contents($path)));

            }


            if ($request->mailPort) {

                env('MAIL_PORT') == null ? $mp = 'null' : $mp = env('MAIL_PORT');

                file_put_contents($path, str_replace('MAIL_PORT=' . $mp, 'MAIL_PORT=' . $request->mailPort, file_get_contents($path)));

            }


            if ($request->mailName) {

                env('MAIL_NAME') == null ? $mn = 'null' : $mn = env('MAIL_NAME');

                file_put_contents($path, str_replace('MAIL_NAME=' . $mn, 'MAIL_NAME=' . $request->mailName, file_get_contents($path)));

            }


            if ($request->mailUsername) {

                env('MAIL_USERNAME') == null ? $mu = 'null' : $mu = env('MAIL_USERNAME');

                file_put_contents($path, str_replace('MAIL_USERNAME=' . $mu, 'MAIL_USERNAME=' . $request->mailUsername, file_get_contents($path)));

            }


            if ($request->mailPassword) {

                env('MAIL_PASSWORD') == null ? $mp = 'null' : $mp = env('MAIL_PASSWORD');

                file_put_contents($path, str_replace('MAIL_PASSWORD=' . $mp , 'MAIL_PASSWORD=' . $request->mailPassword, file_get_contents($path)));

            }


            if ($request->mailEncryption) {

                env('MAIL_ENCRYPTION') == null ? $enc = 'null' : $enc = env('MAIL_ENCRYPTION');

                file_put_contents($path, str_replace('MAIL_ENCRYPTION=' . $enc, 'MAIL_ENCRYPTION=' . $request->mailEncryption, file_get_contents($path)));

            }


            Artisan::call('config:clear');


            event(new PanelEmailWasUpdated());

        }


    }





    public function autoUpdate()
    {
        env('AUTO_UPDATE') ? $bool = 'false' : $bool = 'true';

        $path = base_path('.env');

        file_put_contents($path, str_replace('AUTO_UPDATE=' . json_encode(env('AUTO_UPDATE')), 'AUTO_UPDATE=' . $bool, file_get_contents($path)));

        return $bool;
    }
}