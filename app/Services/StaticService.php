<?php


namespace WHoP\Services;

use Cache;

class StaticService
{
    public function validityDay()
    {
        $day = Cache::tags('static')->rememberForever('packageDay', function()
        {
            $day = array();

            for ($i = 1; $i <= 1000; $i++) {
                $day[$i] = $i;
            }

            return $day;
        });

        return $day;
    }


    public function rgbGenerator()
    {
        $randomString = md5(time().rand(0,999));
        $r = substr($randomString,0,2);
        $g = substr($randomString,2,2);
        $b = substr($randomString,4,2);

        return sprintf("%s%s%s", $r, $g, $b);
    }


    public function progressColor($val, $max)
    {
        if ($max == 0) {
            return 'light-green';
        } elseif ($val/$max*100 <= 25) {
            return 'light-green';
        } elseif ($val/$max*100 <= 50) {
            return 'blue';
        } elseif ($val/$max*100 <= 75) {
            return 'orange';
        } else {
            return 'red';
        }
    }

    public function randomString($length = 10) 
    {
        $alphabets = range('A','Z');
        $alphabets2 = range('a','z');
        $numbers = range('0','9');
        $final_array = array_merge($alphabets,$numbers,$alphabets2);

        $string = '';

        while($length--) {
            $key = array_rand($final_array);
            $string .= $final_array[$key];
        }

        return $string;
    }
}