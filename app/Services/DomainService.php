<?php


namespace WHoP\Services;

use WHoP\Domain;
use WHoP\User;

use Illuminate\Pagination\LengthAwarePaginator;

use Input;

class DomainService
{
    public function createOrUpdateDomain(User $user, $domainName, $deleteEdit = 1, $type = 'NATIVE')
    {
        $domain = Domain::firstOrNew([
            'name' => $domainName
            ]);
        
        $domain->user_id = $user->id;
        $domain->name = $domainName;
        $domain->type = $type;
        $domain->deleteEdit = 1;
        $domain->save();

        return $domain;
    }


    public function getUserDomainList(User $user, $paginate = null)
    {
        $domains = Domain::where('user_id', '=', $user->id)->get();

        $domainArray = [];

        if (auth()->user()->hasRole('admin')) {

            foreach ($domains as $domain) {
                $domain->deleteURL = route('admin::app::domain-delete', [$user, $domain]);

                $domain->recordURL = route('admin::app::record-all', [$user, $domain]);

                $domainArray[] = $domain;
            }

        } else if ( auth()->user()->hasRole('owner') ) {

            foreach ($domains as $domain) {
                $domain->deleteURL = route('owner::app::domain-delete', [$domain]);

                $domain->recordURL = route('owner::app::record-all', [$domain]);

                $domainArray[] = $domain;
            }

        }


        if ($paginate !== null) {
            $currentPage = Input::get('page', 1);
            $itemPerPage = $paginate;
            $offset = ($currentPage * $itemPerPage) - $itemPerPage;

            return new LengthAwarePaginator(array_slice($domainArray, $offset, $itemPerPage, true), count($domainArray), $itemPerPage, $currentPage, ['path' => Input::url(), 'query' => Input::query()]);
        }

        return collect($domainArray);
    }


    public function deleteDomain(User $user, Domain $domain)
    {
        app('MailService')->deleteEmailsFromDomain($user, $domain);
        app('WhopletService')->deleteWhopletFromDomain($user, $domain);

        $domain->delete();
        
        return redirect()->back()->with([
            'successMessage' => sprintf('Succesfully delete domain %s.', $domain->name),
            ]);
    }


    public function addDomain(User $user, $request)
    {
        $domain = $this->createOrUpdateDomain($user, $request->domainName);

        $request->merge([
            'domain_id' => $domain->id,
            ]);

        app('RecordService')->populateNewDomain($user, $request);


        return redirect()->back()->with([
            'successMessage' => sprintf('Succesfully add domain name %s.', $request->domainName),
            ]);
    }


    public function find($id)
    {
        return Domain::find($id);
    }
}