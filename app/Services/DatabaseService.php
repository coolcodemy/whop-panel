<?php


namespace WHoP\Services;

use WHoP\User;

use DB; 

class DatabaseService
{
    private $pdo;


    public function __construct()
    {
        $this->pdo = DB::connection()->getPdo();
    }


    public function allDatabase()
    {
        $q = $this->pdo->prepare("SELECT * from information_schema.SCHEMATA
                WHERE SCHEMA_NAME != 'WHoP_Master' 
                AND SCHEMA_NAME != 'WHoP_Roundcubemail'
                AND SCHEMA_NAME != 'information_schema'
                AND SCHEMA_NAME != 'mysql'
                AND SCHEMA_NAME != 'performance_schema'");
        
        $q->execute();

        return $q->fetchAll();
    }


    public function getUserDatabaseList(User $user)
    {
        $q = $this->pdo->prepare("SELECT * from information_schema.SCHEMATA WHERE SCHEMA_NAME LIKE :username");
        $q->execute(array(':username' => $user->username . '_%'));

        return $q->fetchAll();
    }

    public function getUserDatabaseAccountList(User $user)
    {
        $q = $this->pdo->prepare("SELECT * from mysql.user WHERE User LIKE :username");
        $q->execute(array(':username' => $user->username . '_%'));

        return $q->fetchAll();
    }



    public function getUserOfDB(User $user, $dbName)
    {
        $pdo = $this->pdo;
        //$dbName = str_replace('_', '\_', $dbName);

        $q =  $pdo->prepare("SELECT GRANTEE FROM information_schema.SCHEMA_PRIVILEGES WHERE TABLE_SCHEMA = :dbName GROUP BY GRANTEE");
        $q->execute(array(':dbName' => $dbName));
        $userOfDB =  $q->fetchAll();

        $userOfDBArr = array();

        foreach ($userOfDB as $value) {
            $user = preg_replace('(^\')', '', $value['GRANTEE']);
            $userOfDBArr[] = preg_replace('/\'@\'%\'/', '', $user);
        }

        return $userOfDBArr;
    }


    public function getDBSize(User $user, $dbName)
    {
        return DB::select('SELECT sum( data_length + index_length ) / 1024 / 1024 "Size" FROM information_schema.TABLES WHERE TABLE_SCHEMA = :db', array('db' => $dbName));
    }


    public function myDBWithUser(User $user)
    {
        $db = $this->getUserDatabaseList($user);

        $dbArr = [];

        foreach ($db AS $myDB) {
            $dbArr[$myDB['SCHEMA_NAME']] = array(
                'user' => $this->getUserOfDB($user, $myDB['SCHEMA_NAME']),
                'size' => $this->getDBSize($user, $myDB['SCHEMA_NAME']),
                'strip' => substr_replace($myDB['SCHEMA_NAME'],'',0,strlen($user->username . '_')),
                );
        }

        return $dbArr;
    }


    public function myDBRemoveUseraname(User $user)
    {
        $db = $this->getUserDatabaseList($user);

        $dbArrRemoveUsername = [];

        foreach ($db as $key => $value) {
            $dbArrRemoveUsername[] = array(
                'strip' => substr_replace($value['SCHEMA_NAME'],'',0,strlen($user->username . '_')),
                'realname' => $value['SCHEMA_NAME']
                );
        }

        return $dbArrRemoveUsername;
    }


    public function myAllDBUser(User $user)
    {
        $dbUser = $this->getUserDatabaseAccountList($user);

        $dbUserArrRemoveUsername = [];

        foreach ($dbUser as $key => $value) {
            $dbUserArrRemoveUsername[] = array(
                'strip' => substr_replace($value['User'],'',0,strlen($user->username . '_')),
                'realname' => $value['User']
                );
        }

        return $dbUserArrRemoveUsername;
    }


    public function userOfDatabase(User $user, $dbName)
    {
        $pdo = $this->pdo;

        $q =  $pdo->prepare("SELECT GRANTEE FROM information_schema.SCHEMA_PRIVILEGES WHERE TABLE_SCHEMA = :dbName GROUP BY GRANTEE");
        $q->execute(array(':dbName' => $dbName));
        $userOfDB =  $q->fetchAll();

        $userOfDBArr = array();

        foreach ($userOfDB as $value) {
            $user = preg_replace('(^\')', '', $value['GRANTEE']);
            $userOfDBArr[] = preg_replace('/\'@\'%\'/', '', $user);
        }

        return $userOfDBArr;
    }


    public function create($request)
    {
        DB::statement("CREATE DATABASE `$request->databaseFullName`");

        return redirect()->back()
            ->with([
                'successMessage' => sprintf('Successfully add database %s.', $request->databaseFullName),
                ]);
    }


    public function createUser(User $user, $request)
    {
        $package = $user->userpackage;
        DB::statement("GRANT USAGE ON *.* TO '" . $request->usernameFull . "'@'%' IDENTIFIED BY '" . $request->password . "' WITH MAX_QUERIES_PER_HOUR " . $package->maximumQueriesPerHour . " MAX_CONNECTIONS_PER_HOUR " . $package->maximumConnectionsPerHour . " MAX_UPDATES_PER_HOUR " . $package->maximumUpdatesPerHour . " MAX_USER_CONNECTIONS " . $package->maximumUserConnections);

        return redirect()->back()->with([
            'successMessage' => sprintf('Succesfully create new database user %s.', $request->usernameFull),
            ]);
    }


    public function assignUserToDatabase($request)
    {
        $grantString = '';


        foreach ($request->permission as $key => $value) 
        {
            if($value == 'SELECT')
            {
                $grantString .= 'SELECT, ';
            }
            else if($value == 'INSERT')
            {
                $grantString .= 'INSERT, ';
            }
            else if($value == 'UPDATE')
            {
                $grantString .= 'UPDATE, ';
            }
            else if($value == 'DELETE')
            {
                $grantString .= 'DELETE, ';
            }
            else if($value == 'CREATE')
            {
                $grantString .= 'CREATE, ';
            }
            else if($value == 'DROP')
            {
                $grantString .= 'DROP, ';
            }
            else if($value == 'INDEX')
            {
                $grantString .= 'INDEX, ';
            }
            else if($value == 'ALTER')
            {
                $grantString .= 'ALTER, ';
            }
            else if($value == 'CREATE-TEMPORARY-TABLE')
            {
                $grantString .= 'CREATE TEMPORARY TABLES, ';
            }
            else if($value == 'CREATE-VIEW')
            {
                $grantString .= 'CREATE VIEW, ';
            }           
            else if($value == 'SHOW-VIEW')
            {
                $grantString .= 'SHOW VIEW, ';
            }
            else if($value == 'CREATE-ROUTINE')
            {
                $grantString .= 'CREATE ROUTINE, ';
            }
            else if($value == 'ALTER-ROUTINE')
            {
                $grantString .= 'ALTER ROUTINE, ';
            }
            else if($value == 'EXECUTE')
            {
                $grantString .= 'EXECUTE, ';
            }
            else if($value == 'EVENT')
            {
                $grantString .= 'EVENT, ';
            }
            else if($value == 'TRIGGER')
            {
                $grantString .= 'TRIGGER, ';
            }

        }

        $grantString = substr($grantString, 0, -2);

        if ($grantString == '' || $grantString == null) {
            return redirect()->back()->with([
                'errorMessage' => 'Unknown permission.',
                ]);
        }


        DB::update("GRANT $grantString ON `" . $request->databaseFullName . "`.* TO " . $request->usernameFull . "@'%'");

        $this->flush();

        return redirect()->back()->with([
            'successMessage' => sprintf('Succesfully add %s to database %s.', $request->usernameFull, $request->databaseFullName),
            ]);
    }


    public function flush()
    {
        DB::statement("FLUSH PRIVILEGES");
    }


    public function revokeUser($request)
    {
        DB::statement("REVOKE ALL PRIVILEGES ON `" . $request->databaseFullName . "`.* FROM " . $request->usernameFull . "@'%'");

        return redirect()->back()->with([
            'successMessage' => sprintf('Succesfully revoke %s from %s.', $request->usernameFull, $request->databaseFullName),
            ]);
    }



    public function delete($request)
    {
        DB::statement("DROP DATABASE `$request->databaseFullName`");

        return redirect()->back()->with([
            'successMessage' => sprintf('Succesfully delete database %s.', $request->databaseFullName),
            ]);
    }


    public function deleteUser($request)
    {
        DB::statement("DROP USER " . $request->usernameFull);

        $this->flush();

        return redirect()->back()->with([
            'successMessage' => sprintf('Succesfully delete user %s.', $request->usernameFull),
            ]);
    }


    public function deleteUserRaw($dbUser) 
    {

        DB::statement("DROP USER " . $dbUser);

    }


    public function deleteDBRaw($db) 
    {

        DB::statement("DROP DATABASE `$db`");

    }
}