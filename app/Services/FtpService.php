<?php


namespace WHoP\Services;

use WHoP\FtpUser;
use WHoP\User;
use WHoP\Record;

use Input;
use Illuminate\Pagination\LengthAwarePaginator;


class FtpService
{
    public function getUserFtpUserList(User $user, $paginate = null)
    {
        $accounts =  FtpUser::where('user_id', '=', $user->id)->get();

        $accountsArray = [];

        foreach ($accounts as $account) {

            if (auth()->user()->hasRole('admin')) {

                $account->enableDisableURL = route('admin::app::ftp-enabledisable', [$user, $account]);
                $account->editURL = route('admin::app::ftp-edit', [$user, $account]);
                $account->deleteURL = route('admin::app::ftp-destroy', [$user, $account]);

                $accountsArray[] = $account;

            } else if (auth()->user()->hasRole('owner')) {

                $account->enableDisableURL = route('owner::app::ftp-enabledisable', [$account]);
                $account->editURL = route('owner::app::ftp-edit', [$account]);
                $account->deleteURL = route('owner::app::ftp-destroy', [$account]);

                $accountsArray[] = $account;
            }
        }

        if ($paginate !== null) {
            $currentPage = Input::get('page', 1);
            $itemPerPage = $paginate;
            $offset = ($currentPage * $itemPerPage) - $itemPerPage;

            return new LengthAwarePaginator(array_slice($accountsArray, $offset, $itemPerPage, true), count($accountsArray), $itemPerPage, $currentPage, ['path' => Input::url(), 'query' => Input::query()]);
        }

        return collect($accountsArray);
    }

    public function getUserFtpQuotaSum(User $user)
    {
        return FtpUser::where('user_id', '=', $user->id)->sum('quotaSize');
    }


    public function getFtpDomains(User $user)
    {
    	$ftpDomains = Record::whereHas('domain', function($q) use ($user)
    	{
    		$q->where('user_id', '=', $user->id);
    	})
    	->where('type', '=', 'A')
    	->lists('name', 'id');

    	return $ftpDomains;
    }


    public function create(User $user, $request)
    {
        $linuxUser = posix_getpwnam($user->username);

        $ftpUser = new FtpUser;
        $ftpUser->user_id = $user->id;
        $ftpUser->username = $request->ftpUsername;
        $ftpUser->record_id = $request->domain;
        $ftpUser->password = md5($request->password);
        $ftpUser->uid = $linuxUser['uid'];
        $ftpUser->gid = $linuxUser['gid'];
        $ftpUser->homedir = '/var/www/' . $user->username . $request->homeDir;
        $ftpUser->ULBW = 0;
        $ftpUser->DLBW = 0;
        $ftpUser->quotaSize = $request->quota;
        $ftpUser->quotaFile = 0;
        $ftpUser->disable = 0;
        $ftpUser->masterDisable = 0;
        $ftpUser->save();

        return $ftpUser;
    }


    public function update(FtpUser $ftpUser, $request)
    {
        if ($request->password !== null) {
            $ftpUser->password = md5($request->password);
        }
        $ftpUser->quotaSize = $request->quota;
        $ftpUser->save();

        return $ftpUser;
    }


    public function enableDisable(FtpUser $ftpUser)
    {
        if ($ftpUser->disable === 0) {
            $ftpUser->disable = 1;
        } else {
            $ftpUser->disable = 0;
        }

        $ftpUser->save();
    }
}