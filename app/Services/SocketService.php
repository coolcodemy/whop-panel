<?php


namespace WHoP\Services;

use ElephantIO\Client;
use ElephantIO\Engine\SocketIO\Version1X;

class SocketService
{
    public function generateChannel()
    {
        $channel = auth()->user()->username . '-' . strtotime("now") . md5(rand());
        return $channel;
    }


    public function emit($channel, $data)
    {
        $elephant = new Client(new Version1X('http://127.0.0.1:30001'));
        $elephant->initialize();
        $elephant->emit($channel, $data);
        $elephant->close();
    }
}