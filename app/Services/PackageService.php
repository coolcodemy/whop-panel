<?php


namespace WHoP\Services;

use WHoP\Package;
use WHoP\UserPackage;
use WHoP\User;

use WHoP\Events\OverwriteOwnerPackage;

class PackageService
{
    public function createOrUpdate($request)
    {
        $package = Package::firstOrNew([
            'name' => $request->name,
            ]);
        $package->fill($request->all());
        $package->validFor = $request->input('validityNumber') . ' ' . $request->input('validityTime');
        $package->save();

        return $package;
    }

    public function addPackageToUser(Package $package, User $user)
    {
        $userPackage = new UserPackage;
        $userPackage->user_id = $user->id;
        $userPackage->package_id = $package->id;
        $userPackage->activeDate = date('Y-m-d');
        $userPackage->endDate = date('Y-m-d', strtotime('+' . $package->validFor));
        $userPackage->apcuSupport = $package->apcuSupport;
        $userPackage->enableSsl = $package->enableSsl;
        $userPackage->ssh = $package->ssh;
        $userPackage->diskQuota = $package->diskQuota;
        $userPackage->emailQuota = $package->emailQuota;
        $userPackage->ftpQuota = $package->ftpQuota;
        $userPackage->websiteQuota = $package->websiteQuota;
        $userPackage->numberOfDatabase = $package->numberOfDatabase;
        $userPackage->numberOfDatabaseUser = $package->numberOfDatabaseUser;
        $userPackage->maximumQueriesPerHour = $package->maximumQueriesPerHour;
        $userPackage->maximumConnectionsPerHour = $package->maximumConnectionsPerHour;
        $userPackage->maximumUpdatesPerHour = $package->maximumUpdatesPerHour;
        $userPackage->maximumUserConnections = $package->maximumUserConnections;
        $userPackage->numberOfEmailAccount = $package->numberOfEmailAccount;
        $userPackage->numberOfFtpAccount = $package->numberOfFtpAccount;
        $userPackage->numberOfDomain = $package->numberOfDomain;
        $userPackage->numberOfWhoplet = $package->numberOfWhoplet;
        $userPackage->save();
    }

    public function paginate($item = 15)
    {
        return Package::paginate($item);
    }

    public function all()
    {
        return Package::all();
    }

    public function usageChart()
    {
        $packages = $this->all();

        $datamain = [];

        foreach ($packages as $package) {
            $data = [];
            $data['value'] = count($package->userpackage);
            $data['label'] = $package->name;
            $data['color'] = "#" . app('StaticService')->rgbGenerator();
            $datamain[] = $data;
        }

        return response()->json($datamain);
    }


    public function getUserPackage(User $user)
    {
        return $user->userpackage;
    }


    public function updateUserPackage(User $user, $request)
    {
        $up = UserPackage::whereUserId($user->id)->first();


        $up->apcuSupport = $request->apcuSupport;
        $up->enableSsl = $request->enableSsl;
        $up->ssh = $request->ssh;
        $up->diskQuota = $request->diskQuota;
        $up->emailQuota = $request->emailQuota;
        $up->ftpQuota = $request->ftpQuota;
        $up->websiteQuota = $request->websiteQuota;
        $up->numberOfDatabase = $request->numberOfDatabase;
        $up->numberOfDatabaseUser = $request->numberOfDatabaseUser;
        $up->maximumQueriesPerHour = $request->maximumQueriesPerHour;
        $up->maximumConnectionsPerHour = $request->maximumConnectionsPerHour;
        $up->maximumUpdatesPerHour = $request->maximumUpdatesPerHour;
        $up->maximumUserConnections = $request->maximumUserConnections;
        $up->numberOfEmailAccount = $request->numberOfEmailAccount;
        $up->numberOfFtpAccount = $request->numberOfFtpAccount;
        $up->numberOfDomain = $request->numberOfDomain;
        $up->numberOfWhoplet = $request->numberOfWhoplet;

        $up->save();

        // Event
        event(new OverwriteOwnerPackage($user, $up));

        return $up;
    }
}