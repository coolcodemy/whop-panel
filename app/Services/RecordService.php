<?php


namespace WHoP\Services;

use WHoP\Record;
use WHoP\Domain;
use WHoP\User;

class RecordService
{
    public function createRecord($data)
    {
        $record = Record::firstOrNew([
            'name' => $data['name'],
            'type' => $data['type'],
            'content' => $data['content'],
            ]);
        $record->domain_id = $data['domain_id'];
        $record->ttl = $data['ttl'];
        $record->prio = isset($data['prio']) ? $data['prio'] : null;
        $record->deleteEdit = isset($data['deleteEdit']) ? $data['deleteEdit'] : 1;
        $record->save();

        return $record;
    }



    public function createRecordByRequest($request)
    {
        if ($request->type == "SPF" || $request->type == "TXT") {
            if($request->name == '@') {
                $request->name = $request->recordName;
            }

            $data = [
                'domain_id' => $request->domain,
                'name' => $request->name,
                'type' => $request->type,
                'content' => $request->content,
                'ttl' => $request->ttl,
            ];

        } else if ($request->type == 'MX') {
            $data = [
                'domain_id' => $request->domain,
                'name' => $request->recordName,
                'type' => $request->type,
                'content' => $request->content,
                'ttl' => $request->ttl,
                'prio' => $request->priority,
            ];

        } else {
            $data = [
                'domain_id' => $request->domain,
                'name' => $request->recordName,
                'type' => $request->type,
                'content' => $request->content,
                'ttl' => $request->ttl,
            ];
        }

        return $this->createRecord($data);
    }


    public function updateRecord(Record $record, $data)
    {
        $record->type = $data['type'];
        $record->content = $data['content'];
        $record->ttl = $data['ttl'];
        $record->prio = isset($data['prio']) ? $data['prio'] : null;
        $record->save();

        return $record;
    }


    public function updateRecordByRequest(Record $record, $request)
    {
        if ($request->type == "SPF" || $request->type == "TXT") {
            if($request->name == '@') {
                $request->name = $request->recordName;
            }


            $data = [
                'type' => $request->type,
                'content' => $request->content,
                'ttl' => $request->ttl,
            ];

        } else if ($request->type == 'MX') {
            $data = [
                'type' => $request->type,
                'content' => $request->content,
                'ttl' => $request->ttl,
                'prio' => $request->priority,
            ];

        } else {
            $data = [
                'type' => $request->type,
                'content' => $request->content,
                'ttl' => $request->ttl,
            ];
        }


        return $this->updateRecord($record, $data);
    }


    public function search($modifier)
    {
        $records = Record;

        if ($modifier !== null) {
            foreach ($modifier as $key => $value) {
                $records->where($key, $value[0], $value[1]);
            }
        }

        return $records->get();
    }


    public function getARecordsByUserPointToThisServer(User $user)
    {
        return Record::whereHas('domain', function($q) use ($user)
        {
            $q->where('user_id', '=', $user->id);
        })
        ->where('type', '=', 'A')
        ->orWhere('type', '=', 'AAAA')
        ->get();
    }


    public function populateNewDomain(User $user, $request)
    {
        $this->createRecord([
            'domain_id' => $request->domain_id,
            'name' => $request->domainName,
            'type' => 'SOA',
            'content' => Record::where('type', '=', 'NS')->first()->content . ' whop.'. $request->server('SERVER_NAME'). ' ' . date('Ymd') .'01 86400 7200 604800 300',
            'ttl' => 86400,
            'deleteEdit' => 0,
            ]);

        $mailDomain = $this->createRecord([
            'domain_id' => $request->domain_id,
            'name' => $request->domainName,
            'type' => 'A',
            'content' => $request->server('SERVER_ADDR'),
            'ttl' => 86400,
            ]);

        $this->createRecord([
            'domain_id' => $request->domain_id,
            'name' => $request->domainName,
            'type' => 'MX',
            'content' => $request->domainName,
            'ttl' => 600,
            'prio' => 0,
            ]);

        $this->createRecord([
            'domain_id' => $request->domain_id,
            'name' => $request->domainName,
            'type' => 'TXT',
            'content' => 'v=spf1 mx a -all',
            'ttl' => 86400,
            ]);
    }


    public function getRecordsByDomainID($id,array $modifier = null)
    {
        $records =  Record::where('domain_id', '=', $id);

        if ($modifier !== null) {
            foreach ($modifier as $key => $value) {
                $records->where($key, $value[0], $value[1]);
            }
        }

        return $records->get();
    }


    public function delete(User $user, Record $record)
    {
        if ($record->deleteEdit == 0) {
            return redirect()->back()->with([
                'errorMessage' => 'Cannot delete this record.',
                ]);
        }

        $record->delete();

        app('WhopletService')->deleteWhopletFromRecord($user, $record);
        app('MailService')->deleteEmailsFromRecord($user, $record);

        return redirect()->back()->with([
            'successMessage' => 'Succesfully delete record.',
            ]);
    }
}