<?php


namespace WHoP\Services;

use WHoP\MailUser;
use WHoP\User;
use WHoP\Record;
use WHoP\Domain;
use WHoP\MailForwarding;

use Illuminate\Pagination\LengthAwarePaginator;
use Input;

class MailService
{
    public function getUserMailAccountList(User $user, $paginate = null)
    {
        $emails = MailUser::where('user_id', '=', $user->id)->get();

        $emailArray = [];

        if (auth()->user()->hasRole('admin')) {

	        foreach ($emails as $email) {

                $email->enableDisableURL = route('admin::app::email-enabledisable', [$user, $email]);
	        	$email->deleteURL = route('admin::app::email-delete', [$user, $email]);
                $email->editURL = route('admin::app::email-edit', [$user, $email]);
	        	$emailArray[] = $email;

	        }

        } else if (auth()->user()->hasRole('owner')) {

            foreach ($emails as $email) {

                $email->enableDisableURL = route('owner::app::email-enabledisable', [$email]);
                $email->deleteURL = route('owner::app::email-delete', [$email]);
                $email->editURL = route('owner::app::email-edit', [$email]);
                $emailArray[] = $email;

            }

        }

        if ($paginate !== null) {
            $currentPage = Input::get('page', 1);
            $itemPerPage = $paginate;
            $offset = ($currentPage * $itemPerPage) - $itemPerPage;

            return new LengthAwarePaginator(array_slice($emailArray, $offset, $itemPerPage, true), count($emailArray), $itemPerPage, $currentPage, ['path' => Input::url(), 'query' => Input::query()]);
        }
        return collect($emails);
    }

    public function getUserMailQuota(User $user)
    {
        return MailUser::where('user_id', '=', $user->id)->sum('quota');
    }


    public function getDomain(User $user)
    {
    	return Record::where('type', '=', 'A')
            ->whereHas('domain', function($q) use ($user)
            {
                $q->where('user_id', '=', $user->id);
            })
            ->get();
    }


    public function newEmail(User $user, $request)
    {
        $mail = new MailUser;
        $mail->user_id = $user->id;
        $mail->record_id = $request->mailDomain;
        $mail->email = $request->email;
        $mail->password = crypt($request->password, '$6$' . app('StaticService')->randomString(13));
        $mail->quota = $request->quota;
        $mail->disable = 0;
        $mail->save();

        return $mail;
    }


    public function update(MailUser $email, $request)
    {
        if ($request->password != null) {
            $email->password = crypt($request->password, '$6$' . app('StaticService')->randomString(13));
        }

        if ($request->quota != null) {
            $email->quota = $request->quota;
        }

        $email->save();

        return $email;
    }


    public function deleteEmailsFromRecord(User $user, Record $record)
    {
        $emails =  MailUser::where('record_id', '=', $record->id)->get();

        $emailArray = [];

        foreach ($emails as $email) {
            $emailArray[] = $email->email;
        }

        if ( count($emailArray) !== 0 ) {
            $this->deleteSystemEmailArray($user, $emailArray);
        }
    }


    public function deleteEmailsFromDomain(User $user, Domain $domain)
    {
        $emails =  MailUser::whereHas('record', function($q) use ($domain)
        {
            $q->whereHas('domain', function($q) use ($domain)
            {
                $q->where('id', '=', $domain->id);
            });
        })->get();

        $emailArray = [];

        foreach ($emails as $email) {
            $emailArray[] = $email->email;
        }

        if ( count($emailArray) !== 0 ) {
            $this->deleteSystemEmailArray($user, $emailArray);
        }
    }


    public function deleteSystemEmail(User $user, MailUser $email)
    {
        $socketData =  [
                'userID' => $user->id,
                'email' => $email->email,
                'MyUsername' => $user->username,
                'MyKey' => $user->secretKey,
                'NODE_KEY' => env('NODE_KEY'),
            ];


        app('SocketService')->emit('deleteuseremail-server', $socketData);
    }


    public function deleteSystemEmailArray(User $user, $emails)
    {
        $socketData =  [
                'userID' => $user->id,
                'emails' => $emails,
                'MyUsername' => $user->username,
                'MyKey' => $user->secretKey,
                'NODE_KEY' => env('NODE_KEY'),
            ];


        app('SocketService')->emit('deleteuseremailarray-server', $socketData);
    }


    public function enableDisable(MailUser $email)
    {
        $email->disable === 0 ? $email->disable = 1 : $email->disable = 0;
        $email->save();
    }


    public function getForwardings(User $user, $paginate = null)
    {
        $forwardings = MailForwarding::where('user_id', '=', $user->id)->get();

        $forwardingArray = [];

        if (auth()->user()->hasRole('admin')) {

            foreach ($forwardings as $forwarding) {

                $forwarding->enableDisableURL = route('admin::app::email-forwarding-enabledisable', [$user, $forwarding]);
                $forwarding->deleteURL = route('admin::app::email-forwarding-delete', [$user, $forwarding]);
                $forwardingArray[] = $forwarding;

            }

        } else if (auth()->user()->hasRole('owner')) {

            foreach ($forwardings as $forwarding) {

                $forwarding->enableDisableURL = route('owner::app::email-forwarding-enabledisable', [$forwarding]);
                $forwarding->deleteURL = route('owner::app::email-forwarding-delete', [$forwarding]);
                $forwardingArray[] = $forwarding;

            }

        }

        if ($paginate !== null) {
            $currentPage = Input::get('page', 1);
            $itemPerPage = $paginate;
            $offset = ($currentPage * $itemPerPage) - $itemPerPage;

            return new LengthAwarePaginator(array_slice($forwardingArray, $offset, $itemPerPage, true), count($forwardingArray), $itemPerPage, $currentPage, ['path' => Input::url(), 'query' => Input::query()]);
        }

        return collect($forwardingArray);
    }


    public function createForwarding(User $user, $request)
    {
        $mf = new MailForwarding;
        $mf->user_id = $user->id;
        $mf->record_id = $request->emailDomain;
        $mf->source = $request->emailFrom;
        $mf->destination = $request->destinationTo;

        $mf->save();

        return $mf;
    }


    public function enableDisableForwarding(MailForwarding $forwarding)
    {
        $forwarding->disable === 0 ? $forwarding->disable = 1 : $forwarding->disable = 0;
        $forwarding->save();
    }
}