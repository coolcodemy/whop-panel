<?php


namespace WHoP\Services;

use WHoP\User,
    WHoP\Role,
    WHoP\Record,
    WHoP\Package;

use PRedis;

use WHoP\Events\OwnerAccountStatusChanged;
use WHoP\Events\OwnerAccountWasDeleted;

class UserService
{
    public function createOwner($request)
    {
        $user = $this->createOrUpdateUser($request);
        $package = Package::find($request->package);


        $role = Role::where('name', '=', 'owner')->first();
        $user->attachRole($role);


        app('PackageService')->addPackageToUser($package, $user);

        $domain = app('DomainService')->createOrUpdateDomain($user, $request->domainName);

        $request->merge([
            'domain_id' => $domain->id,
            ]);

        app('RecordService')->populateNewDomain($user, $request);

        $socketData =  [
                'username' => $request->username, 
                'password' => $request->password,
                'websiteQuota' => $package->websiteQuota,
                'diskQuota' => $package->diskQuota,
                'apcuSupport' => $package->apcuSupport,
                'ssh' => $package->ssh,
                'NODE_KEY' => env('NODE_KEY'),
                'MyUsername' => auth()->user()->username,
                'MyKey' => auth()->user()->secretKey,
                'channel' => $request->channel,
            ];


        app('SocketService')->emit('adduser-server', $socketData);
    }

    public function createOrUpdateUser($request)
    {
        $user = User::firstOrNew([
            'username' => $request->username,
            ]);

        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->name = $request->name;
        $user->save();

        return $user;
    }

    public function login($username, $password)
    {
        if (auth()->attempt(['username' => $username, 'password' => $password, 'disable' => 0])) {

            $secretKey = MD5(str_random(40));

            $user = auth()->user();
            $user->secretKey = $secretKey;
            $user->save();

            $json['role'] = $user->roles()->first()->name;
            $json['key'] = $secretKey;
            $json = json_encode($json);


            PRedis::set('whop:user:' . $user->username, $json);
            PRedis::expire('whop:user:' . $user->username, config('session.lifetime')*60);

            return true;
        }

        return false;
    }


    public function getUsersByRole($role, $paginate = 15)
    {
        return User::whereHas('roles', function($q) use ($role)
        {
            $q->where('name', '=', $role);
        })
        ->orderBy('created_at', 'desc')
        ->paginate(app('SettingService')->getInt('OwnerPerPage'));
    }


    public function disableAccount(User $user)
    {
        $user->disable = 1;
        $user->save();


        event(new OwnerAccountStatusChanged($user));

    }


    public function enableAccount(User $user)
    {
        $user->disable = 0;
        $user->save();


        event(new OwnerAccountStatusChanged($user));

    }


    public function resetAccount(User $user)
    {
        $user->disable = 0;
        $user->save();


        $userPackage = $user->userpackage;

        $package = $user->userpackage->package;


        $now = date('Y-m-d');


        if ( $userPackage->endDate > $now ) {

            $userPackage->endDate = date('Y-m-d', strtotime($userPackage->endDate . '+' . $package->validFor));
            $userPackage->save();

        } else {

            $userPackage->endDate = date('Y-m-d', strtotime('+' . $package->validFor));
            $userPackage->save();

        }


        event(new OwnerAccountStatusChanged($user));
    }



    public function delete(User $user)
    {
        event(new OwnerAccountWasDeleted($user));
        
        $user->delete();
    }
}