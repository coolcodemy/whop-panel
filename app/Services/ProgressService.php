<?php


namespace WHoP\Services;

use WHoP\Services\DomainService;
use WHoP\Services\FtpService;
use WHoP\Services\DatabaseService;
use WHoP\Services\MailService;

use WHoP\User;
use WHoP\Domain;
use WHoP\FtpUser;
use WHoP\MailUser;


class ProgressService
{
    private $DomainService,
        $FtpService,
        $DatabaseService,
        $MailService;

    public function __construct(DomainService $DomainService, FtpService $FtpService, DatabaseService $DatabaseService, MailService $MailService)
    {
        $this->DomainService = $DomainService;
        $this->FtpService = $FtpService;
        $this->DatabaseService = $DatabaseService;
        $this->MailService = $MailService;
    }

    public function all(User $user)
    {
        $data = [
            'domains' => $this->domain($user),
            'ftpUsers' => $this->ftpUsers($user),
            'ftpQuota' => $this->ftpQuota($user),
            'db' => $this->db($user),
            'dbUsers' => $this->dbUsers($user),
            'mailUsers' => $this->mailUsers($user),
            'mailQuota' => $this->mailQuota($user),
        ];

        return $data;
    }

    public function domain(User $user)
    {
        $domains = $this->DomainService->getUserDomainList($user);

        return [
            'color' => app('StaticService')->progressColor($domains->count(), $user->userpackage->numberOfDomain),
            'value' => $domains->count(),
            'max' => $user->userpackage->numberOfDomain == 0 ? 'Unlimited' : $user->userpackage->numberOfDomain,
            'percentage' => ($user->userpackage->numberOfDomain == 0) ? 100 : $domains->count()/$user->userpackage->numberOfDomain*100,
        ];
    }

    public function ftpUsers(User $user)
    {
        $ftpUsers = $this->FtpService->getUserFtpUserList($user);

        return [
            'color' => app('StaticService')->progressColor($ftpUsers->count(), $user->userpackage->numberOfFtpAccount),
            'value' => $ftpUsers->count(),
            'max' => $user->userpackage->numberOfFtpAccount == 0 ? 'Unlimited' : $user->userpackage->numberOfFtpAccount,
            'percentage' => ($user->userpackage->numberOfFtpAccount == 0) ? 100 : $ftpUsers->count()/$user->userpackage->numberOfFtpAccount*100,
        ];
    }


    public function ftpQuota(User $user)
    {
        $ftpQuota = $this->FtpService->getUserFtpQuotaSum($user);

        return [
            'color' => app('StaticService')->progressColor($ftpQuota, $user->userpackage->numberOfFtpAccount),
            'value' => $ftpQuota,
            'max' => $user->userpackage->ftpQuota == 0 ? 'Unlimited' : $user->userpackage->ftpQuota . ' MB',
            'percentage' => ($user->userpackage->ftpQuota == 0) ? 100 : $ftpQuota/$user->userpackage->ftpQuota*100,
        ];
    }

    public function db(User $user)
    {
        $db = $this->DatabaseService->getUserDatabaseList($user);

        return [
            'color' => app('StaticService')->progressColor(count($db), $user->userpackage->numberOfDatabase),
            'value' => count($db),
            'max' => $user->userpackage->numberOfDatabase == 0 ? 'Unlimited' : $user->userpackage->numberOfDatabase,
            'percentage' => ($user->userpackage->numberOfDatabase == 0) ? 100 : count($db)/$user->userpackage->numberOfDatabase*100,
        ];
    }


    public function dbUsers(User $user)
    {
        $dbUsers = $this->DatabaseService->getUserDatabaseAccountList($user);

        return [
            'color' => app('StaticService')->progressColor(count($dbUsers), $user->userpackage->numberOfDatabaseUser),
            'value' => count($dbUsers),
            'max' => $user->userpackage->numberOfDatabaseUser == 0 ? 'Unlimited' : $user->userpackage->numberOfDatabaseUser,
            'percentage' => ($user->userpackage->numberOfDatabaseUser == 0) ? 100 : count($dbUsers)/$user->userpackage->numberOfDatabaseUser*100,
        ];
    }


    public function mailUsers(User $user)
    {
        $mailUsers = $this->MailService->getUserMailAccountList($user);

        return [
            'color' => app('StaticService')->progressColor($mailUsers->count(), $user->userpackage->numberOfEmailAccount),
            'value' => $mailUsers->count(),
            'max' => $user->userpackage->numberOfEmailAccount == 0 ? 'Unlimited' : $user->userpackage->numberOfEmailAccount,
            'percentage' => ($user->userpackage->numberOfEmailAccount == 0) ? 100 : $mailUsers->count()/$user->userpackage->numberOfEmailAccount*100,
        ];
    }


    public function mailQuota(User $user)
    {
        $mailQuota = $this->MailService->getUserMailQuota($user);

        return [
            'color' => app('StaticService')->progressColor($mailQuota, $user->userpackage->emailQuota),
            'value' => $mailQuota,
            'max' => $user->userpackage->emailQuota == 0 ? 'Unlimited' : $user->userpackage->emailQuota . ' MB',
            'percentage' => ($user->userpackage->emailQuota == 0) ? 100 : $mailQuota/$user->userpackage->emailQuota*100,
        ];
    }


    public function allDomainCount()
    {
        return Domain::all()->count();
    }


    public function allDatabaseCount()
    {
        return count($this->DatabaseService->allDatabase());
    }


    public function allFtpCount()
    {
        return FtpUser::all()->count();
    }


    public function allEmailCount()
    {
        return MailUser::all()->count();
    }


    public function allDomainOwner()
    {
        return User::whereHas('roles', function($q) {

            $q->where('name', '=', 'owner');

        })->get()->count();
    }


}