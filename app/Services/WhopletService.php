<?php


namespace WHoP\Services;

use WHoP\Whoplet;

use WHoP\User;
use WHoP\Record;
use WHoP\Domain;


class WhopletService
{
    private $config;
    private $unique = array();
    private $uniqueScratch = array();
    private $uniqueContent = array();
    private $uniqueContentRules = array('root', 'autoindex', 'error_page', 'etag', 'expires', 'gunzip', 'gzip', 'if_modifier_since', 'limit_rate', 'limit_rate_after', 'try_files');
    private $modifierRules = array('', '=', '~', '~*', '^~');
    private $locationRules = '/^((\\\.\(([0-9A-Za-z]*(\|)?([0-9A-Za-z]+)){1,20}\)\$)|(\/){1}(([0-9A-Za-z*]+(\/{1})?){0,20})|(\/){1}(([0-9A-Za-z*]+(\/{1})?){0,20})(\(([0-9A-Za-z]*(\|)?([0-9A-Za-z]+)){1,20}\)\$))$/';


    //Key Value Test Rules
    private $try_files = '/^[\w\d \$=.\/\?]*$/';
    private $root = '/^(\/){1}(([0-9A-Za-z*]+(\/{1})?){0,20})\/$/';
    private $allowDeny = '/^(((2(5[0-5]|[0-4][0-9])|[01]?[0-9][0-9]?)\.){3}(2(5[0-5]|[0-4][0-9])|[01]?[0-9][0-9]?)(\/(3[012]|[12]?[0-9])))$/';
    private $onOff = array('on', 'off');
    private $errorPage = '/^(404|500|502|503|504) ((\/){1}(([0-9A-Za-z*]+(\/{1})?){0,20})((\.[a-z]{1,10}))|(\/){1}(([0-9A-Za-z*]+(\/{1})?){0,20}))$/';
    private $ifModified = array('off', 'exact', 'before');
    private $expires = array('epoch', 'max', 'off');
    private $cacheControl = array('public', 'private', 'no-cache', 'no-store');


    public function myTemplates(User $user)
    {
        $templates = Whoplet::where('user_id', '=', $user->id)
                    ->where('name', 'NOT LIKE', 'skeleton_%')
                    ->orderBy('name', 'asc')
                    ->paginate(10);

        return $templates;
    }


    public function whopletGeneratorTemplatesList($user)
    {
        $templates = Whoplet::where('user_id', '=', $user->id)
                    ->orWhere('user_id', '=', 1)
                    ->where('name', 'NOT LIKE', 'skeleton_%')
                    ->where('status', '=', 1)
                    ->orderBy('name', 'asc')
                    ->get();

        return $templates;
    }


    public function getTemplateToBuild($user)
    {
        $templates = Whoplet::where('user_id', '=', $user->id)
                    ->orWhere('user_id', '=', 1)
                    ->where('status', '=', 1)
                    ->orWhere(function($q)
                    {
                        $q->where('name', 'LIKE', 'skeleton_%');
                    })
                    ->orderBy('name', 'asc')
                    ->paginate(10);

        return $templates;
    }


    public function skeleton($request)
    {
        if ($request->type == 'http') {

            $skeleton = 'skeleton_80';
        } elseif ($request->type == 'https') {

            $skeleton = 'skeleton_443';
        } else {

            return response()->json('Unknown template file', 404);
        }

        return Whoplet::where('name', '=', $skeleton)
            ->where('user_id', '=', auth()->user()->id)
            ->first()->content;
    }


    public function createTemplate($request, User $user)
    {
        if ($user->hasRole('admin')) {
            $request->merge([
                'templateName' => 'WHoPlet_' . $request->templateName,
            ]);
        } else {
            $request->merge([
                'templateName' => $user->username . '_' . $request->templateName,
            ]);
        }

        $template = new Whoplet;
        $template->user_id = $user->id;
        $template->name = $request->templateName;
        $template->content = $request->templateContent;
        $template->status = 1;

        $template->save();

        return $template;
    }

    public function updateTemplate($request, User $user)
    {
        if ($user->hasRole('admin')) {
            $request->merge([
                'templateName' => 'WHoPlet_' . $request->templateName,
            ]);
        } else {
            $request->merge([
                'templateName' => $user->username . '_' . $request->templateName,
            ]);
        }

        $template = Whoplet::where('user_id', '=', $user->id)
            ->where('id', '=', $request->templateId)
            ->first();

        $template->user_id = $user->id;
        $template->name = $request->templateName;
        $template->content = $request->templateContent;
        $template->status = 1;

        $template->save();

        return $template;
    }

    public function update($request)
    {
        $template = $this->find($request->templateId);
        $template->name = $request->templateName;
        $template->content = $request->templateContent;
        $template->save();
    }


    public function enableDisable(Whoplet $template, $status)
    {
        $template->status = $status;
        $template->save();
    }


    public function formGenerator($templateID, User $user)
    {
        $template = Whoplet::find($templateID);

        if (strpos($template->content,'{DOMAINNAME}') !== false) {
            $records = app('RecordService')->getARecordsByUserPointToThisServer($user);
        } else {
            $records = false;
        }

        if (strpos($template->content,'{ROOT}') !== false) {
            $root = true;
        } else {
            $root = false;
        }

        if (strpos($template->content,'{SSL}') !== false) {
            $cert = true;
        } else {
            $cert = false;
        }

        return response()->json([
            'records' => $records,
            'root' => $root,
            'ssl' => $cert,
            ]);
    }


    public function buildWhoplet($request, User $user)
    {
        $socketData =  [
                'userID' => $user->id, 
                'username' => $user->username,
                'templateID' => $request->templateID,
                'domainName' => $request->domainName,
                'homeDir' => $request->homeDir,
                'ssl' => $request->ssl,
                'NODE_KEY' => env('NODE_KEY'),
                'MyUsername' => auth()->user()->username,
                'MyKey' => auth()->user()->secretKey,
                'channel' => $request->channel,
            ];


        app('SocketService')->emit('buildWhoplet-server', $socketData);
    }




    public function checkPortion($request, $preview = false)
    {
        $data = json_decode($request->data);

        if (!isset($data->modifier) || !isset($data->location) || !isset($data->content)) {

            if ($preview == true) {
                return false;
            } else {
                return response()->json([
                    'message' => 'Your location file do not have modifier or location or content!',
                ], 422);
            }
        } else if (!in_array($data->modifier, $this->modifierRules)) {
            if ($preview == true) {
                return false;
            } else {
                return response()->json(
                    array(
                        'message' => 'The modifier type that you have provided is unknown.',
                        )
                    , 422);
            }
        } else if (!preg_match($this->locationRules, $data->location)) {

            if ($preview == true) {
                return false;
            } else {
                return response()->json(
                    array(
                        'message' => 'The location that you have provided is unknown.',
                        )
                    , 422);
            }
        } else if ($this->configTest($data->content) == false) {

            if ($preview == true) {
                return false;
            } else {
                return response()->json(
                    array(
                        'message' => 'Your configuration have error/s.',
                        )
                    , 422);
            }
        } else if (in_array("location " . $data->modifier . " " . $data->location, $this->unique)) {

            if ($preview == true) {
                return false;
            } else {
                return response()->json(
                    array(
                        'message' => 'Please use different location for each location block.',
                        )
                    , 422);
            }
        } else {

            array_push($this->unique, "location " . $data->modifier . " " . $data->location);

            if ($preview == true) {
                return true;
            } else {
                return response()->json(
                    array(
                        'message' => 'This location block have no error. You can save or create new location block.',
                        )
                    , 200);
            }       
        }
    }



    public function configTest($content)
    {
        foreach ($content as $key => $value) {
            if (!isset($value->key) || !isset($value->content)) {
                return false;

            } else if ($this->keyValueTest($value->key, $value->content) === false) {
                return false;

            } else if ($this->testUniqueContent($value->key) === false) {
                return false;

            }
        }

        return true;
    }


    public function testUniqueContent($key)
    {
        if (in_array($key, $this->uniqueContent)) {
            return false;
        } 


        if (in_array($key, $this->uniqueContentRules)) {
            array_push($this->uniqueContent, $key);
        }

        return true;
    }


    public function keyValueTest($key, $value)
    {
        if ($key == "root" && preg_match($this->root, $value)) {

            return true;
        } else if ($key == "allow" && (filter_var($value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) != false || $value == "all" || preg_match($this->allowDeny, $value))) {

            return true;
        } else if ($key == "autoindex" && in_array($value, $this->onOff)) {

            return true;
        }else if ($key == "cache-control" && in_array($value, $this->cacheControl)) {

            return true;
        } else if ($key == "deny" && (filter_var($value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) != false || $value == "all" || preg_match($this->allowDeny, $value))) {

            return true;
        } else if ($key == "error_page" && preg_match($this->errorPage, $value)) {

            return true;
        } else if ($key == "etag" && in_array($value, $this->onOff)) {

            return true;
        } else if ($key == "expires" && ($this->validateTime($value) || in_array($value, $this->expires))) {

            return true;
        } else if ($key == "gunzip" && in_array($value, $this->onOff)) {

            return true;
        } else if ($key == "gzip" && in_array($value, $this->onOff)) {

            return true;
        } else if ($key == "if_modified_since" && in_array($value, $this->ifModified)) {

            return true;
        } else if ($key == "limit_rate" && $this->validateSize($value)) {

            return true;
        } else if ($key == "limit_rate_after" && $this->validateSize($value)) {

            return true;
        } else if ($key == "try_files" && preg_match($this->try_files, $value)) {

            return true;
        } else {

            return false;
        }
    }


    public function validateTime($time)
    {
        $suffix = array('ms', 's', 'm', 'h', 'd', 'w', 'M', 'y');
        $timeInt = intval($time);

        $suf = str_replace($timeInt, '', $time);

        if ($timeInt > 0 && $timeInt <= 1000 && in_array($suf, $suffix) || $timeInt === -1 || $timeInt === 0) {
            return true;
        } else {
            return false;
        }
    }


    public function validateSize($size)
    {
        $suffix = array('m', 'M', 'k', 'K');
        $sizeInt = intval($size);

        $suf = str_replace($sizeInt, '', $size);

        if ($sizeInt >= 1 && $sizeInt <= 1000 && in_array($suf, $suffix)) {
            return true;
        } else {
            return false;
        }
    }

    public function preview(User $user, $request)
    {

        $id = $request->templateID;

        $dataAll = $request->dataall;

        $flag = true;


        $skeleton = Whoplet::find($id);

        if ( $dataAll !== null ) {

            foreach ( $dataAll as $key => $value ) {

                $request->merge([

                    'data' => $value,

                ]);

                $res = $this->checkPortion( $request, true );

                $res2 = $this->uniqueDefaultLocation( $skeleton, $value );

                if ( $res == false ) {

                    $flag = false;

                } else if( $res2 == false ) {

                    $flag = false;

                } else {

                    $this->configBuilder( $user, $request );
                }
            }
        }


        if ( $this->checkCacheTime($request) === false ) {

            return response()->json([

                'message' => 'Unknown cache time provided.',

            ], 422);

        }

        if ( $flag == false ) {

            return response()->json([

                'message' => 'There is an error in your template! Please check it one by one to fix it.',

            ], 422);

        } else {

            $content = str_replace('/home/whopjail/{USERNAME}/var/log/nginx/', '/var/log/nginx/', $skeleton->content);

            $content = str_replace('/home/whopjail/' . $user->username . '/var/log/nginx/', '/var/log/nginx/', $content);

            $content = str_replace('{USERNAME}', $user->username, $content);

            $content = str_replace('#BEGIN', $this->config . "\t#BEGIN", $content);


            $content = $this->changeTemplateSetting($content, $request);

            return $content;
        }
    }


    public function uniqueDefaultLocation(Whoplet $d, $json)
    {
        $pattern = '/location.*{/';

        preg_match_all($pattern, $d->content, $matches);

        foreach ($matches[0] as $value) {
            $content = str_replace('{', '', $value);
            $content = preg_replace('/( )+/', ' ', $content);
            array_push($this->uniqueScratch, trim($content));
        }

        $json = json_decode($json);

        $json->modifier == "" ? $locationBlock = "location " . $json->location : $locationBlock = "location " . $json->modifier . " " . $json->location;

        if (in_array($locationBlock, $this->uniqueScratch)) {
            return false;
        } else {
            return true;
        }
    }


    public function configBuilder(User $user, $request)
    {
        $data = json_decode( $request->data );

        $this->config == null ? $this->config = "location " : $this->config .= "\tlocation ";
        
        if ( $data->modifier != "" ) {

            $this->config .= $data->modifier;

            $this->config .= " ";

        }


        $this->config .= $data->location;


        $this->config .= " {\n";

        foreach ($data->content as $value) {

            if ($value->key == "cache-control") {

                $this->config .= "\t\tadd_header Cache-Control " . $value->content . ";\n";

            } else if ($value->key == "root") {

                $this->config .= "\t\troot /var/www/" . $user->username . "/web/public" . $value->content . ";\n";

            } else {

                $this->config .= "\t\t" . $value->key . " " . $value->content . ";\n";

            }

        }

        $this->config .= "\t}\n\n";
    }


    public function changeTemplateSetting($content, $request)
    {
        if (filter_var($request->sameorigin, FILTER_VALIDATE_BOOLEAN) == true) {

            $content = preg_replace('/#*add_header X-Frame-Options "SAMEORIGIN"/', 'add_header X-Frame-Options "SAMEORIGIN"', $content);

        } else {

            $content = preg_replace('/#*add_header X-Frame-Options "SAMEORIGIN"/', '#add_header X-Frame-Options "SAMEORIGIN"', $content);

        }


        if (filter_var($request->xssprotection, FILTER_VALIDATE_BOOLEAN) == true) {

            $content = preg_replace('/#*add_header X-XSS-Protection "1; mode=block"/', 'add_header X-XSS-Protection "1; mode=block"', $content);

        } else {

            $content = preg_replace('/#*add_header X-XSS-Protection "1; mode=block"/', '#add_header X-XSS-Protection "1; mode=block"', $content);

        }



        if (filter_var($request->nosniff, FILTER_VALIDATE_BOOLEAN) == true) {

            $content = preg_replace('/#*add_header X-Content-Type-Options "nosniff"/', 'add_header X-Content-Type-Options "nosniff"', $content);

        } else {

            $content = preg_replace('/#*add_header X-Content-Type-Options "nosniff"/', '#add_header X-Content-Type-Options "nosniff"', $content);

        }




        if (filter_var($request->sts, FILTER_VALIDATE_BOOLEAN) == true) {

            $content = preg_replace('/#*add_header Strict-Transport-Security "max-age=31536000"/', 'add_header Strict-Transport-Security "max-age=31536000"', $content);

        } else {

            $content = preg_replace('/#*add_header Strict-Transport-Security "max-age=31536000"/', '#add_header Strict-Transport-Security "max-age=31536000"', $content);

        }




        if (filter_var($request->cache, FILTER_VALIDATE_BOOLEAN) == true) {

            $content = preg_replace('/#*fastcgi_cache WHOPWEBAPP;/', 'fastcgi_cache WHOPWEBAPP;', $content);

            $content = preg_replace('/#*fastcgi_cache_valid 302 200 {CACHETIME};/', 'fastcgi_cache_valid 302 200 {CACHETIME};', $content);

            $content = preg_replace('/#*fastcgi_cache_valid 302 200 [\d]{1,2}(s|m);/', 'fastcgi_cache_valid 302 200 {CACHETIME};', $content);

            $content = preg_replace('/#*fastcgi_cache_valid 404 1m;/', 'fastcgi_cache_valid 404 1m;', $content);

            $content = preg_replace('/#*fastcgi_no_cache \$http_pragma/', 'fastcgi_no_cache \$http_pragma', $content);


            $content = preg_replace('/{CACHETIME}/', $request->cacheTime, $content);

        } else {

            $content = preg_replace('/#*fastcgi_cache WHOPWEBAPP;/', '#fastcgi_cache WHOPWEBAPP;', $content);

            $content = preg_replace('/#*fastcgi_cache_valid 302 200 {CACHETIME};/', '#fastcgi_cache_valid 302 200 {CACHETIME};', $content);

            $content = preg_replace('/#*fastcgi_cache_valid 302 200 [\d]{1,2}(s|m);/', '#fastcgi_cache_valid 302 200 {CACHETIME};', $content);

            $content = preg_replace('/#*fastcgi_cache_valid 404 1m;/', '#fastcgi_cache_valid 404 1m;', $content);

            $content = preg_replace('/#*fastcgi_no_cache \$http_pragma/', '#fastcgi_no_cache \$http_pragma', $content);

        }


        return $content;
    }


    public function checkCacheTime($request)
    {
        if ($request->cache === false) {

            return true;

        }


        if ($request->cacheTime == null) {

            return false;

        }

        $sizeInt = intval($request->cacheTime);

        $suffix = str_replace($sizeInt, '', $request->cacheTime);

        if ($sizeInt >= 10 && $sizeInt <= 60 && $suffix === 's') {

            return true;

        } else if ($sizeInt >= 1 && $sizeInt <= 10 && $suffix === 'm') {

            return true;

        } else {

            return false;

        }
    }



    public function maxUserTemplate(User $user)
    {
        $max = app('SettingService')->getInt('MaxUserTemplate');

        $count = Whoplet::where('user_id', '=', $user->id)->count();

        if ($count >= $max) {

            return false;

        } else {

            return true;

        }
    }


    public function deleteWhopletFromRecord(User $user, Record $record)
    {
        $socketData =  [

                'whoplets' => [$record->name, $record->name . '|https'],

                'MyUsername' => $user->username,

                'MyKey' => $user->secretKey,

                'NODE_KEY' => env('NODE_KEY'),

            ];


        app('SocketService')->emit('deleteWhopletMultiple-server', $socketData);
    }


    public function deleteWhopletFromDomain(User $user, Domain $domain)
    {
        $whoplets = [];

        $recordsName = Record::where('domain_id', '=', $domain->id)
                        ->where('type', '=', 'A')
                        ->lists('name');


        foreach($recordsName as $name) {

            $whoplets[] = $name;

            $whoplets[] = $name . '|https';

        }

        $socketData =  [

                'whoplets' => $whoplets,

                'MyUsername' => $user->username,

                'MyKey' => $user->secretKey,

                'NODE_KEY' => env('NODE_KEY'),
                
            ];


        app('SocketService')->emit('deleteWhopletMultiple-server', $socketData);
    }
}