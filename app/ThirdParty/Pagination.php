<?php 

namespace WHop\ThirdParty;

use Landish\Pagination\Materialize;

class Pagination extends Materialize {

    protected $paginationWrapper = '<ul class="pagination" style="margin-left:-15px">%s %s %s</ul>';
}