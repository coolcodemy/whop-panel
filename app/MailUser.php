<?php

namespace WHoP;

use Illuminate\Database\Eloquent\Model;

class MailUser extends Model
{
    public function record()
    {
    	return $this->belongsTo(\WHoP\Record::class);
    }
}
