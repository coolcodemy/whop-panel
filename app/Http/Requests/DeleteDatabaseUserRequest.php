<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class DeleteDatabaseUserRequest extends Request
{
    private $user;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user;

        $myDBUser = app('DatabaseService')->getUserDatabaseAccountList($user);

        $dbUserArr = [];

        foreach ($myDBUser as $value) {
            $dbUserArr[] = $value['User'];
        }

        return [
            'databaseUser' => 'required|alpha_num',
            'usernameFull' => 'required|in:' . implode(',', $dbUserArr),
        ];
    }


    public function all()
    {
        $this->user = $this->route('user') ? $this->route('user') : auth()->user();
        $user = $this->user;


        $input = parent::all();
        $input['usernameFull'] = $user->username . '_' . $input['databaseUser'];
        $this->replace($input);
        return parent::all();
    }

    public function messages()
    {
        return [
            'usernameFull.in' => 'No database user with that username',
        ];
    }
}
