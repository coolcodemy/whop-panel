<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class CreateWhopletTemplateFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'templateName' => 'required|min:1|max:32|alpha_dash', //min 9 bcos it will add WHoPlet_ (8) prefix infront
            'templateContent' => 'required',
        ];
    }
}
