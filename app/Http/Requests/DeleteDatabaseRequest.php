<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class DeleteDatabaseRequest extends Request
{
    private $user;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user;

        $myDB = app('DatabaseService')->getUserDatabaseList($user);

        $myDBArr = [];


        foreach ($myDB as $value) {
            $myDBArr[] = $value['SCHEMA_NAME'];
        }

        return [
            'database' => 'required|alpha_num',
            'databaseFullName' => 'required|in:' . implode(',', $myDBArr),
        ];
    }

    public function all()
    {
        $this->user = $this->route('user') ? $this->route('user') : auth()->user();
        $user = $this->user;


        $input = parent::all();
        $input['databaseFullName'] = $user->username . '_' . $input['database'];
        $this->replace($input);
        return parent::all();
    }

    public function messages()
    {
        return [
            'databaseFullName.in' => 'No database with that name.',
        ];
    }
}
