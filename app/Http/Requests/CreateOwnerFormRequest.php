<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class CreateOwnerFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|human_name|min:2|max:255',
            'username' => 'required|alphanum|min:2|max:8|unique:users,username|not_in:' . $this->blacklist,
            'password' => 'required|min:5',
            'repeatPassword' => 'required|same:password',
            'email' => 'required|email',
            'package' => 'required|exists:packages,id',
            'domainName' => 'required|domain|unique:domains,name',
            'channel' => 'required',
        ];
    }
}
