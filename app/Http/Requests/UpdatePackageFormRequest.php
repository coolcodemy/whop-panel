<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class UpdatePackageFormRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:packages,name,' . $this->package->id,
            'validityNumber' => 'required|between:1,999',
            'validityTime' => 'required|in:DAY,MONTH,YEAR',
            'apcuSupport' => 'required|integer|digits_between:0,1',
            'enableSsl' => 'required|integer|digits_between:0,1',
            'ssh' => 'required|integer|digits_between:0,1',
            'diskQuota' => 'required|integer|numeric|min:0',
            'emailQuota' => 'required|integer|numeric|min:0',
            'ftpQuota' => 'required|integer|numeric|min:0',
            'websiteQuota' => 'required|integer|numeric|min:0',
            'numberOfDatabase' => 'required|integer|numeric|min:0',
            'numberOfDatabaseUser' => 'required|integer|numeric|min:0',
            'maximumQueriesPerHour' => 'required|integer|numeric|min:0',
            'maximumConnectionsPerHour' => 'required|integer|numeric|min:0',
            'maximumUpdatesPerHour' => 'required|integer|numeric|min:0',
            'maximumUserConnections' => 'required|integer|numeric|min:0',
            'numberOfEmailAccount' => 'required|integer|numeric|min:0',
            'numberOfFtpAccount' => 'required|integer|numeric|min:0',
            'numberOfDomain' => 'required|integer|numeric|min:0',
            'numberOfWhoplet' => 'required|integer|numeric|min:0'
        ];
    }
}
