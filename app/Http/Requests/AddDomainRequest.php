<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class AddDomainRequest extends Request
{
    private $user;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user;

        $rules =  [

            'domainName' => 'required|domain|unique:domains,name',

        ];

        if ($user->userpackage->numberOfDomain !== 0) {

            $rules['userDomainCount'] = 'required|max:' . ($user->userpackage->numberOfDomain - 1);
            
        }


        return $rules;
    }


    public function all()
    {
        $this->user = $this->route('user') ? $this->route('user') : auth()->user();
        $user = $this->user;
        $input = parent::all();

        $input['userDomainCount'] = count(app('DomainService')->getUserDomainList($user));

        $this->replace($input);
        return parent::all();
    }

    public function messages()
    {
        return [
            'ns1.exists' => 'NS1 of this domain must point to this server.',
            'ns2.exists' => 'NS2 of this domain must point to this server.',
            'nsCount.min' => 'Unknown domain name server. Please point nameserver 1 and nameserver 2 to this server.',
            'userDomainCount' => 'You have reach maximum allowed addon domain name.',
        ];
    }
}
