<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

use WHoP\Record;

class AddEmailRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->route('user') ? $this->route('user') : auth()->user();

        $emailQuota = $user->userpackage->emailQuota;
        $totalQuota = app('MailService')->getUserMailQuota($user);
        $maxQuota = $emailQuota - $totalQuota;

        $rules =  [
            'username' => 'required|min:1|max:32|alpha_num',
            'domain' => 'required|integer|exists:domains,id,user_id,' . $user->id,
            'mailDomain' => 'required|exists:records,id,type,A',
            'email' => 'required|email|max:255|unique:mail_users,email',
            'password' => 'required|min:5|max:255',
            'passwordAgain' => 'required|same:password',
        ];

        if ($emailQuota == 0) {
            $rules['quota'] = 'required|integer|min:0';

        } else {
            $rules['quota'] = 'required|integer|min:1|max:' . $maxQuota;

        }


        return $rules;
    }


    public function all()
    {
        $input = parent::all();

        $mailDomain = Record::findOrFail($input['mailDomain']);


        $input['username'] = strtolower($input['username']);
        $input['email'] = $input['username'] . '@' . $mailDomain->name;
        $input['domain'] = $mailDomain->domain_id;

        $this->replace($input);

        return parent::all();
    }
}
