<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class AddDatabaseUserRequest extends Request
{
    private $user;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user;
        $maxDBUser = $user->userpackage->numberOfDatabaseUser;

        $myDBUser = app('DatabaseService')->getUserDatabaseAccountList($user);

        $dbUserArr = [];

        foreach ($myDBUser as $value) {
            $dbUserArr[] = $value['User'];
        }

        $rules =  [
            'username' => 'required|alpha_num|min:1|max:16',
            'usernameFull' => 'required|not_in:'. implode(',', $dbUserArr),
            'password' => 'required|min:5',
            'verifyPassword' => 'required|same:password',
        ];

        if ($maxDBUser !== 0) {
            $rules['numberOfDatabaseUser'] = 'required|max:' . ($maxDBUser - 1);
        }

        return $rules;
    }


    public function all()
    {
        $this->user = $this->route('user') ? $this->route('user') : auth()->user();
        $user = $this->user;

        $input = parent::all();
        $input['usernameFull'] = $user->username . '_' . $input['username'];
        $input['numberOfDatabaseUser'] = count(app('DatabaseService')->getUserDatabaseAccountList($user));
        $this->replace($input);
        return parent::all();
    }


    public function messages()
    {
        return [
            'numberOfDatabaseUser.max' => 'You have exceeded maximum number of database user.',
            'usernameFull.not_in' => 'The database user already exist.',
        ];
    }
}
