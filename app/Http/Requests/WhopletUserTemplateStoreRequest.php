<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class WhopletUserTemplateStoreRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'dataall' => 'sometimes|array',
            'templateID' => 'required|exists:whoplets,id',
            'sameorigin' => 'required|boolean',
            'xssprotection' => 'required|boolean',
            'nosniff' => 'required|boolean',
            'sts' => 'required|boolean',
            'cache' => 'required|boolean',
            'cacheTime' => 'sometimes',
            'templateName' => 'required|min:1|max:128|alpha_dash',
        ];
    }
}
