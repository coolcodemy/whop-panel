<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

use WHoP\Record;

class AddFtpAccountRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->route('user') ? $this->route('user') : auth()->user();


        $ftpQuota = $user->userpackage->ftpQuota;
        $totalQuota = app('FtpService')->getUserFtpQuotaSum($user);
        $maxQuota = $ftpQuota - $totalQuota;

        $rules =  [
            'username' => 'required|min:1|max:255|alpha_num',
            'domain' => 'required|exists:records,id,type,A',
            'domainIdentifier' => 'required|exists:domains,id,user_id,' . $user->id,
            'ftpUsername' => 'required|unique:ftp_users,username',
            'password' => 'required|min:5',
            'repeatPassword' => 'required|same:password',
            'homeDir' => ['required', 'regex:/^(\/?){1}(([0-9A-Za-z*]+(\/{1})?)*)+$/'],
        ];

        if ($ftpQuota == 0) {
            $rules['quota'] = 'required|integer|min:0';

        } else {
            $rules['quota'] = 'required|integer|min:1|max:' . $maxQuota;

        }


        return $rules;
    }


    public function all()
    {
        $input = parent::all();

        $record = Record::findOrFail($input['domain']);

        $input['ftpUsername'] = $input['username'] . '@' . $record->name;
        $input['domainIdentifier'] = $record->domain->id;

        $this->replace($input);

        return parent::all();
    }
}
