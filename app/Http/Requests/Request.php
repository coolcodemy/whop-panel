<?php

namespace WHoP\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

abstract class Request extends FormRequest
{
    protected $blacklist = "root,bin,daemon,adm,lp,sync,shutdown,halt,mail,uucp,operator,games,gopher,ftp,nobody,dbus,usbmuxd,vcsa,rpc,rtkit,avahi,abrt,prcuser,nsfnobody,haldaemon,gdm,ntp,apache,saslauth,postfix,pulse,sshd,tcpdump,whop,nginx,darkserveremail,mysql,pdns,memcached,dovecot,dovenull,spamd,varnish,clam,spamd,amavis";
}
