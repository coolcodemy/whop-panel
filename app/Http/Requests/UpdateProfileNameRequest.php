<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class UpdateProfileNameRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|human_name|min:2|max:255',
        ];
    }
}
