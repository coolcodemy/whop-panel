<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class EditFtpuserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->route('user') ? $this->route('user') : auth()->user();


        $ftpQuota = $user->userpackage->ftpQuota;
        $totalQuota = app('FtpService')->getUserFtpQuotaSum($user);
        $maxQuota = $ftpQuota - $totalQuota;

        $rules =  [
            'password' => 'sometimes|min:5',
            'repeatPassword' => 'required_with:password|same:password',
        ];

        if ($ftpQuota == 0) {
            $rules['quota'] = 'required|integer|min:0';

        } else {
            $rules['quota'] = 'required|integer|min:1|max:' . $maxQuota;

        }


        return $rules;
    }
}
