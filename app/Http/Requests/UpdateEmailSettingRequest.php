<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class UpdateEmailSettingRequest extends Request
{
    private $encryption = 'null,ssl,tls';
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [

            'mailHost' => 'required|domain_subdomain',

            'mailPort' => 'required|integer',

            'mailName' => 'required|human_name',

            'mailUsername' => 'required|email',

            'mailPassword' => 'required',

            'mailEncryption' => 'sometimes|in:' . $this->encryption,

        ];
    }
}
