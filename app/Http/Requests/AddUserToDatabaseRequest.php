<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class AddUserToDatabaseRequest extends Request
{
    private $user;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user;

        $myDBUser = app('DatabaseService')->getUserDatabaseAccountList($user);
        $myDB = app('DatabaseService')->getUserDatabaseList($user);

        $dbUserArr = [];
        $myDBArr = [];

        foreach ($myDBUser as $value) {
            $dbUserArr[] = $value['User'];
        }

        foreach ($myDB as $value) {
            $myDBArr[] = $value['SCHEMA_NAME'];
        }

        return [
            'username' => 'required|alpha_num',
            'database' => 'required|alpha_num',
            'permission' => 'required|array',
            'usernameFull' => 'required|in:' . implode(',', $dbUserArr),
            'databaseFullName' => 'required|in:' . implode(',', $myDBArr),
        ];
    }


    public function all()
    {
        $this->user = $this->route('user') ? $this->route('user') : auth()->user();
        $user = $this->user;


        $input = parent::all();
        $input['usernameFull'] = $user->username . '_' . $input['username'];
        $input['databaseFullName'] = $user->username . '_' . $input['database'];
        $this->replace($input);
        return parent::all();
    }

    public function messages()
    {
        return [
            'usernameFull.in' => 'No database user with that username',
            'databaseFullName.in' => 'No database with that name.',
        ];
    }
}
