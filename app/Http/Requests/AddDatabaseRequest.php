<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class AddDatabaseRequest extends Request
{
    private $user;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user;

        $maxDB = $user->userpackage->numberOfDatabase;
        $myDB = app('DatabaseService')->getUserDatabaseList($user);

        $myDBArr = [];

        foreach ($myDB as $value) {
            $myDBArr[] = $value['SCHEMA_NAME'];
        }

        $rules = [
            'databaseName' => 'required|alpha_num|min:1|max:32',
            'databaseFullName' => 'required|not_in:' . implode(',', $myDBArr),
        ];

        if ($maxDB !== 0) {
            $rules['numberOfDatabase'] = 'required|integer|max:' . ($maxDB - 1);
        }

        return $rules;
    }


    public function all()
    {
        $this->user = $this->route('user') ? $this->route('user') : auth()->user();

        $user = $this->user;

        $input = parent::all();
        $input['databaseFullName'] = $user->username . '_' . $input['databaseName'];
        $input['numberOfDatabase'] = count(app('DatabaseService')->getUserDatabaseList($user));
        $this->replace($input);
        return parent::all();
    }


    public function messages()
    {
        return [
            'numberOfDatabase.max' => 'You have exceeded maximum number of database.',
            'databaseFullName.not_in' => 'The database with that name already exist.',
        ];
    }
}
