<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class DeleteOwnerRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|same:delete',
        ];
    }


    public function all()
    {
        $input = parent::all();

        $input['delete'] = $this->route('user')->username;

        $this->replace($input);

        return parent::all();
    }


    public function messages()
    {
        return [

            'username.same' => 'You must input the same username as the Owner`s username you want to delete.',

        ];
    }
}
