<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class RevokeUserFromDatabaseRequest extends Request
{
    private $user;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user;

        $myDBUser = app('DatabaseService')->getUserDatabaseAccountList($user);
        $myDB = app('DatabaseService')->getUserDatabaseList($user);
        $existingUser = app('DatabaseService')->userOfDatabase($user, $this->databaseFullName);

        $dbUserArr = [];
        $myDBArr = [];

        foreach ($myDBUser as $value) {
            $dbUserArr[] = $value['User'];
        }

        foreach ($myDB as $value) {
            $myDBArr[] = $value['SCHEMA_NAME'];
        }

        return [
            'databaseUser' => 'required|alpha_num',
            'database' => 'required|alpha_num',
            'databaseFullName' => 'required|in:' . implode(',', $myDBArr),
            'usernameFull' => 'required|in:' . implode(',', $existingUser),
        ];
    }



    public function all()
    {
        $this->user = $this->route('user') ? $this->route('user') : auth()->user();
        $user = $this->user;


        $input = parent::all();
        $input['usernameFull'] = $user->username . '_' . $input['databaseUser'];
        $input['databaseFullName'] = $user->username . '_' . $input['database'];
        $this->replace($input);
        return parent::all();
    }

    public function messages()
    {
        return [
            'usernameFull.in' => 'User is not in database grantee.',
            'databaseFullName.in' => 'No database with that name.',
        ];
    }
}
