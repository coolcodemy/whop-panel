<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class EditEmailRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->route('user') ? $this->route('user') : auth()->user();

        $email = $this->route('email');

        $emailQuota = $user->userpackage->emailQuota;

        $totalQuota = app('MailService')->getUserMailQuota($user);
        
        $maxQuota = $emailQuota - $totalQuota + $email->quota;

        $rules =  [

            'password' => 'sometimes|min:5',

            'passwordAgain' => 'sometimes|same:password',

        ];

        if ($emailQuota == 0) {

            $rules['quota'] = 'sometimes|integer|min:0';


        } else {

            $rules['quota'] = 'sometimes|integer|min:1|max:' . $maxQuota;

        }

        return $rules;
    }
}
