<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

use WHoP\Record;

class AddEmailForwardingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->route('user') ? $this->route('user') : auth()->user();

        return [
            'forwardFrom' => 'required',
            'emailDomain' => 'required|integer|exists:records,id,type,A',
            'domain' => 'required|integer|exists:domains,id,user_id,' . $user->id,
            'destinationTo' => 'required|email',
            'emailFrom' => 'required|email|unique_with:mail_forwardings,destinationTo = destination, emailFrom = source',
        ];
    }


    public function all()
    {
        $input = parent::all();

        $mailDomain = Record::findOrFail($input['emailDomain']);

        $input['domain'] = $mailDomain->domain_id;
        $input['emailFrom'] = $input['forwardFrom'] . '@' . $mailDomain->name;

        $this->replace($input);

        return parent::all();
    }


    public function messages()
    {
        return [
            'emailFrom.unique_with' => 'Forwarding already exists.',
        ];
    }
}
