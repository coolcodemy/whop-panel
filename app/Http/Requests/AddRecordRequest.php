<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

use WHoP\Domain;

class AddRecordRequest extends Request
{
    private $eloquentDomain, $user;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = $this->user;


        $rules = [
            'domain' => 'required|exists:domains,id,user_id,' . $user->id,
            'type' => 'required|in:A,AAAA,CNAME,MX,NS,SPF,TXT',
            'name' => ['required', 'regex:/^([a-z0-9]+|@)$/', 'min:1', 'max:128'],
            'ttl' => 'required|integer|min:300|max:86400',
            'priority' => 'required_if:type,MX|min:0|max:100',
            'recordName' => 'required|unique_with:records,content,recordName = name|domain_subdomain',
        ];

        $type = $this->type == null ? '' : $this->type;

        if ($type == 'A') {
            $rules['content'] = 'required|ipv4';

        } else if ($type == 'AAAA') {
            $rules['content'] = 'required|ipv6';

        } else if ($type == 'CNAME') {
            $rules['recordName'] = 'required|unique:records,name|domain_subdomain';
            $rules['content'] = 'required|domain_subdomain|min:1|max:255';

        } else if ($type == 'MX') {
            $rules['recordName'] = 'required|unique_with:records,content,recordName = name|domain_subdomain';
            $rules['content'] = 'required|domain_subdomain';

        } else if ($type == 'NS') {
            $rules['content'] = 'required|domain_subdomain|min:1|max:255';
            $rules['recordName'] = 'required|unique_with:records,content,recordName = name|domain_subdomain';

        } else if ($type == 'SPF') {
            $rules['content'] = 'required|min:1|max:64000';
            $rules['name'] = 'required|domain_key|min:1|max:256';

        } else if ($type == 'TXT') {
            $rules['content'] = 'required|min:1|max:64000';
            $rules['name'] = 'required|domain_key|min:1|max:256';

        }


        return $rules;
    }


    public function all()
    {
        $this->user = $this->route('user') ? $this->route('user') : auth()->user();
        
        $input = parent::all();

        $domain = Domain::find($input['domain']);
        $this->eloquentDomain = $domain;

        $input['recordName'] = ($input['name'] == '@' ? $domain->name : $input['name'] . '.' . $domain->name);

        $this->replace($input);

        return parent::all();
    }


    public function messages()
    {
        return [
            'name.regex' => 'Unknown record name.',
            'recordName.unique_with' => 'Record name already exists.',
        ];
    }
}
