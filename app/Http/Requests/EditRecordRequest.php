<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class EditRecordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $record = $this->route('record');

        $rules = [
            'ttl' => 'required|integer|min:300|max:86400',
            'priority' => 'required_if:type,MX|min:0|max:100',
            'deleteEdit' => 'required|numeric|size:1',
        ];

        $type = $this->type == null ? '' : $this->type;

        if ($type == 'A') {
            $rules['content'] = 'required|ipv4';

        } else if ($type == 'AAAA') {
            $rules['content'] = 'required|ipv6';

        } else if ($type == 'CNAME') {
            $rules['content'] = 'required|domain_subdomain|min:1|max:255';

        } else if ($type == 'MX') {
            $rules['content'] = 'required|domain_subdomain';

        } else if ($type == 'NS') {
            $rules['content'] = 'required|domain_subdomain|min:1|max:255';

        } else if ($type == 'SPF') {
            $rules['content'] = 'required|min:1|max:64000';

        } else if ($type == 'TXT') {
            $rules['content'] = 'required|min:1|max:64000';
        }


        return $rules;
    }


    public function all()
    {
        $input = parent::all();


        $record = $this->route('record');

        $input['deleteEdit'] = $record->deleteEdit;
        $input['type'] = $record->type;

        $this->replace($input);

        return parent::all();
    }


    public function messages()
    {
        return [

            'deleteEdit.size' => 'Cannot edit this record.',

        ];
    }
}
