<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class UpdatePanelSettingRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [

          'MaxUserTemplate' => 'required|integer|min:10|max:10000',

          'OwnerPerPage' => 'required|integer|min:5|max:100',

          'BruteforceTimes' => 'required|integer|min:5|max:100',

          'BruteforceMinutes' => 'required|integer|min:1|max:86400',

          'OwnerDomainItemPerPage' => 'required|integer|min:5|max:100',

          'OwnerMailForwardingItemPerPage' => 'required|integer|min:5|max:100',

          'OwnerMailItemPerPage' => 'required|integer|min:5|max:100',

          'OwnerFtpItemPerPage' => 'required|integer|min:5|max:100',

        ];
    }
}
