<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

use WHoP\Whoplet;


class SelectWhopletTemplateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if (auth()->user()->hasRole('admin')) {
            $user = $this->route('user');
        } else {
            $user = auth()->user();
        }

        $template = Whoplet::findOrFail($this->templateID);
        $templateOwnerRole = $template->user->roles[0]->name;

        $ssl = $user->userpackage->enableSsl;

        if (strpos($template->content,'{SSL}') !== false && $ssl == 0) {
            return false;
        }


        if ($template->user_id == $user->id) {

            return true;

        } else if ($templateOwnerRole == 'admin') {

            return true;
            
        }

        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'templateID' => 'required|numeric|integer|exists:whoplets,id',
        ];
    }
}
