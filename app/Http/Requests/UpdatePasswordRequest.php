<?php

namespace WHoP\Http\Requests;

use WHoP\Http\Requests\Request;

class UpdatePasswordRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [

            'newPassword' => 'required|min:5|max:255',

            'verifyNewPassword' => 'required|same:newPassword',

        ];

        if ( auth()->user()->hasRole('owner') ) {

            $rules['currentPassword'] = 'required|currentPassword';

        } else if ( auth()->user()->hasRole('admin') && $this->route('user') == null) { // if updating own password

            $rules['currentPassword'] = 'required|currentPassword';
        }


        return $rules;
    }
}
