<?php

namespace WHoP\Http\Middleware;

use Closure;

use WHoP\Domain;


class DomainMustBelongToOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->route('user') ? $request->route('user') : auth()->user();
        $domain = $request->route('domain');

        if ($user->id !== $domain->user_id) {
            abort(404);
        }
        
        return $next($request);
    }
}
