<?php

namespace WHoP\Http\Middleware;

use Closure;

class UserMustBeDomainOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->route('user');

        if ($user->roles[0]->name !== 'owner') {
            abort(404);
        }

        return $next($request);
    }
}
