<?php

namespace WHoP\Http\Middleware;

use Closure;

class IsVerifiedWhopLicense
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (env('WHOP_VERIFIED') !== true) {
            return response()->view('whop.licensecheck');
        }
        return $next($request);
    }
}
