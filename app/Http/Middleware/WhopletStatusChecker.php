<?php

namespace WHoP\Http\Middleware;

use Closure;

class WhopletStatusChecker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $status = intval($request->route('status'));
        if ($status !== 1 && $status !== 0) {
            return redirect()->back()->with([
                'errorMessage' => 'Status must be enable or disable only.',
                ]);
        }
        return $next($request);
    }
}
