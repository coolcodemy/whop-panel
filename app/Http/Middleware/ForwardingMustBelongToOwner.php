<?php

namespace WHoP\Http\Middleware;

use Closure;

class ForwardingMustBelongToOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->route('user') ? $request->route('user') : auth()->user();

        $forwarding = $request->route('forwarding');

        if ($user->id !== $forwarding->user_id) {

            abort(404);
            
        }

        
        return $next($request);
    }
}
