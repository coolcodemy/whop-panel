<?php

namespace WHoP\Http\Middleware;

use Closure;

class UserAccountMustBeEnable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->route('user');

        if ( $user->disable == 1 ) {

            abort(404);
            
        }

        return $next($request);
    }
}
