<?php

namespace WHoP\Http\Middleware;

use Closure;

class FtpMustBelongToOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->route('user') ? $request->route('user') : auth()->user();
        $ftpUser = $request->route('ftpuser');


        if ($user->id !== $ftpUser->user_id) {
            abort(404);
        }
        
        return $next($request);
    }
}
