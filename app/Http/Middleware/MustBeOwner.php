<?php

namespace WHoP\Http\Middleware;

use Closure;

class MustBeOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( ! auth()->user()->hasRole('owner') ) {

            abort(404);
            
        }
        return $next($request);
    }
}
