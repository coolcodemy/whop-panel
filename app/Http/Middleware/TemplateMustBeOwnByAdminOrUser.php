<?php

namespace WHoP\Http\Middleware;

use Closure;

use WHoP\Whoplet;

class TemplateMustBeOwnByAdminOrUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $template = Whoplet::find($request->templateID);

        $user = $request->route('user') ? $request->route('user') : auth()->user();

        if ($template == null) {
            abort(404);
        }

        if ($user->id !== $template->user_id && $template->user->roles[0]->name !== 'admin') {
            abort(404);
        } 
        return $next($request);
    }
}
