<?php

namespace WHoP\Http\Middleware;

use Closure;

use PRedis;

class BruteforceProtection
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $bruteforceMinutes = app('SettingService')->getInt('BruteforceMinutes');
        $bruteforceTimes = app('SettingService')->getInt('BruteforceTimes');

        PRedis::incr('bruteforce-' . $request->getClientIp());
        PRedis::expire('bruteforce-' . $request->getClientIp(), $bruteforceMinutes*60);

        $times = PRedis::get('bruteforce-' . $request->getClientIp());
        
        if ($times > $bruteforceTimes) {
            return response()->json([
                'message' => sprintf('You have been blocked for bruteforcing. Please wait for %d minutes.', $bruteforceMinutes),
                ], 422);
        }
        
        return $next($request);
    }
}
