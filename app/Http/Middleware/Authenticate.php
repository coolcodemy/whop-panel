<?php

namespace WHoP\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;

use PRedis;

class Authenticate
{
    /**
     * The Guard implementation.
     *
     * @var Guard
     */
    protected $auth;

    /**
     * Create a new filter instance.
     *
     * @param  Guard  $auth
     * @return void
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($this->auth->guest()) {
            return redirect()->guest(route('guest::login'));
        }

        $user = $this->auth->user();

        $json['role'] = $user->roles()->first()->name;
        $json['key'] = $user->secretKey;
        $json = json_encode($json);


        PRedis::set('whop:user:' . $user->username, $json);
        PRedis::expire('whop:user:' . $user->username, config('session.lifetime')*60);

        return $next($request);
    }
}
