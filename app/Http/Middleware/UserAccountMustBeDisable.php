<?php

namespace WHoP\Http\Middleware;

use Closure;

class UserAccountMustBeDisable
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->route('user');

        if ( $user->disable == 0 ) {

            abort(404);
            
        }
        return $next($request);
    }
}
