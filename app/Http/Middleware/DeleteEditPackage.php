<?php

namespace WHoP\Http\Middleware;

use Closure;

class DeleteEditPackage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $package = $request->route('package');
        
        $action = $request->route()->getActionName();

        if ($action == "WHoP\Http\Controllers\PackageController@destroy") {

            if ($package->name == 'Unlimited' || $package->id == 1) {
                return redirect()->back()->with([
                    'errorMessage' => 'Cannot delete package unlimited',
                    ]);
            } else if (count($package->userpackage) != 0) {
                return redirect()->back()->with([
                    'errorMessage' => 'Cannot delete package that have user',
                    ]);
            }

        } else if ($action == "WHoP\Http\Controllers\PackageController@edit" || $action == "WHoP\Http\Controllers\PackageController@update") {

            if ($package->name == 'Unlimited' || $package->id == 1) {
                return redirect()->back()->with([
                    'errorMessage' => 'Cannot edit package unlimited',
                    ]);
            }
        }
        
        return $next($request);
    }
}
