<?php

namespace WHoP\Http\Middleware;

use Closure;

use WHoP\Whoplet;

class TemplateMustBeOwnByOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $template = $request->route('whoplet');

        if ($template == null) {
            $template = Whoplet::findOrFail($request->templateID);
        }
        
        $user = $request->route('user') ? $request->route('user') : auth()->user();

        if ($user->id !== $template->user_id) {
            abort(404);
        }
        return $next($request);
    }
}
