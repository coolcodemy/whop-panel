<?php

namespace WHoP\Http\Middleware;

use Closure;

class EmailMustBelongToOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $email = $request->email;
        $user = $request->user ? $request->user : auth()->user();

        if ($user->id !== $email->user_id) {
            abort(404);
        }
        return $next($request);
    }
}
