<?php

namespace WHoP\Http\Middleware;

use Closure;

class RecordMustBelongToOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $record = $request->route('record');

        $user = $request->route('user') ? $request->route('user') : auth()->user();

        if ($record->domain->user_id !== $user->id) {
            abort(404);
        }
        return $next($request);
    }
}
