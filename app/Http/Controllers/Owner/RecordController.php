<?php

namespace WHoP\Http\Controllers\Owner;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Services\RecordService;
use WHoP\Services\DomainService;

use WHoP\Http\Requests\AddRecordRequest;
use WHoP\Http\Requests\EditRecordRequest;

use WHoP\User;
use WHoP\Domain;
use WHoP\Record;

class RecordController extends Controller
{
    private $RecordService, $DomainService;

    public function __construct(RecordService $RecordService, DomainService $DomainService)
    {

    	parent::__construct();

    	$this->RecordService = $RecordService;

    	$this->DomainService = $DomainService;

    }


    public function index()
    {
        $urlList = [

            'record-create' => route('owner::app::record-create'),

        ];

    	$domains = $this->DomainService->getUserDomainList($this->user);

    	$user = $this->user;




    	return view(config('whop.themes') . '.shared.apps.record')->with(compact(

    		'domains',

    		'user',

    		'urlList'

    		));
    }

    public function create()
    {
        $url =  route('owner::app::record-store');

        $domains = $this->DomainService->getUserDomainList($this->user);

        $types = $this->recordType;

        $title = 'Add Record';

        $header = 'Add New Record';



        return view(config('whop.themes') . '.shared.apps.record-create-edit')->with(compact(

            'domains',

            'user',

            'types',

            'title',

            'header',

            'url'

            ));
    }



    public function myRecord(Domain $domain)
    {
    	$modifier = [
    		'type' => ['!=', 'SOA'],
    	];

    	$records =  $this->RecordService->getRecordsByDomainID($domain->id, $modifier);

        $recordsWithURL = [];

        foreach ($records as $record) {

            $record->editURL = route('owner::app::record-edit', [$record]);

            $record->deleteURL = route('owner::app::record-delete', [$record]);

            $recordsWithURL[] = $record;

        }


        return $recordsWithURL;
    }



    public function store(AddRecordRequest $request)
    {

    	$record = $this->RecordService->createRecordByRequest($request);



    	return redirect()->back()->with([

    		'successMessage' => 'Succesfully add new record.',

    		]);
    }



    public function edit(Record $record)
    {

        $url = route('owner::app::record-update', [$record]);

        $record->name = rewriteRecordName( $record->name, $record->domain->name );

        $domains = $this->DomainService->getUserDomainList($this->user);

        $types = $this->recordType;

        $title = 'Edit Record';

        $header = 'Edit Record';

        $editMode = true;





        return view(config('whop.themes') . '.shared.apps.record-create-edit')->with(compact(

            'record',

            'domains',

            'types',

            'title',

            'header',

            'url',

            'editMode'

            ));
    }


    public function update(Record $record, EditRecordRequest $request)
    {

        $record = $this->RecordService->updateRecordByRequest($record, $request);




        return redirect()->back()->with([

            'successMessage' => 'Succesfully update record.',

            ]);
    }


    public function delete(Record $record)
    {

        return $this->RecordService->delete($this->user, $record);

    }

}
