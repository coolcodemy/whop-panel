<?php

namespace WHoP\Http\Controllers\Owner;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Services\MailService;
use WHoP\Services\ProgressService;

use WHoP\Http\Requests\AddEmailRequest;
use WHoP\Http\Requests\EditEmailRequest;

use WHoP\User;
use WHoP\MailUser;


class EmailController extends Controller
{
	private $MailService;


    public function __construct(MailService $MailService)
    {

    	parent::__construct();

    	$this->MailService = $MailService;

    }


    public function index(ProgressService $ProgressService)
    {
        $urlList = [

            'email-create' => route('owner::app::email-create'),

        ];

    	$progress = [
    		'mailUsers' => $ProgressService->mailUsers($this->user),
    		'mailQuota' => $ProgressService->mailQuota($this->user),
    	];

    	$emails = $this->MailService->getUserMailAccountList($this->user, app('SettingService')->getInt('OwnerMailItemPerPage'));

    	return view(config('whop.themes') . '.shared.apps.email')->with(compact(

            'urlList',

            'progress',

            'emails'

            ));
    }

    public function create()
    {

        $url = route('owner::app::email-store', $this->user);

    	$mailDomains = $this->MailService->getDomain($this->user);

    	$emailQuota = $this->user->userpackage->emailQuota;

    	$totalQuota = $this->MailService->getUserMailQuota($this->user);

    	$maxQuota = ($emailQuota== 0) ? 'Unlimited' : $emailQuota - $totalQuota;

        $title = 'Add Account';

        $header = 'Add New Email Account';

        $buttonText = 'Add';



    	return view(config('whop.themes') . '.shared.apps.email-create-edit')->with(compact(

    		'mailDomains',

    		'emailQuota',

    		'maxQuota',

            'title',

            'header',

            'url',

            'buttonText'

    		));

    }


    public function store(AddEmailRequest $request)
    {

    	$email = $this->MailService->newEmail($this->user, $request);



    	return redirect()->back()->with([

    		'successMessage' => sprintf('Succesfully add new email %s.', $email->email),

    		]);

    }


    public function edit(MailUser $email)
    {

        $url = route('owner::app::email-update', [$email]);

        $mailDomains = $this->MailService->getDomain($this->user);

        $emailQuota = $this->user->userpackage->emailQuota;

        $totalQuota = $this->MailService->getUserMailQuota($this->user);

        $maxQuota = ($emailQuota== 0) ? 'Unlimited' : $emailQuota - $totalQuota + $email->quota;

        // Add username
        $email->username = explode('@', $email->email)[0];

        $title = 'Edit Account';

        $header = 'Edit Email Account';

        $buttonText = 'Update';

        $disabled = true;

        return view(config('whop.themes') . '.shared.apps.email-create-edit')->with(compact(

            'mailDomains',

            'emailQuota',

            'maxQuota',

            'title',

            'header',

            'url',

            'buttonText',

            'email',

            'disabled'

            ));
    }


    public function update(MailUser $email, EditEmailRequest $request)
    {

        $email = $this->MailService->update($email, $request);


        return redirect()->back()->with([

            'successMessage' => sprintf('Succesfully update email %s.', $email->email),

            ]);

    }


    public function delete(MailUser $email)
    {
        $email->delete();

        $this->MailService->deleteSystemEmail($this->user, $email);



        return redirect()->back()->with([

            'successMessage' => sprintf('Succesfully delete email account %s.', $email->email),
            
            ]);

    }


    public function enableDisable(MailUser $email)
    {

        $this->MailService->enableDisable($email);

        return redirect()->back();

    }
}
