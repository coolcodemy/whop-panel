<?php

namespace WHoP\Http\Controllers\Owner;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

class SslController extends Controller
{
    public function index()
    {

        $urlList = [

            'ssl-add' => route('owner::app::ssl-add'),
        ];

        $user = $this->user;




    	return view(config('whop.themes') . '.shared.apps.ssl')->with(compact(

    		'user',

    		'urlList'

    		));
        
    }


    public function create()
    {
    	$user = $this->user;

    	return view(config('whop.themes') . '.shared.apps.ssl-create')->with(compact('user'));

    }
}
