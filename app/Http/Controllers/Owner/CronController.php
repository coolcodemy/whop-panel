<?php

namespace WHoP\Http\Controllers\Owner;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

class CronController extends Controller
{
    public function index()
    {
        $urlList = [

            'cron-add' => route('owner::app::cron-add'),

        ];

        $user = $this->user;


    	return view(config('whop.themes') . '.shared.apps.cron')->with(compact(

            'user',

            'urlList'
            
            ));
    }


    public function create()
    {
    	$user = $this->user;

    	return view(config('whop.themes') . '.shared.apps.cron-create')->with(compact('user'));
    }
}
