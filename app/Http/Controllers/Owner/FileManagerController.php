<?php

namespace WHoP\Http\Controllers\Owner;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

class FileManagerController extends Controller
{
    public function index()
    {

        $urlList = [

            'editor' => route('owner::app::filemanager-editor'),

        ];

        $user = $this->user;



    	return view(config('whop.themes') . '.shared.apps.filemanager')->with(compact(
    		'user',

    		'urlList'

    		));
    }


    public function editor($location = null, $file = null)
    {
        $file = rewriteEditorFilePath($file);

        $user = $this->user;

    	return view(config('whop.themes') . '.shared.apps.filemanager-editor')->with(compact(
    		'user',

    		'location',

    		'file'
            
    		));
    }
}
