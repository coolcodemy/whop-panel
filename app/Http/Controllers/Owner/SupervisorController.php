<?php

namespace WHoP\Http\Controllers\Owner;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

class SupervisorController extends Controller
{
    public function index()
    {

        $urlList = [

            'supervisor-add' => route('owner::app::supervisor-add'),

        ];


        $user = $this->user;




    	return view(config('whop.themes') . '.shared.apps.supervisor')->with(compact(

    		'user',

    		'urlList'

    		));

    }


    public function create()
    {

    	$user = $this->user;
    	

    	return view(config('whop.themes') . '.shared.apps.supervisor-create-edit')->with(compact('user'));
        
    }
}
