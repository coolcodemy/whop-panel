<?php

namespace WHoP\Http\Controllers\Owner;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Services\WhopletService;

use WHoP\Http\Requests\SelectWhopletTemplateRequest;
use WHoP\Http\Requests\WhopletUserTemplateStoreRequest;

use WHoP\Whoplet;


class UserWhopletTemplateController extends Controller
{
	private $WhopletService;

	public function __construct(WhopletService $WhopletService)
	{
		parent::__construct();


		$this->WhopletService = $WhopletService;

	}


    public function index()
    {
    	$templates = $this->WhopletService->getTemplateToBuild($this->user);

    	$urlList = [

    		'get-template-url' => route('owner::app::whoplettemplatemanager-gettemplate'),

    		'checkportion-url' => route('owner::app::whoplettemplatemanager-checkportion'),

    		'preview-url' => route('owner::app::whoplettemplatemanager-preview'),

    		'save-url' => route('owner::app::whoplettemplatemanager-store'),

    	];

    	$user = $this->user;




    	return view(config('whop.themes') . '.shared.apps.whoplet-template')->with(compact(

    		'templates',

    		'user',

    		'urlList'

    		));
    }


    public function getTemplate(SelectWhopletTemplateRequest $request)
    {

    	return Whoplet::where('id', '=', $request->templateID)->first()->content;

    }


    public function checkPortion(Request $request)
    {

    	return $this->WhopletService->checkPortion($request);

    }


	public function preview(Request $request)
	{

		return $this->WhopletService->preview($this->user, $request);

	}


	public function store(WhopletUserTemplateStoreRequest $request)
	{

		$instance = $this->WhopletService->preview($this->user, $request);

		$type = gettype($instance);


		if ( $type === 'object' ) {

			return $instance;

		} else if ( ! $this->WhopletService->maxUserTemplate( $this->user ) ) {

			return response()->json([

				'message' => 'You have reached maximum template creation set by Admin',

			], 422);

		} else if ( $type === 'string' ) {
			
			$request->merge([

				'templateContent' => $instance,

				]);

			$this->WhopletService->createTemplate( $request, $this->user );
		}



		return response()->json([

			'message' => sprintf('Succesfully save template %s', $request->templateName),

			], 200);
	}


	public function delete(Whoplet $template)
	{
        $template->delete();

        return redirect()->back()->with([

            'successMessage' => sprintf('Succesfully delete template %s.', $template->name),

            ]);
	}
}
