<?php

namespace WHoP\Http\Controllers\Owner;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Http\Requests\AddEmailForwardingRequest;

use WHoP\User;
use WHoP\MailForwarding;

use WHoP\Services\MailService;

class EmailForwardingController extends Controller
{
	private $MailService;


	public function __construct(MailService $MailService)
	{

		parent::__construct();

		$this->MailService = $MailService;

	}


    public function index()
    {
    	$forwardings = $this->MailService->getForwardings($this->user, app('SettingService')->getInt('OwnerMailForwardingItemPerPage'));

        $buttonURL = [

            'create' => route('owner::app::email-forwarding-create'),

        ];



    	return view(config('whop.themes') . '.shared.apps.emailforwarding')->with(compact(

    		'forwardings',

            'buttonURL'

    		));
    }


    public function create()
    {
        $mailDomains = $this->MailService->getDomain($this->user);

        $urlList = [

            'submit' => route('owner::app::email-forwarding-store'),

        ];



        return view(config('whop.themes') . '.shared.apps.emailforwarding-create')->with(compact(

            'mailDomains',

            'urlList'
            ));
    }


    public function store(AddEmailForwardingRequest $request)
    {

        $this->MailService->createForwarding($this->user, $request);



        return redirect()->back()->with([

            'successMessage' => sprintf('Succesfully forward email %s to %s.', $request->emailFrom, $request->destinationTo),
            
            ]);
    }


    public function enableDisable(MailForwarding $forwarding)
    {

        $this->MailService->enableDisableForwarding($forwarding);


        return redirect()->back();
    }


    public function destroy(MailForwarding $forwarding)
    {

        $forwarding->delete();


        return redirect()->back()->with([

            'successMessage' => sprintf('Succesfully delete forwarding from %s to %s', $forwarding->source, $forwarding->destination),

            ]);
    }
}
