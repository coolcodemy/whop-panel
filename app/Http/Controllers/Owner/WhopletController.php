<?php

namespace WHoP\Http\Controllers\Owner;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Http\Requests\SelectWhopletTemplateRequest;
use WHoP\Http\Requests\BuildWhopletRequest;

use WHoP\Services\UserService;
use WHoP\Services\WhopletService;


class WhopletController extends Controller
{
	private $UserService, $WhopletService;


	public function __construct(UserService $UserService, WhopletService $WhopletService)
	{
		parent::__construct();

		$this->UserService = $UserService;

		$this->WhopletService = $WhopletService;

	}

    public function index()
    {
    	$urlList = [

            'getTemplateForm' => route('owner::app::whopletformbuilder'),

            'buildWhoplet' => route('owner::app::buildwhoplet'),

            'streamurl' => route('owner::app::whopletlog'),

        ];

        $ssl = $this->user->userpackage->enableSsl;

        $templates = $this->WhopletService->whopletGeneratorTemplatesList($this->user);

        $channel = app('SocketService')->generateChannel();

        $user = $this->user;





    	return view(config('whop.themes') . '.shared.apps.whoplet')->with(compact(

    		'ssl',

    		'templates',

    		'urlList',

    		'user',

    		'channel'

    		));

    }


    public function whopletFormBuilder(SelectWhopletTemplateRequest $request)
    {

    	return $this->WhopletService->formGenerator($request->templateID, $this->user);

    }


    public function buildWhoplet(BuildWhopletRequest $request)
    {

    	$this->WhopletService->buildWhoplet($request, $this->user);

    }


    public function streamLog()
    {

        return view(config('whop.themes') . '.shared.apps.whoplet-log-stream')->with(compact('user'));

    }
}
