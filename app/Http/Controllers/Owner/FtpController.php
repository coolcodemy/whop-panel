<?php

namespace WHoP\Http\Controllers\Owner;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Services\FtpService;

use WHoP\Http\Requests\AddFtpAccountRequest;
use WHoP\Http\Requests\EditFtpuserRequest;

use WHoP\FtpUser;

class FtpController extends Controller
{
	private $FtpService;



	public function __construct(FtpService $FtpService)
	{

		parent::__construct();

		$this->FtpService = $FtpService;

	}


    public function index()
    {
    	$ftpAccounts = $this->FtpService->getUserFtpUserList($this->user, app('SettingService')->getInt('OwnerFtpItemPerPage'));

        $urlList = [

            'ftp-create' => route('owner::app::ftp-create'),

        ];

    	


    	return view(config('whop.themes') . '.shared.apps.ftp')->with(compact(

    		'ftpAccounts',

    		'urlList'

    		));
    }


    public function create()
    {

        $urlList = [

            'action' => route('owner::app::ftp-store'),

        ];

    	$ftpDomains = $this->FtpService->getFtpDomains($this->user);

    	$ftpQuota = $this->user->userpackage->ftpQuota;

    	$totalQuota = $this->FtpService->getUserFtpQuotaSum($this->user);

    	$maxQuota = ($ftpQuota== 0) ? 'Unlimited' : $ftpQuota - $totalQuota;

        $buttonText = 'Add';

        $user = $this->user;



    	return view(config('whop.themes') . '.shared.apps.ftp-create-edit')->with(compact(

    		'urlList',

    		'user',

    		'ftpDomains',

    		'ftpQuota',

    		'maxQuota',

    		'buttonText'

    		));

    }


    public function store(AddFtpAccountRequest $request)
    {
    	$ftpUser = $this->FtpService->create($this->user, $request);

    	return redirect()->back()->with([

    		'successMessage' => sprintf('Succesfully add new ftp user %s', $ftpUser->username),

    		]);
    }


    public function edit(FtpUser $ftpUser)
    {

        $urlList  = [

            'action' => route('owner::app::ftp-update', [$ftpUser]),

        ];

    	$ftpDomains = $this->FtpService->getFtpDomains($this->user);

    	$ftpQuota = $this->user->userpackage->ftpQuota;

    	$totalQuota = $this->FtpService->getUserFtpQuotaSum($this->user);

    	$maxQuota = ($ftpQuota== 0) ? 'Unlimited' : $ftpQuota - $totalQuota;

    	// Hack to display only username
		$ftpUser->username = rewriteFTPUsername($ftpUser);

        $buttonText = 'Update';

        $user = $this->user;

        $editMode = true;




    	return view(config('whop.themes') . '.shared.apps.ftp-create-edit')->with(compact(

    		'urlList',

    		'user',

    		'ftpDomains',

    		'ftpQuota',

    		'maxQuota',

    		'ftpUser',

    		'buttonText',

    		'editMode'

    		));
    }


    public function update(FtpUser $ftpUser, EditFtpuserRequest $request)
    {

    	$this->FtpService->update($ftpUser, $request);



    	return redirect()->back()->with([

    		'successMessage' => sprintf('Succesfully update %s FTP account.', $ftpUser->username),

    		]);

    }


    public function enableDisable(FtpUser $ftpUser)
    {

    	$this->FtpService->enableDisable($ftpUser);

    	return redirect()->back();

    }


    public function destroy(FtpUser $ftpUser)
    {

    	$ftpUser->delete();


    	return redirect()->back()->with([

    		'successMessage' => sprintf('Succesfully delete ftp account %s', $ftpUser->username),
            
    		]);

    }
}
