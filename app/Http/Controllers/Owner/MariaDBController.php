<?php

namespace WHoP\Http\Controllers\Owner;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Services\ProgressService;
use WHoP\Services\DatabaseService;

use WHoP\Http\Requests\AddDatabaseRequest;
use WHoP\Http\Requests\AddDatabaseUserRequest;
use WHoP\Http\Requests\AddUserToDatabaseRequest;
use WHoP\Http\Requests\RevokeUserFromDatabaseRequest;
use WHoP\Http\Requests\DeleteDatabaseRequest;
use WHoP\Http\Requests\DeleteDatabaseUserRequest;

use WHoP\User;

use DB;

class MariaDBController extends Controller
{
	private $DatabaseService;

	public function __construct(DatabaseService $DatabaseService)
	{

		parent::__construct();

		$this->DatabaseService = $DatabaseService;

	}

    public function index(ProgressService $ProgressService)
    {
    	$progressDB = $ProgressService->db($this->user);

    	$progressDBUser = $ProgressService->dbUsers($this->user);

    	$progress = [

    		'db' => $progressDB,

    		'dbUsers' => $progressDBUser,

    	];

    	$db =  $this->DatabaseService->getUserDatabaseList($this->user);

    	$dbUser = $this->DatabaseService->getUserDatabaseAccountList($this->user);



		$dbArr = $this->DatabaseService->myDBWithUser($this->user);

		$dbUserArrRemoveUsername = $this->DatabaseService->myAllDBUser($this->user);

		$dbArrRemoveUsername = $this->DatabaseService->myDBRemoveUseraname($this->user);

		$user = $this->user;



        $buttonURL = [

            'add-database' => route('owner::app::mariadb-add'),

            'add-user' => route('owner::app::mariadb-adduser'),

            'add-user-to-database' => route('owner::app::mariadb-addusertodatabase'),

            'revoke-user' => route('owner::app::mariadb-revokeuser'),

            'delete-database' => route('owner::app::mariadb-delete'),

            'delete-database-user' => route('owner::app::mariadb-delete-user'),

        ];




        return view(config('whop.themes') . '.shared.apps.mariadb')->with(compact(

            'progress',

            'dbArr',

            'dbUserArrRemoveUsername',

            'dbArrRemoveUsername',

            'buttonURL',

            'user'

            ));

    }


    public function store(AddDatabaseRequest $request)
    {

    	return $this->DatabaseService->create($request);

    }


    public function storeUser(AddDatabaseUserRequest $request)
    {

    	return $this->DatabaseService->createUser($this->user, $request);

    }


    public function addUserToDatabase(AddUserToDatabaseRequest $request)
    {

    	return $this->DatabaseService->assignUserToDatabase($request);

    }


    public function revokeUser(RevokeUserFromDatabaseRequest $request)
    {

        return $this->DatabaseService->revokeUser($request);

    }


    public function delete(DeleteDatabaseRequest $request)
    {

        return $this->DatabaseService->delete($request);

    }


    public function deleteUser(DeleteDatabaseUserRequest $request)
    {

        return $this->DatabaseService->deleteUser($request);
        
    }
}
