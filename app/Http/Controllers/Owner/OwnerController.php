<?php

namespace WHoP\Http\Controllers\Owner;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Services\ProgressService;
use WHoP\Services\PackageService;

use WHoP\Http\Requests\UpdateProfileNameRequest;
use WHoP\Http\Requests\UpdatePasswordRequest;

class OwnerController extends Controller
{
    public function index(ProgressService $ProgressService, PackageService $PackageService)
    {
    	$user = $this->user;

        $package = $PackageService->getUserPackage($this->user);

        $progress = $ProgressService->all($this->user);

        $lastLogin = $this->user->logintime->last() !== null ? date('d F Y', strtotime($this->user->logintime->last()->time->toDateTimeString())) : 'No record';

        $lastLoginHuman = $this->user->logintime->last() !== null ? '(' . $this->user->logintime->last()->time->diffForHumans() . ')' : '';

        $buttonURL = [

            'whoplet-manager' => route('owner::app::whopletmanager'),

            'whoplet-template-manager' => route('owner::app::whoplettemplatemanager'),

            'mariadb' => route('owner::app::mariadb'),

            'domain' => route('owner::app::domain'),

            'record' => route('owner::app::record'),

            'email' => route('owner::app::email'),

            'email-forwarding' => route('owner::app::email-forwarding'),

            'filemanager' => route('owner::app::filemanager'),

            'ftp' => route('owner::app::ftp'),

            'ssl' => route('owner::app::ssl'),

            'cron' => route('owner::app::cron'),

            'supervisor' => route('owner::app::supervisor'),

        ];




        return view(config('whop.themes') . '.shared.viewowner')->with(compact(

            'user',

            'lastLogin',

            'lastLoginHuman',

            'progress',

            'package',

            'buttonURL'
            
            )); 	
    }




    public function edit()
    {
        $user = $this->user;

        $urlList = [

            'updateName' => route('owner::profile-update-name'),

            'updatePassword' => route('owner::profile-update-password'),

        ];



        return view(config('whop.themes') . '.shared.editprofile', compact(

            'user',

            'urlList'

            ));
    }



    public function updateName(UpdateProfileNameRequest $request) {

        $this->user->name = $request->name;

        $this->user->save();



        return redirect()->back()->with([

            'successMessage' => sprintf('Succesfully update your name to %s.', $request->name),

            ]);
    }




    public function updatePassword(UpdatePasswordRequest $request)
    {
        $this->user->password = bcrypt($request->newPassword);
        $this->user->save();

        // event update password here
        event(new \WHoP\Events\OwnerUpdatePassword($this->user, $request));


        return redirect()->back()->with([

            'successMessage' => 'Succesfully update your account to new password',

            ]);
    }
}
