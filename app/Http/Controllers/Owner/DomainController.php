<?php

namespace WHoP\Http\Controllers\Owner;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Services\DomainService;
use WHoP\Services\ProgressService;

use WHoP\Http\Requests\AddDomainRequest;

use WHoP\User;
use WHoP\Domain;


class DomainController extends Controller
{
	private $DomainService;


	public function __construct(DomainService $DomainService)
	{

		parent::__construct();

		$this->DomainService = $DomainService;
        
	}


    public function index(ProgressService $ProgressService)
    {
        $progressDomain = $ProgressService->domain($this->user);

        $progress = [

            'domains' => $progressDomain,

        ];

    	$domains = $this->DomainService->getUserDomainList($this->user, app('SettingService')->getInt('OwnerDomainItemPerPage'));

        $buttonURL = [

            'add-domain' => route('owner::app::domain-add'),

        ];




    	return view(config('whop.themes') . '.shared.apps.domain')->with(compact(

            'domains',

            'buttonURL',

            'progress'
            
            ));
    }

    public function delete(Domain $domain)
    {

    	return $this->DomainService->deleteDomain($this->user, $domain);

    }


    public function store(AddDomainRequest $request)
    {
    	
    	return $this->DomainService->addDomain($this->user, $request);

    }
}
