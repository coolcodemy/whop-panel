<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Services\DomainService;
use WHoP\Services\ProgressService;

use WHoP\Http\Requests\AddDomainRequest;

use WHoP\User;
use WHoP\Domain;

class DomainController extends Controller
{
	private $DomainService;


	public function __construct(DomainService $DomainService)
	{

		$this->DomainService = $DomainService;
        
	}


    public function index(User $user, ProgressService $ProgressService)
    {
        $progressDomain = $ProgressService->domain($user);

        $progress = [

            'domains' => $progressDomain,

        ];

    	$domains = $this->DomainService->getUserDomainList($user, app('SettingService')->getInt('OwnerDomainItemPerPage'));

        $buttonURL = [

            'add-domain' => route('admin::app::domain-add', $user),

        ];




    	return view(config('whop.themes') . '.shared.apps.domain')->with(compact(

            'domains',

            'buttonURL',

            'progress'
            
            ));
    }

    public function delete(User $user, Domain $domain)
    {

    	return $this->DomainService->deleteDomain($user, $domain);

    }


    public function store(User $user, AddDomainRequest $request)
    {

    	return $this->DomainService->addDomain($user, $request);

    }
}
