<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\User;

class SslController extends Controller
{
    public function index(User $user)
    {

        $urlList = [

            'ssl-add' => route('admin::app::ssl-add', $user),
        ];




    	return view(config('whop.themes') . '.shared.apps.ssl')->with(compact(

    		'user',

    		'urlList'

    		));
        
    }


    public function create(User $user)
    {

    	return view(config('whop.themes') . '.shared.apps.ssl-create')->with(compact('user'));

    }
}
