<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Http\Requests\UpdatePanelSettingRequest;
use WHoP\Http\Requests\UpdateEmailSettingRequest;


use WHoP\Setting;

use WHoP\Services\SettingService;

class WhopSettingController extends Controller
{

	private $SettingService;


	public function __construct(SettingService $settingService)
	{

		$this->SettingService = $settingService;
	}



    
    public function panelSetting()
    {
    	$settings = Setting::all();



    	return view(config('whop.themes') . '.admin.panelsetting', compact(

    		'settings'

    		));
    }



    public function panelSettingUpdate(UpdatePanelSettingRequest $request)
    {

    	$this->SettingService->update($request);



    	return redirect()->back()->with([

    		'successMessage' => 'Succesfully save panel setting',

    		]);
    }




    public function emailSetting()
    {

    	return view(config('whop.themes') . '.admin.emailsetting');

    }



    public function emailSettingUpdate(UpdateEmailSettingRequest $request)
    {

    	$this->SettingService->updateEmail($request);



    	return redirect()->back()->with([

    		'successMessage' => 'Succesfully save email setting',

    		]);
    }




    public function sslSetting()
    {


    	return view(config('whop.themes') . '.admin.sslsetting');
    }



    public function autoUpdate()
    {
        $result = $this->SettingService->autoUpdate();



        if ( $result === "true" ) {
            
            $message = 'You have enabled auto update';

        } else {

            $message = 'Auto update have been disabled';

        }


        return response()->json([
            
            'message' => $message,

            ]);

    }
}
