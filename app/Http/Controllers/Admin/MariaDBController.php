<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Services\ProgressService;
use WHoP\Services\DatabaseService;

use WHoP\Http\Requests\AddDatabaseRequest;
use WHoP\Http\Requests\AddDatabaseUserRequest;
use WHoP\Http\Requests\AddUserToDatabaseRequest;
use WHoP\Http\Requests\RevokeUserFromDatabaseRequest;
use WHoP\Http\Requests\DeleteDatabaseRequest;
use WHoP\Http\Requests\DeleteDatabaseUserRequest;

use WHoP\User;

use DB;

class MariaDBController extends Controller
{
	private $DatabaseService;

	public function __construct(DatabaseService $DatabaseService)
	{

		$this->DatabaseService = $DatabaseService;

	}

    public function index(User $user, ProgressService $ProgressService)
    {
    	$progressDB = $ProgressService->db($user);

    	$progressDBUser = $ProgressService->dbUsers($user);

    	$progress = [

    		'db' => $progressDB,

    		'dbUsers' => $progressDBUser,

    	];

    	$db =  $this->DatabaseService->getUserDatabaseList($user);

    	$dbUser = $this->DatabaseService->getUserDatabaseAccountList($user);



		$dbArr = $this->DatabaseService->myDBWithUser($user);

		$dbUserArrRemoveUsername = $this->DatabaseService->myAllDBUser($user);

		$dbArrRemoveUsername = $this->DatabaseService->myDBRemoveUseraname($user);



        $buttonURL = [

            'add-database' => route('admin::app::mariadb-add', $user),

            'add-user' => route('admin::app::mariadb-adduser', $user),

            'add-user-to-database' => route('admin::app::mariadb-addusertodatabase', $user),

            'revoke-user' => route('admin::app::mariadb-revokeuser', $user),

            'delete-database' => route('admin::app::mariadb-delete', $user),

            'delete-database-user' => route('admin::app::mariadb-delete-user', $user),

        ];




        return view(config('whop.themes') . '.shared.apps.mariadb')->with(compact(

            'progress',

            'dbArr',

            'dbUserArrRemoveUsername',

            'dbArrRemoveUsername',

            'buttonURL',

            'user'

            ));

    }


    public function store(User $user, AddDatabaseRequest $request)
    {

    	return $this->DatabaseService->create($request);

    }


    public function storeUser(User $user, AddDatabaseUserRequest $request)
    {

    	return $this->DatabaseService->createUser($user, $request);

    }


    public function addUserToDatabase(User $user, AddUserToDatabaseRequest $request)
    {

    	return $this->DatabaseService->assignUserToDatabase($request);

    }


    public function revokeUser(User $user, RevokeUserFromDatabaseRequest $request)
    {

        return $this->DatabaseService->revokeUser($request);

    }


    public function delete(User $user, DeleteDatabaseRequest $request)
    {

        return $this->DatabaseService->delete($request);

    }


    public function deleteUser(User $user, DeleteDatabaseUserRequest $request)
    {

        return $this->DatabaseService->deleteUser($request);
        
    }
}
