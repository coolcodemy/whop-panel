<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Services\WhopletService;

use WHoP\Http\Requests\SelectWhopletTemplateRequest;
use WHoP\Http\Requests\WhopletUserTemplateStoreRequest;

use WHoP\Whoplet;
use WHoP\User;

class UserWhopletTemplateController extends Controller
{
	private $WhopletService;

	public function __construct(WhopletService $WhopletService)
	{

		$this->WhopletService = $WhopletService;

	}


    public function index(User $user)
    {
    	$templates = $this->WhopletService->getTemplateToBuild($user);

    	$urlList = [

    		'get-template-url' => route('admin::app::whoplettemplatemanager-gettemplate', $user),

    		'checkportion-url' => route('admin::app::whoplettemplatemanager-checkportion', $user),

    		'preview-url' => route('admin::app::whoplettemplatemanager-preview', $user),

    		'save-url' => route('admin::app::whoplettemplatemanager-store', $user),

    	];




    	return view(config('whop.themes') . '.shared.apps.whoplet-template')->with(compact(

    		'templates',

    		'user',

    		'urlList'

    		));
    }


    public function getTemplate(SelectWhopletTemplateRequest $request)
    {

    	return Whoplet::where('id', '=', $request->templateID)->first()->content;

    }


    public function checkPortion(User $user, Request $request)
    {

    	return $this->WhopletService->checkPortion($request);

    }


	public function preview(User $user, Request $request)
	{

		return $this->WhopletService->preview($user, $request);

	}


	public function store(User $user, WhopletUserTemplateStoreRequest $request)
	{

		$instance = $this->WhopletService->preview($user, $request);

		$type = gettype($instance);


		if ( $type === 'object' ) {

			return $instance;

		} else if ( ! $this->WhopletService->maxUserTemplate( $user ) ) {

			return response()->json([

				'message' => 'You have reached maximum template creation set by Admin',

			], 422);

		} else if ( $type === 'string' ) {
			
			$request->merge([

				'templateContent' => $instance,

				]);

			$this->WhopletService->createTemplate( $request, $user );
		}



		return response()->json([

			'message' => sprintf('Succesfully save template %s', $request->templateName),

			], 200);
	}


	public function delete(User $user, Whoplet $template)
	{
        $template->delete();

        return redirect()->back()->with([

            'successMessage' => sprintf('Succesfully delete template %s.', $template->name),

            ]);
	}
}
