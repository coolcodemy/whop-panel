<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Services\RecordService;
use WHoP\Services\DomainService;

use WHoP\Http\Requests\AddRecordRequest;
use WHoP\Http\Requests\EditRecordRequest;

use WHoP\User;
use WHoP\Domain;
use WHoP\Record;

class RecordController extends Controller
{
    private $RecordService, $DomainService;

    public function __construct(RecordService $RecordService, DomainService $DomainService)
    {

    	$this->RecordService = $RecordService;

    	$this->DomainService = $DomainService;

    }


    public function index(User $user)
    {
        $urlList = [

            'record-create' => route('admin::app::record-create', $user),

        ];

    	$domains = $this->DomainService->getUserDomainList($user);




    	return view(config('whop.themes') . '.shared.apps.record')->with(compact(

    		'domains',

    		'user',

    		'urlList'

    		));
    }

    public function create(User $user)
    {
        $url =  route('admin::app::record-store', $user);

        $domains = $this->DomainService->getUserDomainList($user);

        $types = $this->recordType;

        $title = 'Add Record';

        $header = 'Add New Record';



        return view(config('whop.themes') . '.shared.apps.record-create-edit')->with(compact(

            'domains',

            'user',

            'types',

            'title',

            'header',

            'url'

            ));
    }



    public function myRecord(User $user, Domain $domain)
    {
    	$modifier = [
    		'type' => ['!=', 'SOA'],
    	];

    	$records =  $this->RecordService->getRecordsByDomainID($domain->id, $modifier);

        $recordsWithURL = [];

        foreach ($records as $record) {

            $record->editURL = route('admin::app::record-edit', [$user, $record]);

            $record->deleteURL = route('admin::app::record-delete', [$user, $record]);

            $recordsWithURL[] = $record;

        }


        return $recordsWithURL;
    }



    public function store(User $user, AddRecordRequest $request)
    {

    	$record = $this->RecordService->createRecordByRequest($request);



    	return redirect()->back()->with([

    		'successMessage' => 'Succesfully add new record.',

    		]);
    }



    public function edit(User $user, Record $record)
    {

        $url = route('admin::app::record-update', [$user, $record]);

        $record->name = rewriteRecordName( $record->name, $record->domain->name );

        $domains = $this->DomainService->getUserDomainList($user);

        $types = $this->recordType;

        $title = 'Edit Record';

        $header = 'Edit Record';

        $editMode = true;





        return view(config('whop.themes') . '.shared.apps.record-create-edit')->with(compact(

            'record',

            'domains',

            'types',

            'title',

            'header',

            'url',

            'editMode'

            ));
    }


    public function update(User $user, Record $record, EditRecordRequest $request)
    {

        $record = $this->RecordService->updateRecordByRequest($record, $request);




        return redirect()->back()->with([

            'successMessage' => 'Succesfully update record.',

            ]);
    }


    public function delete(User $user, Record $record)
    {

        return $this->RecordService->delete($user, $record);

    }

}
