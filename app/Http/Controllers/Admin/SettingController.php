<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;


class SettingController extends Controller
{
    public function index()
    {
        return view(config('whop.themes') . '.admin.serversetting')->with([

            'title' => 'Server Setting'

            ]);
    }


    public function dovecot()
    {

        return view(config('whop.themes') . '.admin.serversetting.dovecot');

    }

    public function mariadb()
    {

        return view(config('whop.themes') . '.admin.serversetting.mariadb');

    }

    public function php()
    {

        return view(config('whop.themes') . '.admin.serversetting.php');

    }

    public function postfix()
    {

        return view(config('whop.themes') . '.admin.serversetting.postfix');

    }

    public function powerdns()
    {
        return view(config('whop.themes') . '.admin.serversetting.powerdns');
    }

    public function pureftpd()
    {

        return view(config('whop.themes') . '.admin.serversetting.pureftpd');

    }

    public function spamassassin()
    {

        return view(config('whop.themes') . '.admin.serversetting.spamassassin');

    }

    public function ssh()
    {

        return view(config('whop.themes') . '.admin.serversetting.ssh');

    }

    public function nginx()
    {

        return view(config('whop.themes') . '.admin.serversetting.nginx');

    }

    public function csf()
    {

        return view(config('whop.themes') . '.admin.serversetting.csf');

    }

    public function fail2ban()
    {

        return view(config('whop.themes') . '.admin.serversetting.fail2ban');
        
    }
}
