<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Http\Requests\UpdateProfileNameRequest;
use WHoP\Http\Requests\UpdatePasswordRequest;


class AdminController extends Controller
{
    public function dashboard()
    {
    	return view(config('whop.themes') . '.admin.dashboard');
    }



    public function profile()
    {

        $user = $this->user;

        $urlList = [

            'updateName' => route('admin::profile-update-name'),

            'updatePassword' => route('admin::profile-update-password'),

        ];



        return view(config('whop.themes') . '.shared.editprofile', compact(

            'user',

            'urlList'

            ));
    }



    public function updateName(UpdateProfileNameRequest $request) {

        $this->user->name = $request->name;

        $this->user->save();



        return redirect()->back()->with([

            'successMessage' => sprintf('Succesfully update your name to %s.', $request->name),

            ]);
    }




    public function updatePassword(UpdatePasswordRequest $request)
    {
        $this->user->password = bcrypt($request->newPassword);
        $this->user->save();


        return redirect()->back()->with([

            'successMessage' => 'Succesfully update your account to new password',

            ]);
    }
}
