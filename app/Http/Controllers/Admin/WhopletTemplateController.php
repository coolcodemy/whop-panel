<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Http\Requests\CreateWhopletTemplateFormRequest;
use WHoP\Http\Requests\UpdateWhopletTemplateFormRequest;

use WHoP\Services\WhopletService;

use WHoP\Whoplet;


class WhopletTemplateController extends Controller
{
    private $WhopletService;


    public function __construct(WhopletService $WhopletService)
    {

        $this->WhopletService = $WhopletService;

    }

    public function index()
    {
        $templates = $this->WhopletService->myTemplates(auth()->user());



        return view(config('whop.themes') . '.admin.templatemanager')->with(compact(

            'templates'

            ));
    }


    public function skeleton(Request $request)
    {

        return $this->WhopletService->skeleton($request);

    }


    public function show(Whoplet $whoplet)
    {

        return $whoplet;

    }


    public function store(CreateWhopletTemplateFormRequest $request)
    {
        $this->WhopletService->createTemplate($request, auth()->user());



        return response()->json([

            'success' => true,

            'message' => sprintf('Succesfully create WHoPlet Template %s. Reloading page...', $request->templateName),

            ]);
    }


    public function update(UpdateWhopletTemplateFormRequest $request)
    {
        $this->WhopletService->updateTemplate($request, auth()->user());



        return response()->json([

            'success' => true,

            'message' => sprintf('Succesfully update WHoPlet Template %s', $request->templateName),

            ]);
    }


    public function enableDisable(Whoplet $template, $status)
    {

        $this->WhopletService->enableDisable($template, $status);



        return redirect()->back();
    }


    public function delete(Whoplet $template)
    {

        $template->delete();



        return redirect()->back()->with([

            'successMessage' => sprintf('Succesfully delete template %s.', $template->name),
            
            ]);
    }
}
