<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Services\FtpService;

use WHoP\Http\Requests\AddFtpAccountRequest;
use WHoP\Http\Requests\EditFtpuserRequest;

use WHoP\User;
use WHoP\FtpUser;


class FtpController extends Controller
{
	private $FtpService;



	public function __construct(FtpService $FtpService)
	{

		$this->FtpService = $FtpService;

	}


    public function index(User $user)
    {
    	$ftpAccounts = $this->FtpService->getUserFtpUserList($user, app('SettingService')->getInt('OwnerFtpItemPerPage'));

        $urlList = [

            'ftp-create' => route('admin::app::ftp-create', $user),

        ];

    	


    	return view(config('whop.themes') . '.shared.apps.ftp')->with(compact(

    		'ftpAccounts',

    		'urlList'

    		));
    }


    public function create(User $user)
    {

        $urlList = [

            'action' => route('admin::app::ftp-store', $user),

        ];

    	$ftpDomains = $this->FtpService->getFtpDomains($user);

    	$ftpQuota = $user->userpackage->ftpQuota;

    	$totalQuota = $this->FtpService->getUserFtpQuotaSum($user);

    	$maxQuota = ($ftpQuota== 0) ? 'Unlimited' : $ftpQuota - $totalQuota;

        $buttonText = 'Add';



    	return view(config('whop.themes') . '.shared.apps.ftp-create-edit')->with(compact(

    		'urlList',

    		'user',

    		'ftpDomains',

    		'ftpQuota',

    		'maxQuota',

    		'buttonText'

    		));

    }


    public function store(User $user, AddFtpAccountRequest $request)
    {
    	$ftpUser = $this->FtpService->create($user, $request);

    	return redirect()->back()->with([

    		'successMessage' => sprintf('Succesfully add new ftp user %s', $ftpUser->username),

    		]);
    }


    public function edit(User $user, FtpUser $ftpUser)
    {

        $urlList  = [

            'action' => route('admin::app::ftp-update', [$user, $ftpUser]),

        ];

    	$ftpDomains = $this->FtpService->getFtpDomains($user);

    	$ftpQuota = $user->userpackage->ftpQuota;

    	$totalQuota = $this->FtpService->getUserFtpQuotaSum($user);

    	$maxQuota = ($ftpQuota== 0) ? 'Unlimited' : $ftpQuota - $totalQuota;

    	// Hack to display only username
		$ftpUser->username = rewriteFTPUsername($ftpUser);

        $buttonText = 'Update';

        $editMode = true;




    	return view(config('whop.themes') . '.shared.apps.ftp-create-edit')->with(compact(

    		'urlList',

    		'user',

    		'ftpDomains',

    		'ftpQuota',

    		'maxQuota',

    		'ftpUser',

    		'buttonText',

    		'editMode'

    		));
    }


    public function update(User $user, FtpUser $ftpUser, EditFtpuserRequest $request)
    {

    	$this->FtpService->update($ftpUser, $request);



    	return redirect()->back()->with([

    		'successMessage' => sprintf('Succesfully update %s FTP account.', $ftpUser->username),

    		]);

    }


    public function enableDisable(User $user, FtpUser $ftpUser)
    {

    	$this->FtpService->enableDisable($ftpUser);

    	return redirect()->back();

    }


    public function destroy(User $user, FtpUser $ftpUser)
    {

    	$ftpUser->delete();


    	return redirect()->back()->with([

    		'successMessage' => sprintf('Succesfully delete ftp account %s', $ftpUser->username),
            
    		]);

    }
}
