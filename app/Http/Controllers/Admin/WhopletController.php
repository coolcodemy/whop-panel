<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Http\Requests\SelectWhopletTemplateRequest;
use WHoP\Http\Requests\BuildWhopletRequest;

use WHoP\Services\UserService;
use WHoP\Services\WhopletService;

use WHoP\User;

class WhopletController extends Controller
{
	private $UserService, $WhopletService;


	public function __construct(UserService $UserService, WhopletService $WhopletService)
	{

		$this->UserService = $UserService;

		$this->WhopletService = $WhopletService;

	}

    public function index(User $user)
    {
    	$urlList = [

            'getTemplateForm' => route('admin::app::whopletformbuilder', $user),

            'buildWhoplet' => route('admin::app::buildwhoplet', $user),

            'streamurl' => route('admin::app::whopletlog', $user),

        ];

        $ssl = $user->userpackage->enableSsl;

        $templates = $this->WhopletService->whopletGeneratorTemplatesList($user);

        $channel = app('SocketService')->generateChannel();





    	return view(config('whop.themes') . '.shared.apps.whoplet')->with(compact(

    		'ssl',

    		'templates',

    		'urlList',

    		'user',

    		'channel'

    		));

    }


    public function whopletFormBuilder(User $user, SelectWhopletTemplateRequest $request)
    {

    	return $this->WhopletService->formGenerator($request->templateID, $user);

    }


    public function buildWhoplet(User $user, BuildWhopletRequest $request)
    {

    	$this->WhopletService->buildWhoplet($request, $user);

    }


    public function streamLog(User $user)
    {

        return view(config('whop.themes') . '.shared.apps.whoplet-log-stream')->with(compact('user'));

    }
}
