<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;


use WHoP\Http\Requests\CreateOwnerFormRequest;

use WHoP\Http\Requests\UpdatePasswordRequest;
use WHoP\Http\Requests\UpdateProfileNameRequest;
use WHoP\Http\Requests\UpdateOwnerPackageRequest;
use WHoP\Http\Requests\DeleteOwnerRequest;


use WHoP\Services\UserService;
use WHoP\Services\SocketService;
use WHoP\Services\ProgressService;
use WHoP\Services\PackageService;

use WHoP\User;

class OwnerController extends Controller
{
    private $UserService;


    public function __construct(UserService $UserService)
    {

        $this->UserService = $UserService;

    }


    public function index()
    {

        $users = $this->UserService->getUsersByRole('owner');

        $createURL = 'admin::createowner';




    	return view(config('whop.themes') . '.shared.listowner')->with(compact(

            'users',

            'createURL'

            ));
    }


    public function create(SocketService $SocketService)
    {

        $channel = $SocketService->generateChannel();

        $storeURL = 'admin::storeowner';



    	return view(config('whop.themes') . '.shared.addowner')->with(compact(

            'channel',

            'storeURL'

            ));
    }


    public function store(CreateOwnerFormRequest $request)
    {

        $this->UserService->createOwner($request);



        return response()->json([

            'success' => true,

            'message' => sprintf('Please wait while adding %s as system user....', $request->input('name')),

            ]);
    }


    public function show(User $user, ProgressService $ProgressService, PackageService $PackageService)
    {
        $package = $PackageService->getUserPackage($user);

        $progress = $ProgressService->all($user);

        $lastLogin = $user->logintime->last() !== null ? date('d F Y', strtotime($user->logintime->last()->time->toDateTimeString())) : 'No record';

        $lastLoginHuman = $user->logintime->last() !== null ? '(' . $user->logintime->last()->time->diffForHumans() . ')' : '';

        $buttonURL = [

            'whoplet-manager' => route('admin::app::whopletmanager', $user),

            'whoplet-template-manager' => route('admin::app::whoplettemplatemanager', $user),

            'mariadb' => route('admin::app::mariadb', $user),

            'domain' => route('admin::app::domain', $user),

            'record' => route('admin::app::record', $user),

            'email' => route('admin::app::email', $user),

            'email-forwarding' => route('admin::app::email-forwarding', $user),

            'filemanager' => route('admin::app::filemanager', $user),

            'ftp' => route('admin::app::ftp', $user),

            'ssl' => route('admin::app::ssl', $user),

            'cron' => route('admin::app::cron', $user),

            'supervisor' => route('admin::app::supervisor', $user),

        ];




        return view(config('whop.themes') . '.shared.viewowner')->with(compact(

            'user',

            'lastLogin',

            'lastLoginHuman',

            'progress',

            'package',

            'buttonURL'
            
            ));
    }






    public function edit(User $user)
    {

        $urlList = [

            'updateName' => route('admin::ownerprofile-update-name', $user),

            'updatePassword' => route('admin::ownerprofile-update-password', $user),

        ];



        return view(config('whop.themes') . '.shared.editprofile', compact(

            'user',

            'urlList'

            ));
    }



    public function updateName(User $user, UpdateProfileNameRequest $request) {

        $user->name = $request->name;

        $user->save();



        return redirect()->back()->with([

            'successMessage' => sprintf('Succesfully update domain owner name to %s.', $request->name),

            ]);
    }




    public function updatePassword(User $user, UpdatePasswordRequest $request)
    {
        $user->password = bcrypt($request->newPassword);
        $user->save();

        // event update password here
        event(new \WHoP\Events\OwnerUpdatePassword($user, $request));


        return redirect()->back()->with([

            'successMessage' => sprintf('Succesfully update password for %s.', $user->name),

            ]);
    }





    public function phpSetting(User $user)
    {

        return view(config('whop.themes') . '.shared.ownerphpsetting', compact(

            'user'

            ));
    }



    public function packageSetting(User $user)
    {
        $package = $user->userpackage;

        $packageOriginal = $user->userpackage->package;





        return view(config('whop.themes') . '.shared.ownerpackagesetting', compact(

            'user',

            'package',

            'packageOriginal'

            ));
    }




    public function packageSettingUpdate(User $user, UpdateOwnerPackageRequest $request, PackageService $packageService)
    {

        $userPackage = $packageService->updateUserPackage($user, $request);




        return redirect()->back()->with([

            'successMessage' => sprintf('Succesfully updated %s package setting.', $user->username),

            ]);
    }




    public function getOwnerStatus(User $user)
    {

        return response()->json([

            'disable' => $user->disable,

            ]);
    }



    public function disableAccount(User $user)
    {
        $this->UserService->disableAccount($user);




        return response()->json([

            'message' => sprintf('Succesfully disabled %s account.', $user->username),

            ]);
    }




    public function enableAccount(User $user)
    {
        $this->UserService->enableAccount($user);




        return response()->json([

            'message' => sprintf('Succesfully enabled %s account.', $user->username),

            ]);
    }



    public function resetAccount(User $user)
    {

        $this->UserService->resetAccount($user);


        return response()->json([

            'message' => sprintf('Succesfully reset %s account.', $user->username),

            ]);
    }



    public function delete(User $user, DeleteOwnerRequest $request)
    {
        
        $this->UserService->delete($user);



        return response()->json([

            'message' => sprintf('Successfully deleted %s.', $user->name),

            ]);
    }
}
