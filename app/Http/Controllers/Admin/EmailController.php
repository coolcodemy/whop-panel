<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Services\MailService;
use WHoP\Services\ProgressService;

use WHoP\Http\Requests\AddEmailRequest;
use WHoP\Http\Requests\EditEmailRequest;

use WHoP\User;
use WHoP\MailUser;

class EmailController extends Controller
{
	private $MailService;


    public function __construct(MailService $MailService)
    {

    	$this->MailService = $MailService;

    }


    public function index(User $user, ProgressService $ProgressService)
    {
        $urlList = [

            'email-create' => route('admin::app::email-create', $user),

        ];

    	$progress = [
    		'mailUsers' => $ProgressService->mailUsers($user),
    		'mailQuota' => $ProgressService->mailQuota($user),
    	];

    	$emails = $this->MailService->getUserMailAccountList($user, app('SettingService')->getInt('OwnerMailItemPerPage'));

    	return view(config('whop.themes') . '.shared.apps.email')->with(compact(

            'urlList',

            'progress',

            'emails'

            ));
    }

    public function create(User $user)
    {

        $url = route('admin::app::email-store', $user);

    	$mailDomains = $this->MailService->getDomain($user);

    	$emailQuota = $user->userpackage->emailQuota;

    	$totalQuota = $this->MailService->getUserMailQuota($user);

    	$maxQuota = ($emailQuota== 0) ? 'Unlimited' : $emailQuota - $totalQuota;

        $title = 'Add Account';

        $header = 'Add New Email Account';

        $buttonText = 'Add';



    	return view(config('whop.themes') . '.shared.apps.email-create-edit')->with(compact(

    		'mailDomains',

    		'emailQuota',

    		'maxQuota',

            'title',

            'header',

            'url',

            'buttonText'

    		));

    }


    public function store(User $user, AddEmailRequest $request)
    {

    	$email = $this->MailService->newEmail($user, $request);



    	return redirect()->back()->with([

    		'successMessage' => sprintf('Succesfully add new email %s.', $email->email),

    		]);

    }


    public function edit(User $user, MailUser $email)
    {

        $url = route('admin::app::email-update', [$user, $email]);

        $mailDomains = $this->MailService->getDomain($user);

        $emailQuota = $user->userpackage->emailQuota;

        $totalQuota = $this->MailService->getUserMailQuota($user);

        $maxQuota = ($emailQuota== 0) ? 'Unlimited' : $emailQuota - $totalQuota + $email->quota;

        // Add username
        $email->username = explode('@', $email->email)[0];

        $title = 'Edit Account';

        $header = 'Edit Email Account';

        $buttonText = 'Update';

        $disabled = true;

        return view(config('whop.themes') . '.shared.apps.email-create-edit')->with(compact(

            'mailDomains',

            'emailQuota',

            'maxQuota',

            'title',

            'header',

            'url',

            'buttonText',

            'email',

            'disabled'

            ));
    }


    public function update(User $user, MailUser $email, EditEmailRequest $request)
    {

        $email = $this->MailService->update($email, $request);


        return redirect()->back()->with([

            'successMessage' => sprintf('Succesfully update email %s.', $email->email),

            ]);

    }


    public function delete(User $user, MailUser $email)
    {
        $email->delete();

        $this->MailService->deleteSystemEmail($user, $email);



        return redirect()->back()->with([

            'successMessage' => sprintf('Succesfully delete email account %s.', $email->email),
            
            ]);

    }


    public function enableDisable(User $user, MailUser $email)
    {

        $this->MailService->enableDisable($email);

        return redirect()->back();

    }
}
