<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\User;

class SupervisorController extends Controller
{
    public function index(User $user)
    {

        $urlList = [

            'supervisor-add' => route('admin::app::supervisor-add', $user),

        ];

    	return view(config('whop.themes') . '.shared.apps.supervisor')->with(compact(

    		'user',

    		'urlList'

    		));
    }


    public function create(User $user)
    {

    	return view(config('whop.themes') . '.shared.apps.supervisor-create-edit')->with(compact('user'));
        
    }
}
