<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Http\Requests\AddEmailForwardingRequest;

use WHoP\User;
use WHoP\MailForwarding;

use WHoP\Services\MailService;

class EmailForwardingController extends Controller
{
	private $MailService;


	public function __construct(MailService $MailService)
	{

		$this->MailService = $MailService;

	}


    public function index(User $user)
    {
    	$forwardings = $this->MailService->getForwardings($user, app('SettingService')->getInt('OwnerMailForwardingItemPerPage'));

        $buttonURL = [

            'create' => route('admin::app::email-forwarding-create', $user),

        ];



    	return view(config('whop.themes') . '.shared.apps.emailforwarding')->with(compact(

    		'forwardings',

            'buttonURL'

    		));
    }


    public function create(User $user)
    {
        $mailDomains = $this->MailService->getDomain($user);

        $urlList = [

            'submit' => route('admin::app::email-forwarding-store', $user),

        ];



        return view(config('whop.themes') . '.shared.apps.emailforwarding-create')->with(compact(

            'mailDomains',

            'urlList'
            ));
    }


    public function store(User $user, AddEmailForwardingRequest $request)
    {

        $this->MailService->createForwarding($user, $request);



        return redirect()->back()->with([

            'successMessage' => sprintf('Succesfully forward email %s to %s.', $request->emailFrom, $request->destinationTo),
            
            ]);
    }


    public function enableDisable(User $user, MailForwarding $forwarding)
    {

        $this->MailService->enableDisableForwarding($forwarding);


        return redirect()->back();
    }


    public function destroy(User $user, MailForwarding $forwarding)
    {

        $forwarding->delete();


        return redirect()->back()->with([

            'successMessage' => sprintf('Succesfully delete forwarding from %s to %s', $forwarding->source, $forwarding->destination),

            ]);
    }
}
