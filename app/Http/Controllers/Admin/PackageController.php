<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Http\Requests\CreatePackageFormRequest;
use WHoP\Http\Requests\UpdatePackageFormRequest;

use WHoP\Services\PackageService;
use WHoP\Services\StaticService;

use WHoP\Package;

class PackageController extends Controller
{
    private $PackageService, $StaticService;


    public function __construct(PackageService $PackageService, StaticService $StaticService)
    {

        $this->PackageService = $PackageService;

        $this->StaticService = $StaticService;

    }

    public function index()
    {
        $buttonURL = [

            'delete' => 'admin::deletepackage',

            'create' => 'admin::createpackage',

            'edit' => 'admin::editpackage',

        ];


        $packages = $this->PackageService->paginate(4);



    	return view(config('whop.themes') . '.shared.listpackage')->with(compact(

    		'packages',

            'buttonURL'

    		));
    }


    public function create()
    {
        $day = $this->StaticService->validityDay();

        $urlList = [

            'store' => 'admin::storepackage',

        ];

        return view(config('whop.themes') . '.shared.addpackage')->with(compact(

            'day',

            'urlList'

            ));
    }


    public function store(CreatePackageFormRequest $request)
    {

        $this->PackageService->createOrUpdate($request);


        return redirect()->route('admin::packagelist')->with([

            'toastMessage' => sprintf('Successfully add package %s', $request->input('name')),

            ]);

    }


    public function edit(Package $package)
    {
        $urlList = [

            'edit' => 'admin::updatepackage',

        ];

        $day = $this->StaticService->validityDay();

        $valid = explode(' ', $package->validFor);



        return view(config('whop.themes') . '.shared.editpackage')->with(compact(

            'package',

            'day',

            'urlList',

            'valid'

            ));
    }



    public function update(Package $package, UpdatePackageFormRequest $request)
    {
        $this->PackageService->createOrUpdate($request);



        return redirect()->route('admin::packagelist')->with([

            'toastMessage' => sprintf('Successfully update package %s', $request->input('name')),

            ]);
    }



    public function destroy(Package $package)
    {
        $package->delete();



        return redirect()->back()->with([

            'toastMessage' => sprintf('Succesfully delete package %s', $package->name),

            ]);
    }


    public function chart()
    {

        return $this->PackageService->usageChart();
        
    }
}
