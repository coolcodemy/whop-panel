<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\User;

class FileManagerController extends Controller
{
    public function index(User $user)
    {

        $urlList = [

            'editor' => route('admin::app::filemanager-editor', $user),

        ];



    	return view(config('whop.themes') . '.shared.apps.filemanager')->with(compact(
    		'user',

    		'urlList'

    		));
    }


    public function editor(User $user, $location = null, $file = null)
    {
        $file = rewriteEditorFilePath($file);

    	return view(config('whop.themes') . '.shared.apps.filemanager-editor')->with(compact(
    		'user',

    		'location',

    		'file'
            
    		));
    }
}
