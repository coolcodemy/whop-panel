<?php

namespace WHoP\Http\Controllers\Admin;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\User;

class CronController extends Controller
{
    public function index(User $user)
    {
        $urlList = [

            'cron-add' => route('admin::app::cron-add', $user),

        ];


    	return view(config('whop.themes') . '.shared.apps.cron')->with(compact(

            'user',

            'urlList'
            
            ));
    }


    public function create(User $user)
    {
    	return view(config('whop.themes') . '.shared.apps.cron-create')->with(compact('user'));
    }
}
