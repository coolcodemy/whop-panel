<?php

namespace WHoP\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController
{
    use DispatchesJobs, ValidatesRequests;


    protected $recordType = ['A', 'AAAA', 'CNAME', 'MX', 'NS', 'SPF', 'TXT'];


    protected $user;


    public function __construct()
    {
    	$this->user = auth()->user();
    }
}
