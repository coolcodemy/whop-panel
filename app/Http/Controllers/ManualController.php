<?php

namespace WHoP\Http\Controllers;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

class ManualController extends Controller
{
    public function index()
    {


    	return view(config('whop.themes') . '.manual');
    }
}
