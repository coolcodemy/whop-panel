<?php

namespace WHoP\Http\Controllers;

use Illuminate\Http\Request;

use WHoP\Http\Requests;
use WHoP\Http\Controllers\Controller;

use WHoP\Services\UserService;

use PRedis;


class AuthController extends Controller
{


    public function index()
    {
    	return view(config('whop.themes') . '.index');
    }



    public function login(Request $request, UserService $UserService)
    {

        if ( $UserService->login( $request->username, $request->password ) ) {

            PRedis::del('bruteforce-' . $request->getClientIp());

            if (auth()->user()->hasRole('admin')) {

                $redirect = redirect()->intended(route('admin::dashboard'))->getTargetUrl();

            } else if ( auth()->user()->hasRole('owner') ) {

                $redirect = redirect()->intended(route('owner::dashboard'))->getTargetUrl();

            }

            $ip = $request->getClientIp(true);


            event(new \WHoP\Events\UserLoggedIn(auth()->user(), $ip));
            


            return response()->json([

                'message' => sprintf('Welcome %s. Redirecting...', auth()->user()->name),

                'redirect' => $redirect,

            ], 200);

        }



        return response()->json([

            'message' => sprintf('Unable to login with username %s', $request->input('username')),

        ], 422);
    }




    public function logout()
    {

        $user = auth()->user();

        PRedis::del('whop:user:' . $user->username);

    	auth()->logout();
        



    	return redirect()->route('guest::login');

    }
}
