<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['as' => 'guest::', 'middleware' => 'guest'], function()
{
	Route::get('/', ['as' => 'login', 'uses' => 'AuthController@index']);
	Route::post('/', ['middleware' => 'bruteforceprotection', 'as' => 'attemptlogin', 'uses' => 'AuthController@login']);
});


Route::group(['middleware' => 'auth'], function()
{
	Route::get('logout', ['as' => 'logout', 'uses' => 'AuthController@logout']);
	Route::get('manual', ['as' => 'manual', 'uses' => 'ManualController@index']);
});



Route::group(['prefix' => 'admin', 'as' => 'admin::', 'middleware' => array('auth', 'admin'), 'namespace' => 'Admin'], function()
{
	Route::get('/', ['as' => 'dashboard', 'uses' => 'AdminController@dashboard']);

	Route::get('profile', ['as' => 'profile', 'uses' => 'AdminController@profile']);
	Route::post('profile/updatename', ['as' => 'profile-update-name', 'uses' => 'AdminController@updateName']);
	Route::post('profile/updatepassword', ['as' => 'profile-update-password', 'uses' => 'AdminController@updatePassword']);

	/**
	 * Owner Part
	 */
	Route::get('owner', ['as' => 'ownerlist', 'uses' => 'OwnerController@index']);
	Route::get('owner/create', ['as' => 'createowner', 'uses' => 'OwnerController@create']);
	Route::post('owner/store', ['as' => 'storeowner', 'uses' => 'OwnerController@store']);

	Route::group(['middleware' => 'usermustbedomainowner', 'prefix' => 'owner/{user}'], function()
	{
		Route::get('view', ['as' => 'viewowner', 'uses' => 'OwnerController@show']);

		/**
		 * Profile
		 */
		Route::get('profile', ['as' => 'ownerprofile', 'uses' => 'OwnerController@edit']);
		Route::post('profile/name', ['as' => 'ownerprofile-update-name', 'uses' => 'OwnerController@updateName']);
		Route::post('profile/password', ['as' => 'ownerprofile-update-password', 'uses' => 'OwnerController@updatePassword']);


		/**
		 * Setting Part
		 */
		Route::get('phpsetting', ['as' => 'ownerphpsetting', 'uses' => 'OwnerController@phpSetting']);
		Route::get('packagesetting', ['as' => 'ownerpackagesetting', 'uses' => 'OwnerController@packageSetting']);
		Route::post('packagesetting', ['as' => 'ownerpackagesetting-update', 'uses' => 'OwnerController@packageSettingUpdate']);

		/**
		 * Reset
		 */
		Route::get('resetaccount', ['as' => 'ownerresetaccount', 'uses' => 'OwnerController@resetAccount']);

		/**
		 * Enable Disable
		 */
		Route::get('getownerstatus', ['as' => 'ownerstatus', 'uses' => 'OwnerController@getOwnerStatus']);
		Route::get('disableaccount', ['middleware' => 'useraccountmustbeenable', 'as' => 'ownerdisableaccount', 'uses' => 'OwnerController@disableAccount']);
		Route::get('enableaccount', ['middleware' => 'useraccountmustbedisable', 'as' => 'ownerenableaccount', 'uses' => 'OwnerController@enableAccount']);



		/**
		 * Delete
		 */
		Route::post('delete', ['middleware' => 'deleteowner', 'as' => 'ownerdelete', 'uses' => 'OwnerController@delete']);
		

		Route::group(['as' => 'app::', 'prefix' => 'apps'], function()
		{
			/**
			 * WHoPlet Part
			 */
			Route::get('whoplet-manager', ['as' => 'whopletmanager', 'uses' => 'WhopletController@index']);
			Route::post('whoplet-manager/templatechooser', ['as' => 'whopletformbuilder', 'uses' => 'WhopletController@whopletFormBuilder']);
			Route::post('whoplet-manager/build', ['as' => 'buildwhoplet', 'uses' => 'WhopletController@buildWhoplet']);
			Route::get('whoplet-manager/log/streaming', ['as' => 'whopletlog', 'uses' => 'WhopletController@streamLog']);

			/**
			 * WHoPlet Template Part
			 */
			Route::get('whoplet-template-manager', ['as' => 'whoplettemplatemanager', 'uses' => 'UserWhopletTemplateController@index']);
			Route::post('whoplet-template-manager/checkportion', ['as' => 'whoplettemplatemanager-checkportion', 'uses' => 'UserWhopletTemplateController@checkPortion']);
			Route::post('whoplet-template-manager/{whoplet}/delete', ['middleware' => 'templatemustbeownbyowner', 'as' => 'whoplettemplatemanager-delete', 'uses' => 'UserWhopletTemplateController@delete']);
			Route::group(['middleware' => 'templatemustbeownbyadminoruser'], function()
			{
				Route::post('whoplet-template-manager/gettemplate', ['as' => 'whoplettemplatemanager-gettemplate', 'uses' => 'UserWhopletTemplateController@getTemplate']);
				Route::post('whoplet-template-manager/preview', ['as' => 'whoplettemplatemanager-preview', 'uses' => 'UserWhopletTemplateController@preview']);
				Route::post('whoplet-template-manager/store', ['as' => 'whoplettemplatemanager-store', 'uses' => 'UserWhopletTemplateController@store']);
			});

			/**
			 * MariaDB
			 */
			Route::get('mariadb', ['as' => 'mariadb', 'uses' => 'MariaDBController@index']);
			Route::post('mariadb/adddatabase', ['as' => 'mariadb-add', 'uses' => 'MariaDBController@store']);
			Route::post('mariadb/adduser', ['as' => 'mariadb-adduser', 'uses' => 'MariaDBController@storeUser']);
			Route::post('mariadb/addusertodatabase', ['as' => 'mariadb-addusertodatabase', 'uses' => 'MariaDBController@addUserToDatabase']);
			Route::post('mariadb/revokedatabaseuser', ['as' => 'mariadb-revokeuser', 'uses' => 'MariaDBController@revokeUser']);
			Route::post('mariadb/deletedatabase', ['as' => 'mariadb-delete', 'uses' => 'MariaDBController@delete']);
			Route::post('mariadb/deletedatabaseuser', ['as' => 'mariadb-delete-user', 'uses' => 'MariaDBController@deleteUser']);


			/**
			 * Domain
			 */
			Route::get('domain', ['as' => 'domain', 'uses' => 'DomainController@index']);
			Route::post('domain/{domain}/delete', ['middleware' => 'domainmustbelongtoowner', 'as' => 'domain-delete', 'uses' => 'DomainController@delete']);
			Route::post('domain/add', ['as' => 'domain-add', 'uses' => 'DomainController@store']);

			/**
			 * Record
			 */
			Route::get('record', ['as' => 'record', 'uses' => 'RecordController@index']);
			Route::get('record/create', ['as' => 'record-create', 'uses' => 'RecordController@create']);
			Route::post('record/domain/{domain}/domainrecord', ['middleware' => 'domainmustbelongtoowner', 'as' => 'record-all', 'uses' => 'RecordController@myRecord']);
			Route::post('record/store', ['as' => 'record-store', 'uses' => 'RecordController@store']);
			Route::group(['middleware' => 'recordmustbelongtoowner'], function()
			{
				Route::get('record/{record}/edit', ['as' => 'record-edit', 'uses' => 'RecordController@edit']);
				Route::post('record/{record}/update', ['as' => 'record-update', 'uses' => 'RecordController@update']);
				Route::post('record/{record}/delete', ['as' => 'record-delete', 'uses' => 'RecordController@delete']);
			});


			/**
			 * Email
			 */
			Route::get('email', ['as' => 'email', 'uses' => 'EmailController@index']);
			Route::get('email/create', ['as' => 'email-create', 'uses' => 'EmailController@create']);
			Route::post('email/store', ['as' => 'email-store', 'uses' => 'EmailController@store']);
			Route::group(['middleware' => 'emailmustbelongtoowner'], function()
			{
				Route::post('email/{email}/delete', ['as' => 'email-delete', 'uses' => 'EmailController@delete']);
				Route::get('email/{email}/edit', ['as' => 'email-edit', 'uses' => 'EmailController@edit']);
				Route::post('email/{email}/update', ['as' => 'email-update', 'uses' => 'EmailController@update']);
				Route::get('email/{email}/enabledisable', ['as' => 'email-enabledisable', 'uses' => 'EmailController@enableDisable']);
			});


			/**
			 * Email Forwarding
			 */
			Route::get('forwarding', ['as' => 'email-forwarding', 'uses' => 'EmailForwardingController@index']);
			Route::get('forwarding/create', ['as' => 'email-forwarding-create', 'uses' => 'EmailForwardingController@create']);
			Route::post('forwarding/store', ['as' => 'email-forwarding-store', 'uses' => 'EmailForwardingController@store']);
			Route::group(['middleware' => 'forwardingmustbelongtouser'], function()
			{
				Route::get('forwarding/{forwarding}/enabledisable', ['as' => 'email-forwarding-enabledisable', 'uses' => 'EmailForwardingController@enableDisable']);
				Route::post('forwarding/{forwarding}/delete', ['as' => 'email-forwarding-delete', 'uses' => 'EmailForwardingController@destroy']);
			});


			/**
			 * File Manager
			 */
			Route::get('filemanager', ['as' => 'filemanager', 'uses' => 'FileManagerController@index']);
			Route::get('filemanager/editor/{location?}/{file?}', ['as' => 'filemanager-editor', 'uses' => 'FileManagerController@editor']);


			/**
			 * FTP Account
			 */
			Route::get('ftp', ['as' => 'ftp', 'uses' => 'FtpController@index']);
			Route::get('ftp/create', ['as' => 'ftp-create', 'uses' => 'FtpController@create']);
			Route::post('ftp/store', ['as' => 'ftp-store', 'uses' => 'FtpController@store']);
			Route::group(['middleware' => 'ftpmustbelongtoowner'], function()
			{
				Route::get('ftp/{ftpuser}/enabledisable', ['as' => 'ftp-enabledisable', 'uses' => 'FtpController@enableDisable']);
				Route::get('ftp/{ftpuser}/edit', ['as' => 'ftp-edit', 'uses' => 'FtpController@edit']);
				Route::post('ftp/{ftpuser}/delete', ['as' => 'ftp-destroy', 'uses' => 'FtpController@destroy']);
				Route::post('ftp/{ftpuser}/update', ['as' => 'ftp-update', 'uses' => 'FtpController@update']);
			});

			/**
			 * SSL Certificate
			 */
			Route::get('ssl', ['as' => 'ssl', 'uses' => 'SslController@index']);
			Route::get('ssl/add', ['as' => 'ssl-add', 'uses' => 'SslController@create']);

			/**
			 * Cron
			 */
			Route::get('cron', ['as' => 'cron', 'uses' => 'CronController@index']);
			Route::get('cron/add', ['as' => 'cron-add', 'uses' => 'CronController@create']);

			/**
			 * Supervisor
			 */
			Route::get('supervisor', ['as' => 'supervisor', 'uses' => 'SupervisorController@index']);
			Route::get('supervisor/add', ['as' => 'supervisor-add', 'uses' => 'SupervisorController@create']);
		});
	});

	/**
	 * Package Part
	 */
	Route::get('package', ['as' => 'packagelist', 'uses' => 'PackageController@index']);
	Route::get('package/add', ['as' => 'createpackage', 'uses' => 'PackageController@create']);
	Route::post('package/store', ['as' => 'storepackage', 'uses' => 'PackageController@store']);
	Route::get('package/chart', ['as' => 'packagechart', 'uses' => 'PackageController@chart']);
	
	Route::group(['middleware' => 'deleteeditpackage'], function()
	{
		Route::get('package/{package}/edit', ['as' => 'editpackage', 'uses' => 'PackageController@edit']);
		Route::post('package/{package}/update', ['as' => 'updatepackage', 'uses' => 'PackageController@update']);
		Route::post('package/{package}/delete', ['as' => 'deletepackage', 'uses' => 'PackageController@destroy']);
	});


	/**
	 * Server Setting Part
	 */
	Route::get('serversetting', ['as' => 'serversetting', 'uses' => 'SettingController@index']);
	Route::group(['prefix' => 'serversetting'], function()
	{
		Route::get('dovecot', ['as' => 'dovecotsetting', 'uses' => 'SettingController@dovecot']);
		Route::get('mariadb', ['as' => 'mariadbsetting', 'uses' => 'SettingController@mariadb']);
		Route::get('php', ['as' => 'phpsetting', 'uses' => 'SettingController@php']);
		Route::get('postfix', ['as' => 'postfixsetting', 'uses' => 'SettingController@postfix']);
		Route::get('powerdns', ['as' => 'powerdnssetting', 'uses' => 'SettingController@powerdns']);
		Route::get('pureftpd', ['as' => 'pureftpdsetting', 'uses' => 'SettingController@pureftpd']);
		Route::get('spamassassin', ['as' => 'spamassassinsetting', 'uses' => 'SettingController@spamassassin']);
		Route::get('ssh', ['as' => 'sshsetting', 'uses' => 'SettingController@ssh']);
		Route::get('nginx', ['as' => 'nginxsetting', 'uses' => 'SettingController@nginx']);
		Route::get('csf', ['as' => 'csfsetting', 'uses' => 'SettingController@csf']);
		Route::get('fail2ban', ['as' => 'fail2bansetting', 'uses' => 'SettingController@fail2ban']);
	});


	/**
	 * Template controller
	 */
	Route::group(['prefix' => 'templatemanager', 'as' => 'templatemanager::'], function()
	{
		Route::get('/', ['as' => 'index', 'uses' => 'WhopletTemplateController@index']);
		Route::get('skeleton', ['as' => 'skeleton', 'uses' => 'WhopletTemplateController@skeleton']);
		Route::get('template/{whoplet}/show', ['as' => 'view', 'uses' => 'WhopletTemplateController@show']);
		Route::post('store', ['as' => 'store', 'uses' => 'WhopletTemplateController@store']);
		Route::post('update', ['as' => 'update', 'uses' => 'WhopletTemplateController@update']);
		Route::get('enabledisable/{whoplet}/{status}', ['middleware' => 'whopletstatuschecker', 'as' => 'enabledisable', 'uses' => 'WhopletTemplateController@enableDisable']);
		Route::post('delete/{whoplet}', ['as' => 'delete', 'uses' => 'WhopletTemplateController@delete']);
	});


	/**
	 * Whop Setting
	 */
	Route::group(['prefix' => 'whop'], function()
	{
		Route::get('setting', ['as' => 'panelsetting', 'uses' => 'WhopSettingController@panelSetting']);
		Route::post('setting', ['as' => 'panelsetting-update', 'uses' => 'WhopSettingController@panelSettingUpdate']);
		Route::get('setting/autoupdate', ['as' => 'autoupdate', 'uses' => 'WhopSettingController@autoUpdate']);

		Route::get('emailsetting', ['as' => 'emailsetting', 'uses' => 'WhopSettingController@emailSetting']);
		Route::post('emailsetting', ['as' => 'emailsetting-update', 'uses' => 'WhopSettingController@emailSettingUpdate']);

		Route::get('sslsetting', ['as' => 'sslsetting', 'uses' => 'WhopSettingController@sslSetting']);
	});
});










/**
 * Owner Route
 */
Route::group(['prefix' => 'owner', 'as' => 'owner::', 'middleware' => array('auth', 'owner'), 'namespace' => 'Owner'], function()
{
	Route::get('/', ['as' => 'dashboard', 'uses' => 'OwnerController@index']);
	Route::get('profile', ['as' => 'profile', 'uses' => 'OwnerController@edit']);
	Route::post('profile/name', ['as' => 'profile-update-name', 'uses' => 'OwnerController@updateName']);
	Route::post('profile/password', ['as' => 'profile-update-password', 'uses' => 'OwnerController@updatePassword']);


	Route::group(['as' => 'app::', 'prefix' => 'apps'], function()
	{
		/**
		 * WHoPlet Part
		 */
		Route::get('whoplet-manager', ['as' => 'whopletmanager', 'uses' => 'WhopletController@index']);
		Route::post('whoplet-manager/templatechooser', ['as' => 'whopletformbuilder', 'uses' => 'WhopletController@whopletFormBuilder']);
		Route::post('whoplet-manager/build', ['as' => 'buildwhoplet', 'uses' => 'WhopletController@buildWhoplet']);
		Route::get('whoplet-manager/log/streaming', ['as' => 'whopletlog', 'uses' => 'WhopletController@streamLog']);


		/**
		 * WHoPlet Template Part
		 */
		Route::get('whoplet-template-manager', ['as' => 'whoplettemplatemanager', 'uses' => 'UserWhopletTemplateController@index']);
		Route::post('whoplet-template-manager/checkportion', ['as' => 'whoplettemplatemanager-checkportion', 'uses' => 'UserWhopletTemplateController@checkPortion']);
		Route::post('whoplet-template-manager/{whoplet}/delete', ['middleware' => 'templatemustbeownbyowner', 'as' => 'whoplettemplatemanager-delete', 'uses' => 'UserWhopletTemplateController@delete']);
		Route::group(['middleware' => 'templatemustbeownbyadminoruser'], function()
		{
			Route::post('whoplet-template-manager/gettemplate', ['as' => 'whoplettemplatemanager-gettemplate', 'uses' => 'UserWhopletTemplateController@getTemplate']);
			Route::post('whoplet-template-manager/preview', ['as' => 'whoplettemplatemanager-preview', 'uses' => 'UserWhopletTemplateController@preview']);
			Route::post('whoplet-template-manager/store', ['as' => 'whoplettemplatemanager-store', 'uses' => 'UserWhopletTemplateController@store']);
		});


		/**
		 * MariaDB
		 */
		Route::get('mariadb', ['as' => 'mariadb', 'uses' => 'MariaDBController@index']);
		Route::post('mariadb/adddatabase', ['as' => 'mariadb-add', 'uses' => 'MariaDBController@store']);
		Route::post('mariadb/adduser', ['as' => 'mariadb-adduser', 'uses' => 'MariaDBController@storeUser']);
		Route::post('mariadb/addusertodatabase', ['as' => 'mariadb-addusertodatabase', 'uses' => 'MariaDBController@addUserToDatabase']);
		Route::post('mariadb/revokedatabaseuser', ['as' => 'mariadb-revokeuser', 'uses' => 'MariaDBController@revokeUser']);
		Route::post('mariadb/deletedatabase', ['as' => 'mariadb-delete', 'uses' => 'MariaDBController@delete']);
		Route::post('mariadb/deletedatabaseuser', ['as' => 'mariadb-delete-user', 'uses' => 'MariaDBController@deleteUser']);


		/**
		 * Domain
		 */
		Route::get('domain', ['as' => 'domain', 'uses' => 'DomainController@index']);
		Route::post('domain/{domain}/delete', ['middleware' => 'domainmustbelongtoowner', 'as' => 'domain-delete', 'uses' => 'DomainController@delete']);
		Route::post('domain/add', ['as' => 'domain-add', 'uses' => 'DomainController@store']);


		/**
		 * Record
		 */
		Route::get('record', ['as' => 'record', 'uses' => 'RecordController@index']);
		Route::get('record/create', ['as' => 'record-create', 'uses' => 'RecordController@create']);
		Route::post('record/domain/{domain}/domainrecord', ['middleware' => 'domainmustbelongtoowner', 'as' => 'record-all', 'uses' => 'RecordController@myRecord']);
		Route::post('record/store', ['as' => 'record-store', 'uses' => 'RecordController@store']);
		Route::group(['middleware' => 'recordmustbelongtoowner'], function()
		{
			Route::get('record/{record}/edit', ['as' => 'record-edit', 'uses' => 'RecordController@edit']);
			Route::post('record/{record}/update', ['as' => 'record-update', 'uses' => 'RecordController@update']);
			Route::post('record/{record}/delete', ['as' => 'record-delete', 'uses' => 'RecordController@delete']);
		});


		/**
		 * Email
		 */
		Route::get('email', ['as' => 'email', 'uses' => 'EmailController@index']);
		Route::get('email/create', ['as' => 'email-create', 'uses' => 'EmailController@create']);
		Route::post('email/store', ['as' => 'email-store', 'uses' => 'EmailController@store']);
		Route::group(['middleware' => 'emailmustbelongtoowner'], function()
		{
			Route::post('email/{email}/delete', ['as' => 'email-delete', 'uses' => 'EmailController@delete']);
			Route::get('email/{email}/edit', ['as' => 'email-edit', 'uses' => 'EmailController@edit']);
			Route::post('email/{email}/update', ['as' => 'email-update', 'uses' => 'EmailController@update']);
			Route::get('email/{email}/enabledisable', ['as' => 'email-enabledisable', 'uses' => 'EmailController@enableDisable']);
		});


		/**
		 * Email Forwarding
		 */
		Route::get('forwarding', ['as' => 'email-forwarding', 'uses' => 'EmailForwardingController@index']);
		Route::get('forwarding/create', ['as' => 'email-forwarding-create', 'uses' => 'EmailForwardingController@create']);
		Route::post('forwarding/store', ['as' => 'email-forwarding-store', 'uses' => 'EmailForwardingController@store']);
		Route::group(['middleware' => 'forwardingmustbelongtouser'], function()
		{
			Route::get('forwarding/{forwarding}/enabledisable', ['as' => 'email-forwarding-enabledisable', 'uses' => 'EmailForwardingController@enableDisable']);
			Route::post('forwarding/{forwarding}/delete', ['as' => 'email-forwarding-delete', 'uses' => 'EmailForwardingController@destroy']);
		});


		/**
		 * File Manager
		 */
		Route::get('filemanager', ['as' => 'filemanager', 'uses' => 'FileManagerController@index']);
		Route::get('filemanager/editor/{location?}/{file?}', ['as' => 'filemanager-editor', 'uses' => 'FileManagerController@editor']);


		/**
		 * FTP Account
		 */
		Route::get('ftp', ['as' => 'ftp', 'uses' => 'FtpController@index']);
		Route::get('ftp/create', ['as' => 'ftp-create', 'uses' => 'FtpController@create']);
		Route::post('ftp/store', ['as' => 'ftp-store', 'uses' => 'FtpController@store']);
		Route::group(['middleware' => 'ftpmustbelongtoowner'], function()
		{
			Route::get('ftp/{ftpuser}/enabledisable', ['as' => 'ftp-enabledisable', 'uses' => 'FtpController@enableDisable']);
			Route::get('ftp/{ftpuser}/edit', ['as' => 'ftp-edit', 'uses' => 'FtpController@edit']);
			Route::post('ftp/{ftpuser}/delete', ['as' => 'ftp-destroy', 'uses' => 'FtpController@destroy']);
			Route::post('ftp/{ftpuser}/update', ['as' => 'ftp-update', 'uses' => 'FtpController@update']);
		});


		/**
		 * SSL Certificate
		 */
		Route::get('ssl', ['as' => 'ssl', 'uses' => 'SslController@index']);
		Route::get('ssl/add', ['as' => 'ssl-add', 'uses' => 'SslController@create']);

		/**
		 * Cron
		 */
		Route::get('cron', ['as' => 'cron', 'uses' => 'CronController@index']);
		Route::get('cron/add', ['as' => 'cron-add', 'uses' => 'CronController@create']);

		/**
		 * Supervisor
		 */
		Route::get('supervisor', ['as' => 'supervisor', 'uses' => 'SupervisorController@index']);
		Route::get('supervisor/add', ['as' => 'supervisor-add', 'uses' => 'SupervisorController@create']);
	});
});
