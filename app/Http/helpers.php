<?php

function rewriteEditorFilePath($file) {

	return str_replace(";","", $file);
	
}


function rewriteFTPUsername($ftpUser) {

	return substr_replace($ftpUser->username,'',-strlen('@' . $ftpUser->record->name),strlen('@' . $ftpUser->record->name));

}



/** Taken from http://stackoverflow.com/questions/9339619/php-checking-if-the-last-character-is-a-if-not-then-tack-it-on **/
function endWith($FullStr, $needle) {

    $StrLen = strlen($needle);

    $FullStrEnd = substr($FullStr, strlen($FullStr) - $StrLen);



    return $FullStrEnd == $needle;

}



function rewriteRecordName($recordName, $domainName) {

    if ($recordName == $domainName) {

        return '@';

    } else {

        return  endWith($recordName, $domainName) ? substr_replace($recordName, '', -strlen('.' . $domainName), strlen('.' . $domainName)) : $recordName;

    }

}