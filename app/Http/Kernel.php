<?php

namespace WHoP\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \WHoP\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \WHoP\Http\Middleware\VerifyCsrfToken::class,
        \WHoP\Http\Middleware\IsVerifiedWhopLicense::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \WHoP\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \WHoP\Http\Middleware\RedirectIfAuthenticated::class,
        'bruteforceprotection' => \WHoP\Http\Middleware\BruteforceProtection::class,
        'admin' => \WHoP\Http\Middleware\MustBeAdmin::class,
        'owner' => \WHoP\Http\Middleware\MustBeOwner::class,
        'deleteeditpackage' => \WHoP\Http\Middleware\DeleteEditPackage::class,
        'whopletstatuschecker' => \WHoP\Http\Middleware\WhopletStatusChecker::class,
        'usermustbedomainowner' => \WHoP\Http\Middleware\UserMustBeDomainOwner::class,
        'templatemustbeownbyadminoruser' => \WHoP\Http\Middleware\TemplateMustBeOwnByAdminOrUser::class,
        'templatesslcheck' => \WHoP\Http\Middleware\TemplateSSLCheck::class,
        'templatemustbeownbyowner' => \WHoP\Http\Middleware\TemplateMustBeOwnByOwner::class,
        'domainmustbelongtoowner' => \WHoP\Http\Middleware\DomainMustBelongToOwner::class,
        'recordmustbelongtoowner' => \WHoP\Http\Middleware\RecordMustBelongToOwner::class,
        'emailmustbelongtoowner' => \WHoP\Http\Middleware\EmailMustBelongToOwner::class,
        'forwardingmustbelongtouser' => \WHoP\Http\Middleware\ForwardingMustBelongToOwner::class,
        'ftpmustbelongtoowner' => \WHoP\Http\Middleware\FtpMustBelongToOwner::class,
        'useraccountmustbedisable' => \WHoP\Http\Middleware\UserAccountMustBeDisable::class,
        'useraccountmustbeenable' => \WHoP\Http\Middleware\UserAccountMustBeEnable::class,
        'deleteowner' => \WHoP\Http\Middleware\DeleteUserMustNotAdminOwner::class,
    ];
}
