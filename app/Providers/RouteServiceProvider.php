<?php

namespace WHoP\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'WHoP\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        $router->model('user', 'WHoP\User');
        $router->model('package', 'WHoP\Package');
        $router->model('whoplet', 'WHoP\Whoplet');
        $router->model('domain', 'WHoP\Domain');
        $router->model('record', 'WHoP\Record');
        $router->model('email', 'WHoP\MailUser');
        $router->model('forwarding', 'WHoP\MailForwarding');
        $router->model('ftpuser', 'WHoP\FtpUser');

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
