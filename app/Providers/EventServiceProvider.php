<?php

namespace WHoP\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        \WHoP\Events\UserLoggedIn::class => [

            \WHoP\Listeners\UpdateLastLogin::class,

            \WHoP\Listeners\EmailSuccessLogin::class,

        ],


        \WHoP\Events\OwnerUpdatePassword::class => [

            \WHoP\Listeners\UpdateSystemPassword::class,

            \WHoP\Listeners\EmailChangePasswordNotification::class,

        ],


        \WHoP\Events\OverwriteOwnerPackage::class => [

            \WHoP\Listeners\ChangeOwnerPackageConfig::class,

        ],


        \WHoP\Events\OwnerAccountStatusChanged::class => [

            \WHoP\Listeners\EnableDisableApp::class,

            \WHoP\Listeners\EnableDisableSocket::class,
        ],


        \WHoP\Events\OwnerAccountWasDeleted::class => [

            \WHoP\Listeners\DeleteAllOwnerDatabaseUser::class,

            \WHoP\Listeners\DeleteAllOwnerDatabase::class,

            \WHoP\Listeners\DeleteAllOwnerDomain::class,

            \WHoP\Listeners\DeleteOwnerSocket::class,
        ],


        \WHoP\Events\PanelEmailWasUpdated::class => [

            \WHoP\Listeners\ReloadWhopPanelSupervisor::class,

        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}
