<?php

namespace WHoP\Providers;

use Illuminate\Support\ServiceProvider;

use WHoP\Library\ValidatorLibrary;

class WHoPServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['validator']->resolver( function( $translator, $data, $rules, $messages = array(), $customAttributes = array() ) {
            return new ValidatorLibrary( $translator, $data, $rules, $messages, $customAttributes );
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('UserService', function($app)
        {
            return new \WHoP\Services\UserService();
        });

        $this->app->bind('PackageService', function($app)
        {
            return new \WHoP\Services\PackageService();
        });

        $this->app->bind('DomainService', function($app)
        {
            return new \WHoP\Services\DomainService();
        });

        $this->app->bind('RecordService', function($app)
        {
            return new \WHoP\Services\RecordService();
        });

        $this->app->bind('SocketService', function($app)
        {
            return new \WHoP\Services\SocketService();
        });

        $this->app->bind('ProgressService', function($app)
        {
            return new \WHoP\Services\ProgressService();
        });

        $this->app->bind('FtpService', function($app)
        {
            return new \WHoP\Services\FtpService();
        });

        $this->app->bind('StaticService', function($app)
        {
            return new \WHoP\Services\StaticService();
        });

        $this->app->bind('WhopletService', function($app)
        {
            return new \WHoP\Services\WhopletService();
        });

        $this->app->bind('MailService', function($app)
        {
            return new \WHoP\Services\MailService();
        });

        $this->app->bind('SettingService', function($app)
        {
            return new \WHoP\Services\SettingService();
        });

        $this->app->bind('DatabaseService', function($app)
        {
            return new \WHoP\Services\DatabaseService();
        });
    }
}
