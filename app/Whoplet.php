<?php

namespace WHoP;

use Illuminate\Database\Eloquent\Model;

class Whoplet extends Model
{
    public function user()
    {
    	return $this->belongsTo('WHoP\User');
    }
}
