<?php

namespace WHoP;

use Illuminate\Database\Eloquent\Model;

class FtpUser extends Model
{
    public function record()
    {
    	return $this->belongsTo('WHoP\Record');
    }
}
