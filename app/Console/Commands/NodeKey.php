<?php

namespace WHoP\Console\Commands;

use Illuminate\Console\Command;

use Illuminate\Support\Str;
use Symfony\Component\Console\Input\InputOption;

class NodeKey extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'key:nodekey';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This will create new WHoP Node key.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $key = $this->getRandomKey($this->laravel['config']['app.cipher']);

        $path = base_path('.env');

        if (file_exists($path)) {
            file_put_contents($path, str_replace(
                'NODE_KEY='.$this->laravel['config']['whop.nodekey'], 'NODE_KEY='.$key, file_get_contents($path)
            ));
        }

        $this->laravel['config']['whop.nodekey'] = $key;

        $this->info("WHoP key [$key] set successfully.");
    }

    protected function getRandomKey($cipher)
    {
        if ($cipher === 'AES-128-CBC') {
            return Str::random(16);
        }

        return Str::random(32);
    }
}
