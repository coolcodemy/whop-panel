<?php

namespace WHoP\Listeners;

use WHoP\Events\UserLoggedIn;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use WHoP\LastLogin;

class UpdateLastLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLoggedIn  $event
     * @return void
     */
    public function handle(UserLoggedIn $event)
    {
        $user = $event->user;

        $lastlogin = new LastLogin;
        $lastlogin->user_id = $user->id;
        $lastlogin->time = date('Y-m-d H:i:s');
        $lastlogin->save();
    }
}
