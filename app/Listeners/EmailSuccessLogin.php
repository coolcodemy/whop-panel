<?php

namespace WHoP\Listeners;

use WHoP\Events\UserLoggedIn;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail, Request;

class EmailSuccessLogin implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLoggedIn  $event
     * @return void
     */
    public function handle(UserLoggedIn $event)
    {
        $user = $event->user;
        $ip = $event->ip;

        $mailData = [
            'name' => $user->name,
            'time' => date('d F Y (H:i:s)'),
            'ip' => $ip,
        ];

        Mail::send('mail.successlogin', $mailData, function($message) use ($user) {
            $message->subject('Succesfull WHoP login');
            $message->to($user->email);
        });
    }
}
