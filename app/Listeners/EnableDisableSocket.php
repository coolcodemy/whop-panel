<?php

namespace WHoP\Listeners;

use WHoP\Events\OwnerAccountStatusChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EnableDisableSocket
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OwnerAccountStatusChanged  $event
     * @return void
     */
    public function handle(OwnerAccountStatusChanged $event)
    {
        $user = $event->user;


        if ( $user->disable == 0 ) {

            $socketData = [

                'MyUsername' => auth()->user()->username,

                'MyKey' => auth()->user()->secretKey,

                'username' => $user->username,

                'NODE_KEY' => env('NODE_KEY'),

                'ssh' => $user->userpackage->ssh,

            ];

            app('SocketService')->emit('enableOwnerAccount-server', $socketData);

        } else {

            $socketData = [

                'MyUsername' => auth()->user()->username,

                'MyKey' => auth()->user()->secretKey,

                'username' => $user->username,

                'NODE_KEY' => env('NODE_KEY'),
                
            ];

            app('SocketService')->emit('disableOwnerAccount-server', $socketData);

        }

    }
}
