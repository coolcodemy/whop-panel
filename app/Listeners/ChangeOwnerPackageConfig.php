<?php

namespace WHoP\Listeners;

use WHoP\Events\OverwriteOwnerPackage;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ChangeOwnerPackageConfig
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OverwriteOwnerPackage  $event
     * @return void
     */
    public function handle(OverwriteOwnerPackage $event)
    {
        $user = $event->user;
        $userPackage = $event->up;


        $socketData = [

            'MyUsername' => auth()->user()->username,

            'MyKey' => auth()->user()->secretKey,

            'username' => $user->username,

            'NODE_KEY' => env('NODE_KEY'),

            'apc' => $userPackage->apcuSupport,

            'ssh' => $userPackage->ssh,

            'diskQuota' => $userPackage->diskQuota,

            'websiteQuota' => $userPackage->websiteQuota,

        ];


        app('SocketService')->emit('overwriteOwnerPackage-server', $socketData);
    }
}
