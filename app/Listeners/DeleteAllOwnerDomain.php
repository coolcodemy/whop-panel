<?php

namespace WHoP\Listeners;

use WHoP\Events\OwnerAccountWasDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use WHoP\Domain;

use Log;

class DeleteAllOwnerDomain
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OwnerAccountWasDeleted  $event
     * @return void
     */
    public function handle(OwnerAccountWasDeleted $event)
    {
        $user = $event->user;

        Domain::whereUserId($user->id)->delete();
    }
}
