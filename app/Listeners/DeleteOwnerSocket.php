<?php

namespace WHoP\Listeners;

use WHoP\Events\OwnerAccountWasDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteOwnerSocket
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OwnerAccountWasDeleted  $event
     * @return void
     */
    public function handle(OwnerAccountWasDeleted $event)
    {
        $user = $event->user;


        $socketData = [

            'MyUsername' => auth()->user()->username,

            'MyKey' => auth()->user()->secretKey,

            'username' => $user->username,

            'NODE_KEY' => env('NODE_KEY'),

        ];


        app('SocketService')->emit('deleteOwnerAccount-server', $socketData);
    }
}
