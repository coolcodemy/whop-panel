<?php

namespace WHoP\Listeners;

use WHoP\Events\OwnerAccountWasDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteAllOwnerDatabase
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OwnerAccountWasDeleted  $event
     * @return void
     */
    public function handle(OwnerAccountWasDeleted $event)
    {
        $user = $event->user;


        $dbs = app('DatabaseService')->getUserDatabaseList($user);


        foreach ($dbs as $db) {
            
            app('DatabaseService')->deleteDBRaw( $db['SCHEMA_NAME'] );

        }



        app('DatabaseService')->flush();
    }
}
