<?php

namespace WHoP\Listeners;

use WHoP\Events\PanelEmailWasUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReloadWhopPanelSupervisor
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PanelEmailWasUpdated  $event
     * @return void
     */
    public function handle(PanelEmailWasUpdated $event)
    {
        $socketData = [

            'MyUsername' => auth()->user()->username,

            'MyKey' => auth()->user()->secretKey,

            'NODE_KEY' => env('NODE_KEY'),
            
        ];

        app('SocketService')->emit('reloadPanelSupervisor-server', $socketData);
    }
}
