<?php

namespace WHoP\Listeners;

use WHoP\Events\OwnerUpdatePassword;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EmailChangePasswordNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OwnerUpdatePassword  $event
     * @return void
     */
    public function handle(OwnerUpdatePassword $event)
    {
        //
    }
}
