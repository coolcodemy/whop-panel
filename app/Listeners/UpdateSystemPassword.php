<?php

namespace WHoP\Listeners;

use WHoP\Events\OwnerUpdatePassword;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;


class UpdateSystemPassword
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OwnerUpdatePassword  $event
     * @return void
     */
    public function handle(OwnerUpdatePassword $event)
    {
        $user = $event->user;

        $request = $event->ownerRequest;


        $socketData = [

            'MyUsername' => auth()->user()->username,

            'MyKey' => auth()->user()->secretKey,

            'username' => $user->username,

            'newPassword' => $request->newPassword,

            'NODE_KEY' => env('NODE_KEY'),

        ];





        app('SocketService')->emit('updateOwnerPassword-server', $socketData);
    }
}
