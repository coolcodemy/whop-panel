<?php

namespace WHoP\Listeners;

use WHoP\Events\OwnerAccountStatusChanged;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use WHoP\FtpUser,
    WHoP\MailForwarding,
    WHoP\MailUser;

class EnableDisableApp
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OwnerAccountStatusChanged  $event
     * @return void
     */
    public function handle(OwnerAccountStatusChanged $event)
    {
        $user = $event->user;



        if ( $user->disable == 0 ) {

            FtpUser::whereUserId($user->id)->update(['masterDisable' => 0]);

            MailForwarding::whereUserId($user->id)->update(['masterDisable' => 0]);

            MailUser::whereUserId($user->id)->update(['masterDisable' => 0]);

        } else {

            FtpUser::whereUserId($user->id)->update(['masterDisable' => 1]);

            MailForwarding::whereUserId($user->id)->update(['masterDisable' => 1]);

            MailUser::whereUserId($user->id)->update(['masterDisable' => 1]);

        }
    }
}
