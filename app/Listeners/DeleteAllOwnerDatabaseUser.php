<?php

namespace WHoP\Listeners;

use WHoP\Events\OwnerAccountWasDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteAllOwnerDatabaseUser
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OwnerAccountWasDeleted  $event
     * @return void
     */
    public function handle(OwnerAccountWasDeleted $event)
    {
        $user = $event->user;


        $dbUsers = app('DatabaseService')->getUserDatabaseAccountList($user);

        foreach ($dbUsers as $value) {

            app('DatabaseService')->deleteUserRaw($value['User']);

        }

        app('DatabaseService')->flush();


    }
}
