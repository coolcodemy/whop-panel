<?php

namespace WHoP\Events;

use WHoP\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use WHoP\User;
use WHoP\UserPackage;

class OverwriteOwnerPackage extends Event
{
    use SerializesModels;

    public $user, $up;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, UserPackage $up)
    {
        
        $this->user = $user;

        $this->up = $up;

    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
