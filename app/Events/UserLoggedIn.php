<?php

namespace WHoP\Events;

use WHoP\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use WHoP\User;

class UserLoggedIn extends Event
{
    use SerializesModels;

    public $user, $ip;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, $ip)
    {
        $this->user = $user;
        $this->ip = $ip;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [$this->user];
    }
}
