<?php

namespace WHoP\Events;

use WHoP\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

use WHoP\User;

class OwnerUpdatePassword extends Event
{
    use SerializesModels;

    public $user, $ownerRequest;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, $ownerRequest)
    {

        $this->user  = $user;


        $this->ownerRequest = $ownerRequest;
        
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
