<?php


namespace WHoP\Library;

use Illuminate\Validation\Validator;

use Auth, Hash;

use WHoP\Record;


class ValidatorLibrary extends Validator 
{
    private $_custom_messages = array(
        "ipv4" => "The :attribute must be IPv4 format.",
        "ipv6" => "The :attribute must be IPv6 format.",
        "domain" => "The :attribute must be top level domain.",
        "domain_subdomain" => "The :attribute must be top level domain or subdomain.",
        "alphanumeric_space" => "The :attribute may only contain letters, number and space.",
        "common_word_symbol" => "The :attribute value is invalid.",
        "human_name" => "The :attribute value is not a human name.",
        'domain_key' => "The :attribute value is not valid domain key.",
        "unique_with" => "This combination of :fields already exists.",
        "domain_ns_point_to_server" => "This :attribute domain NS1 and NS2 must point to this server.",
        "current_password" => "The :attribute field must match your current password."
    );


    public function __construct( $translator, $data, $rules, $messages = array(), $customAttributes = array() ) {
        parent::__construct( $translator, $data, $rules, $messages, $customAttributes );
 
        $this->_set_custom_stuff();
    }


    protected function _set_custom_stuff() {

        $this->setCustomMessages( $this->_custom_messages );
    }


    public function validateIpv6($attribute, $value, $parameters)
    {
        return filter_var($value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6);
    }


    public function validateIpv4($attribute, $value, $parameters)
    {
        return filter_var($value, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
    }


    public function validateDomain($attribute, $value, $parameters)
    {
        return preg_match('/^[a-zA-Z0-9]{0,61}[a-zA-Z0-9-]{0,61}[a-zA-Z0-9](\.[a-zA-Z]{2,}){1,2}$/', $value);
    }


    public function validateDomainSubdomain($attribute, $value, $parameters)
    {
        return preg_match('/^([a-zA-Z0-9\-_]+\.){0,32}[a-zA-Z0-9\-_]+\.[a-zA-Z]{2,11}$/', $value);
    }


    public function validateAlphanumericSpace($attribute, $value, $parameters)
    {
        if(preg_match("/^[\w\d ]+$/", $value)) {
            return true;
        }

        return false;
    }

    public function validateCommonWordSymbol($attribute, $value, $parameters)
    {
        if(preg_match("/^[\w\d !.,&()\[\]'\":<>\/@]+$/", $value)) {
            return true;
        }

        return false;
    }


    public function validateHumanName($attribute, $value, $parameters)
    {
        if(preg_match("/^[\w .@'`]+$/", $value)) {
            return true;
        }

        return false;
    }


    public function validateDomainKey($attribute, $value, $parameters)
    {
        if(preg_match("/^([\w\d._]+|@)$/", $value)) {
            return true;
        }

        return false;
    }


    public function validateDomainNsPointToServer($attribute, $value, $parameters)
    {

        $ns = dns_get_record($value, DNS_NS);
        $nsCount =  count($ns);

        if ($nsCount < 2) {
            return false;
        }


        $res1 = Record::whereType('NS')->whereContent($ns[0]['target'])->first();

        $res2 = Record::whereType('NS')->whereContent($ns[1]['target'])->first();

        if ( $res1 == null ) {

            return false;

        } else if ( $res2 == null ) {

            return false;

        } else {

            return true;

        }

    }


    public function validateCurrentPassword($attribute, $value, $parameters)
    {
        $currentPassword = auth()->user()->password;

        return Hash::check($value, $currentPassword);
    }


    /**
     * @include felixkiss validator
     * Reason: cannot extend original validator
     * All code is belongs to felixkiss
     * https://github.com/felixkiss/uniquewith-validator
     * This code by felixkiss is MIT
     */
    public function validateUniqueWith($attribute, $value, $parameters)
    {
        // cleaning: trim whitespace
        $parameters = array_map('trim', $parameters);
        // first item equals table name
        $table = array_shift($parameters);
        // The second parameter position holds the name of the column that
        // needs to be verified as unique. If this parameter isn't specified
        // we will just assume that this column to be verified shares the
        // attribute's name.
        $column = $attribute;
        // Create $extra array with all other columns, so getCount() will
        // include them as where clauses as well
        $extra = array();
        // Check if last parameter is an integer. If it is, then it will
        // ignore the row with the specified id - useful when updating a row
        list($ignore_id, $ignore_column) = $this->getIgnore($parameters);
        // Figure out whether field_name is the same as column_name
        // or column_name is explicitly specified.
        //
        // case 1:
        //     $parameter = 'last_name'
        //     => field_name = column_name = 'last_name'
        // case 2:
        //     $parameter = 'last_name=sur_name'
        //     => field_name = 'last_name', column_name = 'sur_name'
        foreach ($parameters as $parameter)
        {
            $parameter = array_map('trim', explode('=', $parameter, 2));
            $field_name = $parameter[0];
            if (count($parameter) > 1)
            {
                $column_name = $parameter[1];
            }
            else
            {
                $column_name = $field_name;
            }
            // Figure out whether main field_name has an explicitly specified
            // column_name
            if ($field_name == $column)
            {
                $column = $column_name;
            }
            else
            {
                $extra[$column_name] = array_get($this->data, $field_name);
            }
        }
        // The presence verifier is responsible for counting rows within this
        // store mechanism which might be a relational database or any other
        // permanent data store like Redis, etc. We will use it to determine
        // uniqueness.
        $verifier = $this->getPresenceVerifier();
        return $verifier->getCount(
            $table,
            $column,
            $value,
            $ignore_id,
            $ignore_column,
            $extra
        ) == 0;
    }
    public function replaceUniqueWith($message, $attribute, $rule, $parameters)
    {
        // remove trailing ID param if present
        $this->getIgnore($parameters);
        // merge primary field with conditional fields
        $fields = array($attribute) + $parameters;
        // get full language support due to mapping to validator getAttribute
        // function
        $fields = array_map(array($this, 'getAttribute'), $fields);
        // fields to string
        $fields = implode(', ', $fields);
        return str_replace(':fields', $fields, $message);
    }
    /**
     * Returns an array with value and column name for an optional ignore.
     * Shaves of the ignore_id from the end of the array, if there is one.
     *
     * @param  array $parameters
     * @return array [$ignoreId, $ignoreColumn]
     */
    private function getIgnore(&$parameters)
    {
        $lastParam = end($parameters);
        $lastParam = array_map('trim', explode('=', $lastParam));
        // An ignore_id is only specified if the last param starts with a
        // number greater than 1 (a valid id in the database)
        if (!preg_match('/^[1-9][0-9]*$/', $lastParam[0]))
        {
            return array(null, null);
        }
        $ignoreId = $lastParam[0];
        $ignoreColumn = (sizeof($lastParam) > 1) ? end($lastParam) : null;
        // Shave of the ignore_id from the array for later processing
        array_pop($parameters);
        return array($ignoreId, $ignoreColumn);
    }
}