<?php

namespace WHoP;

use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    protected $fillable = [
        'name',
        'type',
        'content',
    ];

    public function domain()
    {
    	return $this->belongsTo(\WHoP\Domain::class);
    }
}
