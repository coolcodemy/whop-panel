<?php

namespace WHoP;

use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    protected $fillable = [
        'name',
        'user_id',
    ];


    public function records()
    {
    	return $this->hasMany(\WHoP\Record::class);
    }
}
