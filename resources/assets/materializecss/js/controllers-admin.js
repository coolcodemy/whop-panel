WHoP.controller('ServerSettingDovecotController', ['$scope', 'socket', function($scope, socket) {
    var alertBox = $("#alertBox");

    $scope.toggle = function() {

        if ($scope.switch == false) {
            Materialize.toast('Please wait while Dovecot service stop', 2000);
            credential.Command = 'stop';
            socket.emit('serversetting-dovecot-controller-server', credential);

        } else if ($scope.switch == true) {
            Materialize.toast('Please wait while Dovecot service start', 2000);
            credential.Command = 'start';
            socket.emit('serversetting-dovecot-controller-server', credential);

        }

        $scope.disableSwitch = true;
    }


    $scope.restart = function() {
        $scope.isRestart = true;
        Materialize.toast('Please wait while Dovecot service restart', 2000);
        credential.Command = 'restart';
        socket.emit('serversetting-dovecot-controller-server', credential);
    }


    $scope.chooseCertificate = function() {
        $scope.wait = true;
        $scope.certCount = 1;
        $("#universalCertificateModal").openModal({
            dismissible: false,
            ready: function() {
                socket.emit('getUniversalCertificate-server', credential);
            }
        });
    }


    $scope.setCertificate = function(cert) {
        $scope.ssl = cert;
        $("#universalCertificateModal").closeModal();
    }


    $scope.saveConfig = function() {
        $scope.isSaving = true;
        credential.Cert = $scope.ssl;
        Materialize.toast('Saving Dovecot configuration', 2000);
        socket.emit('serversetting-dovecot-store-server', credential);
    }



    socket.emit('serversetting-dovecot-server', credential);

    socket.on('serversetting-dovecot-client', function(content)
    {
        alertBox.slideUp();
        $scope.cpuUsage = content.CPU;
        $scope.ssl = content.Cert;
        $scope.memoryUsage = content.Memory;

        if (content.CPU == "Service not running") {
            $scope.switch = false;
        } else {
            $scope.switch = true;
        }
    });


    socket.on('serversetting-dovecot-controller-client', function(message)
    {
        $scope.disableSwitch = false;

        socket.emit('serversetting-dovecot-server', credential);
        if (message.Command == 'start') {
            Materialize.toast('Service Dovecot started!', 2000);
        } else if (message.Command == 'stop') {
            Materialize.toast('Service Dovecot stopped!', 2000);
        } else if (message.Command == 'restart') {
            Materialize.toast('Service Dovecot restarted!', 2000);
            $scope.isRestart = false;
        } else {
            Materialize.toast('Unknown command!!', 4000);
        }
    });


    socket.on('getUniversalCertificate-client', function(jsonArray)
    {
        $scope.wait = false;
        $scope.certCount = jsonArray.length;

        if ($scope.certCount > 0) {
            $scope.certificates = jsonArray;
        }
    });


    socket.on('serversetting-dovecot-store-client', function(message)
    {
        $scope.isSaving = false;
        Materialize.toast(message.message, 2000);
    });
}]);


WHoP.controller('ServerSettingMariaDBController', ['$scope', 'socket', function($scope, socket) {
    var alertBox = $("#alertBox");

    socket.emit('serversetting-mariadb-server', credential);


    $scope.toggle = function() {
        $scope.disableSwitch = true;

        if ($scope.switch) {
            Materialize.toast('Please wait while MariaDB service start', 2000);
            credential.Command = 'start';
            socket.emit('serversetting-mariadb-controller-server', credential);
        } else {
            Materialize.toast('Please wait while MariaDB service stop', 2000);
            credential.Command = 'stop';
            socket.emit('serversetting-mariadb-controller-server', credential);
        }
    }


    $scope.restart = function() {
        $scope.isRestart = true;
        Materialize.toast('Please wait while MariaDB service restart', 2000);
        credential.Command = 'restart';
        socket.emit('serversetting-mariadb-controller-server', credential);
    }


    $scope.saveConfig = function () {
        $scope.isSaving = true;

        credential.innodbbufferpoolsize  = $scope.innodbbufferpoolsize;
        credential.maxconnections = $scope.maxconnections;
        credential.maxuserconnections = $scope.maxuserconnections;
        credential.querycachesize = $scope.querycachesize;
        credential.querycachelimit = $scope.querycachelimit;
        credential.querycacheminresunit = $scope.querycacheminresunit;
        credential.threadcachesize = $scope.threadcachesize;
        credential.innodbbufferpoolinstances = $scope.innodbbufferpoolinstances;

        Materialize.toast('Saving MariaDB configuration', 4000);

        socket.emit('serversetting-mariadb-store-server', credential);
    };


    socket.on('serversetting-mariadb-client', function(content)
    {
        alertBox.slideUp();

        angular.forEach(content, function(value, key) {
            $scope[key] = value;
        });

        $scope.cpuUsage = content.CPU;
        $scope.memoryUsage = content.Memory;

        if (content.CPU == "Service not running") {
            $scope.switch = false;
        } else {
            $scope.switch = true;
        }
    });


    socket.on('serversetting-mariadb-controller-client', function(message)
    {
        $scope.disableSwitch = false;

        socket.emit('serversetting-mariadb-server', credential);
        if (message.Command == 'start') {
            Materialize.toast('Service MariaDB started!', 2000);
        } else if (message.Command == 'stop') {
            Materialize.toast('Service MariaDB stopped!', 2000);
        } else if (message.Command == 'restart') {
            Materialize.toast('Service MariaDB restarted!', 2000);
            $scope.isRestart = false;
        } else {
            Materialize.toast('Unknown command!!', 4000);
        }
    });

    socket.on('serversetting-mariadb-store-client', function(message)
    {
        $scope.isSaving = false;
        Materialize.toast(message.message, 2000);
    });
}]);


WHoP.controller('ServerSettingPHP', ['$scope', 'socket', '$timeout', function($scope, socket, $timeout) {
    var alertBox = $("#alertBox");

    $scope.toggle = function() {
        $scope.disableSwitch = true;

        if ($scope.switch) {
            Materialize.toast('Please wait while PHP-FPM service start', 2000);
            credential.Command = 'start';
            socket.emit('serversetting-php-controller-server', credential);
        } else {
            Materialize.toast('Please wait while PHP-FPM service stop', 2000);
            credential.Command = 'stop';
            socket.emit('serversetting-php-controller-server', credential);
        }
    }


    $scope.restart = function() {
        Materialize.toast('Please wait while php service restart', 2000);
        credential.Command = 'restart';
        socket.emit('serversetting-php-controller-server', credential);
    }


    $scope.saveConfig = function() {
        $scope.isSaving = true;

        credential.disable_functions  = $scope.disable_functions;
        credential.expose_php = $scope.expose_php;
        credential.max_execution_time = $scope.max_execution_time;
        credential.max_input_time = $scope.max_input_time;
        credential.memory_limit = $scope.memory_limit;
        credential.display_errors = $scope.display_errors;
        credential.post_max_size = $scope.post_max_size;
        credential.cgi_fix_pathinfo = $scope.cgi_fix_pathinfo;
        credential.file_uploads = $scope.file_uploads;
        credential.upload_max_filesize = $scope.upload_max_filesize;
        credential.allow_url_fopen = $scope.allow_url_fopen;
        credential.allow_url_include = $scope.allow_url_include;
        credential.mail_add_x_header = $scope.mail_add_x_header;
        credential.sql_safe_mode = $scope.sql_safe_mode;
        credential.session_name = $scope.session_name;
        credential.session_gc_maxlifetime = $scope.session_gc_maxlifetime;

        Materialize.toast('Saving php.ini configuration', 2000);

        socket.emit('serversetting-php-store-server', credential);
    }


    socket.emit('serversetting-php-server', credential);

    socket.on('serversetting-php-client', function(content)
    {
        alertBox.slideUp();

        $('.select-wrapper span.caret').remove();
        $('select').material_select('destroy');

        angular.forEach(content, function(value, key) {
            $scope[key] = value;
        });

        $('select').material_select();

        $scope.cpuUsage = content.CPU;
        $scope.memoryUsage = content.Memory;
        
        if (content.CPU == "Service not running") {
            $scope.switch = false;
        } else {
            $scope.switch = true;
        }


        $timeout(function() {
            $('.select-wrapper span.caret').remove();
            $('select').material_select('destroy');
            $('select').material_select();
        }, 1000);
    });



    socket.on('serversetting-php-controller-client', function(message)
    {
        $scope.disableSwitch = false;

        socket.emit('serversetting-php-server', credential);
        if (message.Command == 'start') {
            Materialize.toast('Service PHP-FPM started!', 2000);
        } else if (message.Command == 'stop') {
            Materialize.toast('Service PHP-FPM stopped!', 2000);
        } else if (message.Command == 'restart') {
            Materialize.toast('Service PHP-FPM restarted!', 2000);
            $scope.isRestart = false;
        } else {
            Materialize.toast('Unknown command!!', 4000);
        }
    });


    socket.on('serversetting-php-store-client', function(message)
    {
        $scope.isSaving = false;
        Materialize.toast(message.message, 2000);
    });
}]);


WHoP.controller('ServerSettingPostfixController', ['$scope', 'socket', function($scope, socket) {
    var alertBox = $("#alertBox");


    $scope.toggle = function () {
        $scope.disableSwitch = true;

        if ($scope.switch) {
            Materialize.toast('Please wait while Postfix service start', 2000);
            credential.Command = 'start';
            socket.emit('serversetting-postfix-controller-server', credential);
        } else {
            Materialize.toast('Please wait while Postfix service stop', 2000);
            credential.Command = 'stop';
            socket.emit('serversetting-postfix-controller-server', credential);
        }
    }


    $scope.restart = function () {
        $scope.isRestart = true;
        Materialize.toast('Please wait while Postfix service restart', 2000);
        credential.Command = 'restart';
        socket.emit('serversetting-postfix-controller-server', credential);
    }

    $scope.chooseCertificate = function() {
        $scope.wait = true;
        $scope.certCount = 1;
        $("#universalCertificateModal").openModal({
            dismissible: false,
            ready: function() {
                socket.emit('getUniversalCertificate-server', credential);
            }
        });
    }


    $scope.setCertificate = function(cert) {
        $scope.ssl = cert;
        $("#universalCertificateModal").closeModal();
    }


    $scope.saveConfig = function() {
        $scope.isSaving = true;

        credential.Cert = $scope.ssl;
        credential.HostName = $scope.hostname;

        Materialize.toast('Saving Postfix configuration', 4000);

        socket.emit('serversetting-postfix-store-server', credential);
    }

    socket.emit('serversetting-postfix-server', credential);

    socket.on('serversetting-postfix-client', function(content)
    {
        alertBox.slideUp();

        $scope.ssl = content.Cert;
        $scope.hostname = content.HostName;
        $scope.cpuUsage = content.CPU;
        $scope.memoryUsage = content.Memory;


        if (content.CPU == "Service not running") {
            $scope.switch = false;
        } else {
            $scope.switch = true;
        }
    });


    socket.on('serversetting-postfix-controller-client', function(message)
    {
        $scope.disableSwitch = false;

        socket.emit('serversetting-postfix-server', credential);
        if (message.Command == 'start') {
            Materialize.toast('Service Postfix started!', 2000);
        } else if (message.Command == 'stop') {
            Materialize.toast('Service Postfix stopped!', 2000);
        } else if (message.Command == 'restart') {
            Materialize.toast('Service Postfix restarted!', 2000);
            $scope.isRestart = false;
        } else {
            Materialize.toast('Unknown command!!', 4000);
        }
    });

    socket.on('serversetting-postfix-store-client', function(message)
    {
        $scope.isSaving = false;
        Materialize.toast(message.message, 2000);
    });


    socket.on('getUniversalCertificate-client', function(jsonArray)
    {
        $scope.wait = false;
        $scope.certCount = jsonArray.length;

        if ($scope.certCount > 0) {
            $scope.certificates = jsonArray;
        }
    });
}]);


WHoP.controller('ServerSettingPDNSController', ['$scope', 'socket', function($scope, socket) {
    var alertBox = $("#alertBox");


    $scope.toggle = function() {
        $scope.disableSwitch = true;

        if ($scope.switch) {
            Materialize.toast('Please wait while PowerDNS service start', 2000);
            credential.Command = 'start';
            socket.emit('serversetting-pdns-controller-server', credential);
        } else {
            Materialize.toast('Please wait while PowerDNS service stop', 2000);
            credential.Command = 'stop';
            socket.emit('serversetting-pdns-controller-server', credential);
        }
    }

    $scope.restart = function() {
        $scope.isRestart = true;

        Materialize.toast('Please wait while PowerDNS service restart', 2000);
        credential.Command = 'restart';
        socket.emit('serversetting-pdns-controller-server', credential);
    }

    socket.emit('serversetting-pdns-server', credential);

    socket.on('serversetting-pdns-client', function(content)
    {
        alertBox.slideUp();

        $scope.cpuUsage = content.CPU;
        $scope.memoryUsage = content.Memory;

        if (content.CPU == "Service not running") {
            $scope.switch = false;
        } else {
            $scope.switch = true;
        }
    });


    socket.on('serversetting-pdns-controller-client', function(message)
    {
        $scope.disableSwitch = false;
        if (message.Command == 'start') {
            Materialize.toast('Service PowerDNS started!', 2000);
            socket.emit('serversetting-pdns-server', credential);
        } else if (message.Command == 'stop') {
            Materialize.toast('Service PowerDNS stopped!', 2000);
            setTimeout(function() {
                location.reload();
            }, 1000);
        } else if (message.Command == 'restart') {
            Materialize.toast('Service PowerDNS restarted!', 2000);
            socket.emit('serversetting-pdns-server', credential);
            $scope.isRestart = false;
        } else {
            Materialize.toast('Unknown command!!', 4000);
        }
    });
}]);


WHoP.controller('ServerSettingPureFTPDController', ['$scope', 'socket', '$timeout', function ($scope, socket, $timeout) {
    var alertBox = $("#alertBox");

    $scope.toggle = function () {
        $scope.disableSwitch = true;

        if ($scope.switch) {
            Materialize.toast('Please wait while Pure-FTPD service start', 2000);
            credential.Command = 'start';
            socket.emit('serversetting-pureftpd-controller-server', credential);
        } else {
            Materialize.toast('Please wait while Pure-FTPD service stop', 2000);
            credential.Command = 'stop';
            socket.emit('serversetting-pureftpd-controller-server', credential);
        }
    }


    $scope.restart = function() {
        $scope.isRestart = true;

        Materialize.toast('Please wait while Pure-FTPD service restart', 2000);
        credential.Command = 'restart';
        socket.emit('serversetting-pureftpd-controller-server', credential);
    }


    $scope.saveConfig = function() {
        $scope.isSaving = true;

        credential.MaxClientsNumber = $scope.MaxClientsNumber;
        credential.MaxClientsPerIP = $scope.MaxClientsPerIP;
        credential.PassivePortRangeFrom = $scope.PassivePortRangeFrom;
        credential.PassivePortRangeTo = $scope.PassivePortRangeTo;
        credential.NoAnonymous = $scope.NoAnonymous;
        credential.DisplayDotFiles = $scope.DisplayDotFiles;
        credential.MaxIdleTime = $scope.MaxIdleTime;
        credential.TLS = $scope.TLS;
        credential.PublicCert = $scope.PublicCert;
        credential.PrivateKey = $scope.PrivateKey;

        Materialize.toast('Saving Pure-FTPD configuration', 4000);

        socket.emit('serversetting-pureftpd-store-server', credential);
    }

    socket.emit('serversetting-pureftpd-server', credential);


    socket.on('serversetting-pureftpd-client', function(content)
    {
        alertBox.slideUp();

        $('.select-wrapper span.caret').remove(); // temporary hack to remove excess caret
        $('select').material_select('destroy');

        angular.forEach(content, function(value, key) {
            $scope[key] = value;
        });

        $('select').material_select();

        $scope.cpuUsage = content.CPU;
        $scope.memoryUsage = content.Memory;

        if (content.CPU == "Service not running") {
            $scope.switch = false;
        } else {
            $scope.switch = true;
        }

        $timeout(function() {
            $('.select-wrapper span.caret').remove(); // temporary hack to remove excess caret
            $('select').material_select('destroy');
            $('select').material_select();
        }, 1000);
    });


    socket.on('serversetting-pureftpd-controller-client', function(message)
    {
        $scope.disableSwitch = false;

        socket.emit('serversetting-pureftpd-server', credential);
        if (message.Command == 'start') {
            Materialize.toast('Service Pure-FTPD started!', 2000);
        } else if (message.Command == 'stop') {
            Materialize.toast('Service Pure-FTPD stopped!', 2000);
        } else if (message.Command == 'restart') {
            Materialize.toast('Service Pure-FTPD restarted!', 2000);
            $scope.isRestart = false;
        } else {
            Materialize.toast('Unknown command!!', 4000);
        }
    });

    socket.on('serversetting-pureftpd-store-client', function(message)
    {
        $scope.isSaving = false;
        Materialize.toast(message.message, 2000);
    });
}]);


WHoP.controller('ServerSettingSpamassassinController', ['$scope', 'socket', '$timeout', function($scope, socket, $timeout) {
    var alertBox = $("#alertBox");

    $scope.toggle = function () {
        $scope.disableSwitch = true;

        if ($scope.switch) {
            Materialize.toast('Please wait while Spamassassin service start', 2000);
            credential.Command = 'start';
            socket.emit('serversetting-spamassassin-controller-server', credential);
        } else {
            Materialize.toast('Please wait while Spamassassin service stop', 2000);
            credential.Command = 'stop';
            socket.emit('serversetting-spamassassin-controller-server', credential);
        }
    }


    $scope.restart = function() {
        $scope.isRestart = true;

        Materialize.toast('Please wait while Spamassassin service restart', 2000);
        credential.Command = 'restart';
        socket.emit('serversetting-spamassassin-controller-server', credential);
    }


    $scope.saveConfig = function() {
        $scope.isSaving = true;

        credential.required_score = $scope.required_score;
        credential.report_safe = $scope.report_safe;

        Materialize.toast('Saving Spamassassin configuration', 4000);

        socket.emit('serversetting-spamassassin-store-server', credential);
    }

    socket.emit('serversetting-spamassassin-server', credential);

    socket.on('serversetting-spamassassin-client', function(content)
    {
        alertBox.slideUp();
        $('.select-wrapper span.caret').remove(); // temporary hack to remove excess caret
        $('select').material_select('destroy');

        angular.forEach(content, function(value, key) {
            $scope[key] = value;
        });

        $('select').material_select();

        $scope.cpuUsage = content.CPU;
        $scope.memoryUsage = content.Memory;

        if (content.CPU == "Service not running") {
            $scope.switch = false;
        } else {
            $scope.switch = true;
        }

        $timeout(function() {
            $('.select-wrapper span.caret').remove(); // temporary hack to remove excess caret
            $('select').material_select('destroy');
            $('select').material_select();
        }, 1000);
    });


    socket.on('serversetting-spamassassin-controller-client', function(message)
    {
        $scope.disableSwitch = false;
        socket.emit('serversetting-spamassassin-server', credential);
        if (message.Command == 'start') {
            Materialize.toast('Service Spamassassin started!', 2000);
        } else if (message.Command == 'stop') {
            Materialize.toast('Service Spamassassin stopped!', 2000);
        } else if (message.Command == 'restart') {
            Materialize.toast('Service Spamassassin restarted!', 2000);
            $scope.isRestart = false;
        } else {
            Materialize.toast('Unknown command!!', 4000);
        }
    });

    socket.on('serversetting-spamassassin-store-client', function(message)
    {
        $scope.isSaving = false;
        Materialize.toast(message.message, 2000);
    });
}]);


WHoP.controller('ServerSerttingSSHController', ['$scope', 'socket', '$timeout', function($scope, socket, $timeout) {
    var alertBox = $("#alertBox");


    $scope.toggle = function() {
        $scope.disableSwitch = true;

        if ($scope.switch) {
            Materialize.toast('Please wait while SSH service start', 2000);
            credential.Command = 'start';
            socket.emit('serversetting-ssh-controller-server', credential);
        } else {
            Materialize.toast('Please wait while SSH service stop', 2000);
            credential.Command = 'stop';
            socket.emit('serversetting-ssh-controller-server', credential);
        }
    }


    $scope.restart = function() {
        $scope.isRestart = true;

        Materialize.toast('Please wait while SSH service restart', 2000);
        credential.Command = 'restart';
        socket.emit('serversetting-ssh-controller-server', credential);
    }


    $scope.saveConfig = function() {
        $scope.isSaving = true;

        credential.Port = $scope.Port;
        credential.UseDNS = $scope.UseDNS;

        Materialize.toast('Saving SSH configuration', 4000);

        socket.emit('serversetting-ssh-store-server', credential);
    }


    socket.emit('serversetting-ssh-server', credential);

    socket.on('serversetting-ssh-client', function(content)
    {
        alertBox.slideUp();
        $('.select-wrapper span.caret').remove(); // temporary hack to remove excess caret
        $('select').material_select('destroy');

        angular.forEach(content, function(value, key) {
            $scope[key] = value;
        });

        $('select').material_select();



        $scope.cpuUsage = content.CPU;
        $scope.memoryUsage = content.Memory;

        if (content.CPU == "Service not running") {
            $scope.switch = false;
        } else {
            $scope.switch = true;
        }

        $timeout(function() {
            $('.select-wrapper span.caret').remove(); // temporary hack to remove excess caret
            $('select').material_select('destroy');
            $('select').material_select();
        });
    });


    socket.on('serversetting-ssh-controller-client', function(message)
    {
        $scope.disableSwitch = false;

        socket.emit('serversetting-ssh-server', credential);
        if (message.Command == 'start') {
            Materialize.toast('Service SSH started!', 2000);
        } else if (message.Command == 'stop') {
            Materialize.toast('Service SSH stopped!', 2000);
        } else if (message.Command == 'restart') {
            Materialize.toast('Service SSH restarted!', 2000);
            $scope.isRestart = false;
        } else {
            Materialize.toast('Unknown command!!', 4000);
        }
    });

    socket.on('serversetting-ssh-store-client', function(message)
    {
        $scope.isSaving = false;
        Materialize.toast(message.message, 2000);
    });
}]);


WHoP.controller('ServerSettingNginx', ['$scope', 'socket', '$timeout', function($scope, socket, $timeout) {
    var alertBox = $("#alertBox");

    $scope.reload = function () {
        $scope.isReload = true;

        Materialize.toast('Please wait while Nginx service reload', 2000);
        credential.Command = 'reload';
        socket.emit('serversetting-nginx-controller-server', credential);
    }

    $scope.restart = function () {
        Materialize.toast('Please wait while Nginx service restart', 2000);
        credential.Command = 'restart';
        $timeout(function() {
            location.reload();
        }, 3000);
        socket.emit('serversetting-nginx-controller-server', credential);
    }

    $scope.saveConfig = function() {
        $scope.isSaving = true;

        credential.worker_processes = $scope.worker_processes;
        credential.worker_rlimit_nofile = $scope.worker_rlimit_nofile;
        credential.worker_connections = $scope.worker_connections;
        credential.multi_accept = $scope.multi_accept;
        credential.sendfile = $scope.sendfile;
        credential.tcp_nopush = $scope.tcp_nopush;
        credential.tcp_nodelay = $scope.tcp_nodelay;
        credential.client_body_timeout = $scope.client_body_timeout;
        credential.client_header_timeout = $scope.client_header_timeout;
        credential.keepalive_timeout = $scope.keepalive_timeout;
        credential.client_header_buffer_size = $scope.client_header_buffer_size;
        credential.client_max_body_size = $scope.client_max_body_size;
        credential.open_file_cache_valid = $scope.open_file_cache_valid;
        credential.open_file_cache_min_uses = $scope.open_file_cache_min_uses;
        credential.open_file_cache_errors = $scope.open_file_cache_errors;
        credential.keepalive_requests = $scope.keepalive_requests;
        credential.reset_timedout_connection = $scope.reset_timedout_connection;

        Materialize.toast('Saving Nginx configuration', 4000);

        socket.emit('serversetting-nginx-store-server', credential);
    }

    socket.emit('serversetting-nginx-server', credential);

    socket.on('serversetting-nginx-client', function(content)
    {
        alertBox.slideUp();
        $('.select-wrapper span.caret').remove(); // temporary hack to remove excess caret
        $('select').material_select('destroy');

        angular.forEach(content, function(value, key) {
            $scope[key] = value;
        });

        $('select').material_select();

        $scope.cpuUsage = content.CPU;
        $scope.memoryUsage = content.Memory;

        $timeout(function() {
            $('.select-wrapper span.caret').remove(); // temporary hack to remove excess caret
            $('select').material_select('destroy');
            $('select').material_select();
        }, 1000);
    });


    socket.on('serversetting-nginx-controller-client', function(message)
    {
        socket.emit('serversetting-nginx-server', credential);
        if (message.Command == 'reload') {
            Materialize.toast('Service Nginx reloaded!', 2000);
            $scope.isReload = false;
        }
    });

    socket.on('serversetting-nginx-store-client', function(message)
    {
        $scope.isSaving = false;
        Materialize.toast(message.message, 2000);
    });
}]);


WHoP.controller('SystemSettingFail2BanController', ['$scope', 'socket', '$timeout' , function($scope, socket, $timeout) {
    var alertBox = $("#alertBox");


    $scope.toggle = function () {
        $scope.disableSwitch = true;

        if ($scope.switch) {
            Materialize.toast('Please wait while Fail2Ban service start', 2000);
            credential.Command = 'start';
            socket.emit('serversetting-fail2ban-controller-server', credential);

        } else {
            Materialize.toast('Please wait while Fail2Ban service stop', 2000);
            credential.Command = 'stop';
            socket.emit('serversetting-fail2ban-controller-server', credential);
        }
    }


    $scope.restart = function () {
        $scope.isRestart = true;
        Materialize.toast('Please wait while Fail2Ban service restart', 2000);
        credential.Command = 'restart';
        socket.emit('serversetting-fail2ban-controller-server', credential);
    }


    $scope.saveConfig = function () {
        $scope.isSaving = true;

        credential.ignoreip  = $scope.ignoreip;
        credential.bantime = $scope.bantime;
        credential.findtime = $scope.findtime;
        credential.destemail = $scope.destemail;
        credential.senderemail = $scope.senderemail;

        Materialize.toast('Saving Fail2Ban configuration', 4000);

        socket.emit('serversetting-fail2ban-store-server', credential);
    }

    socket.emit('serversetting-fail2ban-server', credential);

    socket.on('serversetting-fail2ban-client', function(content)
    {
        alertBox.slideUp();
        $('.select-wrapper span.caret').remove(); // temporary hack to remove excess caret
        $('select').material_select('destroy');

        angular.forEach(content, function(value, key) {
            $scope[key] = value;
        });

        $('select').material_select();

        $scope.cpuUsage = content.CPU;
        $scope.memoryUsage = content.Memory;

        if (content.CPU == "Service not running") {
            $scope.switch = false;
        } else {
            $scope.switch = true;
        }

        $timeout(function() {
            $('.select-wrapper span.caret').remove(); // temporary hack to remove excess caret
            $('select').material_select('destroy');
            $('select').material_select();
        }, 1000);
    });


    socket.on('serversetting-fail2ban-controller-client', function(message)
    {
        $scope.disableSwitch = false;

        socket.emit('serversetting-fail2ban-server', credential);
        if (message.Command == 'start') {
            Materialize.toast('Service Fail2Ban started!', 2000);
        } else if (message.Command == 'stop') {
            Materialize.toast('Service Fail2Ban stopped!', 2000);
        } else if (message.Command == 'restart') {
            Materialize.toast('Service Fail2Ban restarted!', 2000);
            $scope.isRestart = false;
        } else {
            Materialize.toast('Unknown command!!', 4000);
        }
    });

    socket.on('serversetting-fail2ban-store-client', function(message)
    {
        $scope.isSaving = false;
        Materialize.toast(message.message, 2000);
    });
}]);


WHoP.controller('ServerSettingCSFController', ['$scope', 'socket', '$timeout', function($scope, socket, $timeout) {
    var alertBox = $("#alertBox");

    $scope.toggle = function() {
        $scope.disableSwitch = true;

        if ($scope.switch) {
            Materialize.toast('Please wait while CSF service start', 2000);
            credential.Command = 'start';
            socket.emit('serversetting-csf-controller-server', credential);
        } else {
            Materialize.toast('Please wait while CSF service stop', 2000);
            credential.Command = 'stop';
            socket.emit('serversetting-csf-controller-server', credential);
        }
    }


    $scope.reload = function () {
        $scope.isReload = true;

        Materialize.toast('Please wait while CSF service reload', 2000);
        credential.Command = 'restart';
        socket.emit('serversetting-csf-controller-server', credential);
    }

    $scope.tool = function (tool) {
        credential.tool = tool;

        if (credential.tool == 'a') {
            credential.ip = $scope.aip;
            Materialize.toast('Adding IP Address to allow list', 2000);

        } else if (credential.tool == 'd') {
            credential.ip = $scope.dip;
            Materialize.toast('Adding IP Address to deny list', 2000);

        } else {
            Materialize.toast('Please wait while getting data...', 2000);
        }

        socket.emit('serversetting-csf-tools-server', credential);
    }


    $scope.saveConfig = function() {
        $scope.isSaving = true;

        credential.LF_ALERT_TO = $scope.LF_ALERT_TO;
        credential.LF_ALERT_FROM = $scope.LF_ALERT_FROM;
        credential.TCP_IN = $scope.TCP_IN;
        credential.TCP_OUT = $scope.TCP_OUT;
        credential.UDP_IN = $scope.UDP_IN;
        credential.UDP_OUT = $scope.UDP_OUT;
        credential.ICMP_IN = $scope.ICMP_IN;
        credential.ICMP_OUT = $scope.ICMP_OUT;
        credential.SMTP_BLOCK = $scope.SMTP_BLOCK;
        credential.SYNFLOOD = $scope.SYNFLOOD;
        credential.UDPFLOOD = $scope.UDPFLOOD;
        credential.LF_PERMBLOCK_COUNT = $scope.LF_PERMBLOCK_COUNT;
        credential.LF_SSH_EMAIL_ALERT = $scope.LF_SSH_EMAIL_ALERT;
        credential.PT_USERPROC = $scope.PT_USERPROC;
        credential.PT_USERMEM = $scope.PT_USERMEM;
        credential.PT_USERTIME = $scope.PT_USERTIME;
        credential.PT_LOAD_AVG = $scope.PT_LOAD_AVG;
        credential.PT_LOAD_LEVEL = $scope.PT_LOAD_LEVEL;
        credential.PT_FORKBOMB = $scope.PT_FORKBOMB;
        credential.PS_INTERVAL = $scope.PS_INTERVAL;
        credential.PS_LIMIT = $scope.PS_LIMIT;
        credential.AT_ALERT = $scope.AT_ALERT;

        Materialize.toast('Saving CSF configuration', 4000);

        socket.emit('serversetting-csf-store-server', credential);
    }


    socket.emit('serversetting-csf-server', credential);

    socket.on('serversetting-csf-client', function(content)
    {
        alertBox.slideUp();

        $('.select-wrapper span.caret').remove(); // temporary hack to remove excess caret
        $('select').material_select('destroy');

        angular.forEach(content, function(value, key) {
            $scope[key] = value;
        });

        $('select').material_select();

        if (content.Status == "disabled") {
            $scope.csfStatus = 'Disabled';
            $scope.switch = false;
        } else {
            $scope.csfStatus = 'Enabled';
            $scope.switch = true;
        }

        $timeout(function(){
            $('.select-wrapper span.caret').remove(); // temporary hack to remove excess caret
            $('select').material_select('destroy');
            $('select').material_select();
        }, 1000);
    });

    socket.on('serversetting-csf-controller-client', function(message)
    {
        $scope.disableSwitch = false;

        socket.emit('serversetting-csf-server', credential);
        if (message.Command == 'start') {
            Materialize.toast('Service CSF started!', 2000);
        } else if (message.Command == 'stop') {
            Materialize.toast('Service CSF stopped!', 2000);
        } else if (message.Command == 'restart') {
            $scope.isReload = false;
            Materialize.toast('Service CSF reloaded!', 2000);
        } else {
            Materialize.toast('Unknown command!!', 4000);
        }
    });

    socket.on('serversetting-csf-tools-client', function(result, tool)
    {
        Materialize.toast('Result shown in CSF Tool Result', 2000);
        $scope.toolResult = result;
    });

    socket.on('serversetting-csf-store-client', function(message)
    {
        $scope.isSaving = false;
        Materialize.toast(message.message, 2000);
    });



    // editor
    $('#csfAllow').addClass('editor');
    var csfAllow = ace.edit("csfAllow");
    csfAllow.setTheme("ace/theme/github");
    csfAllow.getSession().setMode("ace/mode/text");
    csfAllow.setShowPrintMargin(false);
    csfAllow.setValue('Please wait while config load...');
    csfAllow.gotoLine(1);


    socket.on('serversetting-csf-allow', function(data)
    {
        csfAllow.setValue(data);
        csfAllow.gotoLine(1);
    });

    $scope.saveCSFAllow = function () {
        $scope.isSaving = true;
        Materialize.toast('Saving CSF Allow and reloading CSF', 2000);
        credential.data = csfAllow.getSession().getValue();
        socket.emit('serversetting-csf-allow-save-server', credential);
    }

    socket.on('serversetting-csf-allow-save-client', function(message)
    {
        $scope.isSaving = false;
        Materialize.toast(message.message, 2000);
    });




    $('#csfDeny').addClass('editor');
    var csfDeny = ace.edit("csfDeny");
    csfDeny.setTheme("ace/theme/github");
    csfDeny.getSession().setMode("ace/mode/text");
    csfDeny.setShowPrintMargin(false);
    csfDeny.setValue('Please wait while config load...');
    csfDeny.gotoLine(1);

    socket.on('serversetting-csf-deny', function(data)
    {
        csfDeny.setValue(data);
        csfDeny.gotoLine(1);
    });

    $scope.saveCSFDeny = function () {
        $scope.isSaving = true;
        Materialize.toast('Saving CSF Deny and reloading CSF', 2000);
        credential.data = csfDeny.getSession().getValue();
        socket.emit('serversetting-csf-deny-save-server', credential);
    }

    socket.on('serversetting-csf-deny-save-client', function(message)
    {
        $scope.isSaving = false;
        Materialize.toast(message.message, 2000);
    });





    $('#csfIgnore').addClass('editor');
    var csfIgnore = ace.edit("csfIgnore");
    csfIgnore.setTheme("ace/theme/github");
    csfIgnore.getSession().setMode("ace/mode/text");
    csfIgnore.setShowPrintMargin(false);
    csfIgnore.setValue('Please wait while config load...');
    csfIgnore.gotoLine(1);

    socket.on('serversetting-csf-ignore', function(data)
    {
        csfIgnore.setValue(data);
        csfIgnore.gotoLine(1);
    });

    $scope.saveCSFIgnore = function () {
        $scope.isSaving = true;
        Materialize.toast('Saving CSF Ignore and reloading CSF', 2000);
        credential.data = csfIgnore.getSession().getValue();
        socket.emit('serversetting-csf-ignore-save-server', credential);
    }

    socket.on('serversetting-csf-ignore-save-client', function(message)
    {
        $scope.isSaving = false;
        Materialize.toast(message.message, 2000);
    });




    $('#csfPignore').addClass('editor');
    var csfPignore = ace.edit("csfPignore");
    csfPignore.setTheme("ace/theme/github");
    csfPignore.getSession().setMode("ace/mode/text");
    csfPignore.setShowPrintMargin(false);
    csfPignore.setValue('Please wait while config load...');
    csfPignore.gotoLine(1);

    socket.on('serversetting-csf-pignore', function(data)
    {
        csfPignore.setValue(data);
        csfPignore.gotoLine(1);
    });


    $scope.saveCSFPignore = function () {
        $scope.isSaving = true;
        Materialize.toast('Saving CSF Pignore and reloading CSF', 2000);
        credential.data = csfPignore.getSession().getValue();
        socket.emit('serversetting-csf-pignore-save-server', credential);
    }

    socket.on('serversetting-csf-pignore-save-client', function(message)
    {
        $scope.isSaving = false;
        Materialize.toast(message.message, 2000);
    });


    $scope.resizeAce = function () {
        $timeout(function() {
            csfAllow.resize();
            csfDeny.resize();
            csfIgnore.resize();
            csfPignore.resize();
        }); 
    }
}]);


WHoP.controller('WhopletAdminTemplateController', ['$scope', 'socket', '$timeout', '$http', function($scope, socket, $timeout, $http) {

    $scope.isBusy = false;

    $scope.view = function (url) {
        Materialize.toast('Viewing template', 2000);
        $scope.isBusy = true;
        $scope.addEdit = false;

        $http.get(url, null).then(function(success) {
            Materialize.toast('Done', 2000);
            initEditor(success.data.content, 1);
            $scope.isBusy = false;
        }, function (error) {
            if (error.status == 404) {
                Materialize.toast('Template not found!', 4000);
            } else {
                Materialize.toast('Unknown error. Please refresh page.', 4000);
            }
            $scope.isBusy = false;
        });
    }


    $scope.edit = function(url) {
        Materialize.toast('Getting template', 2000);
        $scope.isBusy = true;

        $http.get(url, null).then(function(success) {
            Materialize.toast('Done. Do not forget to save your template after editing', 2000);
            initEditor(success.data.content);

            $scope.addEdit = true;
            $scope.isBusy = false;
            $scope.editID = success.data.id;
            $scope.templateName = success.data.name.replace('WHoPlet_', '');
            $scope.saveType = 'edit';
        }, function (error) {
            if (error.status == 404) {
                Materialize.toast('Template not found!', 4000);
            } else {
                Materialize.toast('Unknown error. Please refresh page.', 4000);
            }
            $scope.isBusy = false;
        });
    }


    $scope.skeleton = function(url) {
        Materialize.toast('Getting skeleton template', 2000);
        $scope.isBusy = true;

        $http.get(url, null).then(function(success) {
            Materialize.toast('Done. Do not forget to save your template after editing', 2000);
            initEditor(success.data);

            $scope.addEdit = true;
            $scope.isBusy = false;
            $scope.saveType = 'add';
            $scope.templateName = '';
        }, function (error) {
            if (error.status == 404) {
                Materialize.toast(error.data, 4000);
            } else {
                Materialize.toast('Unknown error. Please refresh page.', 4000);
            }
            $scope.isBusy = false;
        });
    }


    $scope.save = function () {
        $scope.isBusy = true;
        var editor = ace.edit("templateEditor");
        editor.setReadOnly(true);
        credential.template = editor.getSession().getValue();
        socket.emit('checkwhoplettemplate-server', credential);
    }


    $scope.delete = function(url) {
        $('#deleteTemplateModal').openModal({
            dismissible: false,
            ready: function() {
                $("#deleteTemplateForm").attr('action', url);
            }
        });
    }

    socket.on('checkwhoplettemplate-client', function(json)
    {
        var editor = ace.edit("templateEditor");
        editor.setReadOnly(false);

        if (json.success == true) {

            Materialize.toast('Template have no error. Saving...', 2000);

            var data = {
                templateName: $scope.templateName,
                templateContent: editor.getSession().getValue(),
                templateId : $scope.editID,
            };

            if ($scope.saveType == 'add') {
                $http.post($('meta[name="store-template-url"]').attr('content'), data).then(function(success) {
                    Materialize.toast(success.data.message, 2000);
                    $timeout(function() {
                        location.reload();
                    }, 1500);

                }, function(error) {
                    if (error.status == 422) {
                        angular.forEach(error.data, function(value, key) {
                            Materialize.toast(value, 4000);
                        });
                        $scope.isBusy = false;
                    } else {
                        Materialize.toast('Unknown error. Please refresh page.', 4000);
                    }
                });
            } else if ($scope.saveType == 'edit') {

                $http.post($('meta[name="update-template-url"]').attr('content'), data).then(function(success) {
                    Materialize.toast(success.data.message, 2000);
                    $scope.isBusy = false;

                }, function(error) {
                    if (error.status == 422) {
                        angular.forEach(error.data, function(value, key) {
                            Materialize.toast(value, 4000);
                        });
                        $scope.isBusy = false;
                    } else {
                        Materialize.toast('Unknown error. Please refresh page.', 4000);
                    }
                });
            }
        } else {
            Materialize.toast(json.message, 2000);
            $scope.isBusy = false;
        }
    });

    function initEditor(content, view)
    {
        $('#templateEditor').addClass('editor');
        var editor = ace.edit("templateEditor");
        editor.setTheme("ace/theme/github");
        editor.getSession().setMode("ace/mode/yaml");
        editor.setShowPrintMargin(false);
        view == 1 ? editor.setReadOnly(true) : editor.setReadOnly(false);
        editor.setValue(content);
        editor.gotoLine(1);
    }
}]);


WHoP.controller('AdminDashboardController', ['$scope', 'socket', '$http', function($scope, socket, $http) {
    socket.emit('admindashboard-essential-server', credential);
    socket.emit('admindashboard-live-server', credential);
    socket.emit('admindashboard-graph-server', credential);


    var data = {
        labels: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
        datasets: [
            {
                label: "My First dataset",
                fillColor: "rgba(135,206,250,0.2)",
                strokeColor: "rgba(220,220,220,1)",
                pointColor: "rgba(220,220,220,1)",
                pointStrokeColor: "#fff",
                pointHighlightFill: "#fff",
                pointHighlightStroke: "rgba(135,206,250,1)",
                data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
            }
        ]
    };

    Chart.defaults.global.responsive = true;
    Chart.defaults.global.maintainAspectRatio = false;

    var ctx = $("#liveLoad").get(0).getContext("2d");
    var liveLoad = new Chart(ctx).Line(data, {
        animation: false,
    });


    socket.on('admindashboard-graph-client', function(load)
    {
        ct = new Date();
        liveLoad.addData([load], ('0' + ct.getHours()).slice(-2) + ':' + ('0' + ct.getMinutes()).slice(-2) + ':' + ('0' + ct.getSeconds()).slice(-2));
        liveLoad.update();
        liveLoad.removeData();
    });


    $http.get($('#packageUsage').data('url')).success(function(data) {
        var ctx = $("#packageUsage").get(0).getContext("2d");
        new Chart(ctx).Pie(data, {
            animateScale : true,
        });
    });

    socket.on('admindashboard-essential-client', function(kernelVersion, processorName, freeMemory, totalMemory, diskFree, diskTotal, version)
    {
        $scope.dashboardKernelVersion = kernelVersion;
        $scope.dashboardProcessorName = processorName;
        $scope.dashboardTotalMemory = bytesToSize(totalMemory);
        $scope.dashboardTotalDisk = bytesToSize(diskTotal);
        $scope.dashboardFreeDisk = bytesToSize(diskFree);
        $scope.dashboardNodeVersion = version;
    });


    socket.on('admindashboard-live-client', function(uptime, freeMemory, load)
    {
        $scope.dashboardServerUptime = uptime;
        $scope.dashboardFreeMemory = bytesToSize(freeMemory);
        $scope.dashboardServerLoad = load;
    });


    socket.on('admindashboard-nginxcount-client', function(total)
    {
        $scope.dashboardNginxCount = total;
    });


    socket.on('admindashboard-license-client', function(license)
    {
        $scope.mylicenseOwner = license.owner;
        $scope.mylicenseType = license.type;
        $scope.mylicenseExpiry = license.expiry;
    });
}]);


WHoP.controller('AddOwnerController', ['$scope', 'socket', '$http', '$sce', function($scope, socket, $http, $sce) {

    $scope.universalMessage = null;
    $scope.isBusy = false;

    $scope.buttonAddOwner = function() {
        $scope.isBusy = true;
        socket.emit('verify-add-user-server', credential);
    }


    $scope.cancel = function() {
        $scope.isBusy = false;
        $scope.universalMessage = null;
    }


    var addOwnerChannel = $('meta[name=add-owner-channel]').attr('content');

    socket.on(addOwnerChannel, function(data)
    {      
        if(data == 'FINISH') {
            $scope.universalMessage += '<p><strong>Succesfully add new owner.</strong></p>';
            $scope.isBusy = false;
        } else {
            $scope.universalMessage += '<p>' + data + '</p>';
        }
    });

    socket.on('verify-add-user-client', function(json)
    {
        if (json.success == true) { // submit the form using ajax

            var data = {
                name: $scope.name,
                username: $scope.username,
                password: $scope.password,
                repeatPassword: $scope.repeatPassword,
                email: $scope.email,
                package: $scope.package,
                domainName: $scope.domainName,
                channel: $('meta[name="add-owner-channel"]').attr('content'),
            };

            var url = $("#formAddOwner").attr('action');

            $("#addOwnerModal").openModal({
                dismissible: false,
            });


            $http.post(url, data).then(function(success) {
                $scope.universalMessage = '';
                $scope.universalMessage += '<p>' + success.data.message + '</p>';
            }, function(error) {
                $scope.universalMessage = '';
                $scope.isBusy = false;
                angular.forEach(error.data, function(value, key) {
                    angular.forEach(value, function(value2, key2) {
                        $scope.universalMessage += '<p>' + value2 + '</p>';
                    });
                });
            });
        } else {

            $("#addOwnerModal").openModal({
                dismissible: false,
            });
            $scope.isBusy = false;
            $scope.universalMessage = '';
            $scope.universalMessage += 'Fail!!! Please upgrade your WHoP license to add more user';
        }
    });
}]);


WHoP.controller('OwnerPHPController', ['$scope', 'socket', function($scope, socket) {

    $scope.isBusy = false;

    credential.User = $('meta[name="username"]').attr('content');

    $('#phpSetting').addClass('editor');

    var editor = ace.edit("phpSetting");
    editor.setTheme("ace/theme/github");
    editor.getSession().setMode("ace/mode/text");
    editor.setShowPrintMargin(false);
    editor.setValue('Please wait while setting load...');
    editor.gotoLine(1);

    socket.emit('getOwnerPHPSetting-server', credential);


    




    $scope.savePHPSetting = function() {

        $scope.isBusy = true;

        credential.data = editor.getSession().getValue();

        socket.emit('saveOwnerPHPSetting-server', credential);
    }




    socket.on('saveOwnerPHPSetting-client', function(json) {

        $scope.isBusy = false;

        if (json.success == true) {

            Materialize.toast(json.message, 2000);

        } else {

            Materialize.toast(json.message, 4000);

        }
    });

    socket.on('getOwnerPHPSetting-client', function(data) {

        Materialize.toast('PHP CLI setting loaded', 1500);

        editor.setValue(data);
        editor.gotoLine(1);
    });

}]);


WHoP.controller('OwnerSystemController', ['$scope', 'socket', '$http', '$timeout', '$window', function($scope, socket, $http, $timeout, $window) {

    $scope.disable = false;
    $scope.isBusy = false;

    getUserStatus();


    $scope.disableAccount = function (url) {

        $scope.isBusy = true;

        $http.get( url ).success(function(data) {

            $scope.isBusy = false;

            getUserStatus();

            Materialize.toast(data.message, 4000);

        });

    }



    $scope.enableAccount = function (url) {

        $scope.isBusy = true;

        $http.get( url ).success(function(data) {

            $scope.isBusy = false;

            getUserStatus();

            Materialize.toast(data.message, 4000);

        });

    }


    $scope.deleteAccount = function() {

        $("#deleteAccountModal").openModal({
            dismissible: false,
        });

    }



    $scope.deleteAccountCommit = function() {

        var url = $("#btnDeleteAccount").data('url');

        $scope.isBusy = true;

        var data = {
            username: $scope.deleteUsername,
        }

        $http.post( url, data ).then(function(success) {

            $scope.isBusy = false;

            $scope.universalMessage = success.data.message;

            $timeout(function() {

                $window.location.href = '/';

            }, 1500);

        }, function (error) {

            $scope.isBusy = false;

            $scope.universalMessage = '';

            angular.forEach(error.data, function(value, key) {

                $scope.universalMessage += value[0];

            });

        });
    }


    $scope.cancel = function() {

        $scope.universalMessage = null;
        $scope.deleteUsername = null;
        $scope.isBusy = false;
    }





    function getUserStatus()
    {
        $http.get($('#getOwnerStatus').data('url')).success(function(data) {

            if ( data.disable == 0 ) {

                $scope.disable = false;

            } else {

                $scope.disable = true;

            }

        });
    }
}]);


WHoP.controller('PanelSSLController', ['$scope', 'socket', function($scope, socket) {

    socket.emit('getWhopCertificate-server', credential);
    socket.emit('getPanelCertificate-server', credential);

    $scope.isBusy = false;
    $scope.deleteMessage = null;
    $scope.usingCert = null;

    $scope.view = function(cert) {
        credential.Cert = cert.slice(0, -4);
        socket.emit('viewWhopCertificate-server', credential);
    }

    $scope.addCertificate = function() {
        $scope.isBusy = true;

        credential.CertName = $scope.certificateName;
        credential.PublicCert = $scope.publicCertificate;
        credential.PrivateCert = $scope.privateCertificate;

        socket.emit('addWhopCertificate-server', credential);
    }


    $scope.delete = function(cert) {
        $scope.choosenDeleteCertificate = cert;
        $("#deleteCertificateModal").openModal({
            dismissible: false,
        });
    }


    $scope.deleteCertificate = function() {
        $scope.isBusy = true;
        credential.CertName = $scope.choosenDeleteCertificate .slice(0, -4);
        socket.emit('deleteWhopCertificate-server', credential);
    }


    $scope.useAsPanelCertificate = function(cert) {

        credential.Cert = cert.slice(0, -4);
        credential.OldCert = $scope.usingCert.slice(0, -4);

        $scope.isBusy = true;

        socket.emit('useAsPanelCertificate-server', credential);
    }


    $scope.cancel = function () {
        $scope.isBusy = false;
        $scope.deleteMessage = null;
    }

    socket.on('getWhopletCertificate-client', function(jsonArray)
    {
        $scope.certCount = jsonArray.length;
        $scope.certs = jsonArray;
    });


    socket.on('viewcertificate-client', function(content)
    {
        $scope.certContent = content;
        $('#viewCertificateModal').openModal({
            dismissible: false,
            complete: function() {
                $scope.certContent = '';
            }
        });
    });


    socket.on('addCertificate-client', function(message)
    {
        if (message.flag == false) {
            $scope.isBusy = false;
            Materialize.toast(message.message, 4000);
        } else {

            socket.emit('getWhopCertificate-server', credential);

            $scope.certificateName = null;
            $scope.publicCertificate = null;
            $scope.privateCertificate = null;

            $scope.isBusy = false;

            Materialize.toast(message.message, 2000);
        }
    });

    socket.on('deleteWhopCertificate-client', function(message)
    {
        $scope.deleteMessage = message.message;

        if (message.flag == true) {
            socket.emit('getWhopCertificate-server', credential);
        }
    });


    socket.on('getPanelCertificate-client', function(cert)
    {
        $scope.usingCert = cert;
    });


    socket.on('useAsPanelCertificate-client', function(message) {

        Materialize.toast(message, 2000);
        $scope.isBusy = false;
        socket.emit('getPanelCertificate-server', credential);

    });

}]);

WHoP.controller('AutoUpdateController', ['$scope', '$http', function($scope, $http) {

    $scope.isBusy = false;

    $scope.switch = function(url) {

        $scope.isBusy = true;

        $http.get( url ).then(function(success) {

            $scope.isBusy = false;

            Materialize.toast(success.data.message, 2000);


        }, function (error) {

            $scope.isBusy = false;

        });
    }

}]);