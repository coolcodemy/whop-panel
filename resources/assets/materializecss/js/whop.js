$('.serversetting').find('a[href="' + location.href + '"]').addClass('active light-green white-text').removeClass('black-text');

bytesToSize = function(bytes) {
    if (bytes == 0) return '0 Byte';
    var k = 1024;
    var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
    var i = Math.floor(Math.log(bytes) / Math.log(k));
    return (bytes / Math.pow(k, i)).toPrecision(3) + ' ' + sizes[i];
}

bytesToMB = function(bytes) {
    var num = bytes / 1024 / 1024;
    return num.toFixed(2) / 1;
}

KBToMB = function(kb) {
    var num = kb / 1024;
    return num.toFixed(2) / 1;
}

function toggle(source) {
    checkboxes = document.getElementsByName("permission[]");
    for (var i = 0, n = checkboxes.length; i < n; i++) {
        checkboxes[i].checked = source.checked;
    }
}

var universalUsername = $('meta[name="universalUsername"]').attr('content');
var universalKey = $('meta[name="universalKey"]').attr('content');
var username = $('meta[name="username"]').attr('content');

var credential = {
    MyUsername: universalUsername,
    MyKey: universalKey
};

var WHoP = angular.module('app', []);

WHoP.factory('socket', ['$rootScope', function($rootScope) {
    var socket = io.connect();
    return {
        on: function(eventName, callback) {
            socket.on(eventName, function() {
                var args = arguments;
                $rootScope.$apply(function() {
                    callback.apply(socket, args);
                });
            });
        },
        emit: function(eventName, data, callback) {
            socket.emit(eventName, data, function() {
                var args = arguments;
                $rootScope.$apply(function() {
                    if (callback) {
                        callback.apply(socket, args);
                    }
                });
            })
        }
    };
}]);

WHoP.filter('split', function() {
    return function(input, split, index) {
        return input.split(split)[index];
    }
});

WHoP.filter('indexof', function() {
    return function(input, search) {
        if (input.indexOf(search) !== -1) {
            return true;
        } else {
            return false;
        }
    }
});

WHoP.filter('whopletname', function() {
    return function(input) {
        if (input.indexOf('|https|') !== -1) {
            return input.split('|')[0] + '|https';
        } else {
            return input.split('|')[0];
        }
    }
});

WHoP.filter('croncommand', function() {
    return function(input) {
        commandString = '';

        newvalue = input.split('#whopcronid=');
        newvalue2 = newvalue[0].split(' ');

        for (var i = 5; i < newvalue2.length; i++) {
            commandString += ' ' + newvalue2[i];
        }

        return commandString;
    }
});

WHoP.filter("sanitize", ['$sce', function($sce) {
    return function(htmlCode) {
        return $sce.trustAsHtml(htmlCode);
    }
}]);

WHoP.directive('templateAddLocation', ['$compile', function($compile) {
    return {
        link: function(scope, element, attrs) {
            var location = '\
          <li draggable="true" class="hoverable">\
              <div class="collapsible-header"><span class="fa fa-arrows handle"></span> location <span class="modifierBind">(Click here to edit)</span> <span class="locationBind"></span></div>\
              <div class="collapsible-body card">\
                  <div class="card-content">\
                      <form class="locationForm">\
                          <div class="row">\
                              <div class="col s12">\
                                <h5>Location Setting</h5>\
                              </div>\
                              <div class="col s3">\
                                  <label class="active">Location Modifier</label>\
                                  <select name="modifier" class="modifier browser-default">\
                                      <option value=""> </option>\
                                      <option value="=">=</option>\
                                      <option value="~">~</option>\
                                      <option value="~*">~*</option>\
                                      <option value="^~">^~</option>\
                                  </select>\
                              </div>\
                              <div class="input-field col s9">\
                                  <label for="location" class="active">Location</label>\
                                  <input placeholder="Eg: \\.(jpg|html)$ or / or /images/ or /images/(day1|day2)$" name="location" type="text">\
                              </div>\
                          </div>\
                          <div class="row">\
                            <div class="col s12">\
                              <div class="contentDiv"></div>\
                            </div>\
                          </div>\
                      </form>\
                  </div>\
                  <div class="card-action">\
                      <button class="btn blue waves-effect white-text" template-add-content><span class="fa fa-plus"></span> Add Content</button>\
                      <button class="btn light-green waves-effect white-text" ng-click="checkLocation($event)"><span class="fa fa-check"></span> Check Location</button>\
                      <button class="btn red waves-effect white-text" ng-click="deleteLocation($event)"><span class="fa fa-times"></span> Delete Location</button>\
                  </div>\
              </div>\
          </li>';


            element.bind('click', function() {
                $("#locationSettingUL").append($compile(location)(scope));

                $('.collapsible').collapsible();
                $('#locationSettingUL').sortable({
                    handle: '.handle',
                    forcePlaceholderSize: true
                });
            });
        },
    }
}]);

WHoP.directive('templateAddContent', ['$compile', function($compile) {
    return {
        link: function(scope, element, attrs) {
            var content = '\
          <div class="row">\
              <div class="input-field col s2">\
                  <label class="active">Delete</label>\
                  <button class="btn red btnDeleteContent top15px waves-effect" ng-click="deleteContent($event)">x</button>\
              </div>\
              <div class="input-field col s3">\
                  <label class="active">Configuration</label>\
                  <select class="key" name="key">\
                      <option value="allow">allow</option>\
                      <option value="autoindex">autoindex</option>\
                      <option value="cache-control">add_header Cache-Control</option>\
                      <option value="deny">deny</option>\
                      <option value="error_page">error_page</option>\
                      <option value="etag">etag</option>\
                      <option value="expires">expires</option>\
                      <option value="gunzip">gunzip</option>\
                      <option value="gzip">gzip</option>\
                      <option value="if_modified_since">if_modified_since</option>\
                      <option value="limit_rate">limit_rate</option>\
                      <option value="limit_rate_after">limit_rate_after</option>\
                      <option value="root">root</option>\
                      <option value="try_files">try_files</option>\
                  </select>\
              </div>\
              <div class="input-field col s7">\
                  <label for="location" class="active">Content</label>\
                  <input placeholder="See guide for instruction" name="content" type="text">\
              </div>\
          </div>';

            element.bind('click', function() {
                $(this).parent().parent().children().children().find('.contentDiv').append($compile(content)(scope));


                $('.key.select-wrapper span.caret').remove();
                $('.key').material_select('destroy');
                $('.key').material_select();
            });
        }
    }
}]);

ace.config.set("basePath", "/build/ace");
$(".sidebar-collapse").sideNav();
$('select:not([multiple]).material-select').material_select();

$('body').on('click', '.confirmDeletePackage', function() {
    var deleteURL = $(this).attr('data-delete-url');

    $('#deletePackageModal').openModal({
        dismissible: false,
        ready: function() {
            $("#deletePackageForm").attr('action', deleteURL);
        }
    });
});
