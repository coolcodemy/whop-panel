WHoP.controller('MainController', ['$scope', 'socket', function($scope, socket) {

    socket.emit('checklicense-server', []);

}]);


WHoP.controller('LoginController', ['$scope', '$http', '$timeout', function($scope, $http, $timeout) {
    $scope.attempt = false;

    $scope.login = function () {
        $scope.attempt = true;

        var url  = $('form').data('url');

        var data = {
            "username": $scope.username,
            "password": $scope.password,
        };

        $http.post(url, data).then(function (success) {
            Materialize.toast(success.data.message, 2000);

            $timeout(function() {
                window.location.replace(success.data.redirect);
            }, 1000);
        }, function (error) {
            if (error.status == 422) {
                Materialize.toast(error.data.message, 4000);
                $scope.attempt = false;
            }
        });
    }
}]);


WHoP.controller('ViewOwnerController', ['$scope', 'socket', '$http', function($scope, socket, $http) {
    var user = $('meta[name="user"]').attr('content');
    var bandwidth = $('meta[name="bandwidth"]').attr('content');
    var whoplet = $('meta[name="whoplet"]').attr('content');

    credential.User = user;
    credential.Bandwidth = bandwidth;
    credential.Whoplet = whoplet;

    $scope.isBusy = false;
    $scope.universalMessage = null;

    $scope.resetQuota = function() {

        $("#resetQuotaModal").openModal({
            dismissible: false,
        });
    }


    $scope.resetAccount = function() {

        $("#resetAccountModal").openModal({
            dismissible: false,
        });
    }


    $scope.resetAccountCommit = function() {

        $scope.isBusy = true;
        $scope.universalMessage = 'Please wait while requesting...';

        var url = $("#btnResetAccount").data('url');

        $http.get( url ).success(function(data) {

            $scope.isBusy = false;

            $scope.universalMessage = data.message;

        }).error( function(error) {

            $scope.isBusy = false;

        });
    }


    $scope.resetQuotaCommit = function() {

        $scope.isBusy = true;
        
        $scope.universalMessage = 'Please wait while requesting...';

        socket.emit('resetOwnerWebsiteQuota-server', credential);
    }


    $scope.cancel = function() {

        $scope.isBusy = false;
        $scope.universalMessage = null;

    }

    socket.emit('useroverview-user-server', credential);

    socket.on('useroverview-user-diskquota-client', function(color, value, max)
    {
        $scope.progressDiskQuotaColor = color;
        $scope.progressDiskQuotaValue = KBToMB(value);
        $scope.progressDiskQuotaMax = KBToMB(max);
    });

    socket.on('useroverview-user-websitequota-client', function(color, value, max)
    {
        $scope.progressWebsiteQuotaColor = color;
        $scope.progressWebsiteQuotaValue = bytesToMB(value);
        $scope.progressWebsiteQuotaMax = bytesToMB(max);
    });


    socket.on('useroverview-user-whoplet-client', function(color, value, max)
    {
        $scope.progressWhopletColor = color;
        $scope.progressWhopletValue = value;
        $scope.progressWhopletMax = max;
    });


    socket.on('resetOwnerWebsiteQuota-client', function(message) {

        $scope.universalMessage = message;
        $scope.isBusy = false;
        
    });
}]);


WHoP.controller('WhopletController', ['$scope', 'socket', '$http', '$timeout', function($scope, socket, $http, $timeout) {

    $scope.recordsForm = false;
    $scope.rootForm = false;
    $scope.sslForm = false;

    $scope.selected = '/';
    $scope.selectedBefore = '/';

    $scope.channel = $('meta[name="channel"]').attr('content'),
    $scope.streamURL = $('meta[name="streamurl"]').attr('content');

    $scope.isBusy = false;

    $scope.ipv4 = [];
    $scope.ipv6 = [];

    socket.emit('getServerIP-server', []);

    socket.emit('getWhopletList-server', {MyUsername: universalUsername, MyKey: universalKey, User: username});

    $scope.selectTemplate = function() {
        var data = {
            templateID: $scope.choosenTemplate,
        };

        $http.post($('meta[name="getTemplateForm"]').attr('content'), data).then(function(success) {
            $scope.recordsForm = success.data.records;
            $scope.rootForm = success.data.root;
            $scope.sslForm = success.data.ssl;

        }, function (error) {
            if (error.status == 422) {
                var errors = error.data;

                angular.forEach(errors, function(value, key) {
                    Materialize.toast(value, 4000);
                });

            } else if (error.status == 403) {
                Materialize.toast('You are not authorized to make this request', 4000);

            } else {
                Materialize.toast('Unknown error. Please refresh page.', 4000);
            }
        });
    }


    $scope.pickHomeDir = function() {
        $scope.selectedBefore = '/';
        $scope.selected = '/';
        $scope.isLoadingDirectory = true;

        $('#chooseHomeDirModal').openModal({
            dismissible: false,
            ready: function() {
                credential.User = username;
                credential.StartFolder = $scope.selected;
                walk(credential);
            }
        });
    }


    $scope.walkInFolder = function(path) {
        $scope.startFolder = path;

        $scope.selected = $scope.startFolder;
        $scope.selectedBefore = $scope.selected;

        $scope.currentDir = $scope.selectedBefore;
        credential.User = username;
        credential.StartFolder = $scope.startFolder;
        walk(credential);
    }


    $scope.back = function() {
        lastDir = $scope.selectedBefore.split('/')
        lastDir.pop()
        lastDir.pop()
        $scope.selectedBefore = lastDir.join('/') + '/';
        $scope.selected = $scope.selectedBefore;
        $scope.currentDir =  $scope.selected;
        credential.User = username;
        credential.StartFolder = $scope.selectedBefore;
        walk(credential);
    }


    $scope.cancelSelectDir = function () {
        $scope.selected = '/';
    }


    $scope.pickSSL = function() {
        $scope.wait = true;
        $scope.certCount = 1;
        $('#universalCertificateModal').openModal({
            dismissible: false,
            ready: function() {
                credential.User = username;
                socket.emit('getWhopletCertificate-server', credential);
            }
        });
    }

    $scope.setCertificate = function(cert) {
        $scope.ssl = cert;
        $("#universalCertificateModal").closeModal();
    }


    $scope.buildWhoplet = function() {

        var data = {
            domainName: $scope.choosenDomain,
            homeDir: $scope.selected,
            ssl: $scope.ssl,
            templateID: $scope.choosenTemplate,
            channel: $scope.channel,
        };

        $scope.isBusy = true;

        $http.post($('meta[name="buildWhoplet"]').attr('content'), data).then(function(success) {
        }, function (error) {
            $scope.isBusy = false;
            if (error.status == 422) {
                var errors = error.data;

                angular.forEach(errors, function(value, key) {
                    Materialize.toast(value, 4000);
                });
            } else if (error.status == 403) {
                Materialize.toast('You are not authorized to make this request', 4000);
            } else {
                Materialize.toast('Unknown error. Please refresh page', 4000);
            }
        });
    }


    $scope.delete = function (whoplet) {
        $scope.whoplet = whoplet;
        $('#deleteWhopletModal').openModal({
            dismissible: false,
        });
    }


    $scope.deleteWhoplet = function() {

        var json = {MyUsername: universalUsername, MyKey: universalKey, User: username, Whoplet: $scope.whoplet};

        socket.emit('deleteWhoplet-server', json);
    }

    function walk(obj) {
        $scope.haveMoreFolder = true;
        $scope.isLoadingDirectory = true;
        socket.emit('getWhopletHomeDir-server', obj);
    }


    socket.on('getWhopletHomeDir-client', function(jsonArray)
    {
        $scope.folderLists = false;
        $scope.isLoadingDirectory = false;
        if (jsonArray.length == 0) {
            $scope.haveMoreFolder = false;
        } else {
            $scope.haveMoreFolder = true;

            $scope.folderLists = jsonArray;
        }
    });


    socket.on('getWhopletCertificate-client', function(jsonArray)
    {
        $scope.wait = false;
        $scope.certCount = jsonArray.length;

        if ($scope.certCount > 0) {
            $scope.certificates = jsonArray;
        }
    });

    socket.on($scope.channel, function(json)
    {
        $scope.isBusy = false;
        if (json.finish == false) {
            Materialize.toast(json.message, 2000);
        } else {
            Materialize.toast(json.message, 4000);
            $scope.isBusy = false;
            socket.emit('getWhopletList-server', {MyUsername: universalUsername, MyKey: universalKey, User: username});
        }
    });

    socket.on('getWhopletList-client', function(jsonArray)
    {
        $scope.whoplets = jsonArray;
    });


    socket.on('deleteWhoplet-client', function(json)
    {
        if (json.finish == false) {
            Materialize.toast(json.message, 2000);
        } else {
            Materialize.toast(json.message, 4000);
            socket.emit('getWhopletList-server', {MyUsername: universalUsername, MyKey: universalKey, User: username});
        }
    });


    socket.on('getServerIP-client', function(net) {

        angular.forEach(net, function(netint, index) {

            angular.forEach(netint, function(obj, index) {

                if (obj.internal === false && obj.family == "IPv4") {

                    $scope.ipv4.push(obj.address);

                } if (obj.internal === false && obj.family == "IPv6") {

                    $scope.ipv6.push(obj.address);

                }

            });

        });

    });
}]);


WHoP.controller('WhopletLogController', ['$scope', 'socket', function($scope, socket) {
    $scope.logType = gup('type');
    $scope.whoplet = gup('whoplet');


    var editor = ace.edit("logViewer");
    editor.setTheme("ace/theme/github");
    editor.getSession().setMode("ace/mode/php");
    editor.setShowPrintMargin(false);
    editor.setReadOnly(true);
    editor.setValue('Log will be displayed in here in reverse mode');
    editor.gotoLine(1);

    credential.Whoplet = $scope.whoplet;
    credential.Log = $scope.logType;
    credential.User = username;
    editor.setValue('Your log is in streaming mode. Visit your website to start displaying your log...');
    socket.emit('whoplet-log-server', credential);

    socket.on('whoplet-log-client', function(message)
    {
        if (message.flag == false) {
            Materialize.toast(message.message, 4000);
        } else {
            editor.gotoLine(1);
            editor.insert(message.log);
        }
    });


    // code get from http://stackoverflow.com/questions/979975/how-to-get-the-value-from-the-url-parameter/979997#979997
    function gup( name ) {
        url = location.href
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?&]"+name+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec( url );
        return results == null ? null : results[1];
    }
}]);


WHoP.controller('WhopletTemplateController', ['$scope', 'socket', '$http', '$timeout', function($scope, socket, $http, $timeout) {

    $scope.isBusy = false;
    $scope.locationCount = 1;


    $scope.craft = function(templateID) {
        $scope.isBusy = true;
        $scope.templateID = templateID;

        var data = {
            templateID: $scope.templateID,
        }

        $http.post($('meta[name="get-template-url"]').attr('content'), data).then(function(success) {
            initCraft();
            $scope.template = success.data;

            Materialize.toast('Done. Scroll below to start.', 2000);

            templateSetting($scope.template);
            $("#locationSetting").html('');
            $('#previewPlaceholder').addClass('editor');
            var editor = ace.edit("previewPlaceholder");
            editor.setTheme("ace/theme/github");
            editor.getSession().setMode("ace/mode/yaml");
            editor.setShowPrintMargin(false);
            editor.setReadOnly(true);
            editor.setValue($scope.template);
            editor.gotoLine(1);

        }, function(error) {
            $scope.isBusy = false;
            if (error.status == 404) {
                Materialize.toast('Template not found', 4000);
            } else {
                Materialize.toast('Unknown error. Please refresh page.', 4000);
            }
        });
    }


    $scope.checkLocation = function(event) {
        $scope.isBusy = true;
        var form = angular.element(event.target).parent().parent().children().children('form');
        var data = {data: JSON.stringify($(form).serializeObject())};


        $http.post($('meta[name="checkportion-url"]').attr('content'), data).then(function(success) {
            Materialize.toast(success.data.message, 2000);
            $scope.isBusy = false;
        }, function(error) {
            $scope.isBusy = false;
            Materialize.toast(error.data.message, 4000);
        });
    }


    $scope.deleteLocation = function (event) {
        angular.element(event.target).parent().parent().parent().remove();
    }


    $scope.deleteContent = function (event) {
        angular.element(event.target).parent().parent().remove();
    }


    $scope.preview = function () {
        $scope.isBusy = true;

        data = getTemplateData();

        $http.post($('meta[name="preview-url"]').attr('content'), data).then(function(success) {
            $scope.isBusy = false;
            Materialize.toast('Done', 2000);

            $('#previewPlaceholder').addClass('editor');
            var editor = ace.edit("previewPlaceholder");
            editor.setTheme("ace/theme/github");
            editor.getSession().setMode("ace/mode/yaml");
            editor.setShowPrintMargin(false);
            editor.setReadOnly(true);
            editor.setValue(success.data);
            editor.gotoLine(1);

        }, function (error) {
            $scope.isBusy = false;
            if (error.status == 422) {
                Materialize.toast(error.data.message, 4000);
            } else {
                Materialize.toast('Unknown error. Please refresh page.', 4000);
            }
        });
    }


    $scope.saveTemplate = function () {
        data = getTemplateData();
        data.templateName = $scope.templateName;

        console.log(data);

        $http.post($('meta[name="save-url"]').attr('content'), data).then(function(success) {
            $scope.isBusy = false;
            console.log(success);
            Materialize.toast(success.data.message, 4000);
            $timeout(function() {
                window.location.reload();
            }, 1500);
        }, function (error) {
            $scope.isBusy = false;
            if (error.status == 422) {
                angular.forEach(error.data, function(value, key) {
                    Materialize.toast(value, 4000);
                });
            } else {
                Materialize.toast('Unknown error. Please refresh page.', 4000);
            }
        });
    }


    $scope.deleteTemplate = function (url) {
        $('#deleteTemplateModal').openModal({
            dismissible: false,
            ready: function() {
                $("#deleteTemplateForm").attr('action', url);
            }
        });
    }


    $('body').on('change', 'select.key' ,function() {

        if ($(this).val() === 'try_files') {
            Materialize.toast('<span>try_files will only be validated when creating WHoPlet</span><a class="btn-flat yellow-text" onclick="$(this).parent().slideUp();">Understood<a>', 15000);
        }
    });


    $('body').on('keyup', 'input[name="location"]', function(e)
    {
        var location = $(this).val();
        $(this).parent().parent().parent().parent().parent().siblings('.collapsible-header').children('.locationBind').html(location);
    });

    $('body').on('change', '.modifier', function(e)
    {
        var mod = $(this).val();
        $(this).parent().parent().parent().parent().parent().siblings('.collapsible-header').children('.modifierBind').html(mod);
    });


    function templateSetting(template) {

        $scope.sameOrigin = false;
        $scope.hasSameOrigin = false;
        $scope.xssProtection = false;
        $scope.hasXssProtection = false;
        $scope.noSniff = false;
        $scope.hasNoSniff = false;
        $scope.sts = false;
        $scope.hasSts = false;
        $scope.cache = false;
        $scope.hasCache = false;


        if (template.indexOf("#add_header X-Frame-Options \"SAMEORIGIN\";") !== -1) {
            $scope.sameOrigin = false;
            $scope.hasSameOrigin = true;
        } else if (template.indexOf("add_header X-Frame-Options \"SAMEORIGIN\";") !== -1) {
            $scope.sameOrigin = true;
            $scope.hasSameOrigin = true;
        }

        if (template.indexOf("#add_header X-XSS-Protection \"1; mode=block\";") !== -1) {
            $scope.xssProtection = false;
            $scope.hasXssProtection = true;

        } else if (template.indexOf("add_header X-XSS-Protection \"1; mode=block\";") !== -1) {
            $scope.xssProtection = true;
            $scope.hasXssProtection = true;
        }


        if (template.indexOf("#add_header X-Content-Type-Options \"nosniff\";") !== -1) {
            $scope.noSniff = false;
            $scope.hasNoSniff = true;

        } else if (template.indexOf("add_header X-Content-Type-Options \"nosniff\";") !== -1) {
            $scope.noSniff = true;
            $scope.hasNoSniff = true;
        }


        if (template.indexOf("#add_header Strict-Transport-Security \"max-age=31536000\";") !== -1) {
            $scope.sts = false;
            $scope.hasSts = true;

        } else if (template.indexOf("add_header Strict-Transport-Security \"max-age=31536000\";") !== -1) {
            $scope.sts = true;
            $scope.hasSts = true;
        }


        if (template.indexOf("#fastcgi_cache WHOPWEBAPP;") !== -1) {
            $scope.cache = false;
            $scope.hasCache = true;

        } else if (template.indexOf("fastcgi_cache WHOPWEBAPP;") !== -1) {
            $scope.cache = true;
            $scope.hasCache = true;
        }
    }


    function initCraft() {
        $scope.isBusy = false;
        $scope.canModifySetting = true;
        $scope.canAddLocation = true;
        $scope.canPreview = true;
        $scope.canSaveTemplate = true;
    }


    function getTemplateData() {
        var formPreview = [];
        $.each($('.locationForm'), function(index, value)
        {
            formPreview.push(JSON.stringify($(this).serializeObject()));
        });

        var data = {
            dataall: formPreview,
            templateID: $scope.templateID,
            sameorigin: $scope.sameOrigin,
            xssprotection: $scope.xssProtection,
            nosniff: $scope.noSniff,
            sts: $scope.sts,
            cache: $scope.cache,
            cacheTime: $scope.cacheTime,
        };

        return data
    }

    $.fn.serializeObject = function()
    {
        var o = {};
        var a = this.serializeArray();
        var contentArr = [];
        var key;
        $.each(a, function(index) {

            if (this.name === 'modifier' || this.name === 'location' || this.name === 'waf') {
                o[this.name] = this.value;
            }

            if (this.name === 'key' || this.name === 'content') {

                if (this.name == 'key') {
                    key = this.value;
                } else {
                    var content = {};
                    content.key = key;
                    content.content = this.value;
                    contentArr.push(content);
                }
            }

        });
        o.content = contentArr;
        return o;
    };
}]);


WHoP.controller('MariaDBController', ['$scope', function($scope) {

    $scope.revokeUser = function(username, database) {
        
        $scope.database = database;
        $scope.databaseUser = username;

        $("#revokeDatabaseUserModal").openModal();
    }


    $scope.deleteDatabase = function(database) {
        $scope.database = database;

        $("#deleteDatabaseModal").openModal();
    }


    $scope.deleteUser = function(username) {
        $scope.databaseUser = username;
        $("#deleteDatabaseUserModal").openModal();
    }
}]);


WHoP.controller('DomainController', ['$scope', '$sce', function($scope, $sce) {

    $scope.deleteDomain = function (url) {
        $("#deleteDomainForm").attr('action', url);
        $("#deleteDomainModal").openModal();
    }
}]);


WHoP.controller('RecordController', ['$scope', '$http', 'socket', function($scope, $http, socket) {

    $scope.ipv4 = [];
    $scope.ipv6 = [];

    socket.emit('getServerIP-server', []);

    $scope.getRecord = function() {
        $scope.recordURL = $("#selectDomain option:selected").data('record-url');

        $http.post($scope.recordURL).then(function (success) {
            Materialize.toast('Done. Records displayed below.', 2000);

            $scope.records = success.data;
        }, function (error) {
            if (error.status == 404) {
                Materialize.toast('Unknown domain provided.', 4000);
            } else {
                Materialize.toast('Unknown error. Please refresh page.', 4000);
            }
        });
    }


    $scope.deleteRecord = function (url) {
        $("#deleteRecordForm").attr('action', url);

        $("#deleteRecordModal").openModal();
    }


    $('body').on('change', '.recordType', function()
    {
        var val = $(this).val();
        
        if (val == 'MX') {
            $('.priorityInput').attr('disabled', false);
        } else {
            $('.priorityInput').attr('disabled', true);
        }
    });



    socket.on('getServerIP-client', function(net) {

        angular.forEach(net, function(netint, index) {

            angular.forEach(netint, function(obj, index) {

                if (obj.internal === false && obj.family == "IPv4") {

                    $scope.ipv4.push(obj.address);

                } if (obj.internal === false && obj.family == "IPv6") {

                    $scope.ipv6.push(obj.address);

                }

            });

        });

    });
}]);


WHoP.controller('EmailController', ['$scope', function($scope) {

    $scope.deleteEmail = function(url) {
        $("#deleteEmailModal").openModal({
            dismissible: false,
            ready: function() {
                $("#deleteEmailForm").attr('action', url);
            }
        });
    }
}]);


WHoP.controller('EmailForwardingController', ['$scope', function($scope) {
    $scope.delete = function(url) {
        $("#deleteForwardingModal").openModal({
            dismissible: false,
            ready: function() {
                $("#deleteForwardingForm").attr('action', url);
            }
        });
    }
}]);


WHoP.controller('FileManagerController', ['$scope', 'socket', function($scope, socket) {
    credential.User = $('meta[name="username"]').attr('content');
    credential.StartFolder = '/';

    $scope.parent = '/';
    $scope.nofolder = false;
    $scope.selectedItems = [];
    $scope.currentLocation = '/';
    $scope.fileAndFolder;

    /**
     * Button
     */
    $scope.changePermissionButton = false;
    $scope.editButton = false;
    $scope.deleteButton = false;
    $scope.renameButton = false;


    /**
     * Permission
     */
    resetPermission();


    $scope.isLoading = true;
    $scope.isBusy = false;
    $scope.universalMessage = null;


    $scope.walk = function (item) {

        var type = item.type;
        var location = item.location;

        if (type == 'inode/directory') {
            $scope.isLoading = true;
            credential.StartFolder = location;
            socket.emit('filemanager-list-server', credential);
        }
    }


    $scope.goToParent = function() {
        $scope.isLoading = true;

        var location = $scope.parent;
        credential.StartFolder = location;
        socket.emit('filemanager-list-server', credential);
    }


    $scope.newFileModal = function() {
        $("#addFileModal").openModal({
            dismissible: false,
        });
    }

    $scope.newDirectoryModal = function() {
        $("#addDirectoryModal").openModal({
            dismissible: false,
        });
    }


    $scope.deleteFileDirectoryModal = function () {
        $("#deleteFileDirectoryModal").openModal({
            dismissible: false,
        });
    }


    $scope.renameFileModal = function() {
        $("#renameFileModal").openModal({
            dismissible: false,
        });
    }


    $scope.permissionModal = function() {
        $("#permissionModal").openModal({
            dismissible: false,
        })
    }


    $scope.newFile = function () {
        $scope.isBusy = true;
        $scope.universalMessage = 'Please wait while adding new file...';
        credential.StartFolder = $scope.currentLocation;
        credential.FileName = $scope.fileName;
        socket.emit('filemanager-addfile-server', credential);
    }


    $scope.newDirectory = function () {
        $scope.isBusy = true;
        $scope.universalMessage = 'Please wait while adding new folder...';
        credential.StartFolder = $scope.currentLocation;
        credential.DirectoryName = $scope.directoryName;
        socket.emit('filemanager-adddirectory-server', credential);
    }


    $scope.delete = function() {
        $scope.isBusy = true;
        credential.Content = $scope.selectedItems.join('/');
        credential.StartFolder = $scope.currentLocation;
        socket.emit('filemanager-delete-server', credential);
    }


    $scope.rename = function() {
        $scope.isBusy = true;
        credential.NewName = $scope.newName;
        credential.OldName = $scope.selectedItems[0];
        credential.StartFolder = $scope.currentLocation;
        socket.emit('filemanager-rename-server', credential);
    }


    $scope.changePermission = function() {
        $scope.isBusy = true;
        
        var userRead = $scope.userRead == true ? 4 : 0;
        var userWrite = $scope.userWrite == true ? 2: 0;
        var userExecute = $scope.userExecute == true ? 1 : 0;

        var groupRead = $scope.groupRead == true ? 4 : 0;
        var groupWrite = $scope.groupWrite == true ? 2 : 0;
        var groupExecute = $scope.groupExecute == true ? 1 : 0;

        var worldRead = $scope.worldRead == true ? 4 : 0;
        var worldWrite = $scope.worldWrite == true ? 2 : 0;
        var worldExecute = $scope.worldExecute == true ? 1 : 0;

        credential.Content = $scope.selectedItems.join('/');
        credential.StartFolder = $scope.currentLocation;
        credential.PermUser = userRead + userWrite + userExecute;
        credential.PermGroup = groupRead + groupWrite + groupExecute;
        credential.PermWorld = worldRead + worldWrite + worldExecute;

        socket.emit('filemanager-permission-server', credential);
    }


    $scope.cancel = function () {
        $scope.isBusy = false;
        $scope.isLoading = false;
        $scope.universalMessage = null;
        resetPermission();
    }


    $scope.edit = function() {
        var location = $scope.currentLocation.replace(/\//g, ';');
        var file = $scope.selectedItems[0];
        file = file.replace(/\./g, ';.');

        window.open($('meta[name="editor"]').attr('content') + '/' + location + '/' + file,'_blank');
    }


    $('table').multiSelect({
        actcls: 'light-blue',
        selector: 'tbody tr',
        except: ['tbody'],
        callback: function (items) {
            if (items.length == 0) {
                $scope.changePermissionButton = false;
                $scope.editButton = false;
                $scope.deleteButton = false;
                $scope.renameButton = false;
            } else {
                $scope.selectedItems = [];
                $scope.changePermissionButton = true;
                $scope.editButton = false;
                $scope.deleteButton = true;
            }

            if (items.length == 1 && items[0].children[3].innerHTML !== "inode/directory") {
                $scope.editButton = true;
            }

            if (items.length == 1) {
                $scope.renameButton = true;
                $scope.newName = items[0].children[1].innerHTML;
            }

            angular.forEach(items, function(value, key) {
                $scope.selectedItems.push(value.children[1].innerHTML);
            });

            $scope.$apply();
        },
    });


    socket.emit('filemanager-list-server', credential);


    socket.on('filemanager-list-client', function(res)
    {
        $scope.isLoading = false;
        $scope.currentLocation = res.currentlocation;
        $scope.parent = res.parent;
        $scope.fileAndFolder = res.content;

        if (res.success == false) {
            $scope.nofolder = true;
        } else {
            $scope.nofolder = false;
        }

        if (res.parent == res.currentlocation) {
            $scope.superParent = true;
        } else {
            $scope.superParent = false;
        }
    });

    socket.on('filemanager-addfile-client', function(message)
    {
        refresh(message);
    });

    socket.on('filemanager-adddirectory-client', function(message)
    {
        refresh(message);
    });


    socket.on('filemanager-delete-client', function(message)
    {
        refresh(message);
    });


    socket.on('filemanager-permission-client', function(message)
    {
        refresh(message);
    });

    socket.on('filemanager-rename-client', function(message)
    {
        refresh(message);
    });


    function refresh(message) {
        $scope.isBusy = false;
        $scope.universalMessage = message.message;

        if (message.success == true) {
            credential.StartFolder = $scope.currentLocation;
            $scope.isLoading = true;
            socket.emit('filemanager-list-server', credential);
        }
    }

    function resetPermission() {
        $scope.userRead = false;
        $scope.userWrite = false;
        $scope.userExecute = false;

        $scope.groupRead = false;
        $scope.groupWrite = false;
        $scope.groupExecute = false;

        $scope.worldRead = false;
        $scope.worldWrite = false;
        $scope.worldExecute = false;
    }
}]);


WHoP.controller('FileEditorController', ['$scope', 'socket', function($scope, socket) {
    credential.User = $('meta[name="username"]').attr('content');
    credential.StartFolder = $('meta[name="startfolder"]').attr('content');
    credential.File = $('meta[name="file"]').attr('content');

    $scope.isBusy = true;

    socket.emit('filemanager-editor-server', credential);
    Materialize.toast('Please wait while fetching data...', 2000);

    $scope.changeModeModal = function () {
        $("#editorModeModal").openModal({
            dismissible: false,
        });
    }

    $scope.changeEditor = function() {
        $("#editorModeModal").closeModal();

        var editor = ace.edit("editor");
        editor.getSession().setMode("ace/mode/" + $scope.editorMode);
    }


    $scope.save = function() {
        $scope.isBusy = true;
        Materialize.toast('Saving content...', 2000);
        var editor = ace.edit("editor");
        var code = editor.getSession().getValue();
        credential.Content = code;
        socket.emit('filemanager-editsave-server', credential);
    }


    socket.on('filemanager-editor-client', function(message)
    {
        if (message.success == false) {
            Materialize.toast(message.message, 4000);
        } else {
            $scope.isBusy = false;
            $('#editor').addClass('editor');
            var data = message.data.join('\n')
            var editor = ace.edit("editor");
            editor.setTheme("ace/theme/github");
            editor.getSession().setMode("ace/mode/php");
            editor.setShowPrintMargin(false);
            editor.setValue(data);
            editor.gotoLine(1);
        }
    });

    socket.on('filemanager-editsave-client', function(message)
    {
        $scope.isBusy = false;
        if (message.success == false) {
            Materialize.toast(message.message, 4000);
        } else {
            Materialize.toast(message.message, 2000);
        }
    });
}]);


WHoP.controller('FtpController', ['$scope', 'socket', function($scope, socket) {

    $scope.selected = '/';
    $scope.selectedBefore = '/';


    $scope.delete = function(url) {
        $("#deleteFTPModal").openModal({
            dismissible: false,
            ready: function() {
                $("#deleteFTPForm").attr('action', url);
            }
        });
    }

    $scope.pickHomeDir = function() {
        $scope.selectedBefore = '/';
        $scope.selected = '/';
        $scope.isLoadingDirectory = true;

        $('#chooseHomeDirModal').openModal({
            dismissible: false,
            ready: function() {
                credential.User = username;
                credential.StartFolder = $scope.selected;
                walk(credential);
            }
        });
    }


    $scope.walkInFolder = function(path) {
        $scope.startFolder = path;

        $scope.selected = $scope.startFolder;
        $scope.selectedBefore = $scope.selected;

        $scope.currentDir = $scope.selectedBefore;
        credential.User = username;
        credential.StartFolder = $scope.startFolder;
        walk(credential);
    }


    $scope.back = function() {
        lastDir = $scope.selectedBefore.split('/')
        lastDir.pop()
        lastDir.pop()
        $scope.selectedBefore = lastDir.join('/') + '/';
        $scope.selected = $scope.selectedBefore;
        $scope.currentDir =  $scope.selected;
        credential.User = username;
        credential.StartFolder = $scope.selectedBefore;
        walk(credential);
    }


    $scope.cancelSelectDir = function () {
        $scope.selected = '/';
    }


    function walk(obj) {
        $scope.haveMoreFolder = true;
        $scope.isLoadingDirectory = true;
        socket.emit('getFTPHomeDir-server', obj);
    }


    socket.on('getFTPHomeDir-client', function(jsonArray)
    {
        $scope.folderLists = false;
        $scope.isLoadingDirectory = false;
        if (jsonArray.length == 0) {
            $scope.haveMoreFolder = false;
        } else {
            $scope.haveMoreFolder = true;

            $scope.folderLists = jsonArray;
        }
    });
}]);


WHoP.controller('SSLController', ['$scope', 'socket', '$window', function($scope, socket, $window) {
    credential.User = $('meta[name="username"]').attr('content');
    socket.emit('getWhopletCertificate-server', credential);
    $scope.isBusy = false;
    $scope.deleteMessage = null;


    $scope.addCertificate = function() {
        $scope.isBusy = true;

        credential.CertName = $scope.certificateName;
        credential.PublicCert = $scope.publicCertificate;
        credential.PrivateCert = $scope.privateCertificate;

        socket.emit('addCertificate-server', credential);
    }


    $scope.view = function(cert) {
        credential.Cert = cert.slice(0, -4);
        socket.emit('viewcertificate-server', credential);
    }


    $scope.delete = function(cert) {
        $scope.choosenDeleteCertificate = cert;
        $("#deleteCertificateModal").openModal({
            dismissible: false,
        });
    }


    $scope.deleteCertificate = function() {
        $scope.isBusy = true;
        credential.CertName = $scope.choosenDeleteCertificate .slice(0, -4);
        socket.emit('deleteUserCertificate-server', credential);
    }


    $scope.cancel = function () {
        $scope.isBusy = false;
        $scope.deleteMessage = null;
    }


    socket.on('getWhopletCertificate-client', function(jsonArray)
    {
        $scope.certCount = jsonArray.length;
        $scope.certs = jsonArray;
    });


    socket.on('addCertificate-client', function(message)
    {
        if (message.flag == false) {
            $scope.isBusy = false;
            Materialize.toast(message.message, 4000);
        } else {

            $scope.certificateName = null;
            $scope.publicCertificate = null;
            $scope.privateCertificate = null;

            $scope.isBusy = false;

            Materialize.toast(message.message, 2000);
        }
    });

    socket.on('viewcertificate-client', function(content)
    {
        $scope.certContent = content;
        $('#viewCertificateModal').openModal({
            dismissible: false,
            complete: function() {
                $scope.certContent = '';
            }
        });
    });


    socket.on('deleteUserCertificate-client', function(message)
    {
        $scope.deleteMessage = message.message;

        if (message.flag == true) {
            socket.emit('getWhopletCertificate-server', credential);
        }
    });
}]);


WHoP.controller('CronController', ['$scope', 'socket', '$window', function($scope, socket, $window) {

    credential.User = $('meta[name="username"]').attr('content');
    socket.emit('usercronlist-server', credential);

    $scope.isBusy = false;

    $scope.updateMinute = function() {
        $scope.minute = $scope.selectMinute;
    }

    $scope.updateHour = function () {
        $scope.hour = $scope.selectHour;
    }

    $scope.updateDay = function () {
        $scope.day = $scope.selectDay;
    }

    $scope.updateMonth = function () {
        $scope.month = $scope.selectMonth;
    }

    $scope.updateWeekday = function () {
        $scope.weekday = $scope.selectWeekday;
    }


    $scope.addCron = function () {
        $scope.isBusy = true;

        credential.Minute = $scope.minute;
        credential.Hour = $scope.hour;
        credential.Day = $scope.day;
        credential.Month = $scope.month;
        credential.DOW = $scope.weekday;
        credential.Command = $scope.command;

        socket.emit('registerusercron-server', credential);
    }


    $scope.deleteCron = function(job) {
        
        $scope.cronID = job.split('#whopcronid=')[1];

        $("#deleteCronjobModal").openModal({
            dismissible: false,
        });
    }


    $scope.delete = function() {
        $scope.isBusy = true;
        credential.cronID = $scope.cronID;
        socket.emit('deleteusercron-server', credential);
    }


    $scope.cancel = function() {
        $scope.isBusy = false;
        $scope.deleteMessage = null;
    }


    socket.on('registerusercron-client', function(message)
    {
        if (message.success == false) {
            Materialize.toast(message.message, 4000);
            $scope.isBusy = false;
        } else {
            Materialize.toast(message.message, 2000, '', function() {
                $window.location.reload();
            });
        }
    });

    socket.on('usercronlist-client', function(json)
    {
        $scope.cronJobCount = json.content.length;
        $scope.jobs = json.content;
    });


    socket.on('deleteusercron-client', function(message)
    {
        $scope.deleteMessage = message.message;

        if (message.success == true) {
            socket.emit('usercronlist-server', credential);
        }
    });
}]);


WHoP.controller('SupervisorController', ['$scope', 'socket', function($scope, socket) {
    credential.User = $('meta[name="username"]').attr('content');
    socket.emit('usersupervisorlist-server', credential);


    $scope.empty = false;
    $scope.universalMessage = null;
    $scope.contents = [];
    $scope.deleteJob = null;
    $scope.restartJob = null;


    $scope.deleteModal = function(jobName) {
        $scope.deleteJob = jobName;
        $("#deleteSupervisorModal").openModal({
            dismissible: false,
        })
    }


    $scope.restartModal = function(jobName) {
        $scope.restartJob = jobName;
        $("#restartSupervisorModal").openModal({
            dismissible: false,
        })
    }


    $scope.delete = function () {
        $scope.isBusy = true;
        credential.DeleteJob = $scope.deleteJob;

        $scope.universalMessage = "Please wait while deleting your job";
        
        socket.emit('usersupervisordelete-server', credential);
    }

    $scope.restart = function () {
        $scope.isBusy = true;
        credential.RestartJob = $scope.restartJob;

        $scope.universalMessage = "Please wait while restarting your job";
        
        socket.emit('usersupervisorrestart-server', credential);
    }


    $scope.cancel = function () {
        $scope.isBusy = false;
        $scope.universalMessage = null;
        $scope.deleteJob = null;
        $scope.restartJob = null;
    }


    socket.on('usersupervisorlist-client', function (file) {
        if (Object.keys(file).length == 0) {
            $scope.empty = true;
        } else {
            $scope.contents.push(file);
        }
    });

    socket.on('usersupervisordelete-client', function (json) {
        $scope.universalMessage = json.message;
        if (json.success == true) {
            socket.emit('usersupervisorlist-server', credential);
        }
    });

    socket.on('usersupervisorrestart-client', function (json) {
        $scope.universalMessage = json.message;
    });
}]);


WHoP.controller('SupervisorAddController', ['$scope', 'socket', '$window', function($scope, socket, $window) {
    credential.User = $('meta[name="username"]').attr('content');

    $scope.isBusy = false;

    $scope.command = "php /var/www/" + $('meta[name="username"]').attr('content') + "/";

    $scope.addJob = function () {
        $scope.isBusy = true;
        credential.JobName = $scope.jobName;
        credential.Command = $scope.command;

        socket.emit('usersupervisoradd-server', credential);
    }


    socket.on('usersupervisoradd-client', function (data) {
        if (data.success == false) {
            Materialize.toast(data.message, 4000);
            $scope.isBusy = false;
        } else {
            Materialize.toast(data.message, 2000, '', function() {
                $window.location.reload();
            });
        }
    });
}]);