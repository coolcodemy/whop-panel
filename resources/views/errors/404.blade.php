@extends('materializecss.layout.master', ['title' => 'Whooppsss. Page not found'])

@section('content')


<div class="row">

    <div class="col s12">

            <h3 class="center-align">The page that you requested cannot be found</h3>

            <p class="center-align">We are sorry for this inconvenience. We hope that you will never see this page again.</p>

    </div>

</div>


@stop