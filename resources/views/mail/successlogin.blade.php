@extends('mail.layout')


@section('subject')
	Succesfull login to WHoP
@stop


@section('content1')
	<p>Hello {{ $name }},</p>
	<br>
	<p>We sent this email to notify you that your WHoP account was login at <b>{{ $time }}</b> from IP address <b>{{ $ip }}</b>. If you are not the one who perform this action, please contact your administrator.</p>
	<br>
@stop