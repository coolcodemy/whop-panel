<!DOCTYPE html>
<html lang="en-US" ng-app="app" ng-controller="MainController">
	<head>
		<meta charset=utf-8 />
		<title>{{ $title }}</title>
		<meta name="description" content="WHoP Materialize Themes">
		<meta name="author" content="Skeleton by Materializecss, authored by Ahmad Fikrizaman">

		<!--Let browser know website is optimized for mobile-->
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<meta name="universalUsername" content="{{ isset(Auth::user()->username) ? Auth::user()->username : null }}">
		<meta name="universalKey" content="{{ isset(Auth::user()->secretKey) ? Auth::user()->secretKey : null }}">
		<meta name="username" content="{{ isset($user->username) ? $user->username : null }}">
		@yield('meta')

		<!--Import materialize.css-->
		<link type="text/css" rel="stylesheet" href="{{ elixir('css/' . env('WHOP_TEMPLATE') . '/all.css') }}"  media="screen,projection"/>
		@yield('css')
		<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	</head>

	<body class="{{ $bodyClass or ''}}">

		@if (isset($templatestyle) && $templatestyle == 'login')

			@include('materializecss.partial._logincard')

		@else

			@include('materializecss.partial._topbar')

			<div id="main">
			    <div class="wrapper">

					<aside id="left-menu">
					    <ul id="slide-out" class="side-nav fixed">
					        <li>
					            <div class="row" id="sideprofile">
					                <div class="col s8 offset-s2">
					                    <a class='dropdown-button waves-effect waves-light white-text' href='#' data-beloworigin="true" data-activates='dropdownprofile'>{{ Auth::user()->username }} <span style="padding-top:13px" class="fa fa-caret-down right"></span></a>
					                    <ul id="dropdownprofile" class="dropdown-content">
					                        <li><a href="{{ route('manual') }}"><i class="material-icons">description</i>Manual</a></li>
					                        <li><a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i>Logout</a></li>
					                    </ul>
					                </div>
					            </div>
					        </li>

					    	@if ( Auth::user()->hasRole('admin') )

					    		@include('materializecss.partial._navadmin')

					    	@elseif ( Auth::user()->hasRole('owner') )

					    		@include('materializecss.partial._navowner')

					    	@endif
					    	
					    </ul>
					</aside>



			        <section class="whopcontent">
			            <div class="container">
			            	@include('materializecss.errors._message')
			            	@yield('content')
			            </div>
			        </section>
			    </div>
			</div>

			<footer class="page-footer blue darken-1">
				<div class="footer-copyright">
					<div class="container">
						Built with love by Cool Code Sdn. Bhd. (Codename: {{ env('WHOP_CODENAME') }} Version: {{ env('WHOP_VERSION') }})
					</div>
				</div>
			</footer>
		@endif

		@include('materializecss.partial._modal', ['modal' => 'universalmodal'])

		<script src="/socket.io/socket.io.js"></script>
		<script type="text/javascript" src="{{ elixir('js/' . env('WHOP_TEMPLATE') . '/all.js') }}"></script>

		@if ( Auth::check() && Auth::user()->hasRole('admin') )

			<script type="text/javascript" src="{{ elixir('js/' . env('WHOP_TEMPLATE') . '/all-admin.js') }}"></script>

		@endif

		@yield('script')

		@if (Session::has('toastMessage'))
		  <script>
		    $(document).ready(function()
		    {
		      Materialize.toast('{{ Session::get("toastMessage") }}', 4000);
		    });
		  </script>
		@endif
	</body>
</html>