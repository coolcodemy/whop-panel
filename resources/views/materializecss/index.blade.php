@extends('materializecss.layout.master', ['title' => 'Login To Account', 'bodyClass' => 'grey lighten-4', 'templatestyle' => 'login'])



@section('content')
	<form method="POST" class="col s12" data-url="{{ route('guest::attemptlogin') }}" ng-controller="LoginController">
		<div class="row">
			<div class="col s12">
				<span class="card-title black-text">Login to WHoP</span>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12">
				<i class="mdi-action-account-box prefix"></i>
				<input name="username" ng-model="username" type="text" length="8">
				<label class="active" for="username">Username</label>
			</div>
		</div>

		<div class="row">
			<div class="input-field col s12">
				<i class="mdi-action-lock prefix"></i>
				<input name="password" ng-model="password" type="password">
				<label class="active" for="password">Password</label>
			</div>
		</div>

		<button class="waves-effect waves-light btn teal" ng-click="login()" ng-disabled="attempt">Login</button>

		<div class="progress" id="loginProgress" ng-show="attempt">
			<div class="indeterminate"></div>
		</div>
	</form>
@stop