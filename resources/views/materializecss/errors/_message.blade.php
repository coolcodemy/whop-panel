@if (!$errors->isEmpty())
  <div class="row">
      <div class="col s12">
          <div class="card red white-text">
              <div class="card-content">
                  @foreach ($errors->all() as $error)
                    <p>* {{ $error }}</p>
                  @endforeach
              </div>
          </div>
      </div>
  </div>
@endif


@if (Session::has('successMessage'))
  <div class="row">
      <div class="col s12">
          <div class="card light-green white-text">
              <div class="card-content">
                    <p>{{ Session::get('successMessage') }}</p>
              </div>
          </div>
      </div>
  </div>
@endif


@if (Session::has('errorMessage'))
  <div class="row">
      <div class="col s12">
          <div class="card red white-text">
              <div class="card-content">
                    <p>{{ Session::get('errorMessage') }}</p>
              </div>
          </div>
      </div>
  </div>
@endif