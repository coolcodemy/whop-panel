<div class="row">
	<div class="input-field col s12">
		<select class="material-select" name="domain" {{ isset($editMode) && $editMode == true ? 'disabled' : null }}>
			@foreach ($domains as $domain)
				@if ((isset($record->domain_id) && $record->domain_id == $domain->id) || old('domain'))
					<option value="{{ $domain->id }}" selected>{{ $domain->name }}</option>
				@else
					<option value="{{ $domain->id }}">{{ $domain->name }}</option>
				@endif
			@endforeach
		</select>
		<label>Select domain name</label>
	</div>
</div>
<div class="row">
	<div class="input-field col s12">
		<label class="active">Type</label><br>
		<select class="browser-default recordType" name="type" {{ isset($editMode) && $editMode == true ? 'disabled' : null }}>
			<option value="" disabled selected>Please select record type</option>
			@foreach ($types as $key => $type)
				@if ((isset($record->type) && $record->type == $type) || old('type'))
					<option value="{{ $type }}" selected>{{ $type }}</option>
				@else
					<option value="{{ $type }}">{{ $type }}</option>
				@endif
			@endforeach
		</select>
	</div>
</div>
<div class="row">
	<div class="input-field col s12">
		<input placeholder="Use '@' to use root domain as record name. Insert name without domain name" name="name" value="{{ $record->name or old('name') }}" type="text" {{ isset($editMode) && $editMode == true ? 'disabled' : null }}>
		<label>Name</label>
	</div>
</div>
<div class="row">
	<div class="input-field col s12">
		@if (isset($record))
			<textarea name="content" class="materialize-textarea">{{ $record->content or old('content') }}</textarea>
		@else
			<textarea name="content" class="materialize-textarea">{{ old('content') !== null ? old('content') : env('IP_ADDRESS') }}</textarea>
		@endif
		<label for="content" class="active">Content</label>
	</div>
</div>
<div class="row">
	<div class="input-field col s12">
		@if (isset($record))
			<input placeholder="" name="ttl" type="number" value="{{ $record->ttl or old('ttl') }}">
		@else
			<input placeholder="" name="ttl" type="number" value="{{ old('ttl') !== null ? old('ttl') : '86400' }}">
		@endif
		<label>TTL</label>
	</div>
</div>
<div class="row mxPrio">
	<div class="input-field col s12">
		<input placeholder="" name="priority" class="priorityInput" type="number" value="{{ $record->prio or old('priority') }}" {{ isset($record->type) && $record->type == "MX" ? null : "disabled" }}>
		<label>Priority</label>
	</div>
</div>

<div class="row">
	<div class="input-field col s12">
		<button type="submit" class="btn light-green waves-effect">{{ $buttonSubmitText }}</button>
	</div>
</div>