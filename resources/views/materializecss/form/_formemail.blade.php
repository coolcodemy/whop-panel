<div class="row">
	<div class="input-field col s6">
		<label class="active">Username</label>
		<input placeholder="Username of email" name="username" type="text" value="{{ $email->username or old('username') }}" {{ isset($disabled) && $disabled == true ? 'disabled' : null }}>
	</div>

	<div class="input-field col s6 tooltipped" data-position="top" data-delay="50" data-tooltip="To add new domain, please add new A record in Domain Record">
		<label class="active">Domain</label>
		<select class="material-select" name="mailDomain" {{ isset($disabled) && $disabled == true ? 'disabled' : null }}>
			@foreach ($mailDomains as $mailDomain)
				@if ((isset($email->record_id) && $email->record_id == $mailDomain->id) || old('mailDomain') == $mailDomain->id)
					<option value="{{ $mailDomain->id }}" selected>{{ $mailDomain->name }}</option>
				@else
					<option value="{{ $mailDomain->id }}">{{ $mailDomain->name }}</option>
				@endif
			@endforeach
		</select>
	</div>

	<div class="input-field col s12">
		<label class="active">Password</label>
		<input placeholder="Password" name="password" type="password">
	</div>

	<div class="input-field col s12">
		<label class="active">Password (Again)</label>
		<input placeholder="Password" name="passwordAgain" type="password">
	</div>


	<div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Maximum quota is: {{ $emailQuota == 0 ? 'Unlimited MB. Please put 0 for unlimited.' : $maxQuota . ' MB' }}">
		<label class="active">Quota (MB)</label>
		<input placeholder="" name="quota" type="number" value="{{ $email->quota or old('quota') }}">
	</div>

	<div class="input-field col s12">
		<button type="submit" class="btn light-green waves-effect">{{ $buttonText }}</button>
	</div>
	
</div>