<div class="row">
	<div class="input-field col s9">
		<input placeholder="" name="username" type="text" value="{{ $ftpUser->username or old('username') }}" {{ isset($editMode) && $editMode == true ? 'disabled' : null }}>
		<label>Username</label>
	</div>

	<div class="input-field col s3">
		<select name="domain" class="material-select" {{ isset($editMode) && $editMode == true ? 'disabled="disabled"' : null }}>
			@foreach ($ftpDomains as $key => $ftpDomain)
				@if ((isset($ftpUser->record_id) && $ftpUser->record_id == $key) || old('domain') == $key)
					<option value="{{ $key }}" selected>{{ $ftpDomain }}</option>
				@else
					<option value="{{ $key }}">{{ $ftpDomain }}</option>
				@endif
			@endforeach
		</select>
		<label>Domain</label>
	</div>

	<div class="input-field col s12">
		<input placeholder="" name="password" type="password">
		<label>Password</label>
	</div>

	<div class="input-field col s12">
		<input placeholder="" name="repeatPassword" type="password">
		<label>Repeat Password</label>
	</div>


	<div class="input-field col s9">
		<div class="form-group">
			<label class="control-label active" for="">Root Folder</label>
			@if (isset($editMode) && $editMode == true)
				<input value="{{ $ftpUser->homedir }}" id="homeDir" type="text" placeholder="" disabled>
			@else
				<input value="/var/www/{{ $user->username }}@{{ selected }}" id="homeDir" type="text" placeholder="" disabled>
			@endif
			<input value="@{{ selected }}"name="homeDir" type="hidden">
		</div>
	</div>

	<div class="input-field col s3">
		<div class="form-group">
			<a ng-click="pickHomeDir()" class="btn modal-trigger blue waves-effect" style="width:100% !important" {{ isset($editMode) && $editMode == true ? 'disabled="disabled"' : null }}>Browse</a>
		</div>
	</div>

	<div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Maximum quota is: {{ $ftpQuota == 0 ? 'Unlimited MB. Please put 0 for unlimited.' : $maxQuota . ' MB' }}">
		<label class="active">Quota (MB)</label>
		<input placeholder="" name="quota" type="number" value="{{ $ftpUser->quotaSize or old('quota') }}">
	</div>


	<div class="input-field col s12">
		<div class="form-group">
			<button type="submit" class="btn light-green">{{ $buttonText }}</button>
		</div>
	</div>
</div>