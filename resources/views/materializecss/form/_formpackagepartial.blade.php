    <div class="input-field col s12">

        {!! Form::select('apcuSupport', array(0 => 'Disable', 1 => 'Enable'), isset($package->apcuSupport) ? $package->apcuSupport : old('apcuSupport'), ['class' => 'material-select']) !!}
        
        <label>APCu Support (APCu will share APCu cache with other user. Do not enable this APCu for untrusted user)</label>

        @if ( isset($packageOriginal) )

            @if ($packageOriginal->apcuSupport == 0)

                <p class="grey-text">Original value is Disable</p><br><br>

            @else

                <p class="grey-text">Original value is Enable</p><br><br>

            @endif

        @endif


    </div>



    <div class="input-field col s12">

        {!! Form::select('enableSsl', array(0 => 'Disable', 1 => 'Enable'), isset($package->enableSsl) ? $package->enableSsl : old('enableSsl'), ['class' => 'material-select']) !!}
        
        <label>Enable SSL/TLS (Enable SSL/TLS support for WHoPlet)</label>

        @if ( isset($packageOriginal) )

            @if ($packageOriginal->enableSsl == 0)

                <p class="grey-text">Original value is Disable</p><br><br>

            @else

                <p class="grey-text">Original value is Enable</p><br><br>

            @endif

        @endif


    </div>



    <div class="input-field col s12">
        
        {!! Form::select('ssh', array(0 => 'Disable', 1 => 'Enable'), isset($package->ssh) ? $package->ssh : old('ssh'), ['class' => 'material-select']) !!}
        
        <label>Enable SSH for Domain Owner</label>

        @if ( isset($packageOriginal) )

            @if ($packageOriginal->ssh == 0)

                <p class="grey-text">Original value is Disable</p><br><br>

            @else

                <p class="grey-text">Original value is Enable</p><br><br>

            @endif

        @endif

    </div>



    <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Insert '0' for unlimited without ' sign">
        
        <input placeholder="" name="diskQuota" value="{{ $package->diskQuota or old('diskQuota') }}" type="number">
        
        <label>Disk Quota (MB)</label>

        @if ( isset($packageOriginal) )

            <p class="grey-text">Original value is {{ $packageOriginal->diskQuota }}</p><br><br>

        @endif

    </div>



    <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Insert '0' for unlimited without ' sign">
        
        <input placeholder="" name="emailQuota" value="{{ $package->emailQuota or old('emailQuota') }}" type="number">
        
        <label>Email Quota (MB)</label>

        @if ( isset($packageOriginal) )

            <p class="grey-text">Original value is {{ $packageOriginal->emailQuota }}</p><br><br>

        @endif

    </div>



    <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Insert '0' for unlimited without. FTP quota usually unlimited">
        
        <input placeholder="" name="ftpQuota" value="{{ $package->ftpQuota or old('ftpQuota') }}" type="number">
        
        <label>FTP Quota (MB)</label>

        @if ( isset($packageOriginal) )

            <p class="grey-text">Original value is {{ $packageOriginal->ftpQuota }}</p><br><br>

        @endif

    </div>



    <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Insert '0' for unlimited without ' sign">
        
        <input placeholder="" name="websiteQuota" value="{{ $package->websiteQuota or old('websiteQuota') }}" type="number">
        
        <label>Website Quota (GB)</label>

        @if ( isset($packageOriginal) )

            <p class="grey-text">Original value is {{ $packageOriginal->websiteQuota }}</p><br><br>

        @endif

    </div>



    <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Insert '0' for unlimited without ' sign">
        
        <input placeholder="" name="numberOfDatabase" value="{{ $package->numberOfDatabase or  old('numberOfDatabase') }}" type="number">
        
        <label>Number of database</label>

        @if ( isset($packageOriginal) )

            <p class="grey-text">Original value is {{ $packageOriginal->numberOfDatabase }}</p><br><br>

        @endif

    </div>



    <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Insert '0' for unlimited without ' sign">
        
        <input placeholder="" name="numberOfDatabaseUser" value="{{ $package->numberOfDatabaseUser or old('numberOfDatabaseUser') }}" type="number">
        
        <label>Number of database user</label>

        @if ( isset($packageOriginal) )

            <p class="grey-text">Original value is {{ $packageOriginal->numberOfDatabaseUser }}</p><br><br>

        @endif

    </div>



    <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Insert '0' for unlimited without ' sign">
        
        <input placeholder="" name="maximumQueriesPerHour" value="{{ $package->maximumQueriesPerHour or old('maximumQueriesPerHour') }}" type="number">
        
        <label>Maximum Database Queries Per Hour</label>

        @if ( isset($packageOriginal) )

            <p class="grey-text">Original value is {{ $packageOriginal->maximumQueriesPerHour }}. This will not change unless Domain Owner recreate database user.</p><br><br>

        @endif

    </div>



    <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Insert '0' for unlimited without ' sign">
        
        <input placeholder="" name="maximumConnectionsPerHour" value="{{ $package->maximumConnectionsPerHour or old('maximumConnectionsPerHour') }}" type="number">
        
        <label>Maximum Database Connections Per Hour</label>

        @if ( isset($packageOriginal) )

            <p class="grey-text">Original value is {{ $packageOriginal->maximumConnectionsPerHour }}. This will not change unless Domain Owner recreate database user.</p><br><br>

        @endif

    </div>



    <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Insert '0' for unlimited without ' sign">
        

        <input placeholder="" name="maximumUpdatesPerHour" value="{{ $package->maximumUpdatesPerHour or old('maximumUpdatesPerHour') }}" type="number">
        
        <label>Maximum Database Updates Per Hour</label>

        @if ( isset($packageOriginal) )

            <p class="grey-text">Original value is {{ $packageOriginal->maximumUpdatesPerHour }}. This will not change unless Domain Owner recreate database user.</p><br><br>

        @endif

    </div>



    <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Insert '0' for unlimited without ' sign">
        
        <input placeholder="" name="maximumUserConnections" value="{{ $package->maximumUserConnections or old('maximumUserConnections') }}" type="number">
        
        <label>Maximum Database User Connections</label>

        @if ( isset($packageOriginal) )

            <p class="grey-text">Original value is {{ $packageOriginal->maximumUserConnections }}. This will not change unless Domain Owner recreate database user.</p><br><br>

        @endif

    </div>



    <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Insert '0' for unlimited without ' sign">
        
        <input placeholder="" name="numberOfEmailAccount" value="{{ $package->numberOfEmailAccount or old('numberOfEmailAccount') }}" type="number">
        
        <label>Number of email account</label>

        @if ( isset($packageOriginal) )

            <p class="grey-text">Original value is {{ $packageOriginal->numberOfEmailAccount }}</p><br><br>

        @endif

    </div>



    <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Insert '0' for unlimited without ' sign">
        
        <input placeholder="" name="numberOfFtpAccount" value="{{ $package->numberOfFtpAccount or old('numberOfFtpAccount') }}" type="number">
        
        <label>Number of FTP account</label>

        @if ( isset($packageOriginal) )

            <p class="grey-text">Original value is {{ $packageOriginal->numberOfFtpAccount }}</p><br><br>

        @endif

    </div>


    <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Insert '0' for unlimited without ' sign">
        
        <input placeholder="" name="numberOfDomain" value="{{ $package->numberOfDomain or old('numberOfDomain') }}" type="number">
        
        <label>Number of domain</label>

        @if ( isset($packageOriginal) )

            <p class="grey-text">Original value is {{ $packageOriginal->numberOfDomain }}</p><br><br>

        @endif
    </div>



    <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Insert '0' for unlimited without ' sign">
       
        <input placeholder="" name="numberOfWhoplet" value="{{ $package->numberOfWhoplet or old('numberOfWhoplet') }}" type="number">
        
        <label>Number of WHoPlet</label>

        @if ( isset($packageOriginal) )

            <p class="grey-text">Original value is {{ $packageOriginal->numberOfWhoplet }}</p><br><br>

        @endif
    </div>