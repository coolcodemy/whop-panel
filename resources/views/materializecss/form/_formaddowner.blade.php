@inject('Package', 'WHoP\Services\PackageService') 


<div class="row">
    <div class="input-field col s12">
        <input placeholder="" id="name" name="name" ng-model="name" type="text" value="{{ old('name') }}">
        <label>Domain Owner Name</label>
    </div>
    <div class="input-field col s12">
        <input placeholder="" id="username" name="username" ng-model="username" type="text" value="{{ old('username') }}">
        <label>Username</label>
    </div>
    <div class="input-field col s12">
        <input placeholder="" id="password" name="password" ng-model="password" type="password">
        <label>Password</label>
    </div>
    <div class="input-field col s12">
        <input placeholder="" id="repeatPassword" name="repeatPassword" ng-model="repeatPassword" type="password">
        <label>Repeat Password</label>
    </div>
    <div class="input-field col s12">
        <input placeholder="" id="email" name="email" type="email" ng-model="email" value="{{ old('email') }}">
        <label>Email</label>
    </div>
    <div class="input-field col s12">
        <select name="package" id="package" ng-model="package" class="material-select">
            <option value="" selected disabled>Please select package for user</option>
            @foreach ($Package->all() as $package)
                <option value="{{ $package->id }}">{{ $package->name }}</option>
            @endforeach
        </select>
        <label>Package</label>
    </div>
    <div class="input-field col s12">
        <input placeholder="" id="domainName" name="domainName" ng-model="domainName" type="text" value="{{ old('domainName') }}">
        <label>Domain Name</label>
    </div>

    <div class="input-field col s12">
        <button type="button" class="btn" ng-click="buttonAddOwner()">Submit</button>
    </div>
</div>
