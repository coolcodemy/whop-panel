<div class="row">
    <div class="input-field col s12">
        <input placeholder="Package name" name="name" value="{{ $package->name or old('name') }}" type="text">
        <label>Name</label>
    </div>
    <div class="input-field col s6">
        {!! Form::select('validityNumber', $day, isset($valid[0]) ? $valid[0] : old('validityNumber'), ['class' => 'material-select']) !!}
        <label>Validity Number</label>
    </div>
    <div class="input-field col s6">
        {!! Form::select('validityTime', array('DAY' => 'Day/s', 'MONTH' => 'Month/s', 'YEAR' => 'Year/s'), isset($valid[1]) ? $valid[1] : old('validityTime'), ['class' => 'material-select']) !!}
        <label>Validity Time</label>
    </div>

    @include('materializecss.form._formpackagepartial')

    <div class="input-field col s12">
        <a href="{{ URL::previous() }}" class="btn red waves-effect waves-light">Cancel</a>
        <button type="submit" class="btn waves-effect waves-light right">{{ $submitText }}</button>
    </div>
</div>