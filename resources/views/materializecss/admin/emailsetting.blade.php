@extends('materializecss.layout.master', ['title' => 'WHoP Email Setting'])


@section('content')

<div class="row">

	<div class="col s12">

		<h4>Email Setting</h4>

		<p>This is the email setting for panel. This email setting will notify when user succesfully login to the panel.</p>

		{!! Form::open(['route' => 'admin::emailsetting-update']) !!}

			<div class="row">

				<div class="input-field col s12">
					<div class="form-group">
						<label class="control-label" for="">Mail Host</label>
						<input type="text" class="form-control" name="mailHost" placeholder="" value="{{ env('MAIL_HOST') }}">
					</div>
				</div>


				<div class="input-field col s12">
					<div class="form-group">
						<label class="control-label" for="">Mail Port</label>
						<input type="number" class="form-control" name="mailPort" placeholder="" value="{{ env('MAIL_PORT') }}">
					</div>
				</div>


				<div class="input-field col s12">
					<div class="form-group">
						<label class="control-label" for="">Mail Name (Name of the sender)</label>
						<input type="text" class="form-control" name="mailName" placeholder="" value="{{ env('MAIL_NAME') }}">
					</div>
				</div>


				<div class="input-field col s12">
					<div class="form-group">
						<label class="control-label" for="">Mail Username</label>
						<input type="text" class="form-control" name="mailUsername" placeholder="" value="{{ env('MAIL_USERNAME') }}">
					</div>
				</div>


				<div class="input-field col s12">
					<div class="form-group">
						<label class="control-label" for="">Mail Password</label>
						<input type="password" class="form-control" name="mailPassword" placeholder="" value="{{ env('MAIL_PASSWORD') }}">
					</div>
				</div>

				<div class="input-field col s12">
					<div class="form-group">
						<label class="control-label" for="">Mail Encryption (null, ssl, tls)</label>
						<input type="text" class="form-control" name="mailEncryption" placeholder="" value="{{ env('MAIL_ENCRYPTION') }}">
					</div>
				</div>

			</div>
			
			<button type="submit" class="btn light-green">Update</button><br><br>
		
		{!! Form::close() !!}

	</div>
</div>

@stop