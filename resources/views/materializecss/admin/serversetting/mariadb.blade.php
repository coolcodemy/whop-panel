@extends('materializecss.admin.serversetting', ['title' => 'MariaDB Setting'])


@section('serverconfig')

    <div class="row" ng-controller="ServerSettingMariaDBController">
        <div class="col s12">
            <ul class="collection with-header">
                <li class="collection-header"><h4>MariaDB Service Status</h4></li>
                <li class="collection-item">Average CPU usage <span class="badge">@{{ cpuUsage }}</span></li>
                <li class="collection-item">Average Memory usage <span class="badge">@{{ memoryUsage }}</span></li>
                <li class="collection-header"><h4>MariaDB Service Control</h4></li>
                <li class="collection-item">
                    Start/Stop Service
                    <div class="switch right">
                        <label>
                            Stop<input ng-model="switch" ng-click="toggle()" ng-disabled="disableSwitch" type="checkbox"><span class="lever"></span>Start
                        </label>
                    </div>
                </li>
                <li class="collection-item">Restart Service <button class="waves-effect waves-light right btn red bottom15px" ng-click="restart()" ng-disabled="isRestart">Restart</button></li>
            </ul>
        </div>

        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <h4>Setting</h4>

                    <form>
                        <div class="input-field col s12">
                            <input placeholder="" ng-model="innodbbufferpoolsize" type="number" class="active" value="@{{ innodbbufferpoolsize }}">
                            <label>innodb_buffer_pool_size (in MB) @{{ setting }}</label>
                        </div>

                        <div class="input-field col s12">
                            <input placeholder="" ng-model="maxconnections" type="number" class="active" value="@{{ maxconnections }}">
                            <label>max_connections</label>
                        </div>

                        <div class="input-field col s12">
                            <input placeholder="" ng-model="maxuserconnections" type="number" class="active" value="@{{ maxuserconnections }}">
                            <label>max_user_connections</label>
                        </div>

                        <div class="input-field col s12">
                            <input placeholder="" ng-model="querycachesize" type="number" class="active" value="@{{ querycachesize }}">
                            <label>query_cache_size (in MB)</label>
                        </div>

                        <div class="input-field col s12">
                            <input placeholder="" ng-model="querycachelimit" type="number" class="active" value="@{{ querycachelimit }}">
                            <label>query_cache_limit (in MB)</label>
                        </div>

                        <div class="input-field col s12">
                            <input placeholder="" ng-model="querycacheminresunit" type="number" class="active" value="@{{ querycacheminresunit }}">
                            <label>query_cache_min_res_unit (in KB)</label>
                        </div>

                        <div class="input-field col s12">
                            <input placeholder="" ng-model="threadcachesize" type="number" class="active" value="@{{ threadcachesize }}">
                            <label>thread_cache_size</label>
                        </div>

                        <div class="input-field col s12">
                            <input placeholder="" ng-model="innodbbufferpoolinstances" type="number" class="active" value="@{{ innodbbufferpoolinstances }}">
                            <label>innodb_buffer_pool_instances</label>
                        </div>

                        <div class="input-field col s12">
                            <button ng-click="saveConfig()" ng-disabled="isSaving" class="btn right blue waves-effect" style="margin-bottom: 15px;">Save Configuration</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
