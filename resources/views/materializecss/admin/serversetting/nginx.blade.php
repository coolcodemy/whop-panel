@extends('materializecss.admin.serversetting', ['title' => 'Nginx Setting'])


@section('serverconfig')

    <div class="row" ng-controller="ServerSettingNginx">
        <div class="col s12">
            <ul class="collection with-header">
                <li class="collection-header"><h4>Nginx Service Status</h4></li>
                <li class="collection-item">Average CPU usage <span class="badge">@{{ cpuUsage }}</span></li>
                <li class="collection-item">Average Memory usage <span class="badge">@{{ memoryUsage }}</span></li>
                <li class="collection-header"><h4>Nginx Service Control</h4></li>
                <li class="collection-item">Reload Service <button ng-click="reload()" ng-disabled="isReload" class="waves-effect waves-light right btn blue">Reload</button><br><br></li>
                <li class="collection-item">Restart Service <button ng-click="restart()" ng-disabled="isRestart" class="waves-effect waves-light right btn red bottom15px">Restart</button></li>
            </ul>
        </div>

        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <h4>Setting</h4>

                    <form>
                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="worker_processes" value="@{{ worker_processes }}" type="text">
                            <label>Worker Process</label>
                        </div>

                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="worker_rlimit_nofile" value="@{{ worker_rlimit_nofile }}" type="number">
                            <label>worker_rlimit_nofile</label>
                        </div>

                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="worker_connections" value="@{{ worker_connections }}" type="number">
                            <label>worker_connections</label>
                        </div>

                        <div class="input-field col s12">
                            <select ng-model="multi_accept">
                                <option value="off">Off</option>
                                <option value="on">On</option>
                            </select>
                            <label>multi_accept</label>
                        </div>

                        <div class="input-field col s12">
                            <select ng-model="sendfile">
                                <option value="off">Off</option>
                                <option value="on">On</option>
                            </select>
                            <label>sendfile</label>
                        </div>


                        <div class="input-field col s12">
                            <select ng-model="tcp_nopush">
                                <option value="off">Off</option>
                                <option value="on">On</option>
                            </select>
                            <label>tcp_nopush</label>
                        </div>


                        <div class="input-field col s12">
                            <select ng-model="tcp_nodelay">
                                <option value="off">Off</option>
                                <option value="on">On</option>
                            </select>
                            <label>tcp_nodelay</label>
                        </div>

                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="client_body_timeout" value="@{{ client_body_timeout }}" type="number">
                            <label>client_body_timeout</label>
                        </div>

                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="client_header_timeout" value="@{{ client_header_timeout }}" type="number">
                            <label>client_header_timeout</label>
                        </div>

                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="keepalive_timeout" value="@{{ keepalive_timeout }}" type="number">
                            <label>keepalive_timeout</label>
                        </div>

                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="client_header_buffer_size" value="@{{ client_header_buffer_size }}" type="number">
                            <label>client_header_buffer_size</label>
                        </div>

                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="client_max_body_size" value="@{{ client_max_body_size }}" type="text">
                            <label>client_max_body_size</label>
                        </div>

                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="open_file_cache_valid" value="@{{ open_file_cache_valid }}" type="text">
                            <label>open_file_cache_valid</label>
                        </div>

                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="open_file_cache_min_uses" value="@{{ open_file_cache_min_uses }}" type="number">
                            <label>open_file_cache_min_uses</label>
                        </div>

                        <div class="input-field col s12">
                            <select ng-model="open_file_cache_errors">
                                <option value="off">Off</option>
                                <option value="on">On</option>
                            </select>
                            <label>open_file_cache_errors</label>
                        </div>

                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="keepalive_requests" value="@{{ keepalive_requests }}" type="number">
                            <label>keepalive_requests</label>
                        </div>

                        <div class="input-field col s12">
                            <select ng-model="reset_timedout_connection">
                                <option value="off">Off</option>
                                <option value="on">On</option>
                            </select>
                            <label>reset_timedout_connection</label>
                        </div>

                        <div class="input-field col s12">
                            <button ng-click="saveConfig()" ng-disabled="isSaving" class="btn waves-effect right blue" style="margin-bottom: 15px;">Save Configuration</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop