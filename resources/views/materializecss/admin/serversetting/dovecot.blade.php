@extends('materializecss.admin.serversetting', ['title' => 'Dovecot Setting'])


@section('serverconfig')

    <div class="row" ng-controller="ServerSettingDovecotController">
        <div class="col s12">
            <ul class="collection with-header">
                <li class="collection-header"><h4>Dovecot Service Status</h4></li>
                <li class="collection-item">Average CPU usage <span class="badge">@{{ cpuUsage }}</span></li>
                <li class="collection-item">Average Memory usage <span class="badge">@{{ memoryUsage }}</span></li>
                <li class="collection-header"><h4>Dovecot Service Control</h4></li>
                <li class="collection-item">
                    Start/Stop Service
                    <div class="switch right">
                        <label>
                            Stop<input ng-model="switch" ng-click="toggle()" ng-disabled="disableSwitch" type="checkbox"><span class="lever"></span>Start
                        </label>
                    </div>
                </li>
                <li class="collection-item">Restart Service <button class="waves-effect waves-light right btn red bottom15px" ng-model="restart" ng-click="restart()" ng-class="{disabled: isRestart}">Restart</button></li>
            </ul>
        </div>

        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <h4>Setting</h4>

                    <form>
                        <div class="input-field col s12">
                            <p>SSL/TLS Certificate</p>
                        </div>

                        <div class="input-field col s3">
                            <a data-target="universalCertificateModal" class="btn waves-effect modal-trigger" style="width:100% !important" ng-click="chooseCertificate()">Choose</a>
                        </div>

                        <div class="input-field col s9">
                            <input class="active" ng-model="ssl" value="" type="text" disabled>
                        </div>

                        <div class="input-field col s12">
                            <button ng-click="saveConfig()" ng-class="{disabled: isSaving}" class="btn right blue waves-effect" style="margin-bottom: 15px;">Save Configuration</button>
                        </div>
                    </form>

                    @include('materializecss.partial._modal', ['modal' => 'universalcertificate'])
                </div>
            </div>
        </div>
    </div>
@stop