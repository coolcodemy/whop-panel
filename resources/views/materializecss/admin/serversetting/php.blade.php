@extends('materializecss.admin.serversetting', ['title' => 'PHP Setting'])


@section('serverconfig')

    <div class="row" ng-controller="ServerSettingPHP">
        <div class="col s12">
            <ul class="collection with-header">
                <li class="collection-header"><h4>PHP-FPM Service Status</h4></li>
                <li class="collection-item">Average CPU usage <span class="badge">@{{ cpuUsage }}</span></li>
                <li class="collection-item">Average Memory usage <span class="badge">@{{ memoryUsage }}</span></li>
                <li class="collection-header"><h4>PHP-FPM Service Control</h4></li>
                <li class="collection-item">
                    Start/Stop Service
                    <div class="switch right">
                        <label>
                            Stop<input ng-click="toggle()" ng-disabled="disableSwitch" ng-model="switch" type="checkbox"><span class="lever"></span>Start
                        </label>
                    </div>
                </li>
                <li class="collection-item">Restart Service <button ng-click="restart()" ng-disabled="isRestart" class="waves-effect waves-light right btn red bottom15px" id="restart">Restart</button></li>
            </ul>
        </div>

        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <h4>Setting</h4>

                    <form>
                        <div class="row">
                            <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Careful, wrong value will not be check by WHoP and if some value if remove from here will rise the potential of the server to be hack">
                                <textarea placeholder="" ng-model="disable_functions" class="materialize-textarea">@{{ disable_functions }}</textarea>
                                <label>Disable Functions</label>
                            </div>

                            <div class="input-field col s12">
                                <select ng-model="expose_php">
                                    <option value="On">On</option>
                                    <option value="Off">Off</option>
                                </select>
                                <label>Expose PHP</label>
                            </div>

                            <div class="input-field col s12">
                                <input placeholder="" ng-model="max_execution_time" type="number" value="@{{ max_execution_time }}">
                                <label>Max Execution Time</label>
                            </div>

                            <div class="input-field col s12">
                                <input placeholder="" ng-model="max_input_time" type="number" value="@{{ max_input_time }}">
                                <label>Max Input Time</label>
                            </div>

                            <div class="input-field col s12">
                                <input placeholder="" ng-model="memory_limit" type="number" value="@{{ memory_limit }}">
                                <label>Memory Limit (in MB)</label>
                            </div>


                            <div class="input-field col s12">
                                <select ng-model="display_errors" id="display_errors">
                                    <option value="On">On</option>
                                    <option value="Off">Off</option>
                                </select>
                                <label>Display Errors</label>
                            </div>


                            <div class="input-field col s12">
                                <input placeholder="" ng-model="post_max_size" type="number" value="@{{ post_max_size }}">
                                <label>Post Max Size (in MB)</label>
                            </div>


                            <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="This vulnerability when using fastcgi have been fix in WHoPlet by adding try_files $uri /index.php =404;. If you still paranoid, please disable it">
                                <select ng-model="cgi_fix_pathinfo" id="cgi_fix_pathinfo">
                                    <option value="0">Disable</option>
                                    <option value="1">Enable</option>
                                </select>
                                <label>cgi.fix_pathinfo</label>
                            </div>


                            <div class="input-field col s12">
                                <select ng-model="file_uploads" id="file_uploads">
                                    <option value="Off">Disable</option>
                                    <option value="On">Enable</option>
                                </select>
                                <label>File Uploads</label>
                            </div>


                            <div class="input-field col s12">
                                <input placeholder="" ng-model="upload_max_filesize" type="number" value="@{{ upload_max_filesize }}">
                                <label>Upload Max File Size (in MB)</label>
                            </div>


                            <div class="input-field col s12">
                                <select ng-model="allow_url_fopen" id="allow_url_fopen">
                                    <option value="Off">Disable</option>
                                    <option value="On">Enable</option>
                                </select>
                                <label>Allow URL Fopen</label>
                            </div>


                            <div class="input-field col s12">
                                <select ng-model="allow_url_include" id="allow_url_include">
                                    <option value="Off">Disable</option>
                                    <option value="On">Enable</option>
                                </select>
                                <label>Allow URL Include</label>
                            </div>


                            <div class="input-field col s12">
                                <select ng-model="mail_add_x_header" id="mail_add_x_header">
                                    <option value="Off">Disable</option>
                                    <option value="On">Enable</option>
                                </select>
                                <label>Mail Add X-Header</label>
                            </div>


                            <div class="input-field col s12">
                                <select ng-model="sql_safe_mode" id="sql_safe_mode">
                                    <option value="Off">Disable</option>
                                    <option value="On">Enable</option>
                                </select>
                                <label>SQL Safe Mode</label>
                            </div>


                            <div class="input-field col s12">
                                <input placeholder="" ng-model="session_name" type="text" value="@{{ session_name }}">
                                <label>Session Name</label>
                            </div>


                            <div class="input-field col s12">
                                <input placeholder="" ng-model="session_gc_maxlifetime" type="number" value="@{{ session_gc_maxlifetime }}">
                                <label>Session GC Maxlifetime (in seconds)</label>
                            </div>

                            <div class="input-field col s12">
                                <button ng-click="saveConfig()" ng-disabled="isSaving" class="btn right blue waves-effect" style="margin-bottom: 15px;">Save Configuration</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop
