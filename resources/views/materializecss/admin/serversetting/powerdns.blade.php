@extends('materializecss.admin.serversetting', ['title' => 'PowerDNS Setting'])


@section('serverconfig')

    <div class="row" ng-controller="ServerSettingPDNSController">
        <div class="col s12">
            <ul class="collection with-header">
                <li class="collection-header"><h4>PowerDNS Service Status</h4></li>
                <li class="collection-item">Average CPU usage <span class="badge">@{{ cpuUsage }}</span></li>
                <li class="collection-item">Average Memory usage <span class="badge">@{{ memoryUsage }}</span></li>
                <li class="collection-header"><h4>PowerDNS Service Control</h4></li>
                <li class="collection-item">
                    Start/Stop Service
                    <div class="switch right">
                        <label>
                            Stop<input ng-click="toggle()" ng-disabled="disableSwitch" ng-model="switch" type="checkbox"><span class="lever"></span>Start
                        </label>
                    </div>
                </li>
                <li class="collection-item">Restart Service <button ng-click="restart()" ng-disabled="isRestart" class="waves-effect waves-light right btn red bottom15px" id="restart">Restart</button></li>
            </ul>
        </div> 

        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <p>¯\_(ツ)_/¯</p>
                </div>
            </div>
        </div> 
    </div>
@stop