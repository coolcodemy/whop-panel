@extends('materializecss.admin.serversetting', ['title' => 'Pure-FTPD Setting'])


@section('serverconfig')

    <div class="row" ng-controller="ServerSettingPureFTPDController">
        <div class="col s12">
            <ul class="collection with-header">
                <li class="collection-header"><h4>Pure-FTPD Service Status</h4></li>
                <li class="collection-item">Average CPU usage <span class="badge">@{{ cpuUsage }}</span></li>
                <li class="collection-item">Average Memory usage <span class="badge">@{{ memoryUsage }}</span></li>
                <li class="collection-header"><h4>Pure-FTPD Service Control</h4></li>
                <li class="collection-item">
                    Start/Stop Service
                    <div class="switch right">
                        <label>
                            Stop<input ng-click="toggle()" ng-model="switch" ng-disabled="disableSwitch" type="checkbox"><span class="lever"></span>Start
                        </label>
                    </div>
                </li>
                <li class="collection-item">Restart Service <button ng-click="restart()" ng-disabled="isRestart" class="waves-effect waves-light right btn red bottom15px" id="restart">Restart</button></li>
            </ul>
        </div>

        <div class="col s12">
            <div class="card">
                <div class="card-content red lighten-2 white-text">
                    If you change passive port range here, you must update it yourself in csf firewall configuration.
                </div>
            </div>
        </div>

        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <h4>Setting</h4>

                    <form>
                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="MaxClientsNumber" value="@{{ MaxClientsNumber }}" type="number">
                            <label>Maximum Client Number</label>
                        </div>

                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="MaxClientsPerIP" value="@{{ MaxClientsPerIP }}" type="number">
                            <label>Maximum Client per IP</label>
                        </div>


                        <div class="input-field col s6">
                            <input class="active" placeholder="" ng-model="PassivePortRangeFrom" value="@{{ PassivePortRangeFrom }}" type="text">
                            <label>Passive Port Range From</label>
                        </div>


                        <div class="input-field col s6">
                            <input class="active" placeholder="" ng-model="PassivePortRangeTo" value="@{{ PassivePortRangeTo }}" type="text">
                            <label>Passive Port Range To</label>
                        </div>


                        <div class="input-field col s12">
                            <select ng-model="NoAnonymous">
                                <option value="no">No</option>
                                <option value="yes">Yes</option>
                            </select>
                            <label>No Anonymous</label>
                        </div>


                        <div class="input-field col s12">
                            <select ng-model="DisplayDotFiles">
                                <option value="no">No</option>
                                <option value="yes">Yes</option>
                            </select>
                            <label>Display Dot Files</label>
                        </div>

                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="MaxIdleTime" value="@{{ MaxIdleTime }}" type="number">
                            <label>Maximum Idle Time (in minutes)</label>
                        </div>

                        <div class="input-field col s12">
                            <select ng-model="TLS">
                                <option value="0">Disable</option>
                                <option value="1">Enable TLS and non-TLS</option>
                                <option value="2">Disable non-TLS</option>
                            </select>
                            <label>TLS</label>
                        </div>

                        <div class="input-field col s12">
                            <textarea placeholder="" ng-model="PublicCert" class="materialize-textarea">@{{ PublicCert }}</textarea>
                            <label>Public Certificate</label>
                        </div>


                        <div class="input-field col s12">
                            <textarea placeholder="" ng-model="PrivateKey" class="materialize-textarea">@{{ PrivateKey }}</textarea>
                            <label>Private Key</label>
                        </div>

                        <div class="input-field col s12">
                            <button ng-click="saveConfig()" ng-disabled="isSaving" class="btn right blue" style="margin-bottom: 15px;">Save Configuration</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop