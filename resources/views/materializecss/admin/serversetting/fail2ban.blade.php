@extends('materializecss.admin.serversetting', ['title' => 'Fail2Ban Setting'])


@section('serverconfig')

    <div class="row" ng-controller="SystemSettingFail2BanController">
        <div class="col s12">
            <ul class="collection with-header">
                <li class="collection-header"><h4>Fail2Ban Service Status</h4></li>
                <li class="collection-item">Average CPU usage <span class="badge">@{{ cpuUsage }}</span></li>
                <li class="collection-item">Average Memory usage <span class="badge">@{{ memoryUsage }}</span></li>
                <li class="collection-header"><h4>Fail2Ban Service Control</h4></li>
                <li class="collection-item">
                    Start/Stop Service
                    <div class="switch right">
                        <label>
                            Stop<input ng-click="toggle()" ng-model="switch" ng-disabled="disableSwitch" type="checkbox"><span class="lever"></span>Start
                        </label>
                    </div>
                </li>
                <li class="collection-item">Restart Service <button ng-click="restart()" ng-disabled="isRestart" class="waves-effect waves-light right btn red bottom15px" id="restart">Restart</button></li>
            </ul>
        </div>

        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <h4>Setting</h4>

                    <form>
                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="For alert">
                            <input class="active" placeholder="" ng-model="destemail" value="@{{ destemail }}" type="text">
                            <label>Admin Email</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Who will be sending the alert">
                            <input class="active" placeholder="" ng-model="senderemail" value="@{{ senderemail }}" type="text">
                            <label>Sender Email</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="IP Address Format or CIDR format. Split by space">
                            <input class="active" placeholder="" ng-model="ignoreip" value="@{{ ignoreip }}" type="text">
                            <label>Ignore IP List</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="How long to ban in seconds">
                            <input class="active" placeholder="" ng-model="bantime" value="@{{ bantime }}" type="number">
                            <label>Ban Time</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="If Fail2Ban detect intrusion trial between this interval, it will block the IP. Find Time is in seconds">
                            <input class="active" placeholder="" ng-model="findtime" value="@{{ findtime }}" type="number">
                            <label>Find Time</label>
                        </div>


                        <div class="input-field col s12">
                            <button ng-click="saveConfig()" ng-disabled="isSaving" class="btn right blue" style="margin-bottom: 15px;">Save Configuration</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop