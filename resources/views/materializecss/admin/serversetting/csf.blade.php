@extends('materializecss.admin.serversetting', ['title' => 'CSF Setting'])


@section('serverconfig')
<div class="row" ng-controller="ServerSettingCSFController">
    <div class="col s12">
        <ul class="tabs">
            <li class="tab col s3"><a class="active" href="#tab1">Status &amp; Tools</a></li>
            <li class="tab col s3"><a href="#tab2">Setting</a></li>
            <li class="tab col s3"><a href="#csfAllowTab" ng-click="resizeAce()">CSF Allow</a></li>
            <li class="tab col s3"><a href="#csfDenyTab" ng-click="resizeAce()">CSF Deny</a></li>
            <li class="tab col s3"><a href="#csfIgnoreTab" ng-click="resizeAce()">CSF Ignore</a></li>
            <li class="tab col s3"><a href="#csfPignoreTab" ng-click="resizeAce()">CSF Pignore</a></li>
        </ul>
    </div>


    <div id="tab1" class="col s12">
        <div class="row">
            <div class="col s12">
                <ul class="collection with-header">
                    <li class="collection-header"><h4>CSF Service Status</h4></li>
                    <li class="collection-item">Status <span class="badge" ng-class="{'red white-text' : csfStatus == 'Disabled', 'light-green white-text': csfStatus != 'Disabled'}">@{{ csfStatus }}</span></li>
                    <li class="collection-header"><h4>CSF Service Control</h4></li>
                    <li class="collection-item">
                        Start/Stop Service
                        <div class="switch right">
                            <label>
                                Stop<input ng-click="toggle()" ng-disabled="disableSwitch" ng-model="switch" type="checkbox"><span class="lever"></span>Start
                            </label>
                        </div>
                    </li>
                    <li class="collection-item">Reload Service <button ng-click="reload()" ng-disabled="isReload" class="waves-effect waves-light right btn blue">Reload</button><br><br></li>
                    <li class="collection-header"><h4>CSF Tools</h4></li>
                    <li class="collection-item">View all temporary blocked IP Address <button ng-click="tool('t')" class="waves-effect waves-light right btn blue">View</button><br><br></li>
                    <li class="collection-item">Unblock &amp; Remove All IP from csf.deny <button ng-click="tool('df')" class="waves-effect waves-light right btn blue">Unblock &amp; Remove</button><br><br></li>
                    <li class="collection-item">Flush all temporary blocked IP Address <button ng-click="tool('tf')" class="waves-effect waves-light right btn blue">Flush</button><br><br></li>
                    <li class="collection-item">
                        Add IP Address to csf.allow and permit all operation<br><br>
                        <div class="row">
                            <div class="col s8 input-field">
                                  <input placeholder="" ng-model="aip" type="text">
                                  <label>IP Address</label>
                            </div><br>
                            <div class="col s4">
                                <a ng-click="tool('a')" class="waves-effect waves-light btn blue right">Add</a>
                            </div>
                        </div>
                    </li>
                    <li class="collection-item">
                        Add IP Address to csf.deny and block the IP Address<br><br>
                        <div class="row">
                            <div class="col s8 input-field">
                                  <input placeholder="" ng-model="dip" type="text">
                                  <label>IP Address</label>
                            </div><br>
                            <div class="col s4">
                                <a ng-click="tool('d')" class="waves-effect waves-light btn blue right">Add</a>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>


            <div class="col s12">
                <div class="card">
                    <div class="card-content">
                        <h4>CSF Tools Result</h4>
                        <div><pre>@{{ toolResult }}</pre></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="tab2" class="col s12">
        <div class="col s12">
            <div class="card">
                <div class="card-content blue white-text">
                    Carefully input your setting. It will not be check by WHoP.
                </div>
            </div>
        </div>
        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <h4>Setting</h4>

                    <form>
                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="LF_ALERT_TO" value="@{{ LF_ALERT_TO }}" type="text">
                            <label>Alert all CSF notification to</label>
                        </div>

                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="LF_ALERT_FROM" value="@{{ LF_ALERT_FROM }}" type="text">
                            <label>Alert is send from</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Allowed TCP INBOUND port on server">
                            <input class="active" placeholder="" ng-model="TCP_IN" value="@{{ TCP_IN }}" type="text">
                            <label>TCP_IN</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Allowed TCP OUTBOUND port on server">
                            <input class="active" placeholder="" ng-model="TCP_OUT" value="@{{ TCP_OUT }}" type="text">
                            <label>TCP_OUT</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Allowed UDP INBOUND port on server">
                            <input class="active" placeholder="" ng-model="UDP_IN" value="@{{ UDP_IN }}" type="text">
                            <label>UDP_IN</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Allowed UDP OUTBOUND port on server">
                            <input class="active" placeholder="" ng-model="UDP_OUT" value="@{{ UDP_OUT }}" type="text">
                            <label>UDP_OUT</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Allow other host to ping this server">
                            <select ng-model="ICMP_IN">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                            <label>ICMP_IN</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Allow this server to ping other host. Affect all user">
                            <select ng-model="ICMP_OUT">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                            <label>ICMP_OUT</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Block SMTP port except this server for usage with Roundcube">
                            <select ng-model="SMTP_BLOCK">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                            <label>SMTP_BLOCK</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Enable SYN flood Protection. Only enable if you are under DOS attack">
                            <select ng-model="SYNFLOOD">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                            <label>SYNFLOOD</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Enable UDP flood Protection. Only enable if you are under DOS attack">
                            <select ng-model="UDPFLOOD">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                            <label>UDPFLOOD</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="If IP Address have been temporarily block by this value, it will trigger permanent block">
                            <input class="active" placeholder="" ng-model="LF_PERMBLOCK_COUNT" value="@{{ LF_PERMBLOCK_COUNT }}" type="text">
                            <label>LF_PERMBLOCK_COUNT</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Send email to Admin when someone succesfully login to server using SSH">
                            <select ng-model="LF_SSH_EMAIL_ALERT">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                            <label>LF_SSH_EMAIL_ALERT</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="If user process have more process than this value, send email to admin">
                            <input class="active" placeholder="" ng-model="PT_USERPROC" value="@{{ PT_USERPROC }}" type="number">
                            <label>PT_USERPROC</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="If user process use more memory than this set value, send email to admin">
                            <input class="active" placeholder="" ng-model="PT_USERMEM" value="@{{ PT_USERMEM }}" type="number">
                            <label>PT_USERMEM</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="If user process use take time more than this defined value, send email to admin. Value is in seconds">
                            <input class="active" placeholder="" ng-model="PT_USERTIME" value="@{{ PT_USERTIME }}" type="number">
                            <label>PT_USERTIME</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Server load average must be under this value to be consider normal">
                            <input class="active" placeholder="" ng-model="PT_LOAD_AVG" value="@{{ PT_LOAD_AVG }}" type="number">
                            <label>PT_LOAD_AVG</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Send email to admin if server load is more than this value">
                            <input class="active" placeholder="" ng-model="PT_LOAD_LEVEL" value="@{{ PT_LOAD_LEVEL }}" type="number">
                            <label>PT_LOAD_LEVEL</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Process can have this maximum number of children to prevent fork bomb. Do not go to value below 250. Disable some process by defining it in csf.pignore">
                            <input class="active" placeholder="" ng-model="PT_FORKBOMB" value="@{{ PT_FORKBOMB }}" type="number">
                            <label>PT_FORKBOMB</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Interval between port scanning. 0 to disable this feature">
                            <input class="active" placeholder="" ng-model="PS_INTERVAL" value="@{{ PS_INTERVAL }}" type="number">
                            <label>PS_INTERVAL</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="If IP Address have been detect for port scanning by this this number of times in PS_INTERVAL. Server will block the IP Address for port scanning">
                            <input class="active" placeholder="" ng-model="PS_LIMIT" value="@{{ PS_LIMIT }}" type="number">
                            <label>PS_LIMIT</label>
                        </div>

                        <div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="Send email alert if there is new user created, user deleted, user password change and user shell change">
                            <select ng-model="AT_ALERT">
                                <option value="0">Disable this function</option>
                                <option value="1">Enable for all user</option>
                                <option value="2">Enable for superuser account only</option>
                                <option value="3">Enable for root account only</option>
                            </select>
                            <label>AT_ALERT</label>
                        </div>


                        <div class="input-field col s12">
                            <button ng-click="saveConfig()" ng-disabled="isSaving" class="btn right blue waves-effect" style="margin-bottom: 15px;">Save Configuration</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="csfAllowTab" class="col s12">
        <div class="card">
            <div class="card-content" id="csfAllow"></div>
            <div class="card-action">
                <button class="btn right blue bottom15px" ng-click="saveCSFAllow()" ng-disabled="isSaving">Save</button>
            </div>
        </div>
    </div>
    <div id="csfDenyTab" class="col s12">
        <div class="card">
            <div class="card-content" id="csfDeny"></div>
            <div class="card-action">
                <button class="btn right blue bottom15px" ng-click="saveCSFDeny()" ng-disabled="isSaving">Save</button>
            </div>
        </div>
    </div>
    <div id="csfIgnoreTab" class="col s12">
        <div class="card">
            <div class="card-content" id="csfIgnore"></div>
            <div class="card-action">
                <button class="btn right blue bottom15px" ng-click="saveCSFIgnore()" ng-disabled="isSaving">Save</button>
            </div>
        </div>
    </div>
    <div id="csfPignoreTab" class="col s12">
        <div class="card">
            <div class="card-content" id="csfPignore"></div>
            <div class="card-action">
                <button class="btn right blue bottom15px" ng-click="saveCSFPignore()" ng-disabled="isSaving">Save</button>
            </div>
        </div>
    </div>
</div>
@stop