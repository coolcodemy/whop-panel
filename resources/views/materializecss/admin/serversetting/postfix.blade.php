@extends('materializecss.admin.serversetting', ['title' => 'Postfix Setting'])


@section('serverconfig')

    <div class="row" ng-controller="ServerSettingPostfixController">
        <div class="col s12">
            <ul class="collection with-header">
                <li class="collection-header"><h4>Postfix Service Status</h4></li>
                <li class="collection-item">Average CPU usage <span class="badge">@{{ cpuUsage }}</span></li>
                <li class="collection-item">Average Memory usage <span class="badge">@{{ memoryUsage }}</span></li>
                <li class="collection-header"><h4>Postfix Service Control</h4></li>
                <li class="collection-item">
                    Start/Stop Service
                    <div class="switch right">
                        <label>
                            Stop<input ng-click="toggle()" ng-model="switch" ng-disabled="disableSwitch" type="checkbox"><span class="lever"></span>Start
                        </label>
                    </div>
                </li>
                <li class="collection-item">Restart Service <button class="waves-effect waves-light right btn red bottom15px" ng-click="restart()" ng-disabled="isRestart">Restart</button></li>
            </ul>
        </div>

        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <h4>Setting</h4>

                    <form>
                        <div class="input-field col s12">
                            <label class="active">SSL/TLS Certificate</label>
                        </div>

                        <div class="input-field col s3">
                            <a ng-click="chooseCertificate()" class="btn waves-effect" style="width:100% !important">Choose</a>
                        </div>

                        <div class="input-field col s9">
                            <input class="active" ng-model="ssl" value="@{{ ssl }}" type="text">
                        </div>


                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="hostname" value="@{{ hostname }}" type="text">
                            <label>MyHostName</label>
                        </div>

                        <div class="input-field col s12">
                            <button ng-click="saveConfig()" ng-disabled="isSaving" class="btn right blue waves-effect" style="margin-bottom: 15px;">Save Configuration</button>
                        </div>
                    </form>

                    @include('materializecss.partial._modal', ['modal' => 'universalcertificate'])
                </div>
            </div>
        </div>
    </div>
@stop