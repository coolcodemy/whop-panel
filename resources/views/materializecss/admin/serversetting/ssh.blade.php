@extends('materializecss.admin.serversetting', ['title' => 'SSH Setting'])


@section('serverconfig')

    <div class="row" ng-controller="ServerSerttingSSHController">
        <div class="col s12">
            <ul class="collection with-header">
                <li class="collection-header"><h4>SSH Service Status</h4></li>
                <li class="collection-item">Average CPU usage <span class="badge">@{{ cpuUsage }}</span></li>
                <li class="collection-item">Average Memory usage <span class="badge">@{{ memoryUsage }}</span></li>
                <li class="collection-header"><h4>SSH Service Control</h4></li>
                <li class="collection-item">
                    Start/Stop Service
                    <div class="switch right">
                        <label>
                            Stop<input ng-click="toggle()" ng-disabled="disableSwitch" ng-model="switch" type="checkbox"><span class="lever"></span>Start
                        </label>
                    </div>
                </li>
                <li class="collection-item">Restart Service <button ng-click="restart()" ng-disabled="isRestart" class="waves-effect waves-light right btn red bottom15px" id="restart">Restart</button></li>
            </ul>
        </div>

        <div class="col s12">
            <div class="card">
                <div class="card-content">
                    <h4>Setting</h4>

                    <form>
                        <div class="input-field col s12">
                            <input class="active" placeholder="" ng-model="Port" value="@{{ Port }}" type="number">
                            <label>SSH Port</label>
                        </div>

                        <div class="input-field col s12">
                            <select ng-model="UseDNS">
                                <option value="no">No</option>
                                <option value="yes">Yes</option>
                            </select>
                            <label>Use DNS</label>
                        </div>

                        <div class="input-field col s12">
                            <button ng-click="saveConfig()" ng-disabled="isSaving" class="btn waves-effect right blue" style="margin-bottom: 15px;">Save Configuration</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop