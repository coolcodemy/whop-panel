@extends('materializecss.layout.master', ['title' => 'Panel SSL/TLS Setting'])


@section('content')



<div class="row" ng-controller="PanelSSLController">

	<div class="col s12">

		<h4>Owned Certificate</h4>


		<table class="bordered">
			<thead>
				<tr>
					<th>Name</th>
					<th>View</th>
					<th>Use As Panel Certificate</th>
					<th>Delete</th>
				</tr>
			</thead>
		
			<tbody>
				<tr ng-repeat="cert in certs" ng-show="certCount > 0">
					<td>@{{ cert.cert | split:'.crt':0 }}</td>
					<td><button class="btn light-green" ng-click="view(cert.cert)"><i class="fa fa-eye"></i></button></td>
					<td>
						<button class="btn blue" ng-click="useAsPanelCertificate(cert.cert)" ng-disabled="cert.cert == usingCert"><i class="fa fa-check-circle"></i></button>
					</td>
					<td><button class="btn red" ng-click="delete(cert.cert)"><i class="fa fa-trash"></i></button></td>
				</tr>

				<tr ng-show="certCount == 0">
					<td colspan="4">No SSL/TLS certificate available</td>
				</tr>
			</tbody>
		</table>

		@include('materializecss.partial._modal', ['modal' => 'viewcertificate'])
		@include('materializecss.partial._modal', ['modal' => 'deletecertificate'])
	</div>



	<div class="col s12">

		<h4>Add Certificate</h4>


		<form action="#">
			<div class="row">
				<div class="input-field col s12"> 
					<div class="form-group">
						<label class="control-label">Certificate Name</label>
						<input type="text" class="form-control" ng-model="certificateName" placeholder="Alphanumeric only">
					</div>
				</div>

				<div class="input-field col s12"> 
					<div class="form-group">
						<label class="control-label" for="publicCertificate">SSL/TLS Certificate</label>
						<textarea class="materialize-textarea" id="publicCertificate" ng-model="publicCertificate" placeholder=""></textarea>
					</div>
				</div>


				<div class="input-field col s12"> 
					<div class="form-group">
						<label class="control-label" for="privateCertificate">SSL/TLS Certificate Key</label>
						<textarea class="materialize-textarea" id="privateCerficate" ng-model="privateCertificate" placeholder=""></textarea>
					</div>
				</div>

				<div class="input-field col s12"> 
					<button type="button" class="btn light-green" ng-click="addCertificate()" ng-disabled="isBusy">Add</button>
				</div>
			</div>
		</form>

	</div>
</div>



@stop