@extends('materializecss.layout.master', ['title' => 'Dashboard'])


@section('content')
<div ng-controller="AdminDashboardController">
    <div class="row">
        <div class="col s12 l6">
            <ul class="collection">
                <li class="collection-item avatar">
                    <i class="fa fa-desktop circle light-green darken-2"></i>
                    <span class="collection-header">Server Information</span>
                    <p>Information about current server status</p>
                </li>
                <li class="collection-item">
                    <div>Kernel Version <span class="badge">@{{ dashboardKernelVersion }}</span></div>
                </li>
                <li class="collection-item">
                    <div>Processor Name <span class="badge">@{{ dashboardProcessorName }}</span></div>
                </li>
                <li class="collection-item">
                    <div>Free Memory <span class="badge"><span>@{{ dashboardFreeMemory }}</span>/<span>@{{ dashboardTotalMemory }}</span></span></div>
                </li>
                <li class="collection-item">
                    <div>Free Disk Space <span class="badge"><span>@{{ dashboardFreeDisk }}</span>/<span>@{{ dashboardTotalDisk }}</span></span></div>
                </li>
                <li class="collection-item">
                    <div>Server Uptime <span class="badge green white-text">@{{ dashboardServerUptime }}</span></div>
                </li>
                <li class="collection-item">
                    <div>WHoP Version <span class="badge">{{ Config::get('whop.version') }}</span></div>
                </li>
                <li class="collection-item">
                    <div>WHoP Node Version <span class="badge">@{{ dashboardNodeVersion }}</span></div>
                </li>
                <li class="collection-item">
                    <div>WHoP Codename <span class="badge">{{ Config::get('whop.codename') }}</span></div>
                </li>
            </ul>
        </div>
        <div class="col s12 l6">
            <ul class="collection">
                <li class="collection-item avatar">
                	<i class="material-icons circle cyan darken-2">insert_chart</i>
                    <span class="collection-header">Server Load Average</span>
                    <p>WHoP server load (average)</p>
                </li>
                <li class="collection-item">
                	<canvas id="liveLoad" width="100%" height="200px" style="padding-right:30px">jhhj</canvas>
                </li>
            </ul>
        </div>
    </div>
    <div class="row">
        <div class="col s12 l6">
            <ul class="collection">
                <li class="collection-item avatar">
                    <i class="mdi-action-trending-up circle red darken-2"></i>
                    <span class="collection-header">WHoP Information</span>
                    <p>Statistic that you should know</p>

                    @inject('progress', 'WHoP\Services\ProgressService')
                </li>
                <li class="collection-item">
                    <div>
                        Domain Owner
                        <span class="badge">{{ $progress->allDomainOwner() }}</span>
                    </div>
                </li>
                <li class="collection-item">
                    <div>
                        Domain Name
                        <span class="badge">{{ $progress->allDomainCount() }}</span>
                    </div>
                </li>
                <li class="collection-item">
                    <div>
                        Database File
                        <span class="badge">{{ $progress->allDatabaseCount() }}</span>
                    </div>
                </li>
                <li class="collection-item">
                    <div>
                        WHoPlet
                        <span class="badge">@{{ dashboardNginxCount }}</span>
                    </div>
                </li>
                <li class="collection-item">
                    <div>
                        FTP Users
                        <span class="badge">{{ $progress->allFtpCount() }}</span>
                    </div>
                </li>
                <li class="collection-item">
                    <div>
                        Email Users
                        <span class="badge">{{ $progress->allEmailCount() }}</span>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col s12 l6">
            <ul class="collection">
                <li class="collection-item avatar">
                    <i class="material-icons circle cyan darken-2">insert_chart</i>
                    <span class="collection-header">Package Distribution</span>
                    <p>Package usage by user</p>
                </li>
                <li class="collection-item">
                    <canvas id="packageUsage" data-url="{{ route('admin::packagechart') }}" width="200px" height="200px"></canvas>
                </li>
            </ul>
        </div>
        <div class="col s12">
            <ul class="collection">
                <li class="collection-item avatar">
                    <i class="mdi-action-info circle cyan darken-2"></i>
                    <span class="collection-header">License Information</span>
                    <p>WHoP license info</p>
                </li>
                <li class="collection-item">License Owner <span class="badge orange white-text">@{{ mylicenseOwner }}</span></li>
                <li class="collection-item">License Type <span class="badge black white-text">@{{ mylicenseType }}</span></li>
                <li class="collection-item">License Expiry <span class="badge green white-text">@{{ mylicenseExpiry }}</span></li>
            </ul>
        </div>
    </div>
</div>
@stop