@extends('materializecss.layout.master', ['title' => 'WHoP Panel Setting'])


@section('content')

<div class="row">

	<div class="col s12">

		<h4>Auto Update</h4>

		<p>Auto update will enable your panel to automatically updated to latest version. This will enable you to get latest feature from WHoP. Auto update only available for Paid WHoP user.</p>

		<div class="switch" ng-controller="AutoUpdateController">
			<label>
				Off <input ng-disabled="isBusy" ng-click="switch('{{ route('admin::autoupdate') }}')" type="checkbox" {{ env('AUTO_UPDATE') ? 'checked' : null }}><span class="lever"></span> On
			</label>
		</div>

	</div>

	<div class="col s12">

		<h4>Panel Setting</h4>

		{!! Form::open(['route' => 'admin::panelsetting-update']) !!}

			<div class="row">

				@foreach ( $settings as $setting )

					@if ($setting->settingName == 'MaxUserTemplate')


						<div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="This will determine the maximum WHoPlet template that can be created by each Domain Owner">
							<div class="form-group">
								<label class="control-label" for="">Maximum Domain Owner Template</label>
								<input type="number" class="form-control" name="MaxUserTemplate" placeholder="" value="{{ $setting->value }}">
							</div>
						</div>


					@elseif ($setting->settingName == 'OwnerPerPage')

						<div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="How many Domain Owner you want to be displayed for each page">
							<div class="form-group">
								<label class="control-label" for="">Domain Owner Per Page</label>
								<input type="number" class="form-control" name="OwnerPerPage" placeholder="" value="{{ $setting->value }}">
							</div>
						</div>


					@elseif ($setting->settingName == 'BruteforceTimes')

						<div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="How many times user can try login to panel before being block by WHoP">
							<div class="form-group">
								<label class="control-label" for="">Bruteforce Times</label>
								<input type="number" class="form-control" name="BruteforceTimes" placeholder="" value="{{ $setting->value }}">
							</div>
						</div>

					@elseif ($setting->settingName == 'BruteforceMinutes')

						<div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="How many minutes user will be lock by WHoP before he/she can log in back">
							<div class="form-group">
								<label class="control-label" for="">Bruteforce Minutes</label>
								<input type="number" class="form-control" name="BruteforceMinutes" placeholder="" value="{{ $setting->value }}">
							</div>
						</div>


					@elseif ($setting->settingName == 'OwnerDomainItemPerPage')

						<div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="How many domain you want to display for each page">
							<div class="form-group">
								<label class="control-label" for="">Domain Item Per Page</label>
								<input type="number" class="form-control" name="OwnerDomainItemPerPage" placeholder="" value="{{ $setting->value }}">
							</div>
						</div>

					@elseif ($setting->settingName == 'OwnerMailForwardingItemPerPage')

						<div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="How many email forwarding item you want to display for each page">
							<div class="form-group">
								<label class="control-label" for="">Email Forwarding Item Per Page</label>
								<input type="number" class="form-control" name="OwnerMailForwardingItemPerPage" placeholder="" value="{{ $setting->value }}">
							</div>
						</div>

					@elseif ($setting->settingName == 'OwnerMailItemPerPage')

						<div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="How many email item you want to display for each page">
							<div class="form-group">
								<label class="control-label" for="">Email Item Per Page</label>
								<input type="number" class="form-control" name="OwnerMailItemPerPage" placeholder="" value="{{ $setting->value }}">
							</div>
						</div>


					@elseif ($setting->settingName == 'OwnerFtpItemPerPage')

						<div class="input-field col s12 tooltipped" data-position="top" data-delay="50" data-tooltip="How many FTP User you want to display for each page">
							<div class="form-group">
								<label class="control-label" for="">FTP User Per Page</label>
								<input type="number" class="form-control" name="OwnerFtpItemPerPage" placeholder="" value="{{ $setting->value }}">
							</div>
						</div>


					@endif

				@endforeach

			</div>
			
			<button type="submit" class="btn light-green">Update</button><br><br>
		
		{!! Form::close() !!}

	</div>
</div>

@stop