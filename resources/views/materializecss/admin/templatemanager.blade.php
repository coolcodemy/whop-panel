@extends('materializecss.layout.master', ['title' => 'WHoPlet Template Manager'])


@section('content')
<div class="row" ng-controller="WhopletAdminTemplateController">
    <div class="col s12">
        <h4>All Templates</h4>

        <table class="striped bordered responsive-table">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Last Update</th>
                    <th>Status</th>
                    <th>View</th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>
                @foreach($templates as $template)
                    <tr class="hoverable">
                        <td>{{ $template->name }}</td>
                        <td>{{ date('Y-m-d', strtotime($template->updated_at)) }} ({{ $template->updated_at->diffForHumans() }})</td>
                        <td>
                            @if ($template->status == 0)
                                <a href="{{ route('admin::templatemanager::enabledisable', [$template, 1]) }}" class="btn red waves-effect waves-light tooltipped" data-position="top" data-delay="50" data-tooltip="Click to enable">Disable</a>
                            @else
                                <a href="{{ route('admin::templatemanager::enabledisable', [$template, 0]) }}" class="btn light-green waves-effect waves-light tooltipped" data-position="top" data-delay="50" data-tooltip="Click to disable">Enable</a>
                            @endif
                        </td>
                        <td>
                            <button ng-disabled="isBusy" class="btn blue waves-effect waves-light btnView" ng-click="view('{{ route('admin::templatemanager::view', $template) }}')">View</button>
                        </td>
                        <td>
                            <button ng-disabled="isBusy" class="btn waves-effect waves-light btnEdit" ng-click="edit('{{ route('admin::templatemanager::view', $template) }}')">Edit</button>
                        </td>
                        <td>
                            <button ng-disabled="isBusy" ng-click="delete('{{ route('admin::templatemanager::delete', $template) }}')" class="btn red waves-effect waves-light confirmDeleteTemplate">Delete</button>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="col s12">
        {!! (new WHoP\ThirdParty\Pagination($templates))->render() !!}
    </div>

    <div class="col s12">
        <h4>Template Editor</h4>

        <div class="row">

            <div class="col s6">
                <div class="card hoverable">
                    <div class="card-content">
                        <h5>Skeleton HTTP</h5>
                        <p>The skeleton for WHoPlet to build http (port 80) web application</p>
                    </div>
                    <div class="card-action">
                        <a class="waves-effect waves-orange" ng-click="skeleton('{{ route('admin::templatemanager::skeleton', ['type' => 'http']) }}')">Craft</a>
                    </div>
                </div>
            </div>

            <div class="col s6">
                <div class="card hoverable">
                    <div class="card-content">
                        <h5>Skeleton HTTPS</h5>
                        <p>The skeleton for WHoPlet to build https (port 443) web application</p>
                    </div>
                    <div class="card-action">
                        <a class="waves-effect waves-orange" ng-click="skeleton('{{ route('admin::templatemanager::skeleton', ['type' => 'https']) }}')">Craft</a>
                    </div>
                </div>
            </div>

            <div class="col s12">
                <div class="card hoverable">
                    <div class="card-content">
                        <h5>Tag Description</h5>
                        <p><strong>{DOMAINNAME}</strong> - The domain name that will be use by Domain Owner.</p>
                        <p><strong>{ROOT}</strong> - Root folder of Domain Owner.</p>
                        <p><strong>{USERNAME}</strong> - Username of Domain Owner.</p>
                        <p><strong>{SSL}</strong> - SSL Certificate and Key own by Domain Owner.</p>
                        <p>Do not remove <strong class="red white-text">#BEGIN</strong> line! It will be use by Domain Owner to generate their own template based on your modified WHoPlet Template.</p>
                    </div>
                </div>
            </div>


            <div class="col s12">
                <div class="card hoverable">
                    <div class="card-content" id="templateEditor">
                        Craft, View or Edit WHoPlet template to use this section.
                    </div>
                    <div class="card-action">
                        <div class="row" ng-show="addEdit">
                            <div class="input-field col s12">
                                <input placeholder="" ng-model="templateName" type="text" value="@{{ templateName }}">
                                <label>Template Name (Will be save as WHoPlet_@{{ templateName }})</label>
                            </div>
                            <div class="input-field col s12">
                                <button class="btn waves-effect active" ng-click="save()" ng-disabled="isBusy">Save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @include('materializecss.partial._modal', ['modal' => 'deletetemplate'])

        </div>
    </div>
</div>
@stop



@section('meta')
    <meta name="store-template-url" content="{{ route('admin::templatemanager::store') }}">
    <meta name="update-template-url" content="{{ route('admin::templatemanager::update') }}">
@stop