@extends('materializecss.layout.master')



@section('serverconfig')
    <div class="card">
        <div class="card-content">
            <p>Choose which setting you want to change. While changing your setting, every service that you change will be reload in the background to match your current configuration. It is best to keep each setting at their current state. But it is preferable that you change all the SSL/TLS certificate in each service to valid certificate.</p>
        </div>
    </div>
@stop




@section('content')
<div class="row">
    <div class="col s12 m4">
        <div class="row">
            <div class="col s12">
                <ul class="collection with-header serversetting">
                    <li class="collection-header"><h5>Service Setting</h5></li>
                    <a href="{{ route('admin::dovecotsetting') }}" class="collection-item black-text">Dovecot Setting</a>
                    <a href="{{ route('admin::mariadbsetting') }}" class="collection-item black-text">MariaDB Setting</a>
                    <a href="{{ route('admin::phpsetting') }}" class="collection-item black-text">PHP Setting</a>
                    <a href="{{ route('admin::postfixsetting') }}" class="collection-item black-text">Postfix Setting</a>
                    <a href="{{ route('admin::powerdnssetting') }}" class="collection-item black-text">Power DNS Setting</a>
                    <a href="{{ route('admin::pureftpdsetting') }}" class="collection-item black-text">Pure-FTPD Setting</a>
                    <a href="{{ route('admin::spamassassinsetting') }}" class="collection-item black-text">Spamassassin Setting</a>
                    <a href="{{ route('admin::sshsetting') }}" class="collection-item black-text">SSH Setting</a>
                    <a href="{{ route('admin::nginxsetting') }}" class="collection-item black-text">Nginx Setting</a>
                </ul>
            </div>

            <div class="col s12">
                <ul class="collection with-header serversetting">
                    <li class="collection-header"><h5>Security Setting</h5></li>
                    <a href="{{ route('admin::csfsetting') }}" class="collection-item black-text">CSF</a>
                    <a href="{{ route('admin::fail2bansetting') }}" class="collection-item black-text">Fail2Ban</a>
                </ul>
            </div>
        </div>
    </div>

    <div class="col s12 m8">
        <div class="card" id="alertBox">
            <div class="card-content blue white-text">
                <p>Please wait while retrieving setting from server...</p>
            </div>
        </div>
        @yield('serverconfig')
    </div>    
</div>
@stop


@section('script')
    <script>
        $(document).ready(function()
        {
            $('#alertBox').hide();
        });
    </script>
@stop