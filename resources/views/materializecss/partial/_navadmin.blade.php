<li><a href="{{ route('admin::dashboard') }}" class="waves-effect waves-cyan black-text"><i class="mdi-action-dashboard"></i> Dashboard</a></li>
<li>
    <ul class="collapsible" data-collapsible="accordion">
        <li>
            <a class="collapsible-header waves-effect waves-cyan black-text"><i class="fa fa-bolt"></i>Admin</a>
            <div class="collapsible-body" style="display: block">
                <ul>
                    <li><a href="{{ route('admin::ownerlist') }}" class="waves-effect waves-cyan"><i class="fa fa-users"></i>Domain Owner</a></li>
                    <li><a href="{{ route('admin::packagelist') }}" class="waves-effect waves-cyan"><i class="fa fa-cubes"></i>Packages</a></li>
                    <li>
                        <div class="divider"></div>
                    </li>
                    <li><a href="{{ route('admin::templatemanager::index') }}" class="waves-effect waves-cyan"><i class="fa fa-terminal"></i>Template Manager</a></li>
                </ul>
            </div>
        </li>
    </ul>
</li>
<li><a href="{{ route('admin::serversetting') }}" class="black-text waves-effect waves-cyan "><i class="fa fa-desktop"></i> Server Settings</a></li>
<li>
    <ul class="collapsible" data-collapsible="accordion">
        <li>
            <a class="collapsible-header waves-effect waves-cyan black-text"><i class="fa fa-server"></i>WHoP Settings</a>
            <div class="collapsible-body" style="display: block">
                <ul>
                    <li><a href="{{ route('admin::panelsetting') }}" class="black-text waves-effect waves-cyan"><i class="fa fa-cog"></i>Panel Setting</a></li>
                    <li><a href="{{ route('admin::emailsetting') }}" class="black-text waves-effect waves-cyan"><i class="fa fa-envelope"></i>Email Setting</a></li>
                    <li><a href="{{ route('admin::sslsetting') }}" class="black-text waves-effect waves-cyan"><i class="fa fa-lock"></i>SSL/TLS Setting</a></li>
                </ul>
            </div>
        </li>
    </ul>
</li>
<li><a href="{{ route('admin::profile') }}" class="black-text waves-effect waves-cyan "><i class="fa fa-user"></i> My Profile</a></li>