<div class="navbar-fixed">
    <nav class="blue darken-1" role="navigation">
        <div class="nav-wrapper">
            <a href="#" data-activates="slide-out" class="sidebar-collapse waves-effect waves-light hide-on-large-only darken-2" style="padding-left:20px;padding-right:20px"><i class="mdi-navigation-menu" ></i></a>
            <a href="#" class="brand-logo" style="margin-left:3%">WHoP<span class="yellow-text">!</span></a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a class="tooltipped" data-position="left" data-tooltip="Read WHoP manual" href="{{ route('manual') }}"><i class="material-icons">description</i></a></li>
                <li><a class="tooltipped" data-position="left" data-tooltip="Logout from WHoP" href="{{ route('logout') }}"><i class="fa fa-sign-out"></i></a></li>
            </ul>
        </div>
    </nav>
</div>