@if ($modal == "universalmodal")

    <div id="universalModal" class="modal">
        <div class="modal-content">
            <h4>Please wait...</h4>
            <p>Please wait while the content is loading...</p>
        </div>
    </div>

@elseif ($modal == "addownermodal")


    <div id="addOwnerModal" class="modal">
        <div class="modal-content">
            <h4>Add Domain Owner</h4>
            <p ng-show="universalMessage != null"><div ng-bind-html="universalMessage | sanitize"></div></p>
            <p ng-show="universalMessage == null">Please wait while adding new user...</p>
        </div>
        <div class="modal-footer">
            <button type="submit" class="modal-action waves-effect btn-flat" ng-disabled="isBusy" ng-hide="universalMessage != null">Add</button>
            <button class="modal-action modal-close waves-effect waves-green btn-flat" ng-disabled="isBusy" ng-click="cancel()">Close</a>
        </div>
    </div>

@elseif ($modal == "deletepackage")


    <div id="deletePackageModal" class="modal">
        {!! Form::open(['id' => 'deletePackageForm']) !!}
            <div class="modal-content">
                <h4>Delete Package</h4>
                <p>Are you sure you want to delete this package?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="modal-action waves-effect waves-red btn-flat">Delete</button>
                <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            </div>
        {!! Form::close() !!}
    </div>

@elseif ($modal == "universalcertificate")

    <div id="universalCertificateModal" class="modal">
        <div class="modal-content">
            <h4>Select SSL/TLS Certificate</h4>
            <ul class="collection" ng-show="certCount > 0">
                <li class="collection-item" ng-repeat="x in certificates" ng-click="setCertificate(x.cert)"><span class="fa fa-certificate"></span> @{{ x.cert }}</li>
            </ul>
            <strong ng-hide="certCount > 0">Certificate not available</strong>
            <span ng-show="wait">Please wait while the certificate load...</span>
        </div>
        <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
        </div>
    </div>

@elseif ($modal == "deletetemplate")


    <div id="deleteTemplateModal" class="modal">
        {!! Form::open(['id' => 'deleteTemplateForm']) !!}
            <div class="modal-content">
                <h4>Delete Template</h4>
                <p>Are you sure you want to delete this WHoPlet Template?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="modal-action waves-effect waves-red btn-flat">Delete</button>
                <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            </div>
        {!! Form::close() !!}
    </div>

@elseif ($modal == "chooseHomeDir")


    <div id="chooseHomeDirModal" class="modal">
        <div class="modal-content">
            <h4>Choose Root Directory</h4>
            <p>Current Directory - /var/www/{{ $user->username }}/web/public@{{ currentDir }}</span></p>

            <p ng-show="isLoadingDirectory">Please wait while getting folder list...</p>

            <ul class="collection" ng-hide="haveMoreFolder == false">
                <li ng-repeat="folderList in folderLists" class="collection-item" ng-click="walkInFolder(folderList.path + '/')"><span class="fa fa-folder"></span> @{{ folderList.folder }}</li>
            </ul>
            <p ng-hide="haveMoreFolder == true">No more folder to show!</p>
        </div>
        <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-green btn-flat">Choose</a>
            <a class="modal-action waves-effect waves-yellow btn-flat" ng-click="back()">Back</a>
            <a class="modal-action modal-close waves-effect waves-red btn-flat" ng-click="cancelSelectDir()">Cancel</a>
        </div>
    </div>

@elseif ($modal == "deletewhoplet")


    <div id="deleteWhopletModal" class="modal">
        <div class="modal-content">
            <h4>Delete WHoPlet</h4>

            <p>Deleting WHoPlet will not remove file in directory. Do you really want to delete this WHoPlet?</p>
        </div>
        <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-red btn-flat" ng-click="deleteWhoplet()">Delete</a>
            <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
        </div>
    </div>


@elseif ($modal == "revokedbuser")


    <div id="revokeDatabaseUserModal" class="modal">
        {!! Form::open(['url' => $buttonURL['revoke-user']]) !!}
            <div class="modal-content">
                <h4>Revoke user from database</h4>
                <p>Are you sure you want to revoke this user from database?</p>
            </div>
            <input type="hidden" ng-value="database" name="database" id="revokeDatabase">
            <input type="hidden" ng-value="databaseUser" name="databaseUser" id="revokeUser">
            <div class="modal-footer">
                <button type="submit" class="modal-action waves-effect waves-red btn-flat">Delete</button>
                <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            </div>
        {!! Form::close() !!}
    </div>


@elseif ($modal == "deletedatabase")


    <div id="deleteDatabaseModal" class="modal">
        {!! Form::open(['url' => $buttonURL['delete-database']]) !!}
            <div class="modal-content">
                <h4>Delete database</h4>
                <p>Are you sure you want to delete this database?</p>
            </div>
            <input type="hidden" name="database" ng-value="database" value="">
            <div class="modal-footer">
                <button type="submit" class="modal-action waves-effect waves-red btn-flat">Delete</button>
                <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            </div>
        {!! Form::close() !!}
    </div>

@elseif ($modal == "deletedatabaseuser")


    <div id="deleteDatabaseUserModal" class="modal">
        {!! Form::open(['url' => $buttonURL['delete-database-user']]) !!}
            <div class="modal-content">
                <h4>Revoke user from database</h4>
                <p>Are you sure you want to revoke this user from database?</p>
            </div>
            <input type="hidden" name="databaseUser" ng-value="databaseUser" value="">
            <div class="modal-footer">
                <button type="submit" class="modal-action waves-effect waves-red btn-flat">Delete</button>
                <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            </div>
        {!! Form::close() !!}
    </div>

@elseif ($modal == "deletedomain")


    <div id="deleteDomainModal" class="modal">
        {!! Form::open(['id' => 'deleteDomainForm']) !!}
            <div class="modal-content">
                <h4>Delete Domain Name</h4>
                <p>Deleting this domain will also delete all email, ftp account and WHoPlet that associate to this Domain Name. Delete this Domain Name?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="modal-action waves-effect waves-red btn-flat">Delete</button>
                <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            </div>
        {!! Form::close() !!}
    </div>

@elseif ($modal == "deleterecord")


    <div id="deleteRecordModal" class="modal">
        {!! Form::open(['id' => 'deleteRecordForm']) !!}
            <div class="modal-content">
                <h4>Delete Domain Record</h4>
                <p>Deleting this Record will also delete all email, ftp account and WHoPlet that associate to this Record. Delete this Domain Record?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="modal-action waves-effect waves-red btn-flat">Delete</button>
                <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            </div>
        {!! Form::close() !!}
    </div>


@elseif ($modal == "deleteemail")


    <div id="deleteEmailModal" class="modal">
        {!! Form::open(['id' => 'deleteEmailForm']) !!}
            <div class="modal-content">
                <h4>Delete Email Account</h4>
                <p>Deleting this email will delete all email content including email in inbox, spam, trash or other folder. Creating new email account with same name will not recover your email data. Delete this email account?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="modal-action waves-effect waves-red btn-flat">Delete</button>
                <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            </div>
        {!! Form::close() !!}
    </div>


@elseif ($modal == "deleteforwarding")


    <div id="deleteForwardingModal" class="modal">
        {!! Form::open(['id' => 'deleteForwardingForm']) !!}
            <div class="modal-content">
                <h4>Delete Forwarding</h4>
                <p>Are you sure you want to delete this email forwarding?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="modal-action waves-effect waves-red btn-flat">Delete</button>
                <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            </div>
        {!! Form::close() !!}
    </div>


@elseif ($modal == "deleteftpaccount")


    <div id="deleteFTPModal" class="modal">
        {!! Form::open(['id' => 'deleteFTPForm']) !!}
            <div class="modal-content">
                <h4>Delete FTP Account</h4>
                <p>Are you sure you want to delete this FTP Account?</p>
            </div>
            <div class="modal-footer">
                <button type="submit" class="modal-action waves-effect waves-red btn-flat">Delete</button>
                <a class="modal-action modal-close waves-effect waves-green btn-flat">Cancel</a>
            </div>
        {!! Form::close() !!}
    </div>

@elseif ($modal == "viewcertificate")

    <div id="viewCertificateModal" class="modal bottom-sheet">
        <div class="modal-content">
            <h4>View Certificate</h4>
            <pre>@{{ certContent }}</pre>
        </div>
        <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
        </div>
    </div>


@elseif ($modal == "deletecertificate")

    <div id="deleteCertificateModal" class="modal">
        <div class="modal-content">
            <h4>Delete Certificate</h4>
            <p ng-show="deleteMessage == null">Are you sure you want to delete this certificate? Deleted certificate is not recoverable.</p>
            <p ng-show="deleteMessage != null">@{{ deleteMessage }}</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="modal-action waves-effect waves-red btn-flat" ng-click="deleteCertificate()" ng-disabled="isBusy" ng-hide="deleteMessage != null">Delete</button>
            <a class="modal-action modal-close waves-effect waves-green btn-flat" ng-click="cancel()">Close</a>
        </div>
    </div>


@elseif ($modal == "deletecronjob")

    <div id="deleteCronjobModal" class="modal">
        <div class="modal-content">
            <h4>Delete Cronjob</h4>
            <p ng-show="deleteMessage == null">Are you sure you want to delete this cron job?</p>
            <p ng-show="deleteMessage != null">@{{ deleteMessage }}</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="modal-action waves-effect waves-red btn-flat" ng-click="delete()" ng-disabled="isBusy" ng-hide="deleteMessage != null">Delete</button>
            <a class="modal-action modal-close waves-effect waves-green btn-flat" ng-click="cancel()">Close</a>
        </div>
    </div>

@elseif ($modal == "addfile")

    <div id="addFileModal" class="modal">
        <div class="modal-content">
            <h4>Add New File</h4>
            <form ng-show="universalMessage == null">
                <div class="row">
                    <div class="input-field col s12"> 
                        <div class="form-group">
                            <label class="control-label" for="">File Name</label>
                            <input type="text" class="form-control" ng-model="fileName" placeholder="">
                        </div>
                    </div>
                </div>
            </form>
            <p ng-show="universalMessage != null">@{{ universalMessage }}</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="modal-action waves-effect btn-flat" ng-click="newFile()" ng-disabled="isBusy" ng-hide="universalMessage != null">Add</button>
            <a class="modal-action modal-close waves-effect waves-green btn-flat" ng-click="cancel()">Close</a>
        </div>
    </div>

@elseif ($modal == "adddirectory")

    <div id="addDirectoryModal" class="modal">
        <div class="modal-content">
            <h4>Add New Directory</h4>
            <form ng-show="universalMessage == null">
                <div class="row">
                    <div class="input-field col s12"> 
                        <div class="form-group">
                            <label class="control-label" for="">Directory Name</label>
                            <input type="text" class="form-control" ng-model="directoryName" placeholder="">
                        </div>
                    </div>
                </div>
            </form>
            <p ng-show="universalMessage != null">@{{ universalMessage }}</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="modal-action waves-effect btn-flat" ng-click="newDirectory()" ng-disabled="isBusy" ng-hide="universalMessage != null">Add</button>
            <a class="modal-action modal-close waves-effect waves-green btn-flat" ng-click="cancel()">Close</a>
        </div>
    </div>

@elseif ($modal == "renamefile")

    <div id="renameFileModal" class="modal">
        <div class="modal-content">
            <h4>Rename File/Folder</h4>
            <form ng-show="universalMessage == null">
                <div class="row">
                    <div class="input-field col s12"> 
                        <div class="form-group">
                            <label class="control-label" for="">New Name</label>
                            <input type="text" class="form-control" ng-model="newName" placeholder="">
                        </div>
                    </div>
                </div>
            </form>
            <p ng-show="universalMessage != null">@{{ universalMessage }}</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="modal-action waves-effect btn-flat" ng-click="rename()" ng-disabled="isBusy" ng-hide="universalMessage != null">Rename</button>
            <a class="modal-action modal-close waves-effect waves-green btn-flat" ng-click="cancel()">Close</a>
        </div>
    </div>

@elseif ($modal == "deletefiledirectory")

    <div id="deleteFileDirectoryModal" class="modal">
        <div class="modal-content">
            <h4>Delete File/Directory</h4>
            <p ng-show="universalMessage == null">Delete selected file/directory?</p>
            <p ng-show="universalMessage != null">@{{ universalMessage }}</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="modal-action waves-effect waves-red btn-flat" ng-click="delete()" ng-disabled="isBusy" ng-hide="universalMessage != null">Delete</button>
            <a class="modal-action modal-close waves-effect waves-green btn-flat" ng-click="cancel()">Close</a>
        </div>
    </div>

@elseif ($modal == "deletesupervisorjob")

    <div id="deleteSupervisorModal" class="modal">
        <div class="modal-content">
            <h4>Delete Supervisor Job</h4>
            <p ng-show="universalMessage == null">Delete this supervisor job?</p>
            <p ng-show="universalMessage != null">@{{ universalMessage }}</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="modal-action waves-effect waves-red btn-flat" ng-click="delete()" ng-disabled="isBusy" ng-hide="universalMessage != null">Delete</button>
            <a class="modal-action modal-close waves-effect waves-green btn-flat" ng-click="cancel()">Close</a>
        </div>
    </div>


@elseif ($modal == "restartsupervisorjob")

    <div id="restartSupervisorModal" class="modal">
        <div class="modal-content">
            <h4>Restart Supervisor Job</h4>
            <p ng-show="universalMessage == null">Restart this supervisor job?</p>
            <p ng-show="universalMessage != null">@{{ universalMessage }}</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="modal-action waves-effect waves-green btn-flat" ng-click="restart()" ng-disabled="isBusy" ng-hide="universalMessage != null">Restart</button>
            <a class="modal-action modal-close waves-effect waves-green btn-flat" ng-click="cancel()">Close</a>
        </div>
    </div>


@elseif ($modal == "changepermission")

    <div id="permissionModal" class="modal">
        <div class="modal-content">
            <h4>Change Permission</h4>
            <form ng-show="universalMessage == null">
                <div class="row">
                    <div class="input-field col s12 bottom15px"> 
                        <div class="form-group">
                            <label class="control-label active" for="">User</label>
                            <input type="checkbox" id="userRead" ng-model="userRead"/>
                            <label for="userRead" class="margin-right-20px left0">Read</label>

                            <input type="checkbox" id="userWrite" ng-model="userWrite"/>
                            <label for="userWrite" class="margin-right-20px">Write</label>

                            <input type="checkbox" id="userExecute" ng-model="userExecute"/>
                            <label for="userExecute" class="margin-right-20px">Execute</label>
                        </div>
                    </div>

                    <div class="input-field col s12 bottom15px"> 
                        <div class="form-group">
                            <label class="control-label active" for="">Group</label>
                            <input type="checkbox" id="groupRead" ng-model="groupRead"/>
                            <label for="groupRead" class="margin-right-20px left0">Read</label>

                            <input type="checkbox" id="groupWrite" ng-model="groupWrite"/>
                            <label for="groupWrite" class="margin-right-20px">Write</label>

                            <input type="checkbox" id="groupExecute" ng-model="groupExecute"/>
                            <label for="groupExecute" class="margin-right-20px">Execute</label>
                        </div>
                    </div>

                    <div class="input-field col s12 bottom15px"> 
                        <div class="form-group">
                            <label class="control-label active" for="">World</label>
                            <input type="checkbox" id="worldRead" ng-model="worldRead"/>
                            <label for="worldRead" class="margin-right-20px left0">Read</label>

                            <input type="checkbox" id="worldWrite" ng-model="worldWrite"/>
                            <label for="worldWrite" class="margin-right-20px">Write</label>

                            <input type="checkbox" id="worldExecute" ng-model="worldExecute"/>
                            <label for="worldExecute" class="margin-right-20px">Execute</label>
                        </div>
                    </div>
                </div>
            </form>
            <p ng-show="universalMessage != null">@{{ universalMessage }}</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="modal-action waves-effect btn-flat" ng-click="changePermission()" ng-disabled="isBusy" ng-hide="universalMessage != null">Change</button>
            <a class="modal-action modal-close waves-effect waves-green btn-flat" ng-click="cancel()">Close</a>
        </div>
    </div>


@elseif ($modal == "editormode")

    <div id="editorModeModal" class="modal">
        <div class="modal-content">
            <h4>Pick Editor Mode</h4>
            <form ng-show="universalMessage == null">
                <div class="row">
                    <div class="input-field col s12"> 
                        <div class="form-group">
                            <label class="control-label active" for="">Available Mode</label>
                            <select id="aceMode" class="material-select" ng-model="editorMode" ng-change="changeEditor()">
                                <option value="abap">ABAP</option>
                                <option value="actionscript">ActionScript</option>
                                <option value="ada">ADA</option>
                                <option value="apache_conf">Apache Conf</option>
                                <option value="asciidoc">AsciiDoc</option>
                                <option value="assembly_x86">Assembly x86</option>
                                <option value="autohotkey">AutoHotKey</option>
                                <option value="batchfile">BatchFile</option>
                                <option value="c9search">C9Search</option>
                                <option value="c_cpp">C and C++</option>
                                <option value="cirru">Cirru</option>
                                <option value="clojure">Clojure</option>
                                <option value="cobol">Cobol</option>
                                <option value="coffee">CoffeeScript</option>
                                <option value="coldfusion">ColdFusion</option>
                                <option value="csharp">C#</option>
                                <option value="css">CSS</option>
                                <option value="curly">Curly</option>
                                <option value="d">D</option>
                                <option value="dart">Dart</option>
                                <option value="diff">Diff</option>
                                <option value="dockerfile">Dockerfile</option>
                                <option value="dot">Dot</option>
                                <option value="dummy">Dummy</option>
                                <option value="dummysyntax">DummySyntax</option>
                                <option value="eiffel">Eiffel</option>
                                <option value="ejs">EJS</option>
                                <option value="elixir">Elixir</option>
                                <option value="elm">Elm</option>
                                <option value="erlang">Erlang</option>
                                <option value="forth">Forth</option>
                                <option value="ftl">FreeMarker</option>
                                <option value="gcode">Gcode</option>
                                <option value="gherkin">Gherkin</option>
                                <option value="gitignore">Gitignore</option>
                                <option value="glsl">Glsl</option>
                                <option value="golang">Go</option>
                                <option value="groovy">Groovy</option>
                                <option value="haml">HAML</option>
                                <option value="handlebars">Handlebars</option>
                                <option value="haskell">Haskell</option>
                                <option value="haxe">haXe</option>
                                <option value="html">HTML</option>
                                <option value="html_ruby">HTML (Ruby)</option>
                                <option value="ini">INI</option>
                                <option value="io">Io</option>
                                <option value="jack">Jack</option>
                                <option value="jade">Jade</option>
                                <option value="java">Java</option>
                                <option value="javascript">JavaScript</option>
                                <option value="json">JSON</option>
                                <option value="jsoniq">JSONiq</option>
                                <option value="jsp">JSP</option>
                                <option value="jsx">JSX</option>
                                <option value="julia">Julia</option>
                                <option value="latex">LaTeX</option>
                                <option value="less">LESS</option>
                                <option value="liquid">Liquid</option>
                                <option value="lisp">Lisp</option>
                                <option value="livescript">LiveScript</option>
                                <option value="logiql">LogiQL</option>
                                <option value="lsl">LSL</option>
                                <option value="lua">Lua</option>
                                <option value="luapage">LuaPage</option>
                                <option value="lucene">Lucene</option>
                                <option value="makefile">Makefile</option>
                                <option value="markdown">Markdown</option>
                                <option value="mask">Mask</option>
                                <option value="matlab">MATLAB</option>
                                <option value="mel">MEL</option>
                                <option value="mushcode">MUSHCode</option>
                                <option value="mysql">MySQL</option>
                                <option value="nix">Nix</option>
                                <option value="objectivec">Objective-C</option>
                                <option value="ocaml">OCaml</option>
                                <option value="pascal">Pascal</option>
                                <option value="perl">Perl</option>
                                <option value="pgsql">pgSQL</option>
                                <option value="php" selected>PHP</option>
                                <option value="powershell">Powershell</option>
                                <option value="praat">Praat</option>
                                <option value="prolog">Prolog</option>
                                <option value="properties">Properties</option>
                                <option value="protobuf">Protobuf</option>
                                <option value="python">Python</option>
                                <option value="r">R</option>
                                <option value="rdoc">RDoc</option>
                                <option value="rhtml">RHTML</option>
                                <option value="ruby">Ruby</option>
                                <option value="rust">Rust</option>
                                <option value="sass">SASS</option>
                                <option value="scad">SCAD</option>
                                <option value="scala">Scala</option>
                                <option value="scheme">Scheme</option>
                                <option value="scss">SCSS</option>
                                <option value="sh">SH</option>
                                <option value="sjs">SJS</option>
                                <option value="smarty">Smarty</option>
                                <option value="snippets">snippets</option>
                                <option value="soy_template">Soy Template</option>
                                <option value="space">Space</option>
                                <option value="sql">SQL</option>
                                <option value="stylus">Stylus</option>
                                <option value="svg">SVG</option>
                                <option value="tcl">Tcl</option>
                                <option value="tex">Tex</option>
                                <option value="text">Text</option>
                                <option value="textile">Textile</option>
                                <option value="toml">Toml</option>
                                <option value="twig">Twig</option>
                                <option value="typescript">Typescript</option>
                                <option value="vala">Vala</option>
                                <option value="vbscript">VBScript</option>
                                <option value="velocity">Velocity</option>
                                <option value="verilog">Verilog</option>
                                <option value="vhdl">VHDL</option>
                                <option value="xml">XML</option>
                                <option value="xquery">XQuery</option>
                                <option value="yaml">YAML</option>
                            </select>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <a class="modal-action modal-close waves-effect waves-green btn-flat" ng-click="cancel()">Close</a>
        </div>
    </div>


@elseif ($modal == "resetquota")

    <div id="resetQuotaModal" class="modal">
        <div class="modal-content">
            <h4>Reset Website Quota</h4>
            <p ng-show="universalMessage == null">Resetting this Domain Owner website quota will enable back the owner WHoPlet. This will reset back the website quota to zero usage. Reset the website quota?</p>
            <p ng-show="universalMessage != null">@{{ universalMessage }}</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="modal-action waves-effect waves-red btn-flat" ng-click="resetQuotaCommit()" ng-disabled="isBusy" ng-hide="universalMessage != null">Reset</button>
            <a class="modal-action modal-close waves-effect waves-green btn-flat" ng-click="cancel()">Close</a>
        </div>
    </div>


@elseif ($modal == "resetaccount")

    <div id="resetAccountModal" class="modal">
        <div class="modal-content">
            <h4>Reset Account</h4>
            <p ng-show="universalMessage == null">Reset this Domain Owner account and extend the account expiry?</p>
            <p ng-show="universalMessage != null">@{{ universalMessage }}</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="modal-action waves-effect waves-red btn-flat" ng-click="resetAccountCommit()" ng-disabled="isBusy" ng-hide="universalMessage != null">Reset</button>
            <a class="modal-action modal-close waves-effect waves-green btn-flat" ng-click="cancel()">Close</a>
        </div>
    </div>


@elseif ($modal == "deleteaccount")

    <div id="deleteAccountModal" class="modal">
        <div class="modal-content">
            <h4>Delete Owner Account</h4>
            <p ng-show="universalMessage == null">Deleting this Domain Owner account will remove all the files, database and everything that this Domain Owner own. Delete this owner account?</p>

            <div class="row" ng-show="universalMessage == null">
                <div class="input-field col s12">
                    <input placeholder="" ng-model="deleteUsername" type="text">
                    <label>Insert username you want to delete</label>
                </div>
            </div>
            
            <p ng-show="universalMessage != null">@{{ universalMessage }}</p>
        </div>
        <div class="modal-footer">
            <button type="button" class="modal-action waves-effect waves-red btn-flat" ng-click="deleteAccountCommit()" ng-disabled="isBusy" ng-hide="universalMessage != null">Delete</button>
            <a class="modal-action modal-close waves-effect waves-green btn-flat" ng-click="cancel()">Close</a>
        </div>
    </div>

@endif