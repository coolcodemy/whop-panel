<div class="container" style="margin-top:10%">
	<div class="row">
		<div class="col m6 offset-m3 s12">
			<div class="card">
				<div class="row card-content">
					@yield('content')
				</div>
			</div>
		</div>
	</div>
</div>