@extends('materializecss.layout.master', ['title' => sprintf('%s Account', $user->name)])



@section('content')
    <div class="row" ng-controller="ViewOwnerController">
        <div class="col s12">

            <ul class="collection with-header">
                <li class="collection-header">
                    <h4>Domain Owner Account</h4></li>
                <li class="collection-item">Name <span class="secondary-content black-text">{{ $user->name }}</span></li>
                <li class="collection-item">Username <span class="secondary-content black-text">{{ $user->username }}</span></li>
                <li class="collection-item">Package <span class="secondary-content black-text">{{ $user->userpackage->package->name }}</span></li>
                <li class="collection-item">Account Activation Date <span class="secondary-content black-text">{{ date('d F Y', strtotime($user->userpackage->activeDate)) }}</span></li>
                <li class="collection-item">Account Expiry Date <span class="secondary-content black-text">{{ date('d F Y', strtotime($user->userpackage->endDate)) }}</span></li>
                <li class="collection-item">Register Date <span class="secondary-content black-text">{{ date('d F Y', strtotime($user->created_at)) }} ({{ $user->created_at->diffForHumans() }})</span></li>
                <li class="collection-item">Last Login <span class="secondary-content black-text">{{ $lastLogin }} {{ $lastLoginHuman }}</span></li>
            </ul>

        </div>

        @if ( Auth::user()->hasRole('admin') )

            <div class="col s12">

                <h4>Owner Setting</h4>


                <div class="card hoverable">

                    <div class="card-content">

                        <h5>Profile</h5>

                        <a href="{{ route('admin::ownerprofile', $user) }}" class="btn light-green tooltipped" data-position="top" data-delay="50" data-tooltip="Change Domain Owner name and password">Edit Profile</a>


                        <h5>Settings</h5>

                        <a href="{{ route('admin::ownerphpsetting', $user) }}" class="btn blue tooltipped" data-position="top" data-delay="50" data-tooltip="PHP CLI setting for owner to use with Supervisord, Cron and SSH account">PHP Setting</a>

                        <a href="{{ route('admin::ownerpackagesetting', $user) }}" class="btn blue tooltipped" data-position="top" data-delay="50" data-tooltip="Overwrite {{ $user->username }} package setting">Overwrite Package Setting</a>



                        <h5>Reset</h5>

                        <button class="btn orange" ng-disabled="isBusy" ng-click="resetQuota()">Reset Website Quota</button>

                        @include('materializecss.partial._modal', ['modal' => 'resetquota'])

                        <button data-url="{{ route('admin::ownerresetaccount', $user) }}" id="btnResetAccount" ng-disabled="isBusy" ng-click="resetAccount()" href="" class="btn orange">Reset Expiration</button>

                        @include('materializecss.partial._modal', ['modal' => 'resetaccount'])


                        

                        <div ng-controller="OwnerSystemController" id="getOwnerStatus" data-url="{{ route('admin::ownerstatus', $user) }}">

                            <h5>Enable &amp; Disable</h5>

                            <p ng-show="disable == true">Clicking this will automatically enable this owner account. Enabling including SSH login (depends on package setting), enable Panel login, enable email, ftp and WHoPlet.</p>

                            <p ng-show="disable == false">Clicking this will automatically disable this owner account. Disabling including disable SSH login, disable Panel login, disable email, ftp and WHoPlet</p><br>

                            <button ng-click="enableAccount('{{ route('admin::ownerenableaccount', $user) }}')" class="btn blue darken-3" ng-show="disable == true" ng-disabled="isBusy">Enable Account</button>

                            <button ng-click="disableAccount('{{ route('admin::ownerdisableaccount', $user) }}')" class="btn orange darken-3" ng-show="disable == false" ng-disabled="isBusy">Disable Account</button>




                            <h5>Delete</h5>

                            @if ( $user->id == 2)

                                <button class="btn red" disabled><span class="fa fa-exclamation-triangle"></span> Delete</button>

                            @else

                                <button class="btn red" ng-click="deleteAccount()" id="btnDeleteAccount" data-url="{{ route('admin::ownerdelete', $user) }}"><span class="fa fa-exclamation-triangle"></span> Delete</button>

                                @include('materializecss.partial._modal', ['modal' => 'deleteaccount'])

                            @endif

                        </div>

                        

                    </div>

                </div>

            </div>

        @endif

        <div class="col s12">

            <h4>Application</h4>
            <div class="card hoverable">
                <div class="card-content">
                    <h5>WHoPlet</h5>

                    <div class="row">
                        <div class="col s12">
                            WHoPlet Usage: @{{ progressWhopletValue }} / @{{ progressWhopletMax == 0 ? 'Unlimited' : progressWhopletMax }}
                            <div class="progress grey lighten-3">
                                <div class="determinate" ng-style="{'width': progressWhopletMax == 0 ? '100%' : progressWhopletValue/progressWhopletMax*100 + '%'}" ng-class="progressWhopletColor"></div>
                            </div>
                        </div>

                        <div class="col s12">
                            Website Quota Usage: @{{ progressWebsiteQuotaValue }} MB / @{{ progressWebsiteQuotaMax == 0 ? 'Unlimited' : progressWebsiteQuotaMax }} MB
                            <div class="progress grey lighten-3">
                                <div class="determinate" ng-style="{'width': progressWebsiteQuotaMax == 0 ? '100%' : progressWebsiteQuotaValue/progressWebsiteQuotaMax*100 + '%'}" ng-class="progressWebsiteQuotaColor"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-action">
                    <a href="{{ $buttonURL['whoplet-manager'] }}" class="btn light-green white-text waves-effect waves-light"><span class="fa fa-cloud"></span> WHoPlet</a>
                    <a href="{{ $buttonURL['whoplet-template-manager'] }}" class="btn light-green white-text waves-effect waves-light"><span class="fa fa-code-fork"></span> WHoPlet Template</a>
                </div>
            </div>

            <div class="card hoverable">
                <div class="card-content">
                    <h5>MariaDB&trade;</h5>

                    <div class="row">
                        <div class="col s12">
                            Database Usage: {{ $progress['db']['value'] }}/{{ $progress['db']['max'] }}
                            <div class="progress grey lighten-3">
                                <div class="determinate {{ $progress['db']['color'] }}" style="width: {{ $progress['db']['percentage'] }}%"></div>
                            </div>
                        </div>

                        <div class="col s12">
                            Database User Usage: {{ $progress['dbUsers']['value'] }}/{{ $progress['dbUsers']['max'] }}
                            <div class="progress grey lighten-3">
                                <div class="determinate {{ $progress['dbUsers']['color'] }}" style="width: {{ $progress['dbUsers']['percentage'] }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-action">
                    <a  href="{{ $buttonURL['mariadb'] }}" class="btn light-green white-text waves-effect waves-light"><span class="fa fa-database"></span> MariaDB&trade;</a>
                    <a href="/phpMyAdmin" target="_blank" class="btn light-green white-text waves-effect waves-light white-text"><span class="fa fa-table"></span> phpMyAdmin</a>
                </div>
            </div>

            <div class="card hoverable">
                <div class="card-content">
                    <h5>Domain Name</h5>

                    <div class="row">
                        <div class="col s12">
                            Domain Name Usage: {{ $progress['domains']['value'] }}/{{ $progress['domains']['max'] }}
                            <div class="progress grey lighten-3">
                                <div class="determinate {{ $progress['domains']['color'] }}" style="width: {{ $progress['domains']['percentage'] }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-action">
                    <a href="{{ $buttonURL['domain'] }}" class="btn light-green white-text waves-effect waves-light"><span class="fa fa-globe"></span> Domain Name</a>
                    <a href="{{ $buttonURL['record'] }}" class="btn light-green white-text waves-effect waves-light"><span class="fa fa-edit"></span> Domain Records</a>
                </div>
            </div>

            <div class="card hoverable">
                <div class="card-content">
                    <h5>Email</h5>

                    <div class="row">
                        <div class="col s12">
                            Email Account Usage: {{ $progress['mailUsers']['value'] }}/{{ $progress['mailUsers']['max'] }}
                            <div class="progress grey lighten-3">
                                <div class="determinate {{ $progress['mailUsers']['color'] }}" style="width: {{ $progress['mailUsers']['percentage'] }}%"></div>
                            </div>
                        </div>
                        <div class="col s12">
                            Email Quota Usage: {{ $progress['mailQuota']['value'] }} MB/{{ $progress['mailQuota']['max'] }}
                            <div class="progress grey lighten-3">
                                <div class="determinate {{ $progress['mailQuota']['color'] }}" style="width: {{ $progress['mailQuota']['percentage'] }}%"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-action">
                    <a href="{{ $buttonURL['email'] }}" class="btn light-green white-text waves-effect waves-light"><span class="fa fa-envelope"></span> Email Account</a>
                    <a href="{{ $buttonURL['email-forwarding'] }}" class="btn light-green white-text waves-effect waves-light"><span class="fa fa-chevron-right"></span> Email Forwarding</a>
                    <a href="/webmail" target="_blank" class="btn light-green white-text waves-effect waves-light"><span class="fa fa-envelope-o"></span> RoundCube Webmail</a>
                </div>
            </div>

            <div class="card hoverable">
                <div class="card-content">
                    <h5>File</h5>

                    <div class="row">

                        <div class="col s12">
                            Disk Quota Usage: @{{ progressDiskQuotaValue }} MB / @{{ progressDiskQuotaMax == 0 ? 'Unlimited' : progressDiskQuotaMax }} MB
                            <div class="progress grey lighten-3">
                                <div class="determinate" ng-style="{'width': progressDiskQuotaMax == 0 ? '100%' : progressDiskQuotaValue/progressDiskQuotaMax*100 + '%'}" ng-class="progressDiskQuotaColor"></div>
                            </div>
                        </div>

                        <div class="col s12">
                            FTP Account Usage: {{ $progress['ftpUsers']['value'] }}/{{ $progress['ftpUsers']['max'] }}
                            <div class="progress grey lighten-3">
                                <div class="determinate {{ $progress['ftpUsers']['color'] }}" style="width: {{ $progress['ftpUsers']['percentage'] }}%"></div>
                            </div>
                        </div>

                        <div class="col s12">
                            FTP Quota Usage: {{ $progress['ftpQuota']['value'] }} MB/{{ $progress['ftpQuota']['max'] }}
                            <div class="progress grey lighten-3">
                                <div class="determinate {{ $progress['ftpQuota']['color'] }}" style="width: {{ $progress['ftpQuota']['percentage'] }}%"></div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="card-action">
                    <a href="{{ $buttonURL['filemanager'] }}" class="btn light-green white-text waves-effect waves-light"><span class="fa fa-folder-open"></span> WHoP&trade; File Manager</a>
                    <a href="{{ $buttonURL['ftp'] }}" class="btn light-green white-text waves-effect waves-light"><span class="fa fa-exchange"></span> FTP Account</a>
                </div>
            </div>


            <div class="card hoverable">
                <div class="card-content">
                    <h5>SSL/TLS</h5>
                </div>
                <div class="card-action">
                    <a href="{{ $buttonURL['ssl'] }}" class="btn light-green white-text waves-effect waves-light"><span class="fa fa-lock"></span> SSL/TLS Manager</a>
                </div>
            </div>


            <div class="card hoverable">
                <div class="card-content">
                    <h5>Other</h5>
                </div>
                <div class="card-action">
                    <a href="{{ $buttonURL['cron'] }}" class="btn light-green white-text waves-effect waves-light"><span class="fa fa-clock-o"></span> Cron Manager</a>
                    <a href="{{ $buttonURL['supervisor'] }}" class="btn light-green white-text waves-effect waves-light"><span class="fa fa-user-secret"></span>Supervisor</a>
                </div>
            </div>

        </div>
    </div>
@stop


@section('meta')
    <meta name="user" content="{{ $user->username }}">
    <meta name="bandwidth" content="{{ $package->websiteQuota }}">
    <meta name="whoplet" content="{{ $package->numberOfWhoplet }}">
@stop