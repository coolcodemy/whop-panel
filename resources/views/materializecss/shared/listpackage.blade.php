@extends('materializecss.layout.master', ['title' => 'Package List'])


@section('content')
<div class="row">
    @foreach($packages as $package)
        <div class="col s12 m6">
            <ul class="collection">
                <li class="collection-item avatar">
                    <i class="fa fa-cube circle light-green darken-2"></i>
                    <span class="collection-header">{{ $package->name }}</span>
                    <p>Total user/s using this package: {{ count($package->userpackage) }}</p>
                </li>
                <li class="collection-item">
                    <div>Validity <span class="badge">{{ $package->validFor }}</span></div>
                </li>
                <li class="collection-item">
                    <div>APCu Support <span class="badge">{{ ($package->apcuSupport == 0 ? 'Disable' : 'Enable') }}</span></div>
                </li>
                <li class="collection-item">
                    <div>WHoPlet SSL/TLS Support <span class="badge">{{ ($package->enableSsl == 0 ? 'Disable' : 'Enable') }}</span></div>
                </li>
                <li class="collection-item">
                    <div>SSH <span class="badge">{{ ($package->ssh == 0 ? 'Disable' : 'Enable') }}</span></div>
                </li>
                <li class="collection-item">
                    <div>Disk Quota {!! ($package->diskQuota == 0 ? '<span class="badge green white-text">Unlimited</span>' : '<span class="badge">' . $package->diskQuota . ' MB</span>') !!}</div>
                </li>
                <li class="collection-item">
                    <div>Email Quota {!! ($package->emailQuota == 0 ? '<span class="badge green white-text">Unlimited</span>' : '<span class="badge">' . $package->emailQuota . ' MB</span>') !!}</div>
                </li>
                <li class="collection-item">
                    <div>FTP Quota {!! ($package->ftpQuota == 0 ? '<span class="badge green white-text">Unlimited</span>' : '<span class="badge">' . $package->ftpQuota . ' MB</span>') !!}</div>
                </li>
                <li class="collection-item">
                    <div>Website Quota {!! ($package->websiteQuota == 0 ? '<span class="badge green white-text">Unlimited</span>' : '<span class="badge">' . $package->websiteQuota . ' GB</span>') !!}</div>
                </li>
                <li class="collection-item">
                    <div>Number of Database {!! ($package->numberOfDatabase == 0 ? '<span class="badge green white-text">Unlimited</span>' : '<span class="badge">' . $package->numberOfDatabase . '</span>') !!}</div>
                </li>
                <li class="collection-item">
                    <div>Number of Database User {!! ($package->numberOfDatabaseUser == 0 ? '<span class="badge green white-text">Unlimited</span>' : '<span class="badge">' . $package->numberOfDatabaseUser . '</span>') !!}</div>
                </li>
                <li class="collection-item">
                    <div>Maximum Database Queries Per Hour {!! ($package->maximumQueriesPerHour == 0 ? '<span class="badge green white-text">Unlimited</span>' : '<span class="badge">' . $package->maximumQueriesPerHour . '</span>') !!}</div>
                </li>
                <li class="collection-item">
                    <div>Maximum Database Connections Per Hour {!! ($package->maximumConnectionsPerHour == 0 ? '<span class="badge green white-text">Unlimited</span>' : '<span class="badge">' . $package->maximumConnectionsPerHour . '</span>') !!}</div>
                </li>
                <li class="collection-item">
                    <div>Maximum Database Updates Per Hour {!! ($package->maximumUpdatesPerHour == 0 ? '<span class="badge green white-text">Unlimited</span>' : '<span class="badge">' . $package->maximumUpdatesPerHour . '</span>') !!}</div>
                </li>
                <li class="collection-item">
                    <div>Maximum Database User Connections {!! ($package->maximumUserConnections == 0 ? '<span class="badge green white-text">Unlimited</span>' : '<span class="badge">' . $package->maximumUserConnections . '</span>') !!}</div>
                </li>
                <li class="collection-item">
                    <div>Number of Email Account {!! ($package->numberOfEmailAccount == 0 ? '<span class="badge green white-text">Unlimited</span>' : '<span class="badge">' . $package->numberOfEmailAccount . '</span>') !!}</div>
                </li>
                <li class="collection-item">
                    <div>Number of FTP Account {!! ($package->numberOfFtpAccount == 0 ? '<span class="badge green white-text">Unlimited</span>' : '<span class="badge">' . $package->numberOfFtpAccount . '</span>') !!}</div>
                </li>
                <li class="collection-item">
                    <div>Number of Domain {!! ($package->numberOfDomain == 0 ? '<span class="badge green white-text">Unlimited</span>' : '<span class="badge">' . $package->numberOfDomain . '</span>') !!}</div>
                </li>
                <li class="collection-item">
                    <div>Number of WHoPlet {!! ($package->numberOfWhoplet == 0 ? '<span class="badge green white-text">Unlimited</span>' : '<span class="badge">' . $package->numberOfWhoplet . '</span>') !!}</div>
                </li>
                <li class="collection-item">
                    @if (count($package->userpackage) != 0)
                        <a class="btn grey darken-2  tooltipped" data-position="top" data-delay="50" data-tooltip="Package is being used by user/s. Please remove those user/s to delete this package">Delete</a>
                    @else
                        <a data-delete-url="{{ route($buttonURL['delete'], $package) }}" class="btn red waves-effect waves-light confirmDeletePackage">Delete</a>
                    @endif
                    <a href="{{ route($buttonURL['edit'], $package) }}" class="btn blue pull-right waves-effect waves-light">Edit</a>
                </li>
            </ul>
        </div>
    @endforeach

    <div class="col s12">
        {!! (new WHoP\ThirdParty\Pagination($packages))->render() !!}
    </div>
</div>

@include('materializecss.partial._modal', ['modal' => 'deletepackage'])

<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
    <a href="{{ route($buttonURL['create']) }}" class="btn-floating btn-large red waves-effect waves-light tooltipped modal-trigger" data-position="left" data-tooltip="Add new package"><i class="material-icons">add</i></a>
</div>
@stop