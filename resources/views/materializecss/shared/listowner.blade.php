@extends('materializecss.layout.master', ['title' => 'Owner List']) 

@section('content')
	<div class="row">
	    <div class="col s12">
	        <div class="card">
	            <div class="card-content">
	                <span class="card-title black-text">List of Domain Owner</span>
	                <table class="bordered responsive-table striped hoverable">
	                    <thead>
	                        <tr>
	                            <th>Name</th>
	                            <th>Username</th>
	                            <th>Package</th>
	                            <th>View</th>
	                        </tr>
	                    </thead>
	                    <tbody>
	                        @foreach($users as $user)
	                        <tr>
	                            <td>{{ $user->name }}</td>
	                            <td>{{ $user->username }}</td>
	                            <td>{{ $user->userpackage->package->name }}</td>
	                            <td>
	                                <a href="{{ route('admin::viewowner', $user) }}" class="waves-effect waves-light btn light-green"><i class="fa fa-user left"></i>View</a>
	                            </td>
	                        </tr>
	                        @endforeach
	                    </tbody>
	                </table>
					{!! (new WHoP\ThirdParty\Pagination($users))->render() !!}
					<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
						<a 	href="{{ route($createURL) }}" class="btn-floating btn-large red waves-effect waves-light tooltipped modal-trigger" data-position="left" data-tooltip="Add new Domain Owner account"><i class="material-icons">add</i></a>
					</div>
	            </div>
	        </div>
	    </div>
	</div>
@stop