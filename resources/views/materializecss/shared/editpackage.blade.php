@extends('materializecss.layout.master', ['title' => 'Edit Package'])



@section('content')
<div class="row">
    <div class="col s12">
        <div class="card cyan white-text">
            <div class="card-content">
                <p>Current user who use this package will have no effect on setting changes. You need to modify them manually.</p>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col s12 m12">
        <div class="card">
            <div class="card-content">
                <span class="card-title black-text"><i class="fa fa-cube"></i> Add Package</span> 
                {!! Form::open(['route' => array($urlList['edit'], $package) ]) !!}
                    @include('materializecss.form._formaddpackage', ['submitText' => 'Update'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@stop