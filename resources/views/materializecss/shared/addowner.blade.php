@extends('materializecss.layout.master', ['title' => 'Add Domain Owner'])



@section('content')
      <div class="row" ng-controller="AddOwnerController">
        <div class="col s12 m12">
          <div class="card">
            <div class="card-content">
              <span class="card-title black-text"><i class="fa fa-user"></i> Add Domain Owner</span>
              {!! Form::open(['route' => $storeURL, 'id' => 'formAddOwner']) !!}
                @include('materializecss.form._formaddowner')
              {!! Form::close() !!}
            </div>

            @include('materializecss.partial._modal', ['modal' => 'addownermodal'])
          </div>
        </div>
      </div>
@stop


@section('meta')
  <meta name="add-owner-channel" content="{{ $channel }}">
@stop