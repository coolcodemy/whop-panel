@extends('materializecss.layout.master', ['title' => 'Add Package'])


@section('content')
<div class="row">
    <div class="col s12 m12">
        <div class="card">
            <div class="card-content">
                <span class="card-title black-text"><i class="fa fa-cube"></i> Add Package</span> 
                {!! Form::open([ 'route' => $urlList['store'] ]) !!}
                    @include('materializecss.form._formaddpackage', ['submitText' => 'Submit'])
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@stop