@extends('materializecss.layout.master', ['title' => 'Overwrite ' . $user->username . ' package setting'])


@section('content')

	<div class="row" ng-controller="OwnerPackageController">

	    <div class="col s12 m12">
	        <div class="card">
	            <div class="card-content">
	                <span class="card-title black-text"><i class="fa fa-cube"></i> Modify package for {{ $user->username }}</span> 

	                {!! Form::open() !!}

	                	<div class="row">

		                    @include('materializecss.form._formpackagepartial', ['submitText' => 'Update'])


							<div class="input-field col s12">
								<a href="{{ URL::previous() }}" class="btn red waves-effect waves-light">Cancel</a>
								<button type="submit" class="btn waves-effect waves-light right">Save</button>
							</div>
							
						</div>

	                {!! Form::close() !!}

	            </div>
	        </div>
	    </div>

	</div>


@stop