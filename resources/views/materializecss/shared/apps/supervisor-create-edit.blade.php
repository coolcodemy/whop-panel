@extends('materializecss.layout.master', ['title' => 'Add Supervisor Job'])

@section('content')
	<div class="row" ng-controller="SupervisorAddController">
		<div class="col s12">
			<h4>Add New Supervisor Job</h4>

			<p class="red white-text paddingLabel">Supervisor command must start with <strong>php /var/www/{{ $user->username }}/</strong></p>

			<form action="">
				<div class="row">
					<div class="input-field col s12"> 
						<div class="form-group">
							<label class="control-label" for="">Name</label>
							<input type="text" class="form-control" ng-model="jobName" placeholder="Job Name. Only words, numbers and - symbol accepted.">
						</div>
					</div>

					<div class="input-field col s12"> 
						<div class="form-group">
							<label class="control-label" for="">Command</label>
							<input type="text" class="form-control" ng-model="command" placeholder="php /var/www/{{ $user->username }}">
						</div>
					</div>
				</div>
				
				<button type="button" class="btn light-green" ng-disabled="isBusy" ng-click="addJob()">Add</button>
			</form>
		</div>
	</div>
@stop

@section('meta')
	<meta name="username" content="{{ $user->username }}">
@stop