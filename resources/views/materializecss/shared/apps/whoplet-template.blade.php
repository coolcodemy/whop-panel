@extends('materializecss.layout.master', ['title' => 'WHoPlet Template Generator'])


@section('content')
<div ng-controller="WhopletTemplateController">
	<div class="row">
		<div class="col s12 m12">
			<div class="card">
				<div class="card-content">
					<span class="card-title black-text">Craft From Available Template </span>
					<h6>Maximum Template Creation: {{ app('SettingService')->getInt('MaxUserTemplate') }}</h6>
					<table class="striped">
						<thead>
							<tr>
								<th>Template Name</th>
								<th>Craft</th>
								<th>Delete</th>
							</tr>
						</thead>
					
						<tbody>
							@foreach ($templates as $template)
								<tr>
									<td>{{ $template->name }}</td>
									<td><button class="btn light-green waves-effect" ng-click="craft('{{ $template->id }}')" ng-disabled="isBusy">Craft</button></td>
									<td>
										@if ($template->user->id !== $user->id)
											<button class="btn red" disabled><i class="fa fa-trash"></i></button>
										@else
											@if ( Auth::user()->hasRole('admin') )

												<a ng-click="deleteTemplate('{{ route('admin::app::whoplettemplatemanager-delete', [$user,$template]) }}')" class="btn red waves-effect"><i class="fa fa-trash"></i></a>

											@elseif ( Auth::user()->hasRole('owner') )

												<a ng-click="deleteTemplate('{{ route('owner::app::whoplettemplatemanager-delete', [$template]) }}')" class="btn red waves-effect"><i class="fa fa-trash"></i></a>

											@endif
										@endif
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
					{!! $templates->render() !!}<br><br>		
				</div>
			</div>
		</div>
	</div>


	<div class="row" ng-show="canModifySetting">
		<div class="col s12">
			<div class="card">
				<div class="card-content">
					<span class="card-title black-text">Template Header Setting</span>
					<div id="templateSetting2">
						<ul class="collection" id="settingsCollection">
							<li class="collection-item" ng-show="hasSameOrigin">
								Clickjacking Protection<br>
								<small>Tell browser to deny any attempt to embed my page from other website via iframe.</small>
								<span class="secondary-content">
									<div class="switch"><label>Disable<input type="checkbox" ng-model="sameOrigin"><span class="lever"></span>Enable</label></div>
								</span>
							</li>
							<li class="collection-item" ng-show="hasXssProtection">
								XSS Protection<br>
								<small>Enable browser XSS protection for IE8+ and Webkit (eg: Chrome). Only works with non-persistent XSS</small>
								<span class="secondary-content">
									<div class="switch"><label>Disable<input type="checkbox" ng-model="xssProtection"><span class="lever"></span>Enable</label></div>
								</span>
							</li>
							<li class="collection-item" ng-show="hasNoSniff">
								Prevent MIME-sniffing<br>
								<small>Prevent IE and Webkit (eg: Chrome) brwoser from MIME-Sniffing a response away from the declared content-type.</small>
								<span class="secondary-content">
									<div class="switch"><label>Disable<input type="checkbox" ng-model="noSniff"><span class="lever"></span>Enable</label></div>
								</span>
							</li>
							<li class="collection-item" ng-show="hasSts">
								Encrypted Communication Only<br>
								<small>Tell browser to communicate via secure HTTPS connection only.</small>
								<span class="secondary-content">
									<div class="switch"><label>Disable<input type="checkbox" ng-model="sts"><span class="lever"></span>Enable</label></div>
								</span>
							</li>
							<li class="collection-item" ng-show="hasCache">
								Fast-CGI Cache<br>
								<small>Caching will make your website faster. This will cache every php request that you make.<br>
								If you set the caching time higher, every database result, php calculation result will only<br>
								change after the cache expired. If you don't know what is this, just disable it.</small>
								<span class="secondary-content">
									<div class="switch"><label>Disable<input type="checkbox" ng-model="cache"><span class="lever"></span>Enable</label></div>
								</span>
							</li>
							<li class="collection-item" ng-show="cache">
								Caching Time<br>
								<small>Time to cache the php request. Minimum is 10s (10 seconds) and maximum is 10m (10 minutes).<br>
								Unit is 's' and 'm' only.</small>
								<span class="secondary-content" style="margin-top:-20px">
									<label for="location" class="active">Caching Time</label>
									<input placeholder="" ng-model="cacheTime" type="text">
								</span>
							</li>
						</ul>
					</div>			
				</div>
			</div>
		</div>
	</div>


	<div class="row" ng-show="canAddLocation">
		<div class="col s12">
			<h5>Template Location Setting</h5>
			<div class="top15px bottom15px"><button class="btn waves-effect light-green" template-add-location>Add Location Block</button></div>
			<ul class="collapsible" data-collapsible="expendable" id="locationSettingUL">
				
			</ul>
		</div>
	</div>


	<div class="row" ng-show="canPreview">
		<div class="col s12">
			<div class="card">
				<div class="card-content">
					<span class="card-title black-text">Template Preview</span>
					<div class="top15px bottom15px"><button class="btn waves-effect light-green" ng-click="preview()" ng-disabled="isBusy">Preview Template</button></div>
					<div id="previewPlaceholder"></div>			
				</div>
			</div>
		</div>
	</div>


	<div class="row" ng-show="canSaveTemplate">
		<div class="col s12">
			<div class="card">
				<div class="card-content">
					<span class="card-title black-text">Save WHoPlet Template - {{ $user->username }}_@{{ templateName }}</span>
					<div class="input-field saveTemplate"> 
						<div class="form-group">
							<label class="control-label left0" for="">WHoPlet Template Name</label>
							<input type="text" class="form-control" ng-model="templateName" placeholder="Name without space">
						</div>
					</div>
				</div>
				<div class="card-action saveTemplate">
					<a ng-disabled="isBusy" ng-click="saveTemplate()">Save</a>
				</div>
			</div>
		</div>
	</div>
</div>
	@include('materializecss.partial._modal', ['modal' => 'deletetemplate'])
@stop


@section('meta')
	<meta name="username" content="{{ $user->username }}">
	<meta name="get-template-url" content="{{ $urlList['get-template-url'] }}">
	<meta name="checkportion-url" content="{{ $urlList['checkportion-url'] }}">
	<meta name="preview-url" content="{{ $urlList['preview-url'] }}">
	<meta name="save-url" content="{{ $urlList['save-url'] }}">
@stop