@extends('materializecss.layout.master', ['title' => 'FTP Accounts'])


@section('content')

<div class="row" ng-controller="FtpController">
	<div class="col s12">
		<h4>FTP Accounts</h4>

		<table>
			<thead>
				<tr>
					<th>Username</th>
					<th>Home Directory</th>
					<th>Status</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
		
			<tbody>
				@foreach ($ftpAccounts as $account)
					<tr>
						<td>{{ $account->username }}</td>
						<td>{{ $account->homedir }}</td>
						<td>
							@if ($account->disable == 0)
								<a href="{{ $account->enableDisableURL }}" class="btn light-green waves-effect tooltipped" data-position="top" data-delay="50" data-tooltip="Click to disable account">Enable</a>
							@else
								<a href="{{ $account->enableDisableURL }}" class="btn red waves-effect tooltipped" data-position="top" data-delay="50" data-tooltip="Click to enable account">Disable</a>
							@endif
						</td>
						<td><a href="{{ $account->editURL }}" class="btn blue"><i class="fa fa-pencil"></i></a></td>
						<td><button class="btn red" ng-click="delete('{{ $account->deleteURL }}')"><i class="fa fa-trash"></i></button></td>
					</tr>
				@endforeach
			</tbody>
		</table>

		{!! (new WHoP\ThirdParty\Pagination($ftpAccounts))->render() !!}

		@include('materializecss.partial._modal', ['modal' => 'deleteftpaccount'])
	</div>
</div>



<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<a 	href="{{ $urlList['ftp-create'] }}" class="btn-floating btn-large red waves-effect waves-light tooltipped modal-trigger" data-position="left" data-tooltip="Add new FTP Account"><i class="material-icons">add</i></a>
</div>

@stop
