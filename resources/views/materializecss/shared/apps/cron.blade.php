@extends('materializecss.layout.master', ['title' => 'Cron Manager'])

@section('content')
	<div class="row" ng-controller="CronController">
		<div class="col s12">
			<h4>Cron Job List</h4>

			<table class="bordered">
				<thead>
					<tr>
						<th>Minute</th>
						<th>Hour</th>
						<th>Day</th>
						<th>Month</th>
						<th>Weekday</th>
						<th>Command</th>
						<th>Delete</th>
					</tr>
				</thead>
			
				<tbody>
					<tr ng-repeat="job in jobs" ng-show="cronJobCount > 0">
						<td ng-bind="job | split:' ':0"></td>
						<td ng-bind="job | split:' ':1"></td>
						<td ng-bind="job | split:' ':2"></td>
						<td ng-bind="job | split:' ':3"></td>
						<td ng-bind="job | split:' ':4"></td>
						<td ng-bind="job | croncommand"></td>
						<td><button class="btn red" ng-click="deleteCron(job)"><i class="fa fa-trash"></i></button></td>
					</tr>
					<tr ng-show="cronJobCount == 0">
						<td colspan="7">No cron job available</td>
					</tr>
				</tbody>
			</table>

			@include('materializecss.partial._modal', ['modal' => 'deletecronjob'])
		</div>
	</div>

	<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
		<a 	href="{{ $urlList['cron-add'] }}" class="btn-floating btn-large red waves-effect waves-light tooltipped modal-trigger" data-position="left" data-tooltip="Add new Cron Job"><i class="material-icons">add</i></a>
	</div>
@stop


@section('meta')
	<meta name="username" content="{{ $user->username }}">
@stop