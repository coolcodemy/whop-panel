@extends('materializecss.layout.master', ['title' => 'WHoPlet Manager'])


@section('content')
<div class="row" ng-controller="WhopletController">

	<div class="col s12">
		<h4>Server IP Address</h4>

		<table class="bordered striped highlight">
			<thead>
				<tr>
					<th>Type</th>
					<th>Address</th>
				</tr>
			</thead>
		
			<tbody>
				<tr ng-repeat="ip in ipv4">
					<td>IPv4</td>
					<td ng-bind="ip"></td>
				</tr>
				<tr ng-repeat="ip in ipv6">
					<td>IPv6</td>
					<td ng-bind="ip"></td>
				</tr>
			</tbody>
		</table>
		
	</div>

	
	<div class="col s12">
		<h4>WHoPlet Generator</h4>

		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-content">
						<span class="card-title black-text">Pick a Template to start with</span>

						@if ($ssl == 0)
							<p class="red lighten-2 white-text" style="padding:5px">HTTPS WHoPlet have been disabled by server administrator. You can only create HTTP WHoPlet.</p>
						@endif
						
						<form action="">
							<div class="row">
								<div class="input-field col s12"> 
									<div class="form-group">
										<label class="control-label active" for="">Pick WHoPlet template</label><br>
										<select class="material-select" ng-model="choosenTemplate" ng-change="selectTemplate()">
											<option value="" disabled selected>Please select template</option>
											@foreach ($templates as $template)
												<option value="{{ $template->id }}">{{ $template->name }}</option>
											@endforeach
										</select>
									</div>
								</div>
							</div>
						</form>


						<form style="magrin-top: 50px" ng-show="choosenTemplate != null">
							<div class="row" ng-show="recordsForm != false">
								<div class="input-field col s12">
									<div class="form-group">
										<label class="control-label active" for="">Domain Name</label><br>
										<select class="browser-default" ng-model="choosenDomain">
											<option value="" selected hidden></option>
											<option value="@{{ record.name }}" ng-repeat="record in recordsForm">@{{ record.name }} (@{{ record.content }})</option>
										</select>
									</div>
								</div>
							</div>

							<div class="row" ng-show="rootForm != false">
								<div class="input-field col s9">
									<div class="form-group">
										<label class="control-label active" for="">Root Folder</label>
										<input value="/var/www/{{ $user->username }}/web/public@{{ selected }}" id="homeDir" type="text" placeholder="" disabled>
									</div>
								</div>

								<div class="input-field col s3">
									<div class="form-group">
										<a ng-click="pickHomeDir()" class="btn modal-trigger blue waves-effect" style="width:100% !important">Browse</a>
									</div>
								</div>
							</div>

							<div class="row" ng-show="sslForm != false">
								<div class="input-field col s9">
									<div class="form-group">
										<label class="control-label active" for="">TLS/SSL Certificate</label>
										<input value="" ng-model="ssl" type="text" placeholder="" value="@{{ ssl }}" disabled>
									</div>
								</div>

								<div class="input-field col s3">
									<div class="form-group">
										<a ng-click="pickSSL()" class="btn modal-trigger blue waves-effect" style="width:100% !important">Browse</a>
									</div>
								</div>
							</div>

							<div class="row" ng-show="recordsForm != false">
								<div class="input-field col s12">
									<button ng-disabled="isBusy" type="submit" class="btn light-green waves-effect" ng-click="buildWhoplet()">Submit</button>
								</div>
							</div>
						</form>

						@include('materializecss.partial._modal', ['modal' => 'chooseHomeDir'])
						@include('materializecss.partial._modal', ['modal' => 'universalcertificate'])
						@include('materializecss.partial._modal', ['modal' => 'deletewhoplet'])
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="col s12">
		<h4>WHoPlet List</h4>

		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-content">
						<table class="bordered">
							<thead>
								<tr>
									<th>WHoPlet Name</th>
									<th>Directory</th>
									<th>Logs</th>
									<th>Delete</th>
								</tr>
							</thead>
						
							<tbody>
								<tr ng-repeat="whoplet in whoplets" ng-hide="whoplets.length == 0">
									<td>
										<a href="@{{ (whoplet.whoplet | indexof:'|https|') ? 'https://' : 'http://' }}@{{ whoplet.whoplet | split:'|':0 }}" target="_blank">@{{ whoplet.whoplet | split:'|':0 }}</a> <span class="fa fa-lock" ng-show="@{{ whoplet.whoplet | indexof:'|https|' }}"></span>
									</td>
									<td>@{{ whoplet.root }}</td>
									<td>
										<a href="@{{ streamURL }}?type=access&whoplet=@{{ whoplet.whoplet | split:'|':0 }}" class="btn blue waves-effect" target="_blank">Access Log</a> <a href="@{{ streamURL }}?type=error&whoplet=@{{ whoplet.whoplet | split:'|':0 }}" class="btn blue waves-effect errorLog" target="_blank">Error Log</a>
									</td>
									<td><button class="btn red waves-effect" ng-click="delete((whoplet.whoplet | whopletname))"><i class="fa fa-trash"></i></button></td>
								</tr>
								<tr ng-show="whoplets.length == 0">
									<td colspan="4">You do not have any WHoPlet. Start build one!</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@stop


@section('meta')
	<meta name="getTemplateForm" content="{{ $urlList['getTemplateForm'] }}">
	<meta name="buildWhoplet" content="{{ $urlList['buildWhoplet'] }}">
	<meta name="username" content="{{ $user->username }}">
	<meta name="channel" content="{{ $channel }}">
	<meta name="streamurl" content="{{ $urlList['streamurl'] }}">
@stop