@extends('materializecss.layout.master', ['title' => 'WHoP File Editor'])

@section('content')
	<div class="row" ng-controller="FileEditorController">
		<div class="col s12">
			<h4>WHoP File Editor - Editing {{ $file }}</h4>

			<div class="row">
				<div class="col s12">
					<div class="card white">
						<div class="card-content black-text editor" id="editor">
						</div>
						<div class="card-action">
							<a class="waves-effect waves-blue" ng-hide="isBusy" ng-click="changeModeModal()">Change Editor Mode</a>
							<a class="waves-effect waves-green" ng-hide="isBusy" ng-click="save()">Save</a>
						</div>
					</div>
				</div>
			</div>

			@include('materializecss.partial._modal', ['modal' => 'editormode'])
		</div>
	</div>
@stop


@section('meta')
	<meta name="username" content="{{ $user->username }}">
	<meta name="startfolder" content="{{ $location }}">
	<meta name="file" content="{{ $file }}">
@stop