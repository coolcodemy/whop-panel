@extends('materializecss.layout.master', ['title' => 'Email Forwarding'])


@section('content')

<div class="row" ng-controller="EmailForwardingController">
	<div class="col s12">
		<h4>Email Forwarding</h4>

		<p class="light-green white-text" style="padding:5px 10px 5px 10px">Forwarding will delete email sent to real destination. To forward email and send it as copy, add same email to <strong>Forward From</strong> and <strong>Destination To</strong> when adding new email forwarding.</p>
		<table>
			<thead>
				<tr>
					<th>Forward From</th>
					<th>Destination To</th>
					<th>Status</th>
					<th>Delete</th>
				</tr>
			</thead>
		
			<tbody>
				@foreach ($forwardings as $forwarding)
					<tr>
						<td>{{ $forwarding->source }}</td>
						<td>{{ $forwarding->destination }}</td>
						<td>
							@if ($forwarding->disable == 0)
								<a href="{{ $forwarding->enableDisableURL }}" class="btn light-green waves-effect tooltipped" data-position="top" data-delay="50" data-tooltip="Click to disable forwarding">Enable</a>
							@else
								<a href="{{ $forwarding->enableDisableURL }}" class="btn red waves-effect tooltipped" data-position="top" data-delay="50" data-tooltip="Click to enable forwarding">Disable</a>
							@endif
						</td>
						<td><button class="btn red" ng-click="delete('{{ $forwarding->deleteURL }}')"><i class="fa fa-trash"></i></button></td>
					</tr>
				@endforeach
			</tbody>
		</table>

		@include('materializecss.partial._modal', ['modal' => 'deleteforwarding'])

		{!! (new WHoP\ThirdParty\Pagination($forwardings))->render() !!}
	</div>
</div>

<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<a 	href="{{ $buttonURL['create'] }}" class="btn-floating btn-large red waves-effect waves-light tooltipped modal-trigger" data-position="left" data-tooltip="Add new Email Forwarding"><i class="material-icons">add</i></a>
</div>

@stop