@extends('materializecss.layout.master', ['title' => 'Create Email Forwarding'])


@section('content')

<div class="row">
	<div class="col s12">
		<h3>Add Forwarding</h3>


		{!! Form::open(['url' => $urlList['submit']]) !!}

			<div class="row">
				<div class="input-field col s8">
					<div class="form-group">
						<label class="control-label">Forward From</label>
						<input type="text" class="form-control" name="forwardFrom" value="{{ old('forwardFrom') }}" placeholder="Any email account without '@' part">
					</div>
				</div>

				<div class="input-field col s4">
					<div class="form-group">
						<label class="control-label active">Email Domain</label>
						<select name="emailDomain" class="material-select">
							@foreach ($mailDomains as $domain)
								@if (old('emailDomain') == $domain->id)
									<option value="{{ $domain->id }}" selected>{{ '@' . $domain->name }}</option>
								@else
									<option value="{{ $domain->id }}">{{ '@' . $domain->name }}</option>
								@endif
							@endforeach
						</select>
					</div>
				</div>


				<div class="input-field col s12">
					<div class="form-group">
						<label class="control-label">Destination To</label>
						<input type="email" class="form-control" name="destinationTo" value="{{ old('destinationTo') }}" placeholder="Any email account">
					</div>
				</div>
			</div>
			
			<button type="submit" class="btn light-green">Submit</button>
		{!! Form::close() !!}
	</div>
</div>

@stop