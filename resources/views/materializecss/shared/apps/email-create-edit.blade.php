@extends('materializecss.layout.master', ['title' => $title])


@section('content')
<div class="row">
	<div class="col s12">
		<h4>{{ $header }}</h4>

		{!! Form::open(['url' => $url]) !!}
			@include('materializecss.form._formemail')
		{!! Form::close() !!}
	</div>
</div>

@stop