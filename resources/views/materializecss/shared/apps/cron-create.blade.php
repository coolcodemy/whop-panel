@extends('materializecss.layout.master', ['title' => 'Add Cron Job'])

@section('content')
	<div class="row" ng-controller="CronController">
		<div class="col s12">
			<h4>Add New Cron Job</h4>

			<form action="#">
				<div class="row">
					<div class="input-field col s12"> 
						<div class="form-group">
							<label class="control-label" for="">Minute</label>
							<input type="text" ng-model="minute" class="form-control" placeholder="">
						</div>
					</div>

					<div class="input-field col s12"> 
						<div class="form-group">
							<label class="control-label active">Predefined Setting</label>
							<select class="material-select" ng-model="selectMinute" ng-change="updateMinute()">
								<option value="" disabled selected>-- Predefined setting --</option>
								<option value="*">Every minute (*)</option>
								<option value="*/2">Every two minutes (*/2)</option>
								<option value="*/5">Every five minutes (*/5)</option>
								<option value="*/10">Every 10 minutes (*/10)</option>
								<option value="*/15">Every 15 minutes (*/15)</option>
								<option value="0,30">Every 30 minutes (0,30)</option>
								<option value="" disabled>-- Custom setting --</option>
								@for ($i=0; $i < 60; $i++)
									<option value="{{$i}}">Every {{$i}} minute/s ({{$i}})</option>
								@endfor
							</select>
						</div>
					</div>
				</div>


				<div class="row">
					<div class="input-field col s12"> 
						<div class="form-group">
							<label class="control-label" for="">Hour</label>
							<input type="text" ng-model="hour" class="form-control" placeholder="">
						</div>
					</div>

					<div class="input-field col s12"> 
						<div class="form-group">
							<label class="control-label active">Predefined Setting</label>
							<select class="material-select" ng-model="selectHour" ng-change="updateHour()">
								<option value="" selected disabled>-- Predefined setting --</option>
								<option value="*">Every hour (*)</option>
								<option value="*/2">Every two hours (*/2)</option>
								<option value="*/3">Every three hours (*/3)</option>
								<option value="*/5">Every five hours (*/5)</option>
								<option value="*/10">Every 10 hours (*/10)</option>
								<option value="0,12">Every 12 hours (0,12)</option>
								<option value="" disabled>-- Custom setting --</option>
								@for ($i=0; $i < 24; $i++)
									<option value="{{$i}}">Every {{$i}} on clock ({{$i}}.00)</option>
								@endfor
							</select>
						</div>
					</div>
				</div>


				<div class="row">
					<div class="input-field col s12"> 
						<div class="form-group">
							<label class="control-label" for="">Day</label>
							<input type="text" ng-model="day" class="form-control" placeholder="">
						</div>
					</div>

					<div class="input-field col s12"> 
						<div class="form-group">
							<label class="control-label active">Predefined Setting</label>
							<select class="material-select" ng-model="selectDay" ng-change="updateDay()">
								<option value="" selected disabled>-- Predefined setting --</option>
								<option value="*">Every day (*)</option>
								<option value="*/2">Every two days (*/2)</option>
								<option value="*/7">Every seven days (*/7)</option>
								<option value="*/15">Every 15 days (*/15)</option>
								<option value="1,15,30">Every 1, 15 and day 30 in month (1,15,30)</option>
								<option value="" disabled>-- Custom setting --</option>
								@for ($i=1; $i <= 31; $i++)
									<option value="{{$i}}">Every {{$i}} (st/nd/rd) day on calendar ({{$i}})</option>
								@endfor
							</select>
						</div>
					</div>
				</div>


				<div class="row">
					<div class="input-field col s12"> 
						<div class="form-group">
							<label class="control-label" for="">Month</label>
							<input type="text" ng-model="month" class="form-control" placeholder="">
						</div>
					</div>

					<div class="input-field col s12"> 
						<div class="form-group">
							<label class="control-label active">Predefined Setting</label>
							<select class="material-select" ng-model="selectMonth" ng-change="updateMonth()">
								<option value="" selected disabled>-- Predefined setting --</option>
								<option value="*">Every month (*)</option>
								<option value="*/2">Every two months (*/2)</option>
								<option value="*/4">Every three months (*/4)</option>
								<option value="1,7">Every six months (1,7)</option>
								<option value="" disabled>-- Custom setting --</option>
								@for ($i=1; $i <= 12; $i++)
									<option value="{{$i}}">{{date("F", mktime(0, 0, 0, $i))}} ({{$i}})</option>
								@endfor
							</select>
						</div>
					</div>
				</div>


				<div class="row">
					<div class="input-field col s12"> 
						<div class="form-group">
							<label class="control-label" for="">Weekday</label>
							<input type="text" ng-model="weekday" class="form-control" placeholder="">
						</div>
					</div>

					<div class="input-field col s12"> 
						<div class="form-group">
							<label class="control-label active">Predefined Setting</label>
							<select class="material-select" ng-model="selectWeekday" ng-change="updateWeekday()">
								<option value="" selected disabled>-- Predefined setting --</option>
								<option value="*">Every day (*)</option>
								<option value="1-5">Every monday to friday (1-5)</option>
								<option value="6,0">Saturday and sunday (6,0)</option>
								<option value="" disabled>-- Custom setting --</option>
								<option value="0">Sunday (0)</option>
								<option value="1">Monday (1)</option>
								<option value="2">Tuesday (2)</option>
								<option value="3">Wednesday (3)</option>
								<option value="4">Thursday (4)</option>
								<option value="5">Friday (5)</option>
								<option value="6">Saturday (6)</option>
							</select>
						</div>
					</div>
				</div>


				<div class="row">
					<div class="input-field col s12"> 
						<div class="form-group">
							<label class="control-label" for="">Command</label>
							<input type="text" ng-model="command" class="form-control" placeholder="">
						</div>
					</div>
				</div>


				<div class="row">
					<div class="input-field col s12"> 
						<div class="form-group">
							<button type="button" class="btn light-green" ng-click="addCron()" ng-disabled="isBusy">Add Cron Job</button>
						</div>
					</div>
				</div>
			</form>

		</div>
	</div>
@stop


@section('meta')
	<meta name="username" content="{{ $user->username }}">
@stop