@extends('materializecss.layout.master', ['title' => 'Domain Records'])

@section('content')
<div class="row" ng-controller="RecordController">
	<div class="col s12">
		<h4>View, Modify or Delete Domain Record</h4>

		<label>Select domain name</label>
		<select class="material-select" ng-model="domain" ng-change="getRecord()" id="selectDomain">
			<option value="" disabled selected>Please select your domain</option>
			@foreach ($domains as $domain)
				<option value="{{ $domain->id }}" data-record-url="{{ $domain->recordURL }}">{{ $domain->name }}</option>
			@endforeach
		</select>

		<table class="striped" ng-show="records != null">
			<thead>
				<tr>
					<th>Name</th>
					<th>Type</th>
					<th>Content</th>
					<th>TTL</th>
					<th>Prio</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="record in records">
					<td>@{{ record.name }}</td>
					<td>@{{ record.type }}</td>
					<td>@{{ record.content }}</td>
					<td>@{{ record.ttl }}</td>
					<td>@{{ record.prio }}</td>
					<td>
						<button class="btn red waves-effect disabled" ng-show="@{{ record.deleteEdit == 0 }}"><i class="fa fa-pencil"></i></button>
						<a class="btn light-blue waves-effect" href="@{{ record.deleteEdit == 0 ? '#' : record.editURL }}" ng-show="@{{ record.deleteEdit == 1 }}"><i class="fa fa-pencil"></i></a>
					</td>
					<td>
						<button class="btn red waves-effect disabled" ng-show="@{{ record.deleteEdit == 0 }}"><i class="fa fa-trash"></i></button>
						<a class="btn red waves-effect" ng-click="deleteRecord(record.deleteURL)" ng-show="@{{ record.deleteEdit == 1 }}"><i class="fa fa-trash"></i></a>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
</div>

<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<a 	href="{{ $urlList['record-create'] }}" class="btn-floating btn-large red waves-effect waves-light tooltipped modal-trigger" data-position="left" data-tooltip="Add new Domain Record"><i class="material-icons">add</i></a>
</div>

@include('materializecss.partial._modal', ['modal' => 'deleterecord'])
@stop