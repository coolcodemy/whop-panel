@extends('materializecss.layout.master', ['title' => 'Domain Management'])


@section('content')
<div class="row">
	<div class="col s12">
		<h4>Domain Usage</h4>
	</div>

    <div class="col s12">
        Domain Name Usage: {{ $progress['domains']['value'] }}/{{ $progress['domains']['max'] }}
        <div class="progress grey lighten-3">
            <div class="determinate {{ $progress['domains']['color'] }}" style="width: {{ $progress['domains']['percentage'] }}%"></div>
        </div>
    </div>
</div>


<div class="row" ng-controller="DomainController">
	<div class="col s12">
		<h4>Domain List</h4>

		<table class="striped">
			<thead>
				<tr>
					<th>Domain Name</th>
					<th>Delete</th>
				</tr>
			</thead>
		
			<tbody>
				@foreach($domains as $domain)
					<tr>
						<td>{{ $domain->name }}</td>
						<td>
							@if ($domain->deleteEdit == 1)
								<button class="btn red waves-effect" ng-click="deleteDomain('{{ $domain->deleteURL }}')"><i class="fa fa-trash"></i></button>
							@else
								<button class="btn red waves-effect" disabled><i class="fa fa-trash"></i></button>
							@endif
						</td>
					</tr>
				@endforeach
			</tbody>
		</table>

		{!! (new WHoP\ThirdParty\Pagination($domains))->render() !!}
	</div>
</div>


<div class="row">
	<div class="col s12">
		<h4>Add New Domain</h4>

		{!! Form::open(['url' => $buttonURL['add-domain']]) !!}
			<div class="row">
				<div class="input-field col s12">
					<label class="active">Domain Name</label>
					<input name="domainName" placeholder="" type="text">
				</div>
			</div>


			<div class="row">
				<div class="input-field col s12">
					<button type="submit" class="btn light-green">Add</button>
				</div>
			</div>
		{!! Form::close() !!}
	</div>
</div>

@include('materializecss.partial._modal', ['modal' => 'deletedomain'])
@stop
