@extends('materializecss.layout.master', ['title' => $title])

@section('content')
<div class="row" ng-controller="RecordController">

	<div class="col s12">
		<h4>Server IP Address</h4>

		<table class="bordered striped highlight">
			<thead>
				<tr>
					<th>Type</th>
					<th>Address</th>
				</tr>
			</thead>
		
			<tbody>
				<tr ng-repeat="ip in ipv4">
					<td>IPv4</td>
					<td ng-bind="ip"></td>
				</tr>
				<tr ng-repeat="ip in ipv6">
					<td>IPv6</td>
					<td ng-bind="ip"></td>
				</tr>
			</tbody>
		</table>
		
	</div>


	<div class="col s12">
		<h4>{{ $header }}</h4>

		{!! Form::open(['url' => $url]) !!}
			@include('materializecss.form._formdomainrecord', ['buttonSubmitText' => 'Submit'])
		{!! Form::close() !!}
	</div>
</div>
@stop