@extends('materializecss.layout.master', ['title' => 'FTP Accounts'])


@section('content')

<div class="row" ng-controller="FtpController">
	<div class="col s12">
		<h4>New FTP Accounts</h4>

		{!! Form::open(['url' => $urlList['action']]) !!}
			@include('materializecss.form._formaddftp')
		{!! Form::close() !!}
	</div>


	@include('materializecss.partial._modal', ['modal' => 'chooseHomeDir'])
</div>

@stop
