@extends('materializecss.layout.master', ['title' => 'SSL/TLS Manager'])

@section('content')
	<div class="row" ng-controller="SSLController">
		<div class="col s12">
			<h4>Certificate List</h4>

			<table class="bordered">
				<thead>
					<tr>
						<th>Name</th>
						<th>View</th>
						<th>Delete</th>
					</tr>
				</thead>
			
				<tbody>
					<tr ng-repeat="cert in certs" ng-show="certCount > 0">
						<td>@{{ cert.cert | split:'.crt':0 }}</td>
						<td><button class="btn light-green" ng-click="view(cert.cert)"><i class="fa fa-eye"></i></button></td>
						<td><button class="btn red" ng-click="delete(cert.cert)"><i class="fa fa-trash"></i></button></td>
					</tr>

					<tr ng-show="certCount == 0">
						<td colspan="3">No SSL/TLS certificate available</td>
					</tr>
				</tbody>
			</table>

			@include('materializecss.partial._modal', ['modal' => 'viewcertificate'])
			@include('materializecss.partial._modal', ['modal' => 'deletecertificate'])
		</div>
	</div>

	<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
		<a 	href="{{ $urlList['ssl-add'] }}" class="btn-floating btn-large red waves-effect waves-light tooltipped modal-trigger" data-position="left" data-tooltip="Add new SSL/TLS Certificate"><i class="material-icons">add</i></a>
	</div>
@stop


@section('meta')
	<meta name="username" content="{{ $user->username }}">
@stop