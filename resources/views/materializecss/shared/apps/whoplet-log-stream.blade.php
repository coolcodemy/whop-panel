@extends('materializecss.layout.master', ['title' => 'WHoPlet Log Stream'])


@section('content')
<div class="row" ng-controller="WhopletLogController">
	<div class="col s12">
		<h4>WHoPlet Log Streaming  - @{{ logType }} log (@{{ whoplet |split:'|':0 }}<span class="fa fa-lock" ng-show="whoplet|indexof:'|https'" style="margin-left:10px"></span>)</h4>

		<div class="row">
			<div class="col s12">
				<div class="card">
					<div class="card-content editor" id="logViewer">

					</div>
				</div>
			</div>
		</div>
	</div>

</div>
@stop
