@extends('materializecss.layout.master', ['title' => 'Add SSL/TLS Certificate'])

@section('content')
	<div class="row" ng-controller="SSLController">
		<div class="col s12">
			<h4>Add SSL/TLS Certificate</h4>

			<form action="#">
				<div class="row">
					<div class="input-field col s12"> 
						<div class="form-group">
							<label class="control-label">Certificate Name</label>
							<input type="text" class="form-control" ng-model="certificateName" placeholder="Alphanumeric only">
						</div>
					</div>

					<div class="input-field col s12"> 
						<div class="form-group">
							<label class="control-label" for="publicCertificate">SSL/TLS Certificate</label>
							<textarea class="materialize-textarea" id="publicCertificate" ng-model="publicCertificate" placeholder=""></textarea>
						</div>
					</div>


					<div class="input-field col s12"> 
						<div class="form-group">
							<label class="control-label" for="privateCertificate">SSL/TLS Certificate Key</label>
							<textarea class="materialize-textarea" id="privateCerficate" ng-model="privateCertificate" placeholder=""></textarea>
						</div>
					</div>

					<div class="input-field col s12"> 
						<button type="button" class="btn light-green" ng-click="addCertificate()" ng-disabled="isBusy">Add</button>
					</div>
				</div>
			</form>
		</div>
	</div>
@stop


@section('meta')
	<meta name="username" content="{{ $user->username }}">
@stop