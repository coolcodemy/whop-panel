@extends('materializecss.layout.master', ['title' => 'MariaDB Manager'])


@section('content')
<div ng-controller="MariaDBController">
	<div class="row">
		<div class="col s12">
			<h4>Usage</h4>

	        <div class="row">
	            <div class="col s12">
	                Database Usage: {{ $progress['db']['value'] }}/{{ $progress['db']['max'] }}
	                <div class="progress grey lighten-3">
	                    <div class="determinate {{ $progress['db']['color'] }}" style="width: {{ $progress['db']['percentage'] }}%"></div>
	                </div>
	            </div>

	            <div class="col s12">
	                Database User Usage: {{ $progress['dbUsers']['value'] }}/{{ $progress['dbUsers']['max'] }}
	                <div class="progress grey lighten-3">
	                    <div class="determinate {{ $progress['dbUsers']['color'] }}" style="width: {{ $progress['dbUsers']['percentage'] }}%"></div>
	                </div>
	            </div>
	        </div>
		</div>
	</div>


	<div class="row">
		<div class="col s12">
			<h4>Databases</h4>

			<table class="striped">
				<thead>
					<tr>
						<th>Database Name</th>
						<th>Size (MB)</th>
						<th>User</th>
						<th>Delete</th>
					</tr>
				</thead>
			
				<tbody>
					@foreach ($dbArr as $myDB => $value)
						<tr>
							<td>{{ $myDB }}</td>
							<td>
								@foreach ($value['size'] as $dbsize)
									{{ ($dbsize->Size == NULL) ? '0.00' : $dbsize->Size }}
								@endforeach
							</td>
							<td>
								@foreach ($value['user'] as $dbu)
									{{ $dbu }} <a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Revoke {{ $dbu }} from {{ $myDB }}" ng-click="revokeUser('{{ substr_replace($dbu,'',0,strlen($user->username . '_')) }}', '{{ substr_replace($myDB,'',0,strlen($user->username . '_')) }}')"><span class="fa fa-times"></span></a><br>
								@endforeach
							</td>
							<td>
								<a class="btn red waves-effect" ng-click="deleteDatabase('{{ substr_replace($myDB,'',0,strlen($user->username . '_')) }}')"><i class="fa fa-trash"></i></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>



	<div class="row">
		<div class="col s12">
			<h4>Users</h4>

			<table class="striped">
				<thead>
					<tr>
						<th>Username</th>
						<th>Delete</th>
					</tr>
				</thead>
			
				<tbody>
					@foreach ($dbUserArrRemoveUsername as $dbu)
						<tr>
							<td>
								{{ $dbu['realname'] }}
							</td>
							<td>
								<a class="btn red waves-effect" ng-click="deleteUser('{{ substr_replace($dbu['realname'],'',0,strlen($user->username . '_')) }}')"><i class="fa fa-trash"></i></a>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>


	<div class="row">
		<div class="col s12">
			<h4>Add Database</h4>
		</div>

		{!! Form::open(['url' => $buttonURL['add-database'], 'class' => 'col s12']) !!}
			<div class="row">
				<div class="input-field col s12"> 
					<div class="form-group">
						<label class="control-label" for="">Database Name</label>
						<input type="text" class="form-control" name="databaseName" value="{{ old('databaseName') }}" placeholder="Alphanumeric only">
					</div>
				</div>
			</div>
			<button type="submit" class="btn light-green">Submit</button>
		{!! Form::close() !!}
	</div>


	<div class="row">
		<div class="col s12">
			<h4>Add Database User</h4>
		</div>

		{!! Form::open(['url' => $buttonURL['add-user'], 'class' => 'col s12']) !!}
			<div class="row">
				<div class="input-field col s12"> 
					<div class="form-group">
						<label class="control-label" for="">Username</label>
						<input type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="Alphanumeric only">
					</div>
				</div>

				<div class="input-field col s12"> 
					<div class="form-group">
						<label class="control-label" for="">Password</label>
						<input type="password" class="form-control" name="password" placeholder="Password">
					</div>
				</div>

				<div class="input-field col s12"> 
					<div class="form-group">
						<label class="control-label" for="">Verify Password</label>
						<input type="password" class="form-control" name="verifyPassword" placeholder="Repeat password">
					</div>
				</div>
			</div>
			<button type="submit" class="btn light-green">Submit</button>
		{!! Form::close() !!}
	</div>


	<div class="row">
		<div class="col s12">
			<h4>Add User To Database</h4>
		</div>

		{!! Form::open(['url' => $buttonURL['add-user-to-database'], 'class' => 'col s12']) !!}
			<div class="row">
				<div class="input-field col s12"> 
					<div class="form-group">
						<select name="username" class="material-select">
							<option value="" disabled selected>Please select database user</option>
							@foreach ($dbUserArrRemoveUsername as $dbu)
								<option value="{{ $dbu['strip'] }}">{{ $dbu['realname'] }}</option>
							@endforeach
						</select>
						<label class="control-label" for="">Add User</label>
					</div>
				</div>

				<div class="input-field col s12"> 
					<div class="form-group">
						<select name="database" class="material-select">
							<option value="" disabled selected>Please select database user</option>
							@foreach ($dbArrRemoveUsername as $database)
								<option value="{{ $database['strip'] }}">{{ $database['realname'] }}</option>
							@endforeach
						</select>
						<label class="control-label" for="">To Database</label>
					</div>
				</div>

				<div class="input-field col s12"> 
					<div class="form-group">
						<label class="control-label" for="">With Permission</label>
					</div>
					<table>
						<thead>
		        			<tr>
		        				<th colspan="2">
		        					<p>
										<input type="checkbox" id="selectAll" onClick="toggle(this)"/>
										<label for="selectAll">Select All</label>
									</p>
		        				</th>
		        			</tr>
						</thead>
					
						<tbody>
							<tr>
								<td>
									<input type="checkbox" id="SELECT" name="permission[]" value="SELECT"/>
									<label for="SELECT">SELECT</label>
								</td>
								<td>
									<input type="checkbox" id="INSERT" name="permission[]" value="INSERT"/>
									<label for="INSERT">INSERT</label>
								</td>
							</tr>

							<tr>
								<td>
									<input type="checkbox" id="UPDATE" name="permission[]" value="UPDATE"/>
									<label for="UPDATE">UPDATE</label>
								</td>
								<td>
									<input type="checkbox" id="DELETE" name="permission[]" value="DELETE"/>
									<label for="DELETE">DELETE</label>
								</td>
							</tr>

							<tr>
								<td>
									<input type="checkbox" id="CREATE" name="permission[]" value="CREATE"/>
									<label for="CREATE">CREATE</label>
								</td>
								<td>
									<input type="checkbox" id="ALTER" name="permission[]" value="ALTER"/>
									<label for="ALTER">ALTER</label>
								</td>
							</tr>

							<tr>
								<td>
									<input type="checkbox" id="INDEX" name="permission[]" value="INDEX"/>
									<label for="INDEX">INDEX</label>
								</td>
								<td>
									<input type="checkbox" id="DROP" name="permission[]" value="DROP"/>
									<label for="DROP">DROP</label>
								</td>
							</tr>

							<tr>
								<td>
									<input type="checkbox" id="CREATE-TEMPORARY-TABLE" name="permission[]" value="CREATE-TEMPORARY-TABLE"/>
									<label for="CREATE-TEMPORARY-TABLE">CREATE TEMPORARY TABLE</label>
								</td>
								<td>
									<input type="checkbox" id="SHOW-VIEW" name="permission[]" value="SHOW-VIEW"/>
									<label for="SHOW-VIEW">SHOW VIEW</label>
								</td>
							</tr>

							<tr>
								<td>
									<input type="checkbox" id="CREATE-ROUTINE" name="permission[]" value="CREATE-ROUTINE"/>
									<label for="CREATE-ROUTINE">CREATE ROUTINE</label>
								</td>
								<td>
									<input type="checkbox" id="ALTER-ROUTINE" name="permission[]" value="ALTER-ROUTINE"/>
									<label for="ALTER-ROUTINE">ALTER ROUTINE</label>
								</td>
							</tr>

							<tr>
								<td>
									<input type="checkbox" id="EXECUTE" name="permission[]" value="EXECUTE"/>
									<label for="EXECUTE">EXECUTE</label>
								</td>
								<td>
									<input type="checkbox" id="CREATE-VIEW" name="permission[]" value="CREATE-VIEW"/>
									<label for="CREATE-VIEW">CREATE VIEW</label>
								</td>
							</tr>

							<tr>
								<td>
									<input type="checkbox" id="EVENT" name="permission[]" value="EVENT"/>
									<label for="EVENT">EVENT</label>
								</td>
								<td>
									<input type="checkbox" id="TRIGGER" name="permission[]" value="TRIGGER"/>
									<label for="TRIGGER">TRIGGER</label>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			<button type="submit" class="btn light-green bottom15px">Submit</button>
		{!! Form::close() !!}
	</div>

	@include('materializecss.partial._modal', ['modal' => 'revokedbuser'])
	@include('materializecss.partial._modal', ['modal' => 'deletedatabase'])
	@include('materializecss.partial._modal', ['modal' => 'deletedatabaseuser'])
</div>
@stop