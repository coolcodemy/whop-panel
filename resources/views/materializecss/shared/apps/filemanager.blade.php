@extends('materializecss.layout.master', ['title' => 'WHoP File Manager'])

@section('content')
	<div class="row" ng-controller="FileManagerController">
		<div class="col s12">
			<h4>WHoP File Manager</h4>

			<button class="btn light-green waves-effect" ng-click="newFileModal()"><span class="fa fa-file-o"></span> New file </button>
			<button class="btn light-green waves-effect" ng-click="newDirectoryModal()"><span class="fa fa-folder-o"></span> New directory </button>
			<button class="btn light-green waves-effect" ng-click="goToParent()" ng-disabled="superParent"><span class="fa fa-level-up"></span> Parent directory</button>
			<button class="btn light-green waves-effect" ng-click="permissionModal()" ng-disabled="changePermissionButton != true"><span class="fa fa-wrench"></span> Change Permission </button>
			<button class="btn light-green waves-effect" ng-click="renameFileModal()" ng-disabled="renameButton != true"><span class="fa fa-edit"></span> Rename </button>
			<button class="btn light-green waves-effect" ng-click="edit()" ng-disabled="editButton != true"><span class="fa fa-pencil"></span> Edit </button>
			<button class="btn red waves-effect" ng-click="deleteFileDirectoryModal()" ng-disabled="deleteButton != true"><span class="fa fa-times"></span> Delete </button>
		</div>

		<div class="col s12">
			<table class="highlight">
				<thead>
					<tr>
						<th colspan="5">Current Location: /var/www/{{ $user->username }}/web@{{ currentLocation }}</th>
					</tr>
					<tr>
						<th></th>
						<th>File/Folder Name</th>
						<th>Size</th>
						<th>Type</th>
						<th>Permission</th>
					</tr>
				</thead>
				<tbody id="folderContent">
					<tr ng-show="isLoading">
						<td colspan="5"><i>Please wait while content load...</i></td>
					</tr>
					<tr ng-show="nofolder">
						<td colspan="5"><i>No more file or folder in this directory...</i></td>
					</tr>
					<tr ng-show="isLoading != true" ng-repeat="res in fileAndFolder" ng-dblclick="walk(res)">
						<td><i class="fa" ng-class="res.icon"></i></td>
						<td>@{{ res.name }}</td>
						<td>@{{ res.size/1000 }} KiB</td>
						<td>@{{ res.type }}</td>
						<td>@{{ res.permission }}</td>
					</tr>
				</tbody>
			</table>
		</div>

		@include('materializecss.partial._modal', ['modal' => 'addfile'])
		@include('materializecss.partial._modal', ['modal' => 'adddirectory'])
		@include('materializecss.partial._modal', ['modal' => 'deletefiledirectory'])
		@include('materializecss.partial._modal', ['modal' => 'changepermission'])
		@include('materializecss.partial._modal', ['modal' => 'renamefile'])
	</div>
@stop

@section('meta')
	<meta name="username" content="{{ $user->username }}">
	<meta name="editor" content="{{ $urlList['editor'] }}">
@stop