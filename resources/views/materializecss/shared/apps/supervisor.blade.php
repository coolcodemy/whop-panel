@extends('materializecss.layout.master', ['title' => 'Supervisor Manager'])

@section('content')
	<div class="row" ng-controller="SupervisorController">
		<div class="col s12">
			<h4>Supervisor Job List</h4>

			<table>
				<thead>
					<tr>
						<th>Name</th>
						<th>Command</th>
						<th>Status</th>
						<th>Uptime</th>
						<th>Reload</th>
						<th>Delete</th>
					</tr>
				</thead>
			
				<tbody>
					<tr ng-repeat="content in contents" ng-show="empty == false">
						<td ng-bind="content.name"></td>
						<td ng-bind="content.command"></td>
						<td ng-bind="content.status"></td>
						<td ng-bind="content.uptime"></td>
						<td><button class="btn light-green" ng-click="restartModal(content.name)" ng-disabled="isBusy"><i class="fa fa-refresh"></i></button></td>
						<td><button class="btn red" ng-click="deleteModal(content.name)" ng-disabled="isBusy"><i class="fa fa-trash"></i></button></td>
					</tr>
					<tr ng-hide="empty == false">
						<td colspan="4">No supervisor job available</td>
					</tr>
				</tbody>
			</table>

			@include('materializecss.partial._modal', ['modal' => 'deletesupervisorjob'])
			@include('materializecss.partial._modal', ['modal' => 'restartsupervisorjob'])
		</div>
	</div>

	<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
		<a 	href="{{ $urlList['supervisor-add'] }}" class="btn-floating btn-large red waves-effect waves-light tooltipped modal-trigger" data-position="left" data-tooltip="Add new Supervisor Job"><i class="material-icons">add</i></a>
	</div>
@stop


@section('meta')
	<meta name="username" content="{{ $user->username }}">
@stop