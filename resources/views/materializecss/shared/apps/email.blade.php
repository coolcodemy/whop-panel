@extends('materializecss.layout.master', ['title' => 'Email Account'])


@section('content')
<div class="row" ng-controller="EmailController">
	<div class="col s12">
		<h4>Email Usage</h4>

        <div class="row">
            <div class="col s12">
                Email Account Usage: {{ $progress['mailUsers']['value'] }}/{{ $progress['mailUsers']['max'] }}
                <div class="progress grey lighten-3">
                    <div class="determinate {{ $progress['mailUsers']['color'] }}" style="width: {{ $progress['mailUsers']['percentage'] }}%"></div>
                </div>
            </div>
            <div class="col s12">
                Email Quota Usage: {{ $progress['mailQuota']['value'] }} MB/{{ $progress['mailQuota']['max'] }}
                <div class="progress grey lighten-3">
                    <div class="determinate {{ $progress['mailQuota']['color'] }}" style="width: {{ $progress['mailQuota']['percentage'] }}%"></div>
                </div>
            </div>
        </div>
	</div>


	<div class="col s12">
		<h4>My Email Account</h4>

		<table>
			<thead>
				<tr>
					<th>Email</th>
					<th>Assigned Quota (MB)</th>
					<th>Status</th>
					<th>Created On</th>
					<th>Edit</th>
					<th>Delete</th>
				</tr>
			</thead>
		
			<tbody>
				@foreach ($emails as $email)
					<tr>
						<td>{{ $email->email }}</td>
						<td>
							@if ($email->quota == 0)
								Unlimited
							@else
								{{ $email->quota }} MB
							@endif
						</td>
						<td>
							@if ($email->disable == 0)
								<a href="{{ $email->enableDisableURL }}" class="btn light-green waves-effect tooltipped" data-position="top" data-delay="50" data-tooltip="Click to disable account">Enable</a>
							@else
								<a href="{{ $email->enableDisableURL }}" class="btn red waves-effect tooltipped" data-position="top" data-delay="50" data-tooltip="Click to enable account">Disable</a>
							@endif
						</td>
						<td>{{ date('d F Y', strtotime($email->created_at)) }}</td>
						<td><a href="{{ $email->editURL }}" class="btn light-blue waves-effect"><i class="fa fa-pencil"></i></a></td>
						<td><button class="btn red waves-effect" ng-click="deleteEmail('{{ $email->deleteURL }}')"><i class="fa fa-trash"></i></button></td>
					</tr>
				@endforeach
			</tbody>
		</table>

		{!! (new WHoP\ThirdParty\Pagination($emails))->render() !!}
	</div>

	@include('materializecss.partial._modal', ['modal' => 'deleteemail'])
</div>


<div class="fixed-action-btn" style="bottom: 45px; right: 24px;">
	<a 	href="{{ $urlList['email-create'] }}" class="btn-floating btn-large red waves-effect waves-light tooltipped modal-trigger" data-position="left" data-tooltip="Add new Email Account"><i class="material-icons">add</i></a>
</div>
@stop