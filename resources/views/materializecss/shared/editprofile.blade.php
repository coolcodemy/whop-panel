@extends('materializecss.layout.master', ['title' => 'My Profile'])


@section('content')
    <div class="row">

    	<div class="col s12">

    			<h3>Name</h3>


    			{!! Form::open([ 'url' => $urlList['updateName'] ]) !!}

    				<div class="row">

						<div class="input-field col s12">
							<input placeholder="Your name" name="name" id="name" value="{{ $user->name or old('name') }}" type="text">
							<label for="name">Name</label>
						</div>


						<div class="input-field col s12">
							<button type="submit" class="btn waves-effect light-blue">Update</button>
						</div>

    				</div>

    			{!! Form::close() !!}

    	</div>




    	<div class="col s12">

    			<h3>Password</h3>


    			{!! Form::open([ 'url' => $urlList['updatePassword'] ]) !!}

    				<div class="row">

    					@if ( Auth::user()->hasRole('owner') || Auth::user() == $user )

							<div class="input-field col s12">
								<input placeholder="Current Password" name="currentPassword" id="currentPassword" type="password">
								<label for="currentPassword">Current Password</label>
							</div>
							
						@endif

						<div class="input-field col s12">
							<input placeholder="New password" name="newPassword" id="newPassword" type="password">
							<label for="newPassword">New Password</label>
						</div>


						<div class="input-field col s12">
							<input placeholder="Verify ew password" name="verifyNewPassword" id="verifyNewPassword" type="password">
							<label for="verifyNewPassword">Verify New Password</label>
						</div>


						<div class="input-field col s12">
							<button type="submit" class="btn waves-effect light-blue">Update</button>
						</div>

    				</div>

    			{!! Form::close() !!}

    	</div>

    </div>
@stop