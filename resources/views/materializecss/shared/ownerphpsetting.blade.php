@extends('materializecss.layout.master', ['title' => 'PHP CLI Setting'])


@section('content')

	<div class="row" ng-controller="OwnerPHPController">

		<div class="col s12">
			<h4>Editing PHP CLI setting for {{ $user->name }} ({{ $user->username }})</h4>

			<p class="red white-text" style="padding:5px 10px 5px 10px">No validation checking will be done. Please make sure you backup this config before making change.</p>
		</div>

		<div class="col s12">
	        <div class="card">
	            <div class="card-content" id="phpSetting"></div>
	            <div class="card-action">
	                <button class="btn right blue bottom15px" ng-click="savePHPSetting()" ng-disabled="isBusy">Save</button>
	            </div>
	        </div>
		</div>

	</div>

@stop