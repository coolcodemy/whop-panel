<!DOCTYPE HTML>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>License Verification Failed</title>
<script>
function goBack()
  {
  window.history.back()
  }
</script>
<style type="text/css">
body{
	font-family:Arial, Helvetica, sans-serif;
}
.wrap{
	width:1000px;
	margin:0 auto;
}
.logo{
	width:430px;
	position:absolute;
	top:25%;
	left:35%;
}
h1 {
	font-size: 50px;
}
p a{
	color:#eee;
	font-size:13px;
	padding:5px;
	background:#FF3366;
	text-decoration:none;
	-webkit-border-radius:.3em;
	   -moz-border-radius:.3em;
	        border-radius:.3em;
}
p a:hover{
	color: #fff;
}
.footer{
	position:absolute;
	bottom:10px;
	right:10px;
	font-size:12px;
	color:#aaa;
}
.footer a{
	color:#666;
	text-decoration:none;
}

</style>
</head>
<body>
<div class="wrap">
    <div class="logo">
        <h2>License Verification Failed!!</h2>
        <p> This server WHoP license is currently cannot be found or have expired. Please contact the administrator to fix this error. </p>
    </div>
</div>
<div class="footer">
	WHoP&trade; - Cool Code Sdn. Bhd.</a>
</div>

</body>
</html>
